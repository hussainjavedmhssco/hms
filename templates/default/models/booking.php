<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
<script type="text/javascript">
    $(document).ready(function () {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap'
    });
});     

    $(document).ready(function () {
    $('#datepickers').datepicker({
      uiLibrary: 'bootstrap'
    });
});
</script>
<?php
if(isset($_POST['book']) || (ENABLE_BOOKING_REQUESTS == 1 && isset($_POST['request']))){
	
	if(isset($_SESSION['book'])) unset($_SESSION['book']);
    $num_nights = $_POST['nights'];
    
    $_SESSION['book']['from_date'] = $_POST['from_time'];
    $_SESSION['book']['to_date'] = $_POST['to_time'];
    $_SESSION['book']['nights'] = $num_nights;
    $_SESSION['book']['adults'] = $_POST['adults'];
    $_SESSION['book']['children'] = $_POST['children'];
    $_SESSION['book']['extra_services'] = array();
    $_SESSION['book']['activities'] = array();
    $_SESSION['book']['rooms'] = array();
    
    $_SESSION['book']['total'] = 0;
    
    if(isset($_POST['book'])){
        
        $_SESSION['book']['amount_rooms'] = 0;
        $_SESSION['book']['amount_activities'] = 0;
        $_SESSION['book']['amount_services'] = 0;
        
        $_SESSION['book']['duty_free_rooms'] = 0;
        $_SESSION['book']['duty_free_activities'] = 0;
        $_SESSION['book']['duty_free_services'] = 0;
       
        $_SESSION['book']['tax_rooms_amount'] = 0;
        $_SESSION['book']['tax_activities_amount'] = 0;
        $_SESSION['book']['tax_services_amount'] = 0;
        
        $_SESSION['book']['discount'] = 0;
        $_SESSION['book']['discount_type'] = '';
        $_SESSION['book']['discount_amount'] = 0;
        
        $_SESSION['book']['taxes'] = array();
        
        $num_rooms = 0;
        $num_adults = 0;
        $num_children = 0;
        
        if(isset($_POST['amount']) && is_array($_POST['amount'])){
            foreach($_POST['amount'] as $id_room => $values){
                foreach($values as $i => $value){
                    
                    if(isset($_POST['num_adults'][$id_room][$i]) && isset($_POST['num_children'][$id_room][$i]) && isset($_POST['room_'.$id_room])){
                        
                        $room_title = $_POST['room_'.$id_room];
                        $adults = $_POST['num_adults'][$id_room][$i];
                        $children = $_POST['num_children'][$id_room][$i];
                        $duty_free = $_POST['duty_free'][$id_room][$i];
                        
                        if(is_numeric($adults) && is_numeric($children) && ($adults+$children) > 0 && $value > 0){
                            $num_adults += $adults;
                            $num_children += $children;
                            $num_rooms++;
                            
                            $_SESSION['book']['rooms'][$id_room][$i]['title'] = $room_title;
                            $_SESSION['book']['rooms'][$id_room][$i]['adults'] = $adults;
                            $_SESSION['book']['rooms'][$id_room][$i]['children'] = $children;
                            $_SESSION['book']['rooms'][$id_room][$i]['amount'] = $value;
                            $_SESSION['book']['rooms'][$id_room][$i]['duty_free'] = $duty_free;
                            
                            $_SESSION['book']['taxes'] = array();
                            
                            if(isset($_POST['taxes'][$id_room][$i])){
                                $taxes = $_POST['taxes'][$id_room][$i];
                                if(is_array($taxes)){
                                    foreach($taxes as $tax_id => $tax_amount){
                                        $_SESSION['book']['tax_rooms_amount'] += $tax_amount;
                                        if(!isset($_SESSION['book']['taxes'][$tax_id]['rooms'])) $_SESSION['book']['taxes'][$tax_id]['rooms'] = 0;
                                        $_SESSION['book']['taxes'][$tax_id]['rooms'] += $tax_amount;
                                    }
                                }
                            }
                            
                            $_SESSION['book']['amount_rooms'] += $value;
                            $_SESSION['book']['duty_free_rooms'] += $duty_free;
                        }
                    }
                }
            }
        }
        
        $tourist_tax = (TOURIST_TAX_TYPE == 'fixed') ? $_SESSION['book']['adults']*$num_nights*TOURIST_TAX : $_SESSION['book']['amount_rooms']*TOURIST_TAX/100;
        
        $_SESSION['book']['tourist_tax'] = (ENABLE_TOURIST_TAX == 1) ? $tourist_tax : 0;
        
        $_SESSION['book']['total'] = $_SESSION['book']['duty_free_rooms']+$_SESSION['book']['tax_rooms_amount']+$_SESSION['book']['tourist_tax'];
        $_SESSION['book']['down_payment'] = (ENABLE_DOWN_PAYMENT == 1 && DOWN_PAYMENT_RATE > 0 && $_SESSION['book']['total'] >= DOWN_PAYMENT_AMOUNT) ? $_SESSION['book']['total']*DOWN_PAYMENT_RATE/100 : 0;
    }
    
    if(isset($_SESSION['book']['id'])) unset($_SESSION['book']['id']);
    
    $result_activity = $db->query('SELECT * FROM pm_activity WHERE checked = 1 AND lang = '.LANG_ID);
    if(isset($_SESSION['book']['activities'])) unset($_SESSION['book']['activities']);
    
    if($result_activity !== false && $db->last_row_count() > 0){
        $_SESSION['book']['activities'] = array();
        header('Location: '.DOCBASE.$sys_pages['booking-activities']['alias']);
    }else
        header('Location: '.DOCBASE.$sys_pages['details']['alias']);
    
    exit();
}

$field_notice = array();
$msg_error = '';
$msg_success = '';
$room_stock = 1;
$max_people = 30;
$search_limit = 8;
$search_offset = (isset($_GET['offset']) && is_numeric($_GET['offset'])) ? $_GET['offset'] : 0;

/*********** Num adults ************/
if(isset($_POST['num_adults']) && is_numeric($_POST['num_adults'])) $_SESSION['num_adults'] = $_POST['num_adults'];
elseif(isset($_SESSION['book']['adults'])) $_SESSION['num_adults'] = $_SESSION['book']['adults'];
elseif(!isset($_SESSION['num_adults'])) $_SESSION['num_adults'] = 1;

/********** Num children ***********/
if(isset($_POST['num_children']) && is_numeric($_POST['num_children'])) $_SESSION['num_children'] = $_POST['num_children'];
elseif(isset($_SESSION['book']['children'])) $_SESSION['num_children'] = $_SESSION['book']['children'];
elseif(!isset($_SESSION['num_children'])) $_SESSION['num_children'] = 0;

/****** Check in / out date ********/
if(isset($_SESSION['book']['from_date'])) $from_time = $_SESSION['book']['from_date'];
else $from_time = gmtime();

if(isset($_SESSION['book']['to_date'])) $to_time = $_SESSION['book']['to_date'];
else $to_time = gmtime()+86400;

if(isset($_POST['from_date'])) $_SESSION['from_date'] = htmlentities($_POST['from_date'], ENT_QUOTES, 'UTF-8');
elseif(!isset($_SESSION['from_date'])) $_SESSION['from_date'] = gmdate('d/m/Y', $from_time);

if(isset($_POST['to_date'])) $_SESSION['to_date'] = htmlentities($_POST['to_date'], ENT_QUOTES, 'UTF-8');
elseif(!isset($_SESSION['to_date'])) $_SESSION['to_date'] = gmdate('d/m/Y', $to_time);

/********** Searched room **********/
if(isset($_POST['room_id']) && is_numeric($_POST['room_id'])) $_SESSION['room_id'] = $_POST['room_id'];
elseif(isset($_SESSION['room_id']) && is_numeric($_SESSION['room_id'])) $_SESSION['room_id'] = $_SESSION['room_id'];
elseif(!isset($_SESSION['room_id'])) $_SESSION['room_id'] = 0;

$num_people = $_SESSION['num_adults']+$_SESSION['num_children'];

if(!is_numeric($_SESSION['num_adults'])) $field_notice['num_adults'] = $texts['REQUIRED_FIELD'];
if(!is_numeric($_SESSION['num_children'])) $field_notice['num_children'] = $texts['REQUIRED_FIELD'];

if($_SESSION['from_date'] == '') $field_notice['from_date'] = $texts['REQUIRED_FIELD'];
else{
    $time = explode('/', $_SESSION['from_date']);
    if(count($time) == 3) $time = gm_strtotime($time[2].'-'.$time[1].'-'.$time[0].' 00:00:00');
    if(!is_numeric($time)) $field_notice['from_date'] = $texts['REQUIRED_FIELD'];
    else $from_time = $time;
}
if($_SESSION['to_date'] == '') $field_notice['to_date'] = $texts['REQUIRED_FIELD'];
else{
    $time = explode('/', $_SESSION['to_date']);
    if(count($time) == 3) $time = gm_strtotime($time[2].'-'.$time[1].'-'.$time[0].' 00:00:00');
    if(!is_numeric($time)) $field_notice['to_date'] = $texts['REQUIRED_FIELD'];
    else $to_time = $time;
}

if(is_numeric($from_time) && is_numeric($to_time)){
    $num_nights = ($to_time-$from_time)/86400;
}else
    $num_nights = 0;
    
if(count($field_notice) == 0){

    if($num_nights <= 0) $msg_error .= $texts['NO_AVAILABILITY'];
    else{
        require_once(getFromTemplate('common/functions.php', false));
        $res_room = getRoomsResult($from_time, $to_time, $_SESSION['num_adults'], $_SESSION['num_children']);
        if(empty($res_room)) $msg_error .= $texts['NO_AVAILABILITY'];
    }
}

$id_facility = 0;
$result_facility_file = $db->prepare('SELECT * FROM pm_facility_file WHERE id_item = :id_facility AND checked = 1 AND lang = '.DEFAULT_LANG.' AND type = \'image\' AND file != \'\' ORDER BY rank LIMIT 1');
$result_facility_file->bindParam(':id_facility', $id_facility);

$room_facilities = '0';
$result_facility = $db->prepare('SELECT * FROM pm_facility WHERE lang = '.LANG_ID.' AND FIND_IN_SET(id, :room_facilities) ORDER BY rank LIMIT 8');
$result_facility->bindParam(':room_facilities', $room_facilities);

$id_room = 0;
$result_rate = $db->prepare('SELECT MIN(price) as price FROM pm_rate WHERE id_room = :id_room');
$result_rate->bindParam(':id_room', $id_room);

$result_room_file = $db->prepare('SELECT * FROM pm_room_file WHERE id_item = :id_room AND checked = 1 AND lang = '.LANG_ID.' AND type = \'image\' AND file != \'\' ORDER BY rank LIMIT 1');
$result_room_file->bindParam(':id_room', $id_room, PDO::PARAM_STR);

$query_room = 'SELECT * FROM pm_room WHERE checked = 1 AND lang = '.LANG_ID.' ORDER BY';
if($_SESSION['room_id'] != 0) $query_room .= ' CASE WHEN id = '.$_SESSION['room_id'].' THEN 1 ELSE 2 END,';
if(!empty($res_room)) $query_room .= ' CASE WHEN id IN('.implode(',', array_keys($res_room)).') THEN 1 ELSE 2 END,';
$query_room .= ' CASE WHEN (max_people-'.$num_people.') >= 0 THEN (max_people-'.$num_people.') ELSE 100 END, 
                CASE WHEN (max_people-'.$num_people.') < 0 THEN ('.$num_people.'-max_people) ELSE 100 END, 
                rank';

$num_results = 0;
$result_room = $db->query($query_room);
if($result_room !== false) $num_results = $db->last_row_count();

$query_room .= ' LIMIT '.$search_limit.' OFFSET '.$search_offset;

$result_room = $db->query($query_room);

if(isset($_GET['action'])){
    if($_GET['action'] == 'confirm')
        $msg_success .= '<p class="text-center lead">'.$texts['PAYMENT_SUCCESS_NOTICE'].'</p>';
    elseif($_GET['action'] == 'cancel')
        $msg_error .= '<p class="text-center lead">'.$texts['PAYMENT_CANCEL_NOTICE'].'</p>';
}

/* ==============================================
 * CSS AND JAVASCRIPT USED IN THIS MODEL
 * ==============================================
 */
$javascripts[] = DOCBASE.'js/plugins/jquery.event.calendar/js/jquery.event.calendar.js';

if(is_file(SYSBASE.'js/plugins/jquery.event.calendar/js/languages/jquery.event.calendar.'.LANG_TAG.'.js'))
    $javascripts[] = DOCBASE.'js/plugins/jquery.event.calendar/js/languages/jquery.event.calendar.'.LANG_TAG.'.js';
else
    $javascripts[] = DOCBASE.'js/plugins/jquery.event.calendar/js/languages/jquery.event.calendar.en.js';
    
$stylesheets[] = array('file' => DOCBASE.'js/plugins/jquery.event.calendar/css/jquery.event.calendar.css', 'media' => 'all');

require(getFromTemplate('common/header.php', false)); ?>

<section id="page">
    
    <?php include(getFromTemplate('common/page_header.php', false)); ?>
    
    <div id="content" class="pt30 pb30">
        <div class="container">
            <div class="alert alert-success" style="display:none;"></div>
            <div class="alert alert-danger" style="display:none;"></div>
        </div>
        
        <form action="<?php echo DOCBASE.$sys_pages['booking']['alias']; ?>" method="post" class="ajax-form">
            <input type="hidden" name="from_time" value="<?php echo $from_time; ?>">
            <input type="hidden" name="to_time" value="<?php echo $to_time; ?>">
            <input type="hidden" name="nights" value="<?php echo $num_nights; ?>">
            
            <div class="container page-header mb20">
                <div class="row">
                <div class="col-md-1"></div>
                    <div class="col-md-5">
                        <div class="pull-left">
                            <div class="row">
                                <div class="control-group">
                                    <div class="col-sm-4">
                                        <div class="date-lable date-lables">
                                            <label>Check In</label>
                                        </div> 
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                            <input id="datepicker" class="form-control" size="30" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="text-center">
                        <p class="lead mb0"><?php echo $texts['CHECK_IN'].' <big><b>'.$_SESSION['from_date'].'</b></big> <big><i class="fa fa-arrow-right"></i></big> '.$texts['CHECK_OUT'].' <big><b>'.$_SESSION['to_date'].'</b></big>'; ?></p>
                        <span id="booking-amount"></span>
                    </div> -->
                    <div class="col-md-5">
                        <div class="row">
                            <div class="pull-right">
                                <div class="control-group">
                                  <!-- Username -->
                                  <div class="col-sm-4">
                                    <div class="date-lable date-lables">
                                        <label>Check Out</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-8">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input id="datepickers" class="form-control" size="30" />
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
            
            <?php
            if($page['text'] != ''){ ?>
                <div class="container mb20"><?php echo $page['text']; ?></div>
                <?php
            } ?>
            
            <div class="container boxed">
                <?php
                if($result_room !== false){
                    foreach($result_room as $row){
                        $id_room = $row['id'];
                        $room_title = $row['title'];
                        $room_alias = $row['alias'];
                        $room_subtitle = $row['subtitle'];
                        $room_descr = $row['descr'];
                        $room_price = $row['price'];
                        $max_adults = $row['max_adults'];
                        $max_children = $row['max_children'];
                        $max_people = $row['max_people'];
                        $min_people = $row['min_people'];
                        $room_facilities = $row['facilities'];
                        
                        $room_stock = isset($res_room[$id_room]['room_stock']) ? $res_room[$id_room]['room_stock'] : $row['stock'];
                        
                        $min_price = $room_price;
                        $result_rate->execute();
                        if($result_rate !== false && $db->last_row_count() > 0){
                            $row = $result_rate->fetch();
                            $price = $row['price'];
                            if($price > 0) $min_price = $price;
                        }
                        $type = $texts['NIGHT'];
                        if(!isset($res_room[$id_room]) || isset($res_room[$id_room]['error'])){
                            $amount = $min_price;
                            $full_price = 0;
                        }else{
                            $amount = $res_room[$id_room]['amount']+$res_room[$id_room]['fixed_sup'];
                            $full_price = $res_room[$id_room]['full_price']+$res_room[$id_room]['fixed_sup'];
                            $type = $num_nights.' '.getAltText($texts['NIGHT'], $texts['NIGHTS'], $num_nights);
                        } ?>
                        
                        <input type="hidden" name="rooms[]" value="<?php echo $id_room; ?>">
                        <input type="hidden" name="room_<?php echo $id_room; ?>" value="<?php echo $room_title; ?>">
                        
                        <div class="booking-result">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="pull-right">
                                            <div class="col-sm-6">
                                                <div class="room-actives">
                                                    <?php
                                                        $result_room_file->execute();
                                                        if($result_room_file !== false && $db->last_row_count() > 0){
                                                            $row = $result_room_file->fetch(PDO::FETCH_ASSOC);

                                                            $file_id = $row['id'];
                                                            $filename = $row['file'];
                                                            $label = $row['label'];

                                                            $realpath = SYSBASE.'medias/room/small/'.$file_id.'/'.$filename;
                                                            $thumbpath = DOCBASE.'medias/room/small/'.$file_id.'/'.$filename;
                                                            $zoompath = DOCBASE.'medias/room/big/'.$file_id.'/'.$filename;

                                                            if(is_file($realpath)){ ?>
                                                                <div class="img-container md">
                                                                    <img alt="<?php echo $label; ?>" src="<?php echo $thumbpath; ?>" itemprop="photo">
                                                                </div>
                                                                <?php
                                                            }
                                                        } ?>
                                                        <div class="clearfix mt10">
                                                        <?php
                                                        $result_facility->execute();
                                                        if($result_facility !== false && $db->last_row_count() > 0){
                                                            foreach($result_facility as $row){
                                                                $id_facility = $row['id'];
                                                                $facility_name = $row['name'];
                                                                
                                                                $result_facility_file->execute();
                                                                if($result_facility_file !== false && $db->last_row_count() > 0){
                                                                    $row = $result_facility_file->fetch();
                                                                    
                                                                    $file_id = $row['id'];
                                                                    $filename = $row['file'];
                                                                    $label = $row['label'];
                                                                    
                                                                    $realpath = SYSBASE.'medias/facility/big/'.$file_id.'/'.$filename;
                                                                    $thumbpath = DOCBASE.'medias/facility/big/'.$file_id.'/'.$filename;
                                                                        
                                                                    if(is_file($realpath)){ ?>
                                                                        <span class="facility-icon">
                                                                            <img alt="<?php echo $facility_name; ?>" title="<?php echo $facility_name; ?>" src="<?php echo $thumbpath; ?>" class="tips">
                                                                        </span>
                                                                        <?php
                                                                    }
                                                                }
                                                            } ?>
                                                            <span class="facility-icon">
                                                                <a href="<?php echo DOCBASE.$pages[9]['alias'].'/'.text_format($room_alias); ?>" title="<?php echo $texts['READMORE']; ?>" class="tips">...</a>
                                                            </span>
                                                            <?php
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                               <div class="details-section">
                                                    <div class="price-det">
                                                        <h5>R-301</h5>
                                                        <label>Price/Night: 100 $</label>
                                                        <span>Capacity:</span>
                                                        <i class="fa fa-male"></i>x5
                                                    </div>
                                                    <h3><?php echo $room_title; ?></h3>
                                                    <h4><?php echo $room_subtitle; ?></h4>
                                                    <p><?php echo strtrunc(strip_tags($room_descr), 150); ?>
                                                        <a id="booking_detl" title="Read More" href="<?php echo DOCBASE.$pages[9]['alias'].'/'.text_format($room_alias); ?>">
                                                            <i class="fa fa-plus-circle"></i>
                                                            <?php echo $texts['READMORE']; ?>
                                                        </a>
                                                    </p>
                                                    <div class="listing-booking">
                                                        <ul class="booking-model">
                                                            <li><button type="button" class="btn btn-primary" id="services" data-toggle="modal" data-target="#exampleModal">
                                                                <i class="fa fa-plus-circle"></i>Services
                                                            </button></li>

                                                            <li><button type="button" class="btn btn-primary" id="activities" data-toggle="modal" data-target="#exampleModal1">
                                                                <i class="fa fa-plus-circle"></i>Activities
                                                            </button></li>
                                                        
                                                            <li><button type="button" class="btn btn-primary" id="customer-info" data-toggle="modal" data-target="#exampleModal2">
                                                                <i class="fa fa-plus-circle"></i>Customer Info
                                                            </button></li>
                                                        </ul>
                                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                          <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                              <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Services</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                  <span aria-hidden="true">&times;</span>
                                                                </button>
                                                              </div>
                                                                <div class="modal-body row_container">
                                                                    <div class="col-md-12">
                                                                        <div class="col-md-5">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon"><?php echo 'Category'; ?></div>
                                                                                    <select name="num_adults" class="selectpicker form-control">
                                                                                        <?php
                                                                                        for($i = 1; $i <= $max_adults_search; $i++){
                                                                                            $select = ($_SESSION['num_adults'] == $i) ? ' selected="selected"' : '';
                                                                                            echo '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
                                                                                        } ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon"><?php echo 'Title'; ?></div>
                                                                                    <select name="num_adults" class="selectpicker form-control">
                                                                                        <?php
                                                                                        for($i = 1; $i <= $max_adults_search; $i++){
                                                                                            $select = ($_SESSION['num_adults'] == $i) ? ' selected="selected"' : '';
                                                                                            echo '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
                                                                                        } ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <button type="button" class="btn btn-default">
                                                                               <span class="glyphicon glyphicon-search"></span> Search
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-5 text-muted">
                                                                       <div class="popup-dets">
                                                                            <div class="price-det">
                                                                                <h2>R-301</h2>
                                                                                <label>Price/Night: 100 $</label>
                                                                                <span></span>
                                                                            </div>
                                                                            <h3><?php echo $room_title; ?></h3>
                                                                            <h4><?php echo $room_subtitle; ?></h4>
                                                                            <p><?php echo strtrunc(strip_tags($room_descr), 200); ?>
                                                                                
                                                                            </p>
                                                                            
                                                                            <a  href="" class="btn btn-info pull-right add-record" data-added="0"  id="booking_detls" title="Services">
                                                                                <i class="fa fa-plus-circle"></i>
                                                                                <?php echo 'Services'; ?>
                                                                            </a>
                                                                            <a  id="booking_detls1" title="Read More" href="<?php echo DOCBASE.$pages[9]['alias'].'/'.text_format($room_alias); ?>">
                                                                                <i class="fa fa-plus-circle"></i>
                                                                                <?php echo $texts['READMORE']; ?>
                                                                            </a>
                                                                       </div>
                                                                    </div>

                                                                   
                                                                    <div class="col-md-7">
                                                                        <div class="row">
                                                                            <form enctype="multipart/form-data" action="" method="POST">
                                                                                <div class="col-md-12">
                                                                                <h5>Selected Services</h5>
                                                                                  <div class="table-responsive">
                                                                                    <table class="table table-bordered heading" id="tbl_posts">
                                                                                      <thead>
                                                                                        <tr>
                                                                                          <th class="text-center" width="30">Sr #</th>
                                                                                          <th class="text-center">Title</th>
                                                                                          <th class="text-center">Unit</th>
                                                                                          <th class="text-center">Quantity</th>
                                                                                          <th class="text-center">Amount</th>
                                                                                          <th class="text-center">Actions</th>
                                                                                        </tr>
                                                                                      </thead>
                                                                                        
                                                                                      <tbody id="tbl_posts_body">
                                                                                        <tr id="rec-1">
                                                                                          <td><span class="sn">1</span>.</td>
                                                                                          <td><input class="form-control" type="text" name="title" value=""  /></td>
                                                                                          <td><input class="form-control" type="text" name="unit" value=""  /></td>
                                                                                          <td><input class="form-control" type="text" name="Quantity" value=""   /></td>
                                                                                          <td><input class="form-control" type="text" name="amount" value="" /></td>
                                                                                            
                                                                                          <td><a class="btn btn-danger glyphicon glyphicon-remove row-remove delete-record" data-id="1"></a></td>
                                                                                        </tr>
                                                                                        
                                                                                        
                                                                                      </tbody>
                                                                                    </table>
                                                                                  </div>
                                                                                </div>
                                                                            </form>
                                                                          </div>
                                                                        <div style="display:none;">
                                                                          <table id="sample_table">
                                                                            <tr id="">
                                                                             <td><span class="sn"></span>.</td>
                                                                               <td><input class="form-control" type="text" name="title" value=""  /></td>
                                                                               <td><input class="form-control" type="text" name="unit" value=""  /></td>
                                                                               <td><input class="form-control" type="text" name="Quantity" value=""   /></td>
                                                                               <td><input class="form-control" type="text" name="amount" value="" /></td>

                                                                                <td><a class="btn btn-danger glyphicon glyphicon-remove row-remove delete-record" data-id="1"></a></td>
                                                                             </tr>
                                                                         </table>
                                                                        </div>

                                                                        </div>

                                                                    </div>

                                                                <!-- <div class="templates">
                                                                    <div class="col-md-7 row_clone">
                                                                        <div class="col-sm-1">
                                                                           1
                                                                         </div>
                                                                        <div class="col-sm-2">
                                                                          <input type="text" size="8" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                          <input type="text" size="8" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           <input type="text" size="8" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           <input type="text" size="8" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           <input type="text" size="8" />
                                                                        </div>
                                                                         <div class="col-sm-1">
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div> -->
                                                                <div class="modal-footer">
                                                                    <button type="submit" value="" name="services" class="btn btn-info">Save</button>
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                </div>
                                                                

                                                            </div>
                                                          </div>
                                                        </div>

                                                        <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                          <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                              <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Activities</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                  <span aria-hidden="true">&times;</span>
                                                                </button>
                                                              </div>
                                                                <div class="modal-body row_container">
                                                                     <div class="col-md-12">
                                                                        <div class="col-md-5">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon"><?php echo 'Category'; ?></div>
                                                                                    <select name="num_adults" class="selectpicker form-control">
                                                                                        <?php
                                                                                        for($i = 1; $i <= $max_adults_search; $i++){
                                                                                            $select = ($_SESSION['num_adults'] == $i) ? ' selected="selected"' : '';
                                                                                            echo '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
                                                                                        } ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon"><?php echo 'Title'; ?></div>
                                                                                    <select name="num_adults" class="selectpicker form-control">
                                                                                        <?php
                                                                                        for($i = 1; $i <= $max_adults_search; $i++){
                                                                                            $select = ($_SESSION['num_adults'] == $i) ? ' selected="selected"' : '';
                                                                                            echo '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
                                                                                        } ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <button type="button" class="btn btn-default">
                                                                               <span class="glyphicon glyphicon-search"></span> Search
                                                                            </button>
                                                                        </div>
                                                                    </div> 
                                                                    <div class="col-md-5 text-muted">
                                                                       <div class="popup-dets">
                                                                            <div class="price-det">
                                                                                <h2>R-301</h2>
                                                                                <label>Price/Night: 100 $</label>
                                                                                <span></span>
                                                                            </div>
                                                                            <h3><?php echo $room_title; ?></h3>
                                                                            <h4><?php echo $room_subtitle; ?></h4>
                                                                            <p><?php echo strtrunc(strip_tags($room_descr), 200); ?>
                                                                                
                                                                            </p>
                                                                            
                                                                            <a  href="" class="btn btn-info pull-right add-record" data-added="0"  id="booking_detls" title="Services">
                                                                                <i class="fa fa-plus-circle"></i>
                                                                                <?php echo 'Activities'; ?>
                                                                            </a>
                                                                            <a  id="booking_detls1" title="Read More" href="<?php echo DOCBASE.$pages[9]['alias'].'/'.text_format($room_alias); ?>">
                                                                                <i class="fa fa-plus-circle"></i>
                                                                                <?php echo $texts['READMORE']; ?>
                                                                            </a>
                                                                       </div>
                                                                    </div>
                                                                    <div class="col-md-7">
                                                                        <div class="row">
                                                                            <form enctype="multipart/form-data" action="" method="POST">
                                                                                <div class="col-md-12">
                                                                                    <h5>Selected Activities</h5>
                                                                                    <div class="table-responsive">
                                                                                        <div class="table-responsive">
                                                                                    <table class="table table-bordered heading" id="tbl_posts">
                                                                                      <thead>
                                                                                        <tr>
                                                                                          <th class="text-center" width="30">Sr #</th>
                                                                                          <th class="text-center">Title</th>
                                                                                          <th class="text-center">Unit</th>
                                                                                          <th class="text-center">Quantity</th>
                                                                                          <th class="text-center">Amount</th>
                                                                                          <th class="text-center">Actions</th>
                                                                                        </tr>
                                                                                      </thead>
                                                                                        
                                                                                      <tbody id="tbl_posts_body">
                                                                                        <tr id="rec-1">
                                                                                          <td><span class="sn">1</span>.</td>
                                                                                          <td><input class="form-control" type="text" name="title" value=""  /></td>
                                                                                          <td><input class="form-control" type="text" name="unit" value=""  /></td>
                                                                                          <td><input class="form-control" type="text" name="Quantity" value=""   /></td>
                                                                                          <td><input class="form-control" type="text" name="amount" value="" /></td>
                                                                                            
                                                                                          <td><a class="btn btn-danger glyphicon glyphicon-remove row-remove delete-record" data-id="1"></a></td>
                                                                                        </tr>
                                                                                        
                                                                                      </tbody>
                                                                                    </table>
                                                                                  </div>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                        <div style="display:none;">
                                                                            <table id="sample_table">
                                                                             <tr id="">
                                                                               <td><span class="sn"></span>.</td>
                                                                               <td><input class="form-control" type="text" name="title" value=""  /></td>
                                                                               <td><input class="form-control" type="text" name="unit" value=""  /></td>
                                                                               <td><input class="form-control" type="text" name="Quantity" value=""   /></td>
                                                                               <td><input class="form-control" type="text" name="amount" value="" /></td>

                                                                                <td><a class="btn btn-danger glyphicon glyphicon-remove row-remove delete-record" data-id="1"></a></td>
                                                                             </tr>
                                                                           </table>
                                                                        </div>
                                                                    </div>

                                                                    

                                                                </div>
                                                                <!-- <div class="templates">
                                                                    <div class="col-md-7 row_clone">
                                                                        <div class="col-sm-1">
                                                                           1
                                                                         </div>
                                                                        <div class="col-sm-2">
                                                                          <input type="text" size="8" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                          <input type="text" size="8" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           <input type="text" size="8" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           <input type="text" size="8" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           <input type="text" size="8" />
                                                                        </div>
                                                                         <div class="col-sm-1">
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div> -->
                                                                <div class="modal-footer">
                                                                    <button type="submit" value="" name="services" class="btn btn-info">Save</button>
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>
                                                          </div>
                                                        </div>

                                                        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                          <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                              <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Customer Info</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                  <span aria-hidden="true">&times;</span>
                                                                </button>
                                                              </div>
                                                                <div class="modal-body customer-dets">
                                                                   <form method="POST" action="">
                                                                       <div class="col-md-6">
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Customer<span style="color:red;">*</span> </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="customer" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Firstname<span style="color:red;">*</span></label>
                                                                                 </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="fname" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Lastname<span style="color:red;">*</span> </label>
                                                                                 </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="lastname" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Email<span style="color:red;">*</span></label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="email" name="email" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Mobile<span style="color:red;">*</span></label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="mobile" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>CNIC/EID<span style="color:red;">*</span></label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="cnic" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Passport No <span style="color:red;">*</span> </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="passport" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>City <span style="color:red;">*</span> </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="city" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Postcode <span style="color:red;">*</span> </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="postcode" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Address<span style="color:red;">*</span> </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="address" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Country <span style="color:red;">*</span> </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="country" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Notes <span style="color:red;">*</span> </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <textarea cols="4" rows="6" class="form-control" name="notes" value=""></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                   </form>
                                                                </div>
                                                                   
                                                                
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    <button type="button" name="customer-save" class="btn btn-info save-models">Save changes</button>
                                                                </div>
                                                                
                                                            </div>
                                                          </div>
                                                        </div>

                                                    </div>

                                                </div> 
                                            </div>
                                        </div>
                                        <div class="for-bookingtotal">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-male"></i> <?php echo $texts['ADULTS']; ?></div>
                                                        <select name="num_adults" class="selectpicker form-control">
                                                           <option value="0">0</option>
                                                           <option value="1">1</option>
                                                           <option value="2">2</option>
                                                           <option value="3">3</option>
                                                           <option value="4">4</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-child"></i> <?php echo $texts['CHILDREN']; ?></div>
                                                        <select name="num_children" class="selectpicker form-control">
                                                           <option value="0">0</option>
                                                           <option value="1">1</option>
                                                           <option value="2">2</option>
                                                           <option value="3">3</option>
                                                           <option value="4">4</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><?php echo 'Total'; ?></div>
                                                        <input name="total-price" id="booking_total" value="" class="form-control" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">

                                                <button type="button" class="btn btn-primary" id="bookings_hotels" data-toggle="modal" data-target="#examplepayment">
                                                  Book & Payment
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="examplepayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                  <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <div class="details-sections">
                                                            <h5 class="modal-title" id="exampleModalLabel">Payment Info</h5>
                                                        </div>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                        </button>
                                                      </div>
                                                      <div class="modal-body customer-dets">
                                                            <form method="POST" action="">
                                                                <div class="col-md-6">
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Total Room Charges.</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <div class="input-group">
                                                                                <input type="text" name="roomcharge" value="" class="form-control" >
                                                                                <div class="input-group-addon"><i class="fa fa-dollar-sign"></i></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Total Service Charges.</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <div class="input-group">
                                                                                <input class="form-control" type="text" name="service-charges" value="" placeholder="" />
                                                                                <div class="input-group-addon"><i class="fa fa-dollar-sign"></i></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Total Activities Charges.</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <div class="input-group">
                                                                                <input class="form-control" type="text" name="activitie-charges" value="" placeholder="" />
                                                                                <div class="input-group-addon"><i class="fa fa-dollar-sign"></i></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Total Add On Charges.</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <div class="input-group">
                                                                                <input class="form-control" type="text" name="addon-charges" value="" placeholder="" />
                                                                                <div class="input-group-addon"><i class="fa fa-dollar-sign"></i></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Discount Amount.</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <div class="input-group">
                                                                                <input class="form-control" type="text" name="discount-amount" value="" placeholder="" />
                                                                                <div class="input-group-addon"><i class="fa fa-dollar-sign"></i></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Down Payment.</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <div class="input-group">
                                                                                <input class="form-control" type="text" name="down-payment" value="" placeholder="" />
                                                                                <div class="input-group-addon"><i class="fa fa-dollar-sign"></i></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Net Payable Amount.</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <div class="input-group">
                                                                                <input class="form-control" type="text" name="netpayable" value="" placeholder="" />
                                                                                <div class="input-group-addon"><i class="fa fa-dollar-sign"></i></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">

                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Discount Coupon Code.</label>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <input class="form-control" type="text" name="couponcode" value="" placeholder="" />
                                                                            <i>Discount Coupan Name:</i>
                                                                            <i>Discount Amount/(%):</i>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <button type="button" class="btn btn-info">Validate</button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Payment Mode.</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <select class="form-control" id="colorselector">
                                                                                <option value="cash">Cash</option>
                                                                                <option value="bank">Bank</option>
                                                                                <option value="yellow">Card</option>
                                                                            </select>
                                                                        </div>
                                                                        
                                                                        <div id="yellow" class="colors">
                                                                            <form action="" method="POST">
                                                                                <div class="col-md-12">
                                                                                    <h5>Enter Payment Information</h5>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-6">
                                                                                          <label for="inputEmail4">First Name</label>
                                                                                          <input type="text" name="firstname" value="" class="form-control" id="inputEmail4" />
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                          <label for="inputPassword4">Last Name</label>
                                                                                          <input type="text" name="lastname" value="" class="form-control" id="inputPassword4" />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-8">
                                                                                          <label for="inputEmail4">Credit Card Number</label>
                                                                                          <input type="text" name="creditcard" value="" maxlength="16" class="form-control" id="inputEmail4" />
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                          <label for="inputPassword4">CVV</label>
                                                                                          <input type="text" name="cvv" value="" class="form-control" id="inputPassword4" />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-8">
                                                                                          <label for="inputEmail4">Expiration</label>
                                                                                          <input type="text" id="datepicker" class="date-picker form-control" datefor/>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-12">
                                                                                          <label for="inputEmail4">Billing Address *</label>
                                                                                          <input type="text" name="billingaddress" value="" class="form-control" id="inputEmail4" />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-6">
                                                                                          <label for="inputEmail4">City</label>
                                                                                          <input type="text" name="city" value="" class="form-control" id="inputEmail4" />
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                          <label for="inputEmail4">State/province/Region</label>
                                                                                          <input type="text" name="region" value="" class="form-control" id="inputEmail4" />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-6">
                                                                                          <label for="inputEmail4">Postal/Zip Code</label>
                                                                                          <input type="text" name="city" value="" class="form-control" id="inputEmail4" />
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                          <label for="inputState">Country</label>
                                                                                          <select id="inputState" class="form-control">
                                                                                            <option selected value="country">--Choose--</option>
                                                                                            <option value="pk">Pakistan</option>
                                                                                            <option value="in">India</option>
                                                                                            <option value="usa">USA</option>
                                                                                            <option value="uk">UK</option>
                                                                                            <option value="ch">China</option>
                                                                                          </select>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </form>
                                                                        </div>

                                                                    </div>

                                                                    <div class="row" id="hide">
                                                                        <div class="col-sm-5">
                                                                            <label>Transaction / Reference.</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <input class="form-control" type="text" name="trans-refrence" value="" placeholder="" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Comments.</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <textarea cols="2" rows="3" name="comments" class="form-control"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row" id="hide" >
                                                                        <div class="col-sm-5">
                                                                            <label>Status<span style="color:red;">*</span> .</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <select class="form-control">
                                                                                <option value="1">Pending</option>
                                                                                <option value="2">Awaiting</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="" name="payment" class="btn btn-info">Save changes</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="pull-left">
                                            <div class="col-sm-6">
                                                <div class="room-actives">
                                                    <?php
                                                        $result_room_file->execute();
                                                        if($result_room_file !== false && $db->last_row_count() > 0){
                                                            $row = $result_room_file->fetch(PDO::FETCH_ASSOC);

                                                            $file_id = $row['id'];
                                                            $filename = $row['file'];
                                                            $label = $row['label'];

                                                            $realpath = SYSBASE.'medias/room/small/'.$file_id.'/'.$filename;
                                                            $thumbpath = DOCBASE.'medias/room/small/'.$file_id.'/'.$filename;
                                                            $zoompath = DOCBASE.'medias/room/big/'.$file_id.'/'.$filename;

                                                            if(is_file($realpath)){ ?>
                                                                <div class="img-container md">
                                                                    <img alt="<?php echo $label; ?>" src="<?php echo $thumbpath; ?>" itemprop="photo">
                                                                </div>
                                                                <?php
                                                            }
                                                        } ?>
                                                        <div class="clearfix mt10">
                                                        <?php
                                                        $result_facility->execute();
                                                        if($result_facility !== false && $db->last_row_count() > 0){
                                                            foreach($result_facility as $row){
                                                                $id_facility = $row['id'];
                                                                $facility_name = $row['name'];
                                                                
                                                                $result_facility_file->execute();
                                                                if($result_facility_file !== false && $db->last_row_count() > 0){
                                                                    $row = $result_facility_file->fetch();
                                                                    
                                                                    $file_id = $row['id'];
                                                                    $filename = $row['file'];
                                                                    $label = $row['label'];
                                                                    
                                                                    $realpath = SYSBASE.'medias/facility/big/'.$file_id.'/'.$filename;
                                                                    $thumbpath = DOCBASE.'medias/facility/big/'.$file_id.'/'.$filename;
                                                                        
                                                                    if(is_file($realpath)){ ?>
                                                                        <span class="facility-icon">
                                                                            <img alt="<?php echo $facility_name; ?>" title="<?php echo $facility_name; ?>" src="<?php echo $thumbpath; ?>" class="tips">
                                                                        </span>
                                                                        <?php
                                                                    }
                                                                }
                                                            } ?>
                                                            <span class="facility-icon">
                                                                <a href="<?php echo DOCBASE.$pages[9]['alias'].'/'.text_format($room_alias); ?>" title="<?php echo $texts['READMORE']; ?>" class="tips">...</a>
                                                            </span>
                                                            <?php
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                               <div class="details-section">
                                                    <div class="price-det">
                                                        <h5>R-301</h5>
                                                        <label>Price/Night: 100 $</label>
                                                        <span>Capacity:</span>
                                                        <i class="fa fa-male"></i>x5
                                                    </div>
                                                    <h3><?php echo $room_title; ?></h3>
                                                    <h4><?php echo $room_subtitle; ?></h4>
                                                    <p><?php echo strtrunc(strip_tags($room_descr), 150); ?>
                                                        <a id="booking_detl" title="Read More" href="<?php echo DOCBASE.$pages[9]['alias'].'/'.text_format($room_alias); ?>">
                                                            <i class="fa fa-plus-circle"></i>
                                                            <?php echo $texts['READMORE']; ?>
                                                        </a>
                                                    </p>
                                                    <div class="listing-booking">
                                                        <ul class="booking-model">
                                                            <li><button type="button" class="btn btn-primary" id="services" data-toggle="modal" data-target="#exampleModal">
                                                                <i class="fa fa-plus-circle"></i>Services
                                                            </button></li>

                                                            <li><button type="button" class="btn btn-primary" id="activities" data-toggle="modal" data-target="#exampleModal1">
                                                                <i class="fa fa-plus-circle"></i>Activities
                                                            </button></li>
                                                        
                                                            <li><button type="button" class="btn btn-primary" id="customer-info" data-toggle="modal" data-target="#exampleModal2">
                                                                <i class="fa fa-plus-circle"></i>Customer Info
                                                            </button></li>
                                                        </ul>
                                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                          <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                              <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Services</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                  <span aria-hidden="true">&times;</span>
                                                                </button>
                                                              </div>
                                                                <div class="modal-body row_container">

                                                                    <div class="col-md-5">
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                <div class="form-group">
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-addon"><i class="fa fa-servicestack"></i> <?php echo 'Category'; ?></div>
                                                                                        <select name="num_adults" class="selectpicker form-control">
                                                                                            <?php
                                                                                            for($i = 1; $i <= $max_adults_search; $i++){
                                                                                                $select = ($_SESSION['num_adults'] == $i) ? ' selected="selected"' : '';
                                                                                                echo '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
                                                                                            } ?>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-5">
                                                                                <div class="form-group">
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-addon"><i class="fa fa-servicestack"></i> <?php echo 'Title'; ?></div>
                                                                                        <select name="num_adults" class="selectpicker form-control">
                                                                                            <?php
                                                                                            for($i = 1; $i <= $max_adults_search; $i++){
                                                                                                $select = ($_SESSION['num_adults'] == $i) ? ' selected="selected"' : '';
                                                                                                echo '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
                                                                                            } ?>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <div class="form-group">
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-addon searchs"><i class="fa fa-search"></i></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                       <div class="popup-dets">
                                                                            <div class="price-det">
                                                                                <h5>R-301</h5>
                                                                                <label>Price/Night: 100 $</label>
                                                                                <span></span>
                                                                                
                                                                            </div>
                                                                            <h3><?php echo $room_title; ?></h3>
                                                                            <h4><?php echo $room_subtitle; ?></h4>
                                                                            <p><?php echo strtrunc(strip_tags($room_descr), 220); ?>
                                                                                <a  href="" class="btn btn-primary" id="booking_detls" title="Services">
                                                                                    <i class="fa fa-plus-circle"></i>
                                                                                    <?php echo 'Services'; ?>
                                                                                </a>
                                                                                <a  id="booking_detls1" title="Read More" href="<?php echo DOCBASE.$pages[9]['alias'].'/'.text_format($room_alias); ?>">
                                                                                    <i class="fa fa-plus-circle"></i>
                                                                                    <?php echo $texts['READMORE']; ?>
                                                                                </a>
                                                                            </p>
                                                                       </div>
                                                                    </div>

                                                                    <div class="pull-right">
                                                                        <button type="button" id="mine">-</button>
                                                                        <button type="button" id="plus">+</button>
                                                                    </div>

                                                                    <div class="col-md-7">
                                                                        
                                                                        <div class="col-sm-1">
                                                                           ID
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           Service Title
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           Price Unit
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           Unit Price
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           Quantity
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           Amount
                                                                        </div>
                                                                         <div class="col-sm-1">
                                                                           Actions
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="templates">
                                                                    <div class="col-md-7 row_clone">
                                                                        <div class="col-sm-1">
                                                                           1
                                                                         </div>
                                                                        <div class="col-sm-2">
                                                                          <input type="text" size="8" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                          <input type="text" size="8" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           <input type="text" size="8" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           <input type="text" size="8" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           <input type="text" size="8" />
                                                                        </div>
                                                                         <div class="col-sm-1">
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    <button type="button" class="btn btn-primary save-models">Save changes</button>
                                                                </div>

                                                            </div>
                                                          </div>
                                                        </div>

                                                        <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                          <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                              <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Activities</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                  <span aria-hidden="true">&times;</span>
                                                                </button>
                                                              </div>
                                                                <div class="modal-body row_container">
                                                                    <div class="col-md-5">
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                <div class="form-group">
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-addon"><i class="fa fa-servicestack"></i> <?php echo 'Category'; ?></div>
                                                                                        <select name="num_adults" class="selectpicker form-control">
                                                                                            <select name="num_children" class="selectpicker form-control">
                                                                                               <option value="0">0</option>
                                                                                               <option value="1">1</option>
                                                                                               <option value="2">2</option>
                                                                                               <option value="3">3</option>
                                                                                               <option value="4">4</option>
                                                                                            </select>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-5">
                                                                                <div class="form-group">
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-addon"><i class="fa fa-servicestack"></i> <?php echo 'Title'; ?></div>
                                                                                        <select name="num_adults" class="selectpicker form-control">
                                                                                            <select name="num_children" class="selectpicker form-control">
                                                                                               <option value="0">0</option>
                                                                                               <option value="1">1</option>
                                                                                               <option value="2">2</option>
                                                                                               <option value="3">3</option>
                                                                                               <option value="4">4</option>
                                                                                            </select>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <div class="form-group">
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-addon searchs"><i class="fa fa-search"></i></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                       <div class="popup-dets">
                                                                            <div class="price-det">
                                                                                <h5>R-301</h5>
                                                                                <label>Price/Night: 100 $</label>
                                                                                <span></span>
                                                                            </div>
                                                                            <h3><?php echo $room_title; ?></h3>
                                                                            <h4><?php echo $room_subtitle; ?></h4>
                                                                            <p>Test For Picture</p>
                                                                            <p><?php echo strtrunc(strip_tags($room_descr), 220); ?>
                                                                                <a  href="" class="btn btn-primary" id="booking_detls" title="Services">
                                                                                    <i class="fa fa-plus-circle"></i>
                                                                                    <?php echo 'Services'; ?>
                                                                                </a>
                                                                                <a  id="booking_detls1" title="Read More" href="<?php echo DOCBASE.$pages[9]['alias'].'/'.text_format($room_alias); ?>">
                                                                                    <i class="fa fa-plus-circle"></i>
                                                                                    <?php echo $texts['READMORE']; ?>
                                                                                </a>
                                                                            </p>
                                                                       </div>
                                                                    </div>

                                                                    <div class="pull-right">
                                                                        <button type="button" id="mine">-</button>
                                                                        <button type="button" id="plus">+</button>
                                                                    </div>

                                                                    <div class="col-md-7">
                                                                        
                                                                        <div class="col-sm-1">
                                                                           ID
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           Service Title
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           Price Unit
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           Unit Price
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           Quantity
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           Amount
                                                                        </div>
                                                                         <div class="col-sm-1">
                                                                           Actions
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="templates">
                                                                    <div class="col-md-7 row_clone">
                                                                        <div class="col-sm-1">
                                                                           1
                                                                         </div>
                                                                        <div class="col-sm-2">
                                                                          <input type="text" size="8" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                          <input type="text" size="8" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           <input type="text" size="8" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           <input type="text" size="8" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                           <input type="text" size="8" />
                                                                        </div>
                                                                         <div class="col-sm-1">
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                              <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary save-models">Save changes</button>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>

                                                        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                          <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                              <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Customer Info</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                  <span aria-hidden="true">&times;</span>
                                                                </button>
                                                              </div>
                                                                <div class="modal-body customer-dets">
                                                                   <form method="POST" action="" class="form-control">
                                                                       <div class="col-md-6">
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Customer * </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="customer" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Firstname * </label>
                                                                                 </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="fname" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Lastname * </label>
                                                                                 </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="lastname" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Email * </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="email" name="email" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Mobile * </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="mobile" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>CNIC/EID * </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="cnic" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Passport No * </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="passport" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>City * </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="city" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Postcode * </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="postcode" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Address * </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="address" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Country * </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <input class="form-control" type="text" name="country" value="" placeholder="" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label>Notes * </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <textarea cols="4" rows="6" class="form-control" name="notes" value=""></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                   </form>
                                                                </div>
                                                                   
                                                                
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    <button type="button" name="customer-save" class="btn btn-info save-models">Save changes</button>
                                                                </div>
                                                                
                                                            </div>
                                                          </div>
                                                        </div>

                                                    </div>

                                                </div> 
                                            </div>
                                        </div>
                                        <div class="for-bookingtotal">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-male"></i> <?php echo $texts['ADULTS']; ?></div>
                                                        <select name="num_adults" class="selectpicker form-control">
                                                           <option value="0">0</option>
                                                           <option value="1">1</option>
                                                           <option value="2">2</option>
                                                           <option value="3">3</option>
                                                           <option value="4">4</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-child"></i> <?php echo $texts['CHILDREN']; ?></div>
                                                        <select name="num_children" class="selectpicker form-control">
                                                           <option value="0">0</option>
                                                           <option value="1">1</option>
                                                           <option value="2">2</option>
                                                           <option value="3">3</option>
                                                           <option value="4">4</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><?php echo 'Total'; ?></div>
                                                        <input name="total-price" id="booking_total" value="" class="form-control" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">

                                                <button type="button" class="btn btn-primary" id="bookings_hotels" data-toggle="modal" data-target="#examplepayment">
                                                  Book & Payment
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="examplepayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                  <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <div class="details-sections">
                                                            <h5 class="modal-title" id="exampleModalLabel">Payment Info</h5>
                                                        </div>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                        </button>
                                                      </div>
                                                      <div class="modal-body customer-dets">
                                                            <form method="POST" action="">
                                                                <div class="col-md-6">
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Total Room Charges</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <input class="form-control" type="text" name="room-charges" value="" placeholder="" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Total Service Charges</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <input class="form-control" type="text" name="service-charges" value="" placeholder="" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Total Activities Charges</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <input class="form-control" type="text" name="activitie-charges" value="" placeholder="" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Total Add On Charges</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <input class="form-control" type="text" name="addon-charges" value="" placeholder="" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Discount Amount</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <input class="form-control" type="text" name="discount-amount" value="" placeholder="" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Down Payment</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <input class="form-control" type="text" name="down-payment" value="" placeholder="" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Net Payable Amount</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <input class="form-control" type="text" name="netpayable" value="" placeholder="" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">

                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Discount Coupon Code</label>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <input class="form-control" type="text" name="couponcode" value="" placeholder="" />
                                                                            <i>Discount Coupan Name:</i>
                                                                            <i>Discount Amount/(%):</i>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <button type="button" class="btn btn-info">Validate</button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Payment Mode</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <select class="form-control" id="colorselector">
                                                                                <option value="cash">Cash</option>
                                                                                <option value="bank">Bank</option>
                                                                                <option value="yellow">Card</option>
                                                                            </select>
                                                                        </div>
                                                                        
                                                                        <div id="yellow" class="colors">
                                                                            <form action="" method="POST">
                                                                                <div class="col-md-12">
                                                                                    <h5>Enter Payment Information</h5>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-6">
                                                                                          <label for="inputEmail4">First Name</label>
                                                                                          <input type="text" name="firstname" value="" class="form-control" id="inputEmail4" />
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                          <label for="inputPassword4">Last Name</label>
                                                                                          <input type="text" name="lastname" value="" class="form-control" id="inputPassword4" />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-8">
                                                                                          <label for="inputEmail4">Credit Card Number</label>
                                                                                          <input type="text" name="creditcard" value="" maxlength="16" class="form-control" id="inputEmail4" />
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                          <label for="inputPassword4">CVV</label>
                                                                                          <input type="text" name="cvv" value="" class="form-control" id="inputPassword4" />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-8">
                                                                                          <label for="inputEmail4">Expiration</label>
                                                                                          <input type="text" id="datepicker" class="date-picker form-control" datefor/>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-12">
                                                                                          <label for="inputEmail4">Billing Address *</label>
                                                                                          <input type="text" name="billingaddress" value="" class="form-control" id="inputEmail4" />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-6">
                                                                                          <label for="inputEmail4">City</label>
                                                                                          <input type="text" name="city" value="" class="form-control" id="inputEmail4" />
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                          <label for="inputEmail4">State/province/Region</label>
                                                                                          <input type="text" name="region" value="" class="form-control" id="inputEmail4" />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-6">
                                                                                          <label for="inputEmail4">Postal/Zip Code</label>
                                                                                          <input type="text" name="city" value="" class="form-control" id="inputEmail4" />
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                          <label for="inputState">Country</label>
                                                                                          <select id="inputState" class="form-control">
                                                                                            <option selected value="country">--Choose--</option>
                                                                                            <option value="pk">Pakistan</option>
                                                                                            <option value="in">India</option>
                                                                                            <option value="usa">USA</option>
                                                                                            <option value="uk">UK</option>
                                                                                            <option value="ch">China</option>
                                                                                          </select>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </form>
                                                                        </div>

                                                                    </div>

                                                                    <div class="row" id="hide">
                                                                        <div class="col-sm-5">
                                                                            <label>Transaction / Reference</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <input class="form-control" type="text" name="trans-refrence" value="" placeholder="" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-5">
                                                                            <label>Comments</label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <textarea cols="2" rows="3" name="comments" class="form-control"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row" id="hide" >
                                                                        <div class="col-sm-5">
                                                                            <label>Status * </label>
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <select class="form-control">
                                                                                <option value="1">Pending</option>
                                                                                <option value="2">Awaiting</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" name="payment" class="btn btn-info">Save changes</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        <!-- <div class="booking-result">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3">
                                        <?php
                                        $result_room_file->execute();
                                        if($result_room_file !== false && $db->last_row_count() > 0){
                                            $row = $result_room_file->fetch(PDO::FETCH_ASSOC);

                                            $file_id = $row['id'];
                                            $filename = $row['file'];
                                            $label = $row['label'];

                                            $realpath = SYSBASE.'medias/room/small/'.$file_id.'/'.$filename;
                                            $thumbpath = DOCBASE.'medias/room/small/'.$file_id.'/'.$filename;
                                            $zoompath = DOCBASE.'medias/room/big/'.$file_id.'/'.$filename;

                                            if(is_file($realpath)){ ?>
                                                <div class="img-container md">
                                                    <img alt="<?php echo $label; ?>" src="<?php echo $thumbpath; ?>" itemprop="photo">
                                                </div>
                                                <?php
                                            }
                                        } ?>
                                    </div>
                                    <div class="col-lg-4 col-md-3 col-sm-4">
                                        <h3><?php echo $room_title; ?></h3>
                                        <h4><?php echo $room_subtitle; ?></h4>
                                        <?php echo strtrunc(strip_tags($room_descr), 120); ?>
                                        <div class="clearfix mt10">
                                            <?php
                                            $result_facility->execute();
                                            if($result_facility !== false && $db->last_row_count() > 0){
                                                foreach($result_facility as $row){
                                                    $id_facility = $row['id'];
                                                    $facility_name = $row['name'];
                                                    
                                                    $result_facility_file->execute();
                                                    if($result_facility_file !== false && $db->last_row_count() > 0){
                                                        $row = $result_facility_file->fetch();
                                                        
                                                        $file_id = $row['id'];
                                                        $filename = $row['file'];
                                                        $label = $row['label'];
                                                        
                                                        $realpath = SYSBASE.'medias/facility/big/'.$file_id.'/'.$filename;
                                                        $thumbpath = DOCBASE.'medias/facility/big/'.$file_id.'/'.$filename;
                                                            
                                                        if(is_file($realpath)){ ?>
                                                            <span class="facility-icon">
                                                                <img alt="<?php echo $facility_name; ?>" title="<?php echo $facility_name; ?>" src="<?php echo $thumbpath; ?>" class="tips">
                                                            </span>
                                                            <?php
                                                        }
                                                    }
                                                } ?>
                                                <span class="facility-icon">
                                                    <a href="<?php echo DOCBASE.$pages[9]['alias'].'/'.text_format($room_alias); ?>" title="<?php echo $texts['READMORE']; ?>" class="tips">...</a>
                                                </span>
                                                <?php
                                            } ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-3 text-center sep">
                                        <div class="price">
                                            <span itemprop="priceRange"><?php echo formatPrice($amount*CURRENCY_RATE); ?></span>
                                            <?php
                                            if($full_price > 0 && $full_price > $amount){ ?>
                                                <br><s class="text-warning"><?php echo formatPrice($full_price*CURRENCY_RATE); ?></s>
                                                <?php
                                            } ?>
                                        </div>
                                        <div class="mb10 text-muted"><?php echo $texts['PRICE']; ?> / <?php echo $type; ?></div>
                                        <?php echo $texts['CAPACITY']; ?> : <i class="fa fa-male"></i>x<?php echo $max_people; ?>
                                        
                                        <?php
                                        if($room_stock > 0){ ?>
                                            <div class="input-group input-group-sm mt10">
                                                <div class="input-group-addon"><i class="fa fa-tags"></i> Num rooms</div>
                                                <select name="num_rooms[<?php echo $id_room; ?>]" class="form-control sendAjaxForm selectpicker" data-target="#room-options-<?php echo $id_room; ?>" data-extratarget="#booking-amount" data-action="<?php echo getFromTemplate('common/change_num_rooms.php'); ?>?room=<?php echo $id_room; ?>">
                                                    <?php
                                                    for($i = 0; $i <= $room_stock; $i++){ ?>
                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                        <?php
                                                    } ?>
                                                </select>
                                            </div>
                                            <?php
                                        }else{ ?>
                                            <div class="mt10 btn btn-danger btn-block" disabled="disabled"><?php echo $texts['NO_AVAILABILITY']; ?></div>
                                            <?php
                                        } ?>
                                        
                                        <p class="lead">
                                            <span class="clearfix"></span>
                                            <a class="btn btn-primary mt10 btn-block" href="<?php echo DOCBASE.$pages[9]['alias'].'/'.text_format($room_alias); ?>">
                                                <i class="fa fa-plus-circle"></i>
                                                <?php echo $texts['READMORE']; ?>
                                            </a>
                                        </p>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-5 sep">
                                        <div class="hb-calendar" data-cur_month="<?php echo gmdate('n', $from_time); ?>" data-cur_year="<?php echo gmdate('Y', $from_time); ?>" data-custom_var="room=<?php echo $id_room; ?>" data-day_loader="<?php echo getFromTemplate('common/get_days.php'); ?>"></div>
                                    </div>
                                </div>
                            </div>
                            <div id="room-options-<?php echo $id_room; ?>" class="room-options"></div>
                        </div> -->
                        <hr>
                        <?php
                    }
                    if($search_limit > 0){
                        $nb_pages = ceil($num_results/$search_limit);
                        if($nb_pages > 1){ ?>
                            <div class="container text-center">
                                <div class="btn-group">
                                    <?php
                                    for($i = 1; $i <= $nb_pages; $i++){
                                        $offset = ($i-1)*$search_limit;
                                        
                                        if($offset == $search_offset)
                                            echo '<span class="btn btn-default disabled">'.$i.'</span>';
                                        else{
                                            $request = ($offset == 0) ? '' : '?offset='.$offset;
                                            echo '<a class="btn btn-default" href="'.DOCBASE.$sys_pages['booking']['alias'].$request.'">'.$i.'</a>';
                                        }
                                    } ?>
                                </div>
                            </div>
                            <?php
                        }
                    }
                } ?>
            </div>
        </form>
    </div>
</section>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true
        });
    } );
</script>

<style>
    .colors{
        display: none;
    }
</style>

<script>
     $(function() {
        $('#colorselector').change(function(){
            $('.colors').hide();
            $('#' + $(this).val()).show();
        });
    });
</script>

// <script type="text/javascript">
//     $(document).ready(function () {
        
//         $('#mine').on('click', function() {
//            //  alert ("So ja bache na to TB a jai ga");
//             $( ".row_container").find(".row_clone:last-child" ).remove();
//            console.log ($( ".row_container").find(".row_clone:last-child" ));
//         });

//         $('#plus').on('click', function() {
            
//             var row= $('.templates').find(".row_clone")[0].outerHTML;
          
//             $(".row_container").append(row);

//             // $('#row_clone')
//             // .clone()
//             // .appendTo('#row_container');
//         });  
 

//     });
// </script>
<script type="text/javascript">
    $(document).ready(function () {
        $("yellow").change(function(){
            $("#hide").hide();
        });
    });

</script>
<script type="text/javascript">
    jQuery(document).delegate('a.add-record', 'click', function(e) {
     e.preventDefault();    
     var content = jQuery('#sample_table tr'),
     size = jQuery('#tbl_posts >tbody >tr').length + 1,
     element = null,    
     element = content.clone();
     element.attr('id', 'rec-'+size);
     element.find('.delete-record').attr('data-id', size);
     element.appendTo('#tbl_posts_body');
     element.find('.sn').html(size);
   });

    jQuery(document).delegate('a.delete-record', 'click', function(e) {
     e.preventDefault();    
     var didConfirm = confirm("Are you sure You want to delete");
     if (didConfirm == true) {
      var id = jQuery(this).attr('data-id');
      var targetDiv = jQuery(this).attr('targetDiv');
      jQuery('#rec-' + id).remove();
      
    //regnerate index number on table
    $('#tbl_posts_body tr').each(function(index) {
      //alert(index);
      $(this).find('span.sn').html(index+1);
    });
    return true;
  } else {
    return false;
  }
});
</script>