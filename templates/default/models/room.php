<?php
if($article_alias == '') err404();

$result = $db->query('SELECT * FROM pm_room WHERE checked = 1 AND lang = '.LANG_ID.' AND alias = '.$db->quote($article_alias));
if($result !== false && $db->last_row_count() > 0){
    
    $room = $result->fetch(PDO::FETCH_ASSOC);
    
    $room_id = $room['id'];
    $article_id = $room_id;
    $title_tag = $room['title'].' - '.$title_tag;
    $page_title = $room['title'];
    $page_subtitle = '';
    $page_alias = $pages[$page_id]['alias'].'/'.text_format($room['alias']);
    
    $result_room_file = $db->query('SELECT * FROM pm_room_file WHERE id_item = '.$room_id.' AND checked = 1 AND lang = '.DEFAULT_LANG.' AND type = \'image\' AND file != \'\' ORDER BY rank LIMIT 1');
    if($result_room_file !== false && $db->last_row_count() > 0){
        
        $row = $result_room_file->fetch();
        
        $file_id = $row['id'];
        $filename = $row['file'];
        
        if(is_file(SYSBASE.'medias/room/medium/'.$file_id.'/'.$filename))
            $page_img = getUrl(true).DOCBASE.'medias/room/medium/'.$file_id.'/'.$filename;
    }
    
}else err404();

check_URI(DOCBASE.$page_alias);

/* ==============================================
 * CSS AND JAVASCRIPT USED IN THIS MODEL
 * ==============================================
 */
$javascripts[] = DOCBASE.'js/plugins/sharrre/jquery.sharrre.min.js';

$javascripts[] = DOCBASE.'js/plugins/jquery.event.calendar/js/jquery.event.calendar.js';
$javascripts[] = DOCBASE.'js/plugins/jquery.event.calendar/js/languages/jquery.event.calendar.'.LANG_TAG.'.js';
$stylesheets[] = array('file' => DOCBASE.'js/plugins/jquery.event.calendar/css/jquery.event.calendar.css', 'media' => 'all');

$stylesheets[] = array('file' => '//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.2.4/assets/owl.carousel.min.css', 'media' => 'all');
$stylesheets[] = array('file' => '//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.2.4/assets/owl.theme.default.min.css', 'media' => 'all');
$javascripts[] = '//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.2.4/owl.carousel.min.js';

$stylesheets[] = array('file' => '//cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/3.5.5/css/star-rating.min.css', 'media' => 'all');
$javascripts[] = '//cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/3.5.5/js/star-rating.min.js';

require(getFromTemplate('common/send_comment.php', false));

require(getFromTemplate('common/header.php', false)); ?>

<article id="page">
    <?php include(getFromTemplate('common/page_header.php', false)); ?>
    
    <div id="content" class="pt20 pb30">
        
        <div class="container">
            <div class="row">
                <div class="col-md-12 boxed mb20">
                    <div class="row mb10">
                        <div class="col-sm-8">
                            <h1 class="mb0">
                                <?php echo $room['title']; ?>
                                <br><small><?php echo $room['subtitle']; ?></small>
                            </h1>
                            <?php
                            $result_rating = $db->query('SELECT count(*) as count_rating, AVG(rating) as avg_rating FROM pm_comment WHERE item_type = \'room\' AND id_item = '.$room_id.' AND checked = 1 AND rating > 0 AND rating <= 5');
                            if($result_rating !== false && $db->last_row_count() > 0){
                                $row = $result_rating->fetch();
                                $room_rating = $row['avg_rating'];
                                $count_rating = $row['count_rating'];
                                
                                if($room_rating > 0 && $room_rating <= 5){ ?>
                                
                                    <input type="hidden" class="rating pull-left" value="<?php echo $room_rating; ?>" data-rtl="<?php echo (RTL_DIR) ? true : false; ?>" data-size="xs" readonly="true" data-default-caption="<?php echo $count_rating.' '.$texts['RATINGS']; ?>" data-show-caption="true">
                                    <?php
                                }
                            } ?>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-sm-4 text-right">
                            <div class="price text-primary">
                                <?php
                                $min_price = $room['price'];
                                $result_rate = $db->query('SELECT MIN(price) as price FROM pm_rate WHERE id_room = '.$room_id);
                                if($result_rate !== false && $db->last_row_count() > 0){
                                    $row = $result_rate->fetch();
                                    $price = $row['price'];
                                    if($price > 0) $min_price = $price;
                                }
                                if($min_price > 0){
                                    echo $texts['FROM_PRICE']; ?>
                                    <span itemprop="priceRange">
                                        <?php echo formatPrice($min_price*CURRENCY_RATE); ?>
                                    </span>
                                    / <?php echo $texts['NIGHT'];
                                } ?>
                            </div>
                            <p>
                                <?php echo $texts['CAPACITY']; ?> : <i class="fa fa-male"></i>x<?php echo $room['max_people']; ?>
                            </p>
                        </div>
                    </div>
                    <div class="row mb10">
                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="pull-left">
                                <?php
                                $result_facility = $db->query('SELECT * FROM pm_facility WHERE lang = '.LANG_ID.' AND id IN('.$room['facilities'].') ORDER BY id',PDO::FETCH_ASSOC);
                                if($result_facility !== false && $db->last_row_count() > 0){
                                foreach($result_facility as $i => $row){
                                    $facility_id    = $row['id'];
                                    $facility_name  = $row['name'];
                                    
                                    $result_facility_file = $db->query('SELECT * FROM pm_facility_file WHERE id_item = '.$facility_id.' AND checked = 1 AND lang = '.DEFAULT_LANG.' AND type = \'image\' AND file != \'\' ORDER BY rank LIMIT 1',PDO::FETCH_ASSOC);
                                    if($result_facility_file !== false && $db->last_row_count() > 0){
                                        $row = $result_facility_file->fetch();
                                        
                                        $file_id    = $row['id'];
                                        $filename   = $row['file'];
                                        $label      = $row['label'];
                                        
                                        $realpath   = SYSBASE.'medias/facility/big/'.$file_id.'/'.$filename;
                                        $thumbpath  = DOCBASE.'medias/facility/big/'.$file_id.'/'.$filename;
                                            
                                        if(is_file($realpath)){ ?>
                                            <span class="facility-icon">
                                                <img alt="<?php echo $facility_name; ?>" title="<?php echo $facility_name; ?>" src="<?php echo $thumbpath; ?>" class="tips">
                                            </span>
                                            <?php
                                        }
                                    }
                                }
                              } ?>
                            </div>
                            </div>
                            <div class="pull-right">
                                <div class="col-sm-12">
                                    <div class="listing-bookings">
                                        <div class="col-sm-12">
                                            <ul class="booking-model">
                                            <div class="col-sm-4">
                                            <li><button type="button" class="btn btn-primary" id="services room_services"  data-toggle="modal" data-target="#exampleModal">
                                                <i class="fa fa-plus-circle"></i>Services
                                            </button></li>
                                            </div>
                                            <div class="col-sm-4">
                                            <li><button type="button" class="btn btn-primary" id="activities room_activites" data-toggle="modal" data-target="#exampleModal1">
                                                <i class="fa fa-plus-circle"></i>Activities
                                            </button></li>
                                            </div>
                                             <div class="col-sm-4">
                                            <li><button type="button" class="btn btn-primary" id="customer-info room_customer" data-toggle="modal" data-target="#exampleModal2">
                                                <i class="fa fa-plus-circle"></i>Customer Info
                                            </button></li>
                                            </div>
                                        </ul>
                                        </div>
                                        
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Services</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                                <div class="modal-body row_container">
                                                    <div class="col-md-5">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon"><i class="fa fa-servicestack"></i> <?php echo 'Category'; ?></div>
                                                                        <select name="num_adults" class="selectpicker form-control">
                                                                            <?php
                                                                            for($i = 1; $i <= $max_adults_search; $i++){
                                                                                $select = ($_SESSION['num_adults'] == $i) ? ' selected="selected"' : '';
                                                                                echo '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
                                                                            } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon"><i class="fa fa-servicestack"></i> <?php echo 'Title'; ?></div>
                                                                        <select name="num_adults" class="selectpicker form-control">
                                                                            <?php
                                                                            for($i = 1; $i <= $max_adults_search; $i++){
                                                                                $select = ($_SESSION['num_adults'] == $i) ? ' selected="selected"' : '';
                                                                                echo '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
                                                                            } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon searchs"><i class="fa fa-search"></i></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                       <div class="popup-dets">
                                                            <div class="price-det">
                                                                <h5>R-301</h5>
                                                                <label>Price/Night: 100 $</label>
                                                                <span></span>
                                                                
                                                            </div>
                                                            <h3><?php echo $room_title; ?></h3>
                                                            <h4><?php echo $room_subtitle; ?></h4>
                                                            <p><?php echo strtrunc(strip_tags($room_descr), 220); ?>
                                                                <a  href="" class="btn btn-primary" id="booking_detls" title="Services">
                                                                    <i class="fa fa-plus-circle"></i>
                                                                    <?php echo 'Services'; ?>
                                                                </a>
                                                                <a  id="booking_detls1" title="Read More" href="<?php echo DOCBASE.$pages[9]['alias'].'/'.text_format($room_alias); ?>">
                                                                    <i class="fa fa-plus-circle"></i>
                                                                    <?php echo $texts['READMORE']; ?>
                                                                </a>
                                                            </p>
                                                       </div>
                                                    </div>

                                                    <div class="pull-right">
                                                        <button type="button" id="mine">-</button>
                                                        <button type="button" id="plus">+</button>
                                                    </div>

                                                    <div class="col-md-7">
                                                        
                                                        <div class="col-sm-1">
                                                           ID
                                                        </div>
                                                        <div class="col-sm-2">
                                                           Service Title
                                                        </div>
                                                        <div class="col-sm-2">
                                                           Price Unit
                                                        </div>
                                                        <div class="col-sm-2">
                                                           Unit Price
                                                        </div>
                                                        <div class="col-sm-2">
                                                           Quantity
                                                        </div>
                                                        <div class="col-sm-2">
                                                           Amount
                                                        </div>
                                                         <div class="col-sm-1">
                                                           Actions
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="templates">
                                                    <div class="col-md-7 row_clone">
                                                        <div class="col-sm-1">
                                                           1
                                                         </div>
                                                        <div class="col-sm-2">
                                                          <input type="text" size="8" />
                                                        </div>
                                                        <div class="col-sm-2">
                                                          <input type="text" size="8" />
                                                        </div>
                                                        <div class="col-sm-2">
                                                           <input type="text" size="8" />
                                                        </div>
                                                        <div class="col-sm-2">
                                                           <input type="text" size="8" />
                                                        </div>
                                                        <div class="col-sm-2">
                                                           <input type="text" size="8" />
                                                        </div>
                                                         <div class="col-sm-1">
                                                            
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary save-models">Save changes</button>
                                                </div>

                                            </div>
                                          </div>
                                        </div>

                                        <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Activities</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                                <div class="modal-body row_container">
                                                    <div class="col-md-5">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon"><i class="fa fa-servicestack"></i> <?php echo 'Category'; ?></div>
                                                                        <select name="num_adults" class="selectpicker form-control">
                                                                            <?php
                                                                            for($i = 1; $i <= $max_adults_search; $i++){
                                                                                $select = ($_SESSION['num_adults'] == $i) ? ' selected="selected"' : '';
                                                                                echo '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
                                                                            } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon"><i class="fa fa-servicestack"></i> <?php echo 'Title'; ?></div>
                                                                        <select name="num_adults" class="selectpicker form-control">
                                                                            <?php
                                                                            for($i = 1; $i <= $max_adults_search; $i++){
                                                                                $select = ($_SESSION['num_adults'] == $i) ? ' selected="selected"' : '';
                                                                                echo '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
                                                                            } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon searchs"><i class="fa fa-search"></i></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                       <div class="popup-dets">
                                                            <div class="price-det">
                                                                <h5>R-301</h5>
                                                                <label>Price/Night: 100 $</label>
                                                                <span></span>
                                                            </div>
                                                            <h3><?php echo $room_title; ?></h3>
                                                            <h4><?php echo $room_subtitle; ?></h4>
                                                            <p>Test For Picture</p>
                                                            <p><?php echo strtrunc(strip_tags($room_descr), 220); ?>
                                                                <a  href="" class="btn btn-primary" id="booking_detls" title="Services">
                                                                    <i class="fa fa-plus-circle"></i>
                                                                    <?php echo 'Services'; ?>
                                                                </a>
                                                                <a  id="booking_detls1" title="Read More" href="<?php echo DOCBASE.$pages[9]['alias'].'/'.text_format($room_alias); ?>">
                                                                    <i class="fa fa-plus-circle"></i>
                                                                    <?php echo $texts['READMORE']; ?>
                                                                </a>
                                                            </p>
                                                       </div>
                                                    </div>

                                                    <div class="pull-right">
                                                        <button type="button" id="mine">-</button>
                                                        <button type="button" id="plus">+</button>
                                                    </div>

                                                    <div class="col-md-7">
                                                        
                                                        <div class="col-sm-1">
                                                           ID
                                                        </div>
                                                        <div class="col-sm-2">
                                                           Service Title
                                                        </div>
                                                        <div class="col-sm-2">
                                                           Price Unit
                                                        </div>
                                                        <div class="col-sm-2">
                                                           Unit Price
                                                        </div>
                                                        <div class="col-sm-2">
                                                           Quantity
                                                        </div>
                                                        <div class="col-sm-2">
                                                           Amount
                                                        </div>
                                                         <div class="col-sm-1">
                                                           Actions
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="templates">
                                                    <div class="col-md-7 row_clone">
                                                        <div class="col-sm-1">
                                                           1
                                                         </div>
                                                        <div class="col-sm-2">
                                                          <input type="text" size="8" />
                                                        </div>
                                                        <div class="col-sm-2">
                                                          <input type="text" size="8" />
                                                        </div>
                                                        <div class="col-sm-2">
                                                           <input type="text" size="8" />
                                                        </div>
                                                        <div class="col-sm-2">
                                                           <input type="text" size="8" />
                                                        </div>
                                                        <div class="col-sm-2">
                                                           <input type="text" size="8" />
                                                        </div>
                                                         <div class="col-sm-1">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary save-models">Save changes</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Customer Info</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                                <div class="modal-body customer-dets">
                                                   <form method="POST" action="">
                                                       <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <label>Customer * </label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <input class="form-control" type="text" name="customer" value="" placeholder="" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <label>Firstname * </label>
                                                                 </div>
                                                                <div class="col-sm-8">
                                                                    <input class="form-control" type="text" name="fname" value="" placeholder="" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <label>Lastname * </label>
                                                                 </div>
                                                                <div class="col-sm-8">
                                                                    <input class="form-control" type="text" name="lastname" value="" placeholder="" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <label>Email * </label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <input class="form-control" type="email" name="email" value="" placeholder="" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <label>Mobile * </label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <input class="form-control" type="text" name="mobile" value="" placeholder="" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <label>CNIC/EID * </label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <input class="form-control" type="text" name="cnic" value="" placeholder="" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <label>Passport No * </label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <input class="form-control" type="text" name="passport" value="" placeholder="" />
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <label>City * </label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <input class="form-control" type="text" name="city" value="" placeholder="" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <label>Postcode * </label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <input class="form-control" type="text" name="postcode" value="" placeholder="" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <label>Address * </label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <input class="form-control" type="text" name="address" value="" placeholder="" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <label>Country * </label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <input class="form-control" type="text" name="country" value="" placeholder="" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <label>Notes * </label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <textarea cols="4" rows="6" class="form-control" name="notes" value=""></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   </form>
                                                </div>
                                                   
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="button" name="customer-save" class="btn btn-info save-models">Save changes</button>
                                                </div>
                                                
                                            </div>
                                          </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb10">
                        <div class="col-md-12">
                            <div class="owl-carousel owlWrapper" data-items="1" data-autoplay="true" data-dots="true" data-nav="false" data-rtl="<?php echo (RTL_DIR) ? 'true' : 'false'; ?>">
                                <?php
                                $result_room_file = $db->query('SELECT * FROM pm_room_file WHERE id_item = '.$room_id.' AND checked = 1 AND lang = '.DEFAULT_LANG.' AND type = \'image\' AND file != \'\' ORDER BY rank');
                                if($result_room_file !== false){
                                    
                                    foreach($result_room_file as $i => $row){
                                    
                                        $file_id = $row['id'];
                                        $filename = $row['file'];
                                        $label = $row['label'];
                                        
                                        $realpath = SYSBASE.'medias/room/big/'.$file_id.'/'.$filename;
                                        $thumbpath = DOCBASE.'medias/room/big/'.$file_id.'/'.$filename;
                                        
                                        if(is_file($realpath)){ ?>
                                            <img alt="<?php echo $label; ?>" src="<?php echo $thumbpath; ?>" class="img-responsive" style="max-height:600px;"/>
                                            <?php
                                        }
                                    }
                                } ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" itemprop="description">
                            <?php
                            echo $room['descr'];
                            
                            $short_text = strtrunc(strip_tags($room['descr']), 100);
                            $site_url = getUrl(); ?>
                        </div>

                    </div>
                </div>
               
                <div class="col-md-8">
                    <?php
                    $nb_comments = 0;
                    $item_type = 'room';
                    $item_id = $room_id;
                    $allow_comment = ALLOW_COMMENTS;
                    $allow_rating = ALLOW_RATINGS;
                    if($allow_comment == 1){
                        $result_comment = $db->query('SELECT * FROM pm_comment WHERE id_item = '.$item_id.' AND item_type = '.$db->quote($item_type).' AND checked = 1 ORDER BY add_date DESC');
                        if($result_comment !== false)
                            $nb_comments = $db->last_row_count();
                    }
                    //include(getFromTemplate('common/comments.php', false)); ?>
                </div>
            </div>
        </div>
    </div>
</article>

<script type="text/javascript">
    $(document).ready(function () {
        
        $('#mine').on('click', function() {
           //  alert ("So ja bache na to TB a jai ga");
            $( ".row_container").find(".row_clone:last-child" ).remove();
           console.log ($( ".row_container").find(".row_clone:last-child" ));
        });

        $('#plus').on('click', function() {
            
            var row= $('.templates').find(".row_clone")[0].outerHTML;
          
            $(".row_container").append(row);

            // $('#row_clone')
            // .clone()
            // .appendTo('#row_container');
        });  
 

    });
</script>