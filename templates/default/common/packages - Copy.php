<script type="text/javascript">
    $(document).ready(function () {
    $('.btn-vertical-slider').on('click', function () {
        if ($(this).attr('data-slide') == 'next') {
            $('#myCarousel').carousel('next');
        }
        if ($(this).attr('data-slide') == 'prev') {
            $('#myCarousel').carousel('prev')
        }
    });
});
</script>
<?php debug_backtrace() || die ('Direct access not permitted'); ?>
<h4><?php echo 'Our Packages'; ?></h4>
<span class="heading-border"></span>
<form action="#" method="post" class="booking-search" id="booking-forms">
    <div class="left">
        <div id="myCarousel" class="vertical-slider carousel vertical slide col-md-12" data-ride="carousel">
            <div class="row">
                <div class="col-md-12">
                    <span data-slide="next" class="btn-vertical-slider glyphicon glyphicon-circle-arrow-up "
                        style="font-size: 30px"></span>  
                </div>
            </div>
            <br />
            <!-- Carousel items -->
            <div class="carousel-inner">
                <div class="item active">
                    <div class="row">
                        <div class="col-md-12">
                          <div class="ribbon">
                              <h6 class="packages_name">Package Details</h6>
                              <a id="customer-info view_detail"data-toggle="modal" data-target="#exampleModal2"><i class="fa fa-shopping-cart"></i>Buy</a>
                          </div>

                          <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Customer Info</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                  <div class="modal-body customer-dets">
                                     <form method="POST" action="" class="form-control">
                                         <div class="col-md-6">
                                              <div class="row">
                                                  <div class="col-sm-4">
                                                      <label>Customer * </label>
                                                  </div>
                                                  <div class="col-sm-8">
                                                      <input class="form-control" type="text" name="customer" value="" placeholder="" />
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-sm-4">
                                                      <label>Firstname * </label>
                                                   </div>
                                                  <div class="col-sm-8">
                                                      <input class="form-control" type="text" name="fname" value="" placeholder="" />
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-sm-4">
                                                      <label>Lastname * </label>
                                                   </div>
                                                  <div class="col-sm-8">
                                                      <input class="form-control" type="text" name="lastname" value="" placeholder="" />
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-sm-4">
                                                      <label>Email * </label>
                                                  </div>
                                                  <div class="col-sm-8">
                                                      <input class="form-control" type="email" name="email" value="" placeholder="" />
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-sm-4">
                                                      <label>Mobile * </label>
                                                  </div>
                                                  <div class="col-sm-8">
                                                      <input class="form-control" type="text" name="mobile" value="" placeholder="" />
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-sm-4">
                                                      <label>CNIC/EID * </label>
                                                  </div>
                                                  <div class="col-sm-8">
                                                      <input class="form-control" type="text" name="cnic" value="" placeholder="" />
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-sm-4">
                                                      <label>Passport No * </label>
                                                  </div>
                                                  <div class="col-sm-8">
                                                      <input class="form-control" type="text" name="passport" value="" placeholder="" />
                                                  </div>
                                              </div>
                                              
                                          </div>
                                          <div class="col-md-6">
                                              <div class="row">
                                                  <div class="col-sm-4">
                                                      <label>City * </label>
                                                  </div>
                                                  <div class="col-sm-8">
                                                      <input class="form-control" type="text" name="city" value="" placeholder="" />
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-sm-4">
                                                      <label>Postcode * </label>
                                                  </div>
                                                  <div class="col-sm-8">
                                                      <input class="form-control" type="text" name="postcode" value="" placeholder="" />
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-sm-4">
                                                      <label>Address * </label>
                                                  </div>
                                                  <div class="col-sm-8">
                                                      <input class="form-control" type="text" name="address" value="" placeholder="" />
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-sm-4">
                                                      <label>Country * </label>
                                                  </div>
                                                  <div class="col-sm-8">
                                                      <input class="form-control" type="text" name="country" value="" placeholder="" />
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-sm-4">
                                                      <label>Notes * </label>
                                                  </div>
                                                  <div class="col-sm-8">
                                                      <textarea cols="4" rows="6" class="form-control" name="notes" value=""></textarea>
                                                  </div>
                                              </div>
                                          </div>
                                     </form>
                                  </div>
                                     
                                  
                                  <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="button" name="customer-save" class="btn btn-info save-models">Save changes</button>
                                  </div>
                                  
                              </div>
                            </div>
                          </div>

                            <div class="col-md-3">
                                <h5>2 Night Package</h5>
                                   <ul>
                                       <li>Price per night: 2500Rs</li>
                                       <li>Price per night: 2500Rs</li>
                                       <li>Price per night: 2500Rs</li>
                                       <li>Price per night: 2500Rs</li>
                                   </ul>  
                            </div>
                             <div class="col-md-3">
                                <h5>Facilities</h5>
                                   <ul>
                                       <li>Bathroom with shower and hairdryer</li>
                                       <li>Sat television</li>
                                       <li>Healthy mattress (magniflex)</li>
                                       <li>Direct dial phone</li>
                                   </ul>
                            </div>
                            <div class="col-md-3">
                                <h5>Services</h5>
                                    <div class="detailListItem">
                                      <span class="fa fa-wifi"></span><p>Free Wifi</p>
                                    </div>
                                    <div class="detailListItem">
                                      <span class="fa fa-coffee"></span><p>Breakfast included</p>
                                    </div>
                                     <div class="detailListItem">
                                      <span class="fa fa-wheelchair"></span><p>Wheelchair access</p>
                                    </div>
                                     <div class="detailListItem">
                                      <span class="fa fa-plane"></span><p>Airport Transportation</p>
                                    </div>
                            </div>
                            <div class="col-md-3">
                                <h5>Activities</h5>
                                   <ul>
                                       <li>Holiday Games</li>
                                       <li>Water Sports</li>
                                       <li>Splash Games</li>
                                       <li>Special Remarc Events</li>
                                   </ul>
                            </div>
                            <a href="#" class="btn btn-info fa-check-circle" id="view_details">View Details</a>
                            <!-- <div class="form-group">
                                <label class="sr-only" for="from"></label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-bed"></i> <?php echo 'Room'; ?></div>
                                    <select name="num_adults" class="selectpicker form-control">
                                        <?php
                                        for($i = 1; $i <= $max_adults_search; $i++){
                                            $select = ($_SESSION['num_adults'] == $i) ? ' selected="selected"' : '';
                                            echo '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
                                        } ?>
                                    </select>
                                </div>
                                <div class="field-notice" rel="from_date"></div>
                            </div> -->
                        </div>
                    </div>
                    <!--/row-fluid-->
                </div>
                <!--/item-->
                <div class="item">
                    <div class="row">
                        <div class="col-md-12">
                          <div class="ribbon">
                              <h6 class="packages_name">Package Details</h6>
                              <a href="#" id="view_detail"><i class="fa fa-shopping-cart"></i>Buy</a>
                          </div>
                            <div class="col-md-3">
                                <h5>5 Night Package</h5>
                                   <ul>
                                       <li>Price per night: 2500Rs</li>
                                       <li>Price per night: 2500Rs</li>
                                       <li>Price per night: 2500Rs</li>
                                       <li>Price per night: 2500Rs</li>
                                   </ul>  
                            </div>
                             <div class="col-md-3">
                                <h5>Facilities</h5>
                                   <ul>
                                       <li>Bathroom with shower and hairdryer</li>
                                       <li>Sat television</li>
                                       <li>Healthy mattress (magniflex)</li>
                                       <li>Direct dial phone</li>
                                   </ul>
                            </div>
                            <div class="col-md-3">
                                <h5>Services</h5>
                                    <div class="detailListItem">
                                      <span class="fa fa-wifi"></span><p>Free Wifi</p>
                                    </div>
                                    <div class="detailListItem">
                                      <span class="fa fa-coffee"></span><p>Breakfast included</p>
                                    </div>
                                     <div class="detailListItem">
                                      <span class="fa fa-wheelchair"></span><p>Wheelchair access</p>
                                    </div>
                                     <div class="detailListItem">
                                      <span class="fa fa-plane"></span><p>Airport Transportation</p>
                                    </div>
                            </div>
                            <div class="col-md-3">
                                <h5>Activities</h5>
                                   <ul>
                                       <li>Holiday Games</li>
                                       <li>Water Sports</li>
                                       <li>Splash Games</li>
                                       <li>Special Remarc Events</li>
                                   </ul>
                            </div>
                            <a href="#" class="btn btn-info fa-check-circle" id="view_details">View Details</a>
                            <!-- <div class="form-group">
                                <label class="sr-only" for="from"></label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-bed"></i> <?php echo 'Room'; ?></div>
                                    <select name="num_adults" class="selectpicker form-control">
                                        <?php
                                        for($i = 1; $i <= $max_adults_search; $i++){
                                            $select = ($_SESSION['num_adults'] == $i) ? ' selected="selected"' : '';
                                            echo '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
                                        } ?>
                                    </select>
                                </div>
                                <div class="field-notice" rel="from_date"></div>
                            </div> -->
                        </div>
                    </div>
                    <!--/row-fluid-->
                </div>
                <!--/item-->
                <div class="item">
                    <div class="row">
                        <div class="col-md-12">
                          <div class="ribbon">
                              <h6 class="packages_name">Package Details</h6>
                              <a href="#" id="view_detail"><i class="fa fa-shopping-cart"></i>Buy</a>
                          </div>
                            <div class="col-md-3">
                                <h5>7 Night Package</h5>
                                   <ul>
                                       <li>Price per night: 2500Rs</li>
                                       <li>Price per night: 2500Rs</li>
                                       <li>Price per night: 2500Rs</li>
                                       <li>Price per night: 2500Rs</li>
                                   </ul>  
                            </div>
                             <div class="col-md-3">
                                <h5>Facilities</h5>
                                   <ul>
                                       <li>Bathroom with shower and hairdryer</li>
                                       <li>Sat television</li>
                                       <li>Healthy mattress (magniflex)</li>
                                       <li>Direct dial phone</li>
                                   </ul>
                            </div>
                            <div class="col-md-3">
                                <h5>Services</h5>
                                    <div class="detailListItem">
                                      <span class="fa fa-wifi"></span><p>Free Wifi</p>
                                    </div>
                                    <div class="detailListItem">
                                      <span class="fa fa-coffee"></span><p>Breakfast included</p>
                                    </div>
                                     <div class="detailListItem">
                                      <span class="fa fa-wheelchair"></span><p>Wheelchair access</p>
                                    </div>
                                     <div class="detailListItem">
                                      <span class="fa fa-plane"></span><p>Airport Transportation</p>
                                    </div>
                            </div>
                            <div class="col-md-3">
                                <h5>Activities</h5>
                                   <ul>
                                       <li>Holiday Games</li>
                                       <li>Water Sports</li>
                                       <li>Splash Games</li>
                                       <li>Special Remarc Events</li>
                                   </ul>
                            </div>
                            <a href="#" class="btn btn-info fa-check-circle" id="view_details">View Details</a>
                            <!-- <div class="form-group">
                                <label class="sr-only" for="from"></label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-bed"></i> <?php echo 'Room'; ?></div>
                                    <select name="num_adults" class="selectpicker form-control">
                                        <?php
                                        for($i = 1; $i <= $max_adults_search; $i++){
                                            $select = ($_SESSION['num_adults'] == $i) ? ' selected="selected"' : '';
                                            echo '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
                                        } ?>
                                    </select>
                                </div>
                                <div class="field-notice" rel="from_date"></div>
                            </div> -->
                        </div>
                    </div>
                    <!--/row-fluid-->
                </div>
                <!--/item-->
            </div>
                <!--/item-->
            <div class="row">
                <div class="col-md-12">
                    <span data-slide="prev" class="btn-vertical-slider glyphicon glyphicon-circle-arrow-down"
                        style="color: Black; font-size: 30px"></span>
                </div>
            </div>
        </div>
       
        <!-- <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <button class="btn btn-block btn-primary" type="submit" name="check_packages"><i class="fa fa-search"></i> <?php echo $texts['CHECK']; ?></button>
            </div>
        </div> -->
    </div>
</form>

