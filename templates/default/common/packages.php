<script type="text/javascript">
    $(document).ready(function () {
    $('.btn-vertical-slider').on('click', function () {
        if ($(this).attr('data-slide') == 'next') {
            $('#myCarousel').carousel('next');
        }
        if ($(this).attr('data-slide') == 'prev') {
            $('#myCarousel').carousel('prev')
        }
    });
});
</script>
<?php debug_backtrace() || die ('Direct access not permitted'); ?>
<h4><?php echo 'Our Packages'; ?></h4>
<span class="heading-border"></span>
<form action="#" method="post" class="booking-search" id="booking-forms">
    <div class="left">
        <div id="myCarousel" class="vertical-slider carousel vertical slide col-md-12" data-ride="carousel">
            <div class="row">
                <div class="col-md-12">
                    <span data-slide="next" class="btn-vertical-slider glyphicon glyphicon-circle-arrow-up "
                        style="font-size: 30px"></span>  
                </div>
            </div>
            <br />
            <!-- Carousel items -->
            <div class="carousel-inner">
                <div class="item active">
                    <div class="row">
                        <div class="col-md-12">
                          <div class="ribbon">
                              <div class="pull-left">
                                <h6 class="packages_name">Package Details</h6>
                              </div>
                                <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalc">
                                  <i class="fa fa-shopping-cart"></i>Buy
                                </button> -->
                                <div class="pull-right">
                                  <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal">
                                    <i class="fa fa-shopping-cart"></i>Buy
                                  </button>
                                </div>

                              <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header buy-heading">
                                        <h5 class="modal-title" id="exampleModalLabel">Customer Info</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                         <form method="POST" action="">
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                  <div class="row">
                                                      <div class="col-sm-4">
                                                          <label>Customer<span style="color:red;">*</span></label>
                                                      </div>
                                                      <div class="col-sm-8">
                                                          <input class="form-control" type="text" name="customer" value="" placeholder="" />
                                                      </div>
                                                  </div>
                                                  <div class="row">
                                                      <div class="col-sm-4">
                                                          <label>Firstname <span style="color:red;">*</span> </label>
                                                       </div>
                                                      <div class="col-sm-8">
                                                          <input class="form-control" type="text" name="fname" value="" placeholder="" />
                                                      </div>
                                                  </div>
                                                  <div class="row">
                                                      <div class="col-sm-4">
                                                          <label>Lastname <span style="color:red;">*</span> </label>
                                                       </div>
                                                      <div class="col-sm-8">
                                                          <input class="form-control" type="text" name="lastname" value="" placeholder="" />
                                                      </div>
                                                  </div>
                                                  <div class="row">
                                                      <div class="col-sm-4">
                                                          <label>Email <span style="color:red;">*</span> </label>
                                                      </div>
                                                      <div class="col-sm-8">
                                                          <input class="form-control" type="email" name="email" value="" placeholder="" />
                                                      </div>
                                                  </div>
                                                  <div class="row">
                                                      <div class="col-sm-4">
                                                          <label>Company <span style="color:red;">*</span> </label>
                                                      </div>
                                                      <div class="col-sm-8">
                                                          <input class="form-control" type="text" name="company" value="" placeholder="" />
                                                      </div>
                                                  </div>
                                                  <div class="row">
                                                      <div class="col-sm-4">
                                                          <label>Mobile <span style="color:red;">*</span> </label>
                                                      </div>
                                                      <div class="col-sm-8">
                                                          <input class="form-control" type="text" name="mobile" value="" placeholder="" />
                                                      </div>
                                                  </div>
                                                  <div class="row">
                                                      <div class="col-sm-4">
                                                          <label>CNIC/EID <span style="color:red;">*</span> </label>
                                                      </div>
                                                      <div class="col-sm-8">
                                                          <input class="form-control" type="text" name="cnic" value="" placeholder="" />
                                                      </div>
                                                  </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label>Passport No <span style="color:red;">*</span> </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" type="text" name="passport" value="" placeholder="" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label>City <span style="color:red;">*</span></label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" type="text" name="city" value="" placeholder="" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label>Postcode <span style="color:red;">*</span> </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" type="text" name="postcode" value="" placeholder="" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label>Address <span style="color:red;">*</span> </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" type="text" name="address" value="" placeholder="" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label>Country <span style="color:red;">*</span> </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" type="text" name="country" value="" placeholder="" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label>Notes <span style="color:red;">*</span> </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <textarea cols="4" rows="6" class="form-control" name="notes" value=""></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                              
                                            </div>

                                          <div class="modal-header buy-heading">
                                            <h5 class="modal-title" id="exampleModalLabel">Payment Info</h5>
                                          </div>
                                            <div class="col-md-12">
                                              <div class="col-md-6">
                                                 <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Firstname</label>
                                                     </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" type="text" name="fname" value="" placeholder="" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Credit Card Number</label>
                                                     </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" type="text" name="creditnumber" value="" placeholder="" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Expiration</label>
                                                     </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" type="text" name="expiration" value="" placeholder="" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>City</label>
                                                     </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" type="text" name="city" value="" placeholder="" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Postal/Zip Code</label>
                                                     </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" type="text" name="zip" value="" placeholder="" />
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="col-md-6">
                                                <div class="row">
                                                     <div class="col-sm-4">
                                                        <label>Lastname</label>
                                                     </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" type="text" name="lastname" value="" placeholder="" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>CVV</label>
                                                     </div>
                                                    <div class="col-sm-4">
                                                        <input class="form-control" type="text" name="CVV" value="" placeholder="" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Billing Address<span style="color:red;">*</span> </label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <textarea cols="4" rows="6" class="form-control" name="billingaddress" value=""></textarea>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>State</label>
                                                     </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" type="text" name="region" value="" placeholder="" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Country</label>
                                                     </div>
                                                    <div class="col-sm-8">
                                                        <select class="form-control" name="country">
                                                            <option>--Choose--</option>
                                                            <option>Pakistan</option>
                                                            <option>USA</option>
                                                            <option>UK</option>
                                                        </select>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>

                                         </form>
                                      </div>
                                      <div class="modal-footer">
                                          <button type="" value="" name="buy" class="btn btn-info">Save</button>
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      </div>
                                      <!-- <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                      </div> -->
                                    </div>
                                  </div>
                                </div>
                          </div>

                            <div class="col-md-3">
                                <h5>2 Night Package</h5>
                                 <ul>
                                     <li>Price per night: 2500Rs</li>
                                     <li>Price per night: 2500Rs</li>
                                     <li>Price per night: 2500Rs</li>
                                     <li>Price per night: 2500Rs</li>
                                 </ul>  
                            </div>
                            <div class="col-md-3">
                                <h5>Facilities</h5>
                                   <ul>
                                       <li>Bathroom with shower and hairdryer</li>
                                       <li>Sat television</li>
                                       <li>Healthy mattress (magniflex)</li>
                                       <li>Direct dial phone</li>
                                   </ul>
                            </div>
                            <div class="col-md-3">
                                <h5>Services</h5>
                                    <div class="detailListItem">
                                      <span class="fa fa-wifi"></span><p>Free Wifi</p>
                                    </div>
                                    <div class="detailListItem">
                                      <span class="fa fa-coffee"></span><p>Breakfast included</p>
                                    </div>
                                     <div class="detailListItem">
                                      <span class="fa fa-wheelchair"></span><p>Wheelchair access</p>
                                    </div>
                                     <div class="detailListItem">
                                      <span class="fa fa-plane"></span><p>Airport Transportation</p>
                                    </div>
                            </div>
                            <div class="col-md-3">
                                <h5>Activities</h5>
                                   <ul>
                                       <li>Holiday Games</li>
                                       <li>Water Sports</li>
                                       <li>Splash Games</li>
                                       <li>Special Remarc Events</li>
                                   </ul>
                            </div>
                            <a href="#" class="btn btn-info fa-check-circle" id="view_details">View Details</a>
                            
                        </div>
                    </div>
                    <!--/row-fluid-->
                </div>
                <!--/item-->
                <div class="item">
                    <div class="row">
                        <div class="col-md-12">
                          <div class="ribbon">
                              <div class="pull-left">
                                <h6 class="packages_name">Package Details</h6>
                              </div>
                                <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalc">
                                  <i class="fa fa-shopping-cart"></i>Buy
                                </button> -->
                                <div class="pull-right">
                                  <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal">
                                    <i class="fa fa-shopping-cart"></i>Buy
                                  </button>
                                </div>

                              <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header buy-heading">
                                        <h5 class="modal-title" id="exampleModalLabel">Customer Info</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                         <form method="POST" action="">
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                  <div class="row">
                                                      <div class="col-sm-4">
                                                          <label>Customer<span style="color:red;">*</span></label>
                                                      </div>
                                                      <div class="col-sm-8">
                                                          <input class="form-control" type="text" name="customer" value="" placeholder="" />
                                                      </div>
                                                  </div>
                                                  <div class="row">
                                                      <div class="col-sm-4">
                                                          <label>Firstname <span style="color:red;">*</span> </label>
                                                       </div>
                                                      <div class="col-sm-8">
                                                          <input class="form-control" type="text" name="fname" value="" placeholder="" />
                                                      </div>
                                                  </div>
                                                  <div class="row">
                                                      <div class="col-sm-4">
                                                          <label>Lastname <span style="color:red;">*</span> </label>
                                                       </div>
                                                      <div class="col-sm-8">
                                                          <input class="form-control" type="text" name="lastname" value="" placeholder="" />
                                                      </div>
                                                  </div>
                                                  <div class="row">
                                                      <div class="col-sm-4">
                                                          <label>Email <span style="color:red;">*</span> </label>
                                                      </div>
                                                      <div class="col-sm-8">
                                                          <input class="form-control" type="email" name="email" value="" placeholder="" />
                                                      </div>
                                                  </div>
                                                  <div class="row">
                                                      <div class="col-sm-4">
                                                          <label>Company <span style="color:red;">*</span> </label>
                                                      </div>
                                                      <div class="col-sm-8">
                                                          <input class="form-control" type="text" name="company" value="" placeholder="" />
                                                      </div>
                                                  </div>
                                                  <div class="row">
                                                      <div class="col-sm-4">
                                                          <label>Mobile <span style="color:red;">*</span> </label>
                                                      </div>
                                                      <div class="col-sm-8">
                                                          <input class="form-control" type="text" name="mobile" value="" placeholder="" />
                                                      </div>
                                                  </div>
                                                  <div class="row">
                                                      <div class="col-sm-4">
                                                          <label>CNIC/EID <span style="color:red;">*</span> </label>
                                                      </div>
                                                      <div class="col-sm-8">
                                                          <input class="form-control" type="text" name="cnic" value="" placeholder="" />
                                                      </div>
                                                  </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label>Passport No <span style="color:red;">*</span> </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" type="text" name="passport" value="" placeholder="" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label>City <span style="color:red;">*</span></label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" type="text" name="city" value="" placeholder="" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label>Postcode <span style="color:red;">*</span> </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" type="text" name="postcode" value="" placeholder="" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label>Address <span style="color:red;">*</span> </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" type="text" name="address" value="" placeholder="" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label>Country <span style="color:red;">*</span> </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" type="text" name="country" value="" placeholder="" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label>Notes <span style="color:red;">*</span> </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <textarea cols="4" rows="6" class="form-control" name="notes" value=""></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                              
                                            </div>

                                          <div class="modal-header buy-heading">
                                            <h5 class="modal-title" id="exampleModalLabel">Payment Info</h5>
                                          </div>
                                            <div class="col-md-12">
                                              <div class="col-md-6">
                                                 <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Firstname</label>
                                                     </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" type="text" name="fname" value="" placeholder="" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Credit Card Number</label>
                                                     </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" type="text" name="creditnumber" value="" placeholder="" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Expiration</label>
                                                     </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" type="text" name="expiration" value="" placeholder="" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>City</label>
                                                     </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" type="text" name="city" value="" placeholder="" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Postal/Zip Code</label>
                                                     </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" type="text" name="zip" value="" placeholder="" />
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="col-md-6">
                                                <div class="row">
                                                     <div class="col-sm-4">
                                                        <label>Lastname</label>
                                                     </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" type="text" name="lastname" value="" placeholder="" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>CVV</label>
                                                     </div>
                                                    <div class="col-sm-4">
                                                        <input class="form-control" type="text" name="CVV" value="" placeholder="" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Billing Address<span style="color:red;">*</span> </label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <textarea cols="4" rows="6" class="form-control" name="billingaddress" value=""></textarea>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>State</label>
                                                     </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" type="text" name="region" value="" placeholder="" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Country</label>
                                                     </div>
                                                    <div class="col-sm-8">
                                                        <select class="form-control" name="country">
                                                            <option>--Choose--</option>
                                                            <option>Pakistan</option>
                                                            <option>USA</option>
                                                            <option>UK</option>
                                                        </select>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>

                                         </form>
                                      </div>
                                      <div class="modal-footer">
                                          <button type="" value="" name="buy" class="btn btn-info">Save</button>
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      </div>
                                      <!-- <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                      </div> -->
                                    </div>
                                  </div>
                                </div>
                          </div>

                            <div class="col-md-3">
                                <h5>2 Night Package</h5>
                                 <ul>
                                     <li>Price per night: 2500Rs</li>
                                     <li>Price per night: 2500Rs</li>
                                     <li>Price per night: 2500Rs</li>
                                     <li>Price per night: 2500Rs</li>
                                 </ul>  
                            </div>
                            <div class="col-md-3">
                                <h5>Facilities</h5>
                                   <ul>
                                       <li>Bathroom with shower and hairdryer</li>
                                       <li>Sat television</li>
                                       <li>Healthy mattress (magniflex)</li>
                                       <li>Direct dial phone</li>
                                   </ul>
                            </div>
                            <div class="col-md-3">
                                <h5>Services</h5>
                                    <div class="detailListItem">
                                      <span class="fa fa-wifi"></span><p>Free Wifi</p>
                                    </div>
                                    <div class="detailListItem">
                                      <span class="fa fa-coffee"></span><p>Breakfast included</p>
                                    </div>
                                     <div class="detailListItem">
                                      <span class="fa fa-wheelchair"></span><p>Wheelchair access</p>
                                    </div>
                                     <div class="detailListItem">
                                      <span class="fa fa-plane"></span><p>Airport Transportation</p>
                                    </div>
                            </div>
                            <div class="col-md-3">
                                <h5>Activities</h5>
                                   <ul>
                                       <li>Holiday Games</li>
                                       <li>Water Sports</li>
                                       <li>Splash Games</li>
                                       <li>Special Remarc Events</li>
                                   </ul>
                            </div>
                            <a href="#" class="btn btn-info fa-check-circle" id="view_details">View Details</a>
                            
                        </div>
                    </div>
                    <!--/row-fluid-->
                </div>
                <!--/item-->
            </div>

                <!--/item-->
            <div class="row">
                <div class="col-md-12">
                    <span data-slide="prev" class="btn-vertical-slider glyphicon glyphicon-circle-arrow-down"
                        style="color: Black; font-size: 30px"></span>
                </div>
            </div>
        </div>
       
        <!-- <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <button class="btn btn-block btn-primary" type="submit" name="check_packages"><i class="fa fa-search"></i> <?php echo $texts['CHECK']; ?></button>
            </div>
        </div> -->
    </div>
</form>
