    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <style type="text/css">
        .container {
            margin-top: 40px;
        }
        .btn-primary {
            width: 100%;
        }
    </style>

    <script type='text/javascript'>
        $( document ).ready(function() {
            $('#datetimepicker1').datetimepicker();
        });
        $( document ).ready(function() {
            $('#datetimepicker1s').datetimepicker();
        });
    </script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/2.14.1/moment.min.js"></script> 
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

<?php
debug_backtrace() || die ('Direct access not permitted');

$max_adults_search = 30;
$max_children_search = 10;

if(!isset($_SESSION['num_adults']))
    $_SESSION['num_adults'] = (isset($_SESSION['book']['adults'])) ? $_SESSION['book']['adults'] : 1;
if(!isset($_SESSION['num_children']))
    $_SESSION['num_children'] = (isset($_SESSION['book']['children'])) ? $_SESSION['book']['children'] : 0;

$from_date = (isset($_SESSION['from_date'])) ? $_SESSION['from_date'] : date('j/m/Y');
$to_date = (isset($_SESSION['to_date'])) ? $_SESSION['to_date'] : date('j/m/Y', time()+86400); ?>
<h4><?php echo 'Check Room Availabilities'; ?></h4>
<form action="<?php echo DOCBASE.$sys_pages['booking']['alias']; ?>" method="post" class="booking-search" id="booking-forms">
    <?php
    if(isset($hotel_id)){ ?>
        <input type="hidden" name="hotel_id" value="<?php echo $hotel_id; ?>">
        <?php
    } ?>
    <div class="right">
        <div class="col-md-10 col-sm-6 col-xs-6">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-bed"></i> <?php echo 'Room Type'; ?></div>
                    <select name="num_children" class="selectpicker form-control">
                        <option>Room</option>
                        <option>Luxury Room</option>
                        <option>Royal Suite</option>
                        <option>Double Bed Room</option>
                        <option>Single Bed With Loan</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-10 col-sm-6 col-xs-12">
            <div class="form-group">
                <label class="sr-only" for="from"></label>
                    <div class='input-group date' id='datetimepicker1'>
                    <span class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                    </span>
                        <input type='text' class="form-control" placeholder="Check In Date & Time" />
                    </div>
                <div class="field-notice" rel="from_date"></div>
            </div>
        </div>
        <div class="col-md-10 col-sm-6 col-xs-12">
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1s'>
                    <span class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                    </span>
                        <input type='text' class="form-control" placeholder="Check Out Date & Time" />
                </div>
                <div class="field-notice" rel="to_date"></div>
            </div>
        </div>
        <div class="col-md-10 col-sm-6 col-xs-6">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-male"></i> <?php echo $texts['ADULTS']; ?></div>
                    <select name="num_adults" class="selectpicker form-control">
                        <?php
                        for($i = 1; $i <= $max_adults_search; $i++){
                            $select = ($_SESSION['num_adults'] == $i) ? ' selected="selected"' : '';
                            echo '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
                        } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-10 col-sm-6 col-xs-6">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-child"></i> <?php echo $texts['CHILDREN']; ?></div>
                    <select name="num_children" class="selectpicker form-control">
                        <?php
                        for($i = 0; $i <= $max_children_search; $i++){
                            $select = ($_SESSION['num_children'] == $i) ? ' selected="selected"' : '';
                            echo '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
                        } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-10 col-sm-6 col-xs-6">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-bed"></i> <?php echo 'Night'; ?></div>
                    <select name="num_children" class="selectpicker form-control">
                        <?php
                        for($i = 0; $i <= $max_children_search; $i++){
                            $select = ($_SESSION['num_children'] == $i) ? ' selected="selected"' : '';
                            echo '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
                        } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
            <div class="form-group">
                <button class="btn btn-block btn-primary" type="submit" name="check_availabilities"><i class="fa fa-search"></i> <?php echo $texts['CHECK']; ?></button>
            </div>
        </div>
    </div>
</form>




