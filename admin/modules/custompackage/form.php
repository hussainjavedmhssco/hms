<Style>
    .meals {
        padding-top: 7px;

    }

    .days[
    width:-42px ;

    ]

</Style>


<?php
/**
 * Template of the module form
 */
debug_backtrace() || die ('Direct access not permitted');

// Item ID
if (isset($_GET['id']) && is_numeric($_GET['id'])) $id = $_GET['id'];
elseif (isset($_POST['id']) && is_numeric($_POST['id'])) $id = $_POST['id'];
else {
    header('Location: index.php?view=list');
    exit();
}

// Item ID to delete
$id_file = (isset($_GET['file']) && is_numeric($_GET['file'])) ? $_GET['file'] : 0;
$id_row = (isset($_GET['row']) && is_numeric($_GET['row'])) ? $_GET['row'] : 0;

// Action to perform
$back = false;
$action = (isset($_GET['action'])) ? htmlentities($_GET['action'], ENT_QUOTES, 'UTF-8') : '';
if (isset($_POST['edit']) || isset($_POST['edit_back'])) {
    $action = 'edit';
    if (isset($_POST['edit_back'])) $back = true;
}
if (isset($_POST['add']) || isset($_POST['add_back'])) {
    $action = 'add';
    $id = 0;
    if (isset($_POST['add_back'])) $back = true;
}
if ($action != '' && defined('DEMO') && DEMO == 1) {
    $action = '';
    $_SESSION['msg_error'][] = 'This action is disabled in the demo mode';
}

// Initializations
$file = array();
$img = array();
$img_label = array();
$file_label = array();
$fields_checked = true;
$total_lang = 1;
$rank = 0;
$old_rank = 0;
$home = 0;
$checked = 0;
$add_date = date('y-m-d');
$edit_date = date('y-m-d');
$publish_date = time();
$unpublish_date = null;


$users = array($_SESSION['user']['id']);
$referer = DIR . 'index.php?view=form';

// Messages
if (NB_FILES > 0) $_SESSION['msg_notice'][] = $texts['EXPECTED_IMAGES_SIZE'] . ' ' . MAX_W_BIG . ' x ' . MAX_H_BIG . 'px<br>';

// Creation of the unique token for uploadifive
if (!isset($_SESSION['uniqid'])) $_SESSION['uniqid'] = uniqid();
if (!isset($_SESSION['timestamp'])) $_SESSION['timestamp'] = time();
if (!isset($_SESSION['token'])) $_SESSION['token'] = md5('sessid_' . $_SESSION['uniqid'] . $_SESSION['timestamp']);

// Getting languages
if (MULTILINGUAL && $db != false) {
    $result_lang = $db->query('SELECT id, title FROM pm_lang WHERE checked = 1 ORDER BY CASE main WHEN 1 THEN 0 ELSE 1 END, rank');
    if ($result_lang !== false) {
        $total_lang = $db->last_row_count();
        $langs = $result_lang->fetchAll(PDO::FETCH_ASSOC);
    }
}

// Last rank selection
if (RANKING && $db != false) {
    $result_rank = $db->query('SELECT rank FROM pm_' . MODULE . ' ORDER BY rank DESC LIMIT 1');
    $rank = ($result_rank !== false && $db->last_row_count() > 0) ? $result_rank->fetchColumn(0) + 1 : 1;
}

// Inclusions
require_once(SYSBASE . ADMIN_FOLDER . '/includes/fn_form.php');
//here fnform giving the fields from XML File
$fields = getFields($db);
if (is_null($fields)) $fields = array();

// Getting datas in the database
if ($db !== false) {
    $result = $db->query('SELECT * FROM pm_' . MODULE . ' WHERE id = ' . $id);

    if ($result !== false) {

        // Datas of the module

        foreach ($result as $row) {

            $id_lang = (MULTILINGUAL) ? $row['lang'] : 0;

            foreach ($fields[MODULE]['fields'] as $fieldName => $field) {
                if ($field->getType() != 'separator') {
                    $field->setValue($row[$fieldName], 0, $id_lang);

                }
            }

            if ($id_lang == DEFAULT_LANG || $id_lang == 0) {
                if (HOME) $home = $row['home'];
                if (VALIDATION) $checked = $row['checked'];
                if (RANKING) $old_rank = $row['rank'];
                if (DATES) $add_date = $row['add_date'];
                if (RELEASE) {
                    $publish_date = $row['publish_date'];
                    $unpublish_date = $row['unpublish_date'];
                }
                if (db_column_exists($db, 'pm_' . MODULE, 'users')) {
                    $users = explode(',', $row['users']);
                    if (!in_array($_SESSION['user']['type'], array('administrator', 'manager', 'editor')) && !in_array($_SESSION['user']['id'], $users)) {
                        header('Location: index.php?view=list');
                        exit();
                    }
                }
            }
        }
    }

    // Datas of the module's tables

    foreach ($fields as $tableName => $fields_table) {
        if ($tableName != MODULE) {

            $result = $db->query('SELECT * FROM pm_' . $tableName . ' WHERE ' . $fields_table['table']['fieldRef'] . ' = ' . $id);
            if ($result !== false) {

                foreach ($result as $i => $row) {

                    $id_lang = ($fields_table['table']['multi'] == 1 && isset($row['lang'])) ? $row['lang'] : 0;

                    foreach ($fields_table['fields'] as $fieldName => $field) {
                        if ($field->getType() != 'separator') {
                            $field->setValue($row[$fieldName], $i, $id_lang);
                        }
                    }
                }
            }
        }
    }

    // Insersion / update

    if (in_array('add', $permissions) || in_array('edit', $permissions) || in_array('all', $permissions)) {
        if ((($action == 'add') || ($action == 'edit'))) {
            $files = array();

            // Getting POST values
            for ($i = 0; $i < $total_lang; $i++) {

                $id_lang = (MULTILINGUAL) ? $langs[$i]['id'] : 0;

                foreach ($fields as $tableName => $fields_table) {

                    if ($tableName == MODULE || $id_lang == DEFAULT_LANG || $fields_table['table']['multi'] == 1) {

                        foreach ($fields_table['fields'] as $fieldName => $field) {
                            $fieldName = $tableName . '_' . $fieldName . '_';

                            if ($tableName == MODULE)
                                $fieldName .= (MULTILINGUAL && !$field->isMultilingual()) ? DEFAULT_LANG : $id_lang;
                            else {
                                $id_lang = ($fields_table['table']['multi'] == 1) ? $langs[$i]['id'] : 0;
                                $fieldName .= ($fields_table['table']['multi'] == 1 && !$field->isMultilingual()) ? DEFAULT_LANG : $id_lang;
                            }

                            if (isset($_POST[$fieldName])) {

                                foreach ($_POST[$fieldName] as $index => $value) {

                                    switch ($field->getType()) {
                                        case 'date' :
                                            $date = isset($_POST[$fieldName][$index]['date']) ? $_POST[$fieldName][$index]['date'] : '';
                                            if (!empty($date)) $date = gm_strtotime($date . ' 00:00:00');
                                            if (is_numeric($date) && $date !== false)
                                                $field->setValue($date, $index, $id_lang);
                                            else
                                                $field->setValue(NULL, $index, $id_lang);
                                            break;
                                        case 'datetime' :
                                            $date = isset($_POST[$fieldName][$index]['date']) ? $_POST[$fieldName][$index]['date'] : '';
                                            $hour = isset($_POST[$fieldName][$index]['hour']) ? $_POST[$fieldName][$index]['hour'] : '';
                                            $minute = isset($_POST[$fieldName][$index]['minute']) ? $_POST[$fieldName][$index]['minute'] : 0;
                                            if (!empty($date) && is_numeric($hour) && is_numeric($minute)) $date = strtotime($date . ' ' . $hour . ':' . $minute . ':00');
                                            if (is_numeric($date) && $date !== false)
                                                $field->setValue($date, $index, $id_lang);
                                            else
                                                $field->setValue(NULL, $index, $id_lang);
                                            break;
                                        case 'password' :
                                            $value = ($value != '') ? md5($value) : '';
                                            if ($value == '') $value = $field->getValue(false, $index, $id_lang);
                                            $field->setValue($value, $index, $id_lang);
                                            break;

                                        case 'multiselect' :
                                            $value = (isset($_POST[$fieldName][$index]) && is_array($_POST[$fieldName][$index])) ? implode(',', $_POST[$fieldName][$index]) : '';
                                            $field->setValue($value, $index, $id_lang);
                                            break;
                                        case 'alias' :
                                            $value = text_format($_POST[$fieldName][$index]);
                                            $field->setValue($value, $index, $id_lang);
                                            break;
                                        default :
                                            $value = isset($_POST[$fieldName][$index]) ? $_POST[$fieldName][$index] : '';
                                            $field->setValue($value, $index, $id_lang);
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // Remove row if (all fields = empty) and if (tableName != MODULE)

            foreach ($fields as $tableName => $fields_table) {
                if ($tableName != MODULE) {

                    $default_lang = ($fields_table['table']['multi'] == 1) ? DEFAULT_LANG : 0;

                    $numRows = getNumMaxRows($fields, $tableName);
                    for ($index = 0; $index < $numRows; $index++) {

                        $empty = true;
                        $id_row = 0;
                        if (isset($_POST[$tableName . '_id_' . $default_lang][$index]))
                            $id_row = $_POST[$tableName . '_id_' . $default_lang][$index];

                        if ($id_row == 0 || $id_row == '') {

                            foreach ($fields_table['fields'] as $fieldName => $field) {
                                $value = $field->getValue(false, $index, $default_lang);
                                if (!empty($value)) $empty = false;
                            }
                            if ($empty) {
                                foreach ($fields_table['fields'] as $fieldName => $field) {
                                    $field->removeValue($index);
                                }
                            }
                        }
                    }
                }
            }
            if (VALIDATION && isset($_POST['checked']) && is_numeric($_POST['checked'])) $checked = $_POST['checked'];
            if (HOME && isset($_POST['home']) && is_numeric($_POST['home'])) $home = $_POST['home'];
            if (DATES && (!is_numeric($add_date) || $add_date == 0)) $add_date = time();
            if (RELEASE) {
                $day = (isset($_POST['publish_date_day'])) ? $_POST['publish_date_day'] : '';
                $month = (isset($_POST['publish_date_month'])) ? $_POST['publish_date_month'] : '';
                $year = (isset($_POST['publish_date_year'])) ? $_POST['publish_date_year'] : '';
                $hour = (isset($_POST['publish_date_hour'])) ? $_POST['publish_date_hour'] : '';
                $minute = (isset($_POST['publish_date_minute'])) ? $_POST['publish_date_minute'] : '';
                if (is_numeric($day) && is_numeric($month) && is_numeric($year) && is_numeric($hour) && is_numeric($minute))
                    $publish_date = mktime($hour, $minute, 0, $month, $day, $year);
                else
                    $publish_date = NULL;

                $day = (isset($_POST['unpublish_date_day'])) ? $_POST['unpublish_date_day'] : '';
                $month = (isset($_POST['unpublish_date_month'])) ? $_POST['unpublish_date_month'] : '';
                $year = (isset($_POST['unpublish_date_year'])) ? $_POST['unpublish_date_year'] : '';
                $hour = (isset($_POST['unpublish_date_hour'])) ? $_POST['unpublish_date_hour'] : '';
                $minute = (isset($_POST['unpublish_date_minute'])) ? $_POST['unpublish_date_minute'] : '';
                if (is_numeric($day) && is_numeric($month) && is_numeric($year) && is_numeric($hour) && is_numeric($minute))
                    $unpublish_date = mktime($hour, $minute, 0, $month, $day, $year);
                else
                    $unpublish_date = NULL;
            }
            if (isset($_POST['users'])) $users = $_POST['users'];
            if (!is_array($users)) $users = explode(',', $users);

            if (checkFields($db, $fields, $id)) {

                for ($i = 0; $i < $total_lang; $i++) {
                    $id_lang = (MULTILINGUAL) ? $langs[$i]['id'] : 0;

                    // Add / Edit item in the table of the module
                    $data = array();
                    $data['id'] = 0;
                    $data['lang'] = 2;
                    $data['rank'] = 0;
                    $data['home'] = 0;
                    $data['checked'] = 0;
                    $data['add_date'] = $add_date;
                    $data['edit_date'] = $edit_date;
                    $data['publish_date'] = $publish_date;
                    $data['unpublish_date'] = $unpublish_date;
                    $data['users'] = implode(',', $users);
                    $data ['packageid'] = $_POST['packageid'];
                    $data ['packname'] = $_POST['packname'];
                    $data ['descr'] = $_POST['descr'];
                    $data ['fromdate'] = $_POST['fromdate'];
//                    $data ['applyon'] =    implode(',',$_POST['applyon']);
                    $data ['todate'] = $_POST['todate'];
                    $data ['applyondays'] = implode(',', $_POST['applyondays']);
                    $data ['meals'] = implode(',', $_POST['meals']);
                    $data ['maxpeople'] = $_POST['maxpeople'];
                    $data ['maxheight'] = $_POST['maxheight'];
                    $data ['packcharge'] = $_POST['packcharge'];
                    $data ['taxrates'] = $_POST['taxrates'];
                    $data ['taxinclusive'] = $_POST['taxinclusive'];
                    $data ['ecnightadult'] = $_POST['ecnightadult'];
                    $data ['extrafood'] = $_POST['extrafood'];
                    $data ['ecnightchild'] = $_POST['ecnightchild'];
                    //finished on Manager Instructions
//                    $data ['discounttype'] = $_POST['discounttype'];
//                    $data ['discount'] = $_POST['discount'];

                    foreach ($fields[MODULE]['fields'] as $fieldName => $field)
                        $data[$fieldName] = $field->getValue(false, 0, $id_lang);
                    if ($action == 'add' && (in_array('add', $permissions) || in_array('all', $permissions))) {
                        $result_insert = db_prepareInsert_custom($db, 'pm_' . MODULE, $data);
                        add_item($db, MODULE, $result_insert, $id_lang);


                    } elseif ($action == 'edit' && (in_array('edit', $permissions) || in_array('all', $permissions))) {

                        $query_exist = 'SELECT * FROM pm_' . MODULE . ' WHERE id = ' . $id;
                        if (MULTILINGUAL) $query_exist .= ' AND lang = ' . $id_lang;
                        $result_exist = $db->query($query_exist);
                        $data['rank'] = $old_rank;

                        if ($result_exist !== false) {
                            if ($db->last_row_count() > 0) {
                                $result_update = db_prepareUpdate_custom($db, 'pm_' . MODULE, $data);
                                edit_item($db, MODULE, $result_update, $id, $id_lang);
                            } else {
                                $result_insert = db_prepareInsert_custom($db, 'pm_' . MODULE, $data);
                                add_item($db, MODULE, $result_insert, $id_lang);
                            }
                        }
                    }

                    // Add / Edit items in other tables
                    if (empty($_SESSION['msg_error']) && $id > 0) {

                        foreach ($fields as $tableName => $fields_table) {
                            if ($tableName != MODULE) {
                                $numRows = getNumMaxRows($fields, $tableName);
                                for ($index = 0; $index < $numRows; $index++) {

                                    if ($fields_table['table']['multi'] == 0) $id_lang = 0;

                                    $id_row = $fields_table['fields']['id']->getValue(false, $index, $id_lang);

                                    $data = array();
                                    $data['lang'] = $id_lang;
                                    $data[$fields_table['table']['fieldRef']] = $id;

                                    foreach ($fields_table['fields'] as $fieldName => $field)
                                        $data[$fieldName] = $field->getValue(false, $index, $id_lang);

                                    if ($id_row == 0 && (in_array('add', $permissions) || in_array('all', $permissions))) {

                                        $result_insert = db_prepareInsert_custom($db, 'pm_' . $tableName, $data);
                                        if ($result_insert->execute() !== false) {
                                            $fields_table['fields']['id']->setValue($db->lastInsertId(), $index, $id_lang);
                                        }

                                    } elseif ($id_row > 0 && (in_array('edit', $permissions) || in_array('all', $permissions))) {

                                        $query_exist = 'SELECT * FROM pm_' . $tableName . ' WHERE id = ' . $id_row;
                                        if ($fields_table['table']['multi'] == 1) $query_exist .= ' AND lang = ' . $id_lang;
                                        $result_exist = $db->query($query_exist);

                                        if ($result_exist !== false) {
                                            if ($db->last_row_count() > 0) {

                                                $result_update = db_prepareUpdate_custom($db, 'pm_' . $tableName, $data);
                                                $result_update->execute();

                                            } else {
                                                $result_insert = db_prepareInsert_custom($db, 'pm_' . $tableName, $data);
                                                if ($result_insert->execute() !== false) {
                                                    $fields_table['fields']['id']->setValue($db->lastInsertId(), $index, $id_lang);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else
                $_SESSION['msg_error'][] = $texts['FORM_ERRORS'];

        }
    }
    if (($back === true)) {
        header('Location: index.php?view=list');
        exit();
    }
    if (($back === true) && empty($_SESSION['msg_error']) && !empty($_SESSION['msg_success'])) {
        header('Location: index.php?view=list');
        exit();
    }

    if (in_array('edit', $permissions) || in_array('all', $permissions)) {
        // Row deletion
        if ($action == 'delete_row' && $id_row > 0 && isset($_GET['table']) && isset($_GET['fieldref']) && check_token($referer, 'form', 'get'))
            delete_row($db, $id, $id_row, 'pm_' . $_GET['table'], $_GET['fieldref']);

        // File deletion
        if ($action == 'delete_file' && $id_file > 0 && check_token($referer, 'form', 'get'))
            delete_file($db, $id_file);

        if ($action == 'delete_multi_file' && isset($_POST['multiple_file']) && check_token($referer, 'form', 'get'))
            delete_multi_file($db, $_POST['multiple_file'], $id);

        // File activation/deactivation
        if ($action == 'check_file' && $id_file > 0 && check_token($referer, 'form', 'get'))
            check($db, 'pm_' . MODULE . '_file', $id_file, 1);

        if ($action == 'uncheck_file' && $id_file > 0 && check_token($referer, 'form', 'get'))
            check($db, 'pm_' . MODULE . '_file', $id_file, 2);

        if ($action == 'check_multi_file' && isset($_POST['multiple_file']) && check_token($referer, 'form', 'get'))
            check_multi($db, 'pm_' . MODULE . '_file', 1, $_POST['multiple_file']);

        if ($action == 'uncheck_multi_file' && isset($_POST['multiple_file']) && check_token($referer, 'form', 'get'))
            check_multi($db, 'pm_' . MODULE . '_file', 2, $_POST['multiple_file']);

        // Files displayed in homepage
        if ($action == 'display_home_file' && $id_file > 0 && check_token($referer, 'form', 'get'))
            display_home($db, 'pm_' . MODULE . '_file', $id_file, 1);

        if ($action == 'remove_home_file' && $id_file > 0 && check_token($referer, 'form', 'get'))
            display_home($db, 'pm_' . MODULE . '_file', $id_file, 0);

        if ($action == 'display_home_multi_file' && isset($_POST['multiple_file']) && check_token($referer, 'form', 'get'))
            display_home_multi($db, 'pm_' . MODULE . '_file', 1, $_POST['multiple_file']);

        if ($action == 'remove_home_multi_file' && isset($_POST['multiple_file']) && check_token($referer, 'form', 'get'))
            display_home_multi($db, 'pm_' . MODULE . '_file', 0, $_POST['multiple_file']);
    }
}

// File download
if ($action == 'download' && isset($_GET['type'])) {
    $type = $_GET['type'];
    if ($id_file > 0) {
        if ($type == 'image' || $type == 'other') {
            $query_file = 'SELECT file FROM pm_' . MODULE . '_file WHERE id = ' . $id_file;
            if (MULTILINGUAL) $query_file .= ' AND lang = ' . DEFAULT_LANG;
            $result_file = $db->query($query_file);
            if ($result_file !== false && $db->last_row_count() > 0) {
                $file = $result_file->fetchColumn(0);

                if ($type == 'image') {
                    if (is_file(SYSBASE . 'medias/' . MODULE . '/big/' . $id_file . '/' . $file))
                        $filepath = SYSBASE . 'medias/' . MODULE . '/big/' . $id_file . '/' . $file;
                    elseif (is_file(SYSBASE . 'medias/' . MODULE . '/medium/' . $id_file . '/' . $file))
                        $filepath = SYSBASE . 'medias/' . MODULE . '/medium/' . $id_file . '/' . $file;
                    elseif (is_file(SYSBASE . 'medias/' . MODULE . '/small/' . $id_file . '/' . $file))
                        $filepath = SYSBASE . 'medias/' . MODULE . '/small/' . $id_file . '/' . $file;
                } elseif ($type == 'other' && is_file(SYSBASE . 'medias/' . MODULE . '/other/' . $id_file . '/' . $file))
                    $filepath = SYSBASE . 'medias/' . MODULE . '/other/' . $id_file . '/' . $file;
                if (isset($filepath)) {
                    $mime = getFileMimeType($filepath);
                    if (strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE') == false) {
                        header('Content-disposition: attachment; filename=' . $file);
                        header('Content-Type: ' . $mime);
                        header('Content-Transfer-Encoding: ' . $mime . "\n");
                        header('Content-Length: ' . filesize($filepath));
                        header('Pragma: no-cache');
                        header('Cache-Control: must-revalidate, post-check=0, pre-check=0, public');
                        header('Expires: 0');
                    }
                    readfile($filepath);
                }
            }
        }
    }
}

//getting data from table to edit
$oldquery = 'SELECT * FROM pm_' . $tableName . ' WHERE ' . $fields_table['table']['fieldRef'] . ' = ' . $id;
$newquery = 'SELECT * FROM pm_' . $tableName . ' WHERE  id = ' . $id;
$result = $db->query($newquery);
$singlerow = $result->fetch();
$editformid = $id;
$id = $id;

$csrf_token = get_token('form'); ?>
<!DOCTYPE html>
<head>
    <?php include(SYSBASE . ADMIN_FOLDER . '/includes/inc_header_form.php'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
</head>
<body>
<div id="overlay">
    <div id="loading"></div>
</div>
<div id="wrapper">

    <?php
    include(SYSBASE . ADMIN_FOLDER . '/includes/inc_top.php');

    if (!in_array('no_access', $permissions)) {
    include(SYSBASE . ADMIN_FOLDER . '/includes/inc_library.php');

    ?>
    <!--    <form id="form" class="form-horizontal" name="package_form" role="form"-->
    <!--          method="post" enctype="multipart/form-data">-->

    <div id="page-wrapper">
        <div class="page-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 clearfix">
                        <h4 class="pull-left"><i
                                    class="fa fa-<?php echo ICON; ?>"></i> <?php echo TITLE_ELEMENT; ?></h4>
                    </div>
                    <div class="col-xs-6 col-sm-6 clearfix pb15 text-right">
                        <?php
                        if (in_array('add', $permissions) || in_array('all', $permissions)) { ?>
                            <a href="javascript:if(confirm('<?php echo $texts['LOOSE_DATAS']; ?>')) window.location = 'index.php?view=form&id=0';">
                                <button type="button" class="btn btn-primary mt15" data-toggle="tooltip"
                                        data-placement="bottom" title="<?php echo $texts['NEW']; ?>">
                                    <i class="fa fa-plus-circle"></i><span
                                            class="hidden-sm hidden-xs"> <?php echo $texts['NEW']; ?></span>
                                </button
                            </a>
                            <?php
                        } ?>
                        <a href="index.php?view=list">
                            <button type="button" class="btn btn-default mt15" data-toggle="tooltip"
                                    data-placement="bottom" title="<?php echo $texts['BACK_TO_LIST']; ?>">
                                <i class="fa fa-reply"></i><span
                                        class="hidden-sm hidden-xs"> <?php echo $texts['BACK_TO_LIST']; ?></span>
                            </button>
                        </a>
                        <?php
                        if ($db !== false) {
                            if ($id > 0) {
                                if (in_array('edit', $permissions) || in_array('all', $permissions)) { ?>
                                    <span><button name="edit"
                                                  class="btn btn-default mt15 hidden-sm save_all" data-toggle="tooltip"
                                                  data-placement="bottom" data-placement="bottom"
                                                  title="<?php echo $texts['SAVE']; ?>"><i
                                                    class="fa fa-floppy-o"></i><span
                                                    class="hidden-sm hidden-xs"> <?php echo $texts['SAVE']; ?></span></button></span>
                                    <span><button type="submit" name="edit_back" class="btn btn-success mt15"
                                                  data-toggle="tooltip" data-placement="bottom"
                                                  title="<?php echo $texts['SAVE_EXIT']; ?>"><i
                                                    class="fa fa-floppy-o"></i><span
                                                    class="hidden-sm hidden-xs"> <?php echo $texts['SAVE_EXIT']; ?></span></button></span>
                                    <?php
                                }
                                if (in_array('add', $permissions) || in_array('all', $permissions)) { ?>
                                    <span><button name="add" class="btn btn-default mt15"
                                                  data-toggle="tooltip" data-placement="bottom"
                                                  title="<?php echo $texts['REPLICATE']; ?>"><i
                                                    class="fa fa-files-o"></i><span
                                                    class="hidden-sm hidden-xs"> <?php echo $texts['REPLICATE']; ?></span></button></span>
                                    <?php
                                }
                            } else {
                                if (in_array('add', $permissions) || in_array('all', $permissions)) { ?>
                                    <span><button name="add"
                                                  class="btn btn-default mt15 hidden-sm save_all" data-toggle="tooltip"
                                                  data-placement="bottom" title="<?php echo $texts['SAVE']; ?>"><i
                                                    class="fa fa-floppy-o"></i><span
                                                    class="hidden-sm hidden-xs"> <?php echo $texts['SAVE']; ?></span></button></span>
                                    <span><button type="submit" name="add_back" class="btn btn-success mt15"
                                                  data-toggle="tooltip" data-placement="bottom"
                                                  title="<?php echo $texts['SAVE_EXIT']; ?>"><i
                                                    class="fa fa-floppy-o"></i><span
                                                    class="hidden-sm hidden-xs"> <?php echo $texts['SAVE_EXIT']; ?></span></button></span>
                                    <?php
                                }
                            }
                        } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid new-adjusment">
            <div class="alert-container removealert">
                <div class="alert alert-success alert-dismissable"></div>
                <!--      <div class="alert alert-warning alert-dismissable"></div> -->
                <!-- <div class="alert alert-danger alert-dismissable"></div>-->
            </div>
            <?php
            if ($db !== false) { ?>
            <input type="hidden" name="id" value="<?php echo $id; ?>"/>
            <!--            <input type="hidden" name="csrf_token" value="--><?php //echo $csrf_token;
            ?><!--"/>-->

            <div class="container">
                <form id="form" class="form-horizontal" role="form" action="index.php?view=form" method="post"
                      enctype="multipart/form-data">

                    <?php //if($singlerow['packageid']!=null) {;?>
                    <div class="row">
                        <div class="errordate">

                        </div>
                    </div>
                    <div class="row mb10">
                        <input type="hidden" name="form_id" class="form_id" value="<?php echo $_GET['id']?>">
                        <label class="col-md-2" style="padding-top:7px;">
                            Package ID
                        </label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" disabled value="<?php if ($id == 0) {
                                echo get_numbering("pm_package", $db);
                            }
                            if ($id != 0) {
                                echo $singlerow['id'];
                            } ?>" name="packageid[]">
                        </div>
                        <input type="hidden" name="pkgformupdate" value="<?php $editformid ?>">
                        <label class="col-md-2" style="padding-top:7px;">
                            Package Name
                        </label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" value="<?php if ($id != 0) {
                                echo $singlerow['packname'];
                            } ?>" name="packname">
                        </div>
                    </div>

                    <div class="row mb10">


                        <label class="col-md-2" style="padding-top:7px;">
                            Description
                        </label>
                        <div class="col-md-10">
                                <textarea class="form-control" id=descr type="text"
                                          value="<?php if ($id != 0) echo $singlerow['descr']; ?>"
                                          name="descr"><?php if ($id != 0) echo $singlerow['descr']; ?></textarea>
                        </div>
                    </div>
                    <div class="row mb10">

                        <label class="col-md-2" style="padding-top:7px;">
                            Package Valid from Date
                        </label>
                        <div class="col-md-4">
                            <input class="form-control" id="fromdate" type="date"
                                   value="<?php if ($id != 0) echo $singlerow['fromdate']; ?>" name="fromdate">
                        </div>
                        <label class="col-md-2" style="padding-top:7px;">
                            Meals included in package
                        </label>
                        <div class="col-md-4 meals" id="meals">

                            <?php
                            $value = explode(',', $singlerow['meals']);
                            $checked1 = '';
                            $checked2 = '';
                            $checked3 = '';
                            foreach ($value as $key => $val) {
                                if ($val == 1) {
                                    $checked1 = 'checked';
                                }
                                if ($val == 2) {
                                    $checked2 = 'checked';
                                }
                                if ($val == 3) {
                                    $checked3 = 'checked';
                                }
                                ?>
                            <?php }
                            ?>
                            <input type="checkbox" <?php if ($id != 0) {
                                echo $checked1;
                            } ?> name="meals[]" id="meals" value="1"> Breakfast
                            <input type="checkbox" <?php if ($id != 0) {
                                echo $checked2;
                            } ?> name="meals[]" id="meals" value="2"> Lunch
                            <input type="checkbox" <?php if ($id != 0) {
                                echo $checked3;
                            } ?> name="meals[]" id="meals" value="3"> Dinner
                        </div>

                    </div>

                    <div class="row mb10">

                        <label class="col-md-2" style="padding-top:7px;">
                            Package Valid to Date'
                        </label>
                        <div class="col-md-4 endto">
                            <input class="form-control" id="todate" type="date"
                                   value="<?php if ($id != 0) echo $singlerow['todate']; ?>" name="todate">
                        </div>
                        <label class="col-md-2" style="padding-top:7px;">
                            Apply on Days
                        </label>
                        <div class="col-md-4 days">
                            <?php
                            $value = explode(',', $singlerow['applyondays']);
                            $checked1 = '';
                            $checked2 = '';
                            $checked3 = '';
                            $checked4 = '';
                            $checked5 = '';
                            $checked6 = '';
                            $checked7 = '';
                            foreach ($value as $key => $val) {
                                if ($val == 1) {
                                    $checked1 = 'checked';
                                }
                                if ($val == 2) {
                                    $checked2 = 'checked';
                                }
                                if ($val == 3) {
                                    $checked3 = 'checked';
                                }
                                if ($val == 4) {
                                    $checked4 = 'checked';
                                }
                                if ($val == 5) {
                                    $checked5 = 'checked';
                                }
                                if ($val == 6) {
                                    $checked6 = 'checked';
                                }
                                if ($val == 7) {
                                    $checked7 = 'checked';
                                }
                                ?>
                            <?php }
                            ?>
                            <input type="checkbox" <?php if ($id != 0) {
                                echo $checked1;
                            } ?> name="applyondays[]" value="1"> Saturday
                            <input type="checkbox" <?php if ($id != 0) {
                                echo $checked2;
                            } ?> name="applyondays[]" value="2"> Sunday
                            <input type="checkbox" <?php if ($id != 0) {
                                echo $checked3;
                            } ?> name="applyondays[]" value="3"> Monday
                            <input type="checkbox" <?php if ($id != 0) {
                                echo $checked4;
                            } ?> name="applyondays[]" value="4"> Tuesday
                            <input type="checkbox" <?php if ($id != 0) {
                                echo $checked5;
                            } ?> name="applyondays[]" value="5"> Wednesday
                            <input type="checkbox" <?php if ($id != 0) {
                                echo $checked6;
                            } ?> name="applyondays[]" value="6"> Thursday
                            <input type="checkbox" <?php if ($id != 0) {
                                echo $checked7;
                            } ?> name="applyondays[]" value="7"> Friday

                        </div>
                    </div>
                    <div class="row mb10">

                    </div>

                    <div class="row mb10">

                        <label class="col-md-2" style="padding-top:7px;">
                            Max No. of People
                        </label>
                        <div class="col-md-2">
                            <input id="maxpeoplecb" type="checkbox" name="maxpeoplecheck" value="">
                        </div>
                        <div class="col-md-2">
                            <input id="maxpeople" class="form-control" type="text"
                                   value="<?php if ($id != 0) echo $singlerow['maxpeople']; ?>" name="maxpeople">
                        </div>

                        <label class="col-md-2" style="padding-top:7px;">
                            Max No. of Nights
                        </label>
                        <div class="col-md-2">
                            <input id="maxnightcb" type="checkbox" name="maxheightcheck" value="">
                        </div>
                        <div class="col-md-2">
                            <input id="maxnight" class="form-control" type="text"
                                   value="<?php if ($id != 0) echo $singlerow['maxheight']; ?>" name="maxheight">
                        </div>
                    </div>


                    <div class="row mb10">
                        <label class="col-md-2" style="padding-top:7px;">
                            Package Charges
                        </label>
                        <div class="col-md-4">
                            <input class="form-control" type="text"
                                   value="<?php if ($id != 0) echo $singlerow['totalpackcharges']; ?>"
                                   name="totalpackcharge">
                        </div>


                        <label class="col-md-2" style="padding-top:7px;">
                            Package Charges Per Night
                        </label>
                        <div class="col-md-4">
                            <input class="form-control" type="text"
                                   value="<?php if ($id != 0) echo $singlerow['packcharge']; ?>"
                                   name="packchargenight">
                        </div>

                    </div>


                    <div class="row mb10">

                        <label class="col-md-2" style="padding-top:7px;">
                            Applicable Tax Rates
                        </label>

                        <div class="col-md-4"><?php if ($id == 0) {
                                echo getDropDownData('pm_tax', 'taxrates', 'taxrates', 'taxname', 'Tax Rate', 'required', $db);
                            } else {
                                echo getSelectedDropDownData('pm_tax', 'taxrates', 'taxrates', 'taxname', 'Tax Rate', 'required', $singlerow['taxrates'], $db);
                            } ?>
                        </div>
                        <label class="col-md-2" style="padding-top:7px;">
                            Price is Inclusive of Tax
                        </label>
                        <div class="col-md-4">
                            <select name="taxinclusive" id="taxinclusive" class="form-control">
                                <option value="1">Yes</option>
                                <option value="2">No</option>

                            </select>
                        </div>

                    </div>


                    <div class="row mb10">
                        <label class="col-md-2" style="padding-top:7px;">
                            Extra Charges per Night(Adult)
                        </label>
                        <div class="col-md-4">
                            <input class="form-control" type="text"
                                   value="<?php if ($id != 0) echo $singlerow['ecnightadult']; ?>"
                                   name="ecnightadult">
                        </div>

                        <label class="col-md-2" style="padding-top:7px;">
                            Extra Charges per Night(Child)
                        </label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="ecnightchild"
                                   value="<?php if ($id != 0) echo $singlerow['ecnightchild']; ?>"
                                   name="ecnightchild">
                        </div>


                    </div>

                    <div class="row mb10">

                        <label class="col-md-2" style="padding-top:7px;">
                            Extra Food Suppliment Charges
                        </label>
                        <div class="col-md-4">
                            <input class="form-control" type="text"
                                   value="<?php if ($id != 0) echo $singlerow['extrafood']; ?>" name="extrafood">
                        </div>

                        <label class="col-md-2" style="padding-top:7px;">
                            Status
                        </label>
                        <div class="col-md-4">
                            <?php if ($id == 0) {
                                echo getDropDownData('pm_status', 'status', 'package_status', 'status', 'Status', 'required', $db);
                            } else {
                                echo getSelectedDropDownData('pm_status', 'status', 'package_status', 'status', 'Status', 'required', $singlerow['status'], $db);
                            } ?>
                        </div>
                    </div>


                    <div class="row mb10">


                        <label class="col-md-2" style="padding-top:7px;">
                            Allow to edit package on Sell Package?
                        </label>
                        <div class="col-md-4">
                            <?php
                          if ($id != 0) {
                              $value = $singlerow['allow_edit'];
                              if ($value == 1) {

                                  echo '<input id="allowpkgedit" type="checkbox"  checked="checked" name="allowpkgedit" value="">';
                              } else if ($value == 0) {

                                  echo '<input id="allowpkgedit" type="checkbox" name="allowpkgedit" value="">';

                              }
                          }
                          if($id==0){
                              echo '<input id="allowpkgedit" type="checkbox"  name="allowpkgedit" value="">';
                          }

                            ?>

                        </div>
                    </div>

                    <!--                        <div class="row mb10">-->
                    <!---->
                    <!--                            <label class="col-md-2" style="padding-top:24px;">-->
                    <!--                                Save All-->
                    <!--                            </label>-->
                    <!--                            <div class="col-md-4">-->
                    <!--                                <input type="button" name="save" value="Save"-->
                    <!--                                       class="btn btn-success mt15 floatbtn save_all">-->
                    <!--                            </div>-->
                    <!---->
                    <!--                        </div>-->


                    <?php //} ?>

                    <!--startcopy-->

                    <div class="panel panel-default">
                        <ul class="nav nav-tabs pt5" id="tabs">
                            <li class="active"><a data-toggle="tab" href="#BD"><i
                                            class="fas fa-book"></i> <?php echo "Rooms"; ?></a></li>
                            <li onclick="service_tab();"><a data-toggle="tab" href="#services"><i
                                            class="fas fa-wrench"></i> <?php echo 'Services'; ?></a></li>
                            <li onclick="activity_tab();"><a data-toggle="tab" href="#activities"><i
                                            class="fa-ticket"></i> <?php echo 'Activity'; ?>
                                </a></li>
                        </ul>

                        <div class="panel-body">
                            <div class="tab-content">
                                <?php
                                if ($_SESSION['user']['type'] == 'administrator') { ?>
                                <div id="BD" class="tab-pane fade in active">
                                    <div class="row mb10">
                                        <div class="row">
                                            <div class="col-md-4 mb10">
                                                <h5 class="date_error" style="color: red;font-size:small;"></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 mb10">
                                                <h5 class="data_success" style="color: green;font-size:small;"></h5>
                                            </div>
                                        </div>
                                        <form id="booking_form" method="POST">

                                            <div class="row">
                                                <strong class="col-md-4 mb10">
                                                    <strong<h5 class="error"
                                                               style="color: red;font-size:small;"></h5>
                                                </strong>
                                            </div>
                                    </div>
                                    <div class="row">
                                        <input name="addroom" value="Add Room" type="button"
                                               style="margin-bottom: 12px;"
                                               class="btn btn-success floatbtn add_room" onclick="add_room();">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered heading" id="tbl_posts">
                                                <thead>
                                                <tr>
                                                    <th>Room Code</th>
                                                    <th>Room Title</th>
                                                    <th>Room Type</th>
                                                    <th>Unit</th>
                                                    <th>Night Charges</th>
                                                    <th>Adults</th>
                                                    <th>Children</th>
                                                    <th>No. of Nights</th>
                                                    <th>Discount(%)</th>
                                                    <th>Tax Rate(%)</th>
                                                    <th>Net Amount</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbl_posts_body">

                                                <?php
                                                if ($editformid != 0) {
                                                    //$get_booking_id='';
                                                    $get_booking_id = $db->query("SELECT
                                                      pm_packages.*,
                                                      pm_packages.id as room_book_id,
                                                        pm_room.roomcode,
                                                        pm_room.roomtitle,
                                                        pm_packages.nightcharge,
                                                        pm_packages.adults,
                                                        pm_packages.nights,
                                                        pm_packages.discount,
                                                        pm_packages.children,
                                                        (
                                                          SELECT
                                                            pm_room.id_roomtype
                                                          FROM
                                                            pm_room
                                                          where
                                                            pm_room.id = pm_packages.room_id
                                                            and pm_room.lang = 2
                                                        ) as room_type_id,
                                                        (
                                                          SELECT
                                                            pm_roomtypes.roomtype
                                                          from
                                                            pm_roomtypes
                                                          where
                                                            pm_roomtypes.id = pm_room.id_roomtype
                                                            and pm_roomtypes.lang = 2
                                                        ) as room_type_name
                                                      FROM
                                                        pm_packages
                                                        INNER JOIN pm_room ON pm_packages.room_id = pm_room.id
                                                      WHERE
                                                        pm_packages.packages_parent_id = $id
                                                        and pm_room.lang = 2")->fetchAll();

                                                    if ($get_booking_id) {
                                                        foreach ($get_booking_id as $roomdata) { ?>
                                                            <input type="hidden" name="room_id[]"
                                                                   value="<?php echo $roomdata['room_id']; ?>"/>
                                                            <input type="hidden" name="room_book_id[]"
                                                                   value="<?php echo $roomdata['room_book_id']; ?>"/>
                                                            <input type="hidden" name="taxbooking_id[]"
                                                                   class="taxbooking_id<?php echo $roomdata['room_id']; ?>"
                                                                   value="<?php echo $roomdata['tax_rate']; ?>"/>
                                                            <input type="hidden" name="unit_id[]"
                                                                   class="unit_id<?php echo $roomdata['id']; ?>"/>
                                                            <input type="hidden" name="discount_amount[]"
                                                                   value="<?php echo $roomdata['discount_amount']; ?>"
                                                                   class="discount_amount<?php echo $roomdata['id']; ?>"/>
                                                            <input type="hidden" name="vat_amount[]"
                                                                   value="<?php echo $roomdata['vat_amount']; ?>"
                                                                   class="vat_amount<?php echo $roomdata['id']; ?>"/>
                                                            <tr id="rec_<?php echo $roomdata['id']; ?>">
                                                                <td>
                                                                    <span class="sn"><?php echo $roomdata['roomcode']; ?></span>
                                                                </td>
                                                                <td>
                                                                    <span class="sn"><?php echo $roomdata['roomtitle']; ?></span>
                                                                </td>
                                                                <td>
                                                                    <span class="sn"><?php echo $roomdata['room_type_name']; ?></span>
                                                                </td>
                                                                <td>
                                                                    <?php echo getDropDownDataUnitroom('unit_id', 'unit') ?>
                                                                </td>
                                                                <td><input type="text"
                                                                           class="nightcharge<?php echo $roomdata['room_id']; ?>"
                                                                           disabled
                                                                           name="nightcharge[]"
                                                                           value="<?php echo $roomdata['nightcharge']; ?>"
                                                                           size="4"/></td>
                                                                <td><input type="text" name="adults[]"
                                                                           class="maxadults<?php echo $roomdata['room_id']; ?>"
                                                                           onkeyup="findadult(<?php echo $roomdata['room_id']; ?>)"
                                                                           value="<?php echo $roomdata['adults']; ?>"
                                                                           size="4"/>
                                                                </td>
                                                                <td><input type="text" name="children[]"
                                                                           class="maxchild<?php echo $roomdata['room_id']; ?>"
                                                                           onkeyup="findchild(<?php echo $roomdata['room_id']; ?>)"
                                                                           value="<?php echo $roomdata['children']; ?>"
                                                                           size="4"/></td>
                                                                <td><input type="text" name="night[]"
                                                                           class="staynight<?php echo $roomdata['room_id']; ?>"
                                                                           onkeyup="updatenetprice(<?php echo $roomdata['room_id']; ?>)"
                                                                           value="<?php echo $roomdata['nights']; ?>"
                                                                           size="4"/>
                                                                </td>
                                                                <td><input type="text"
                                                                           class="discount<?php echo $roomdata['room_id']; ?>"
                                                                           name="discount[]"
                                                                           onkeyup="updatenetpricewithdiscount(<?php echo $roomdata['room_id']; ?>)"
                                                                           value="<?php echo $roomdata['discount']; ?>"
                                                                           size="4"/></td>
                                                                <td>
                                                                    <?php $tax_id = $roomdata['tax_rate'];
                                                                    $unique_id = $roomdata['room_id'];
                                                                    echo getSelectedDropDownData('pm_tax', 'taxrate', 'tax_id' . $roomdata['room_id'], 'taxname', 'Tax', 'required', $tax_id, $db) ?>
                                                                    <script>
                                                                        $("#tax_id" +<?php echo $unique_id;?>).change(function (event) {
                                                                            $(".error").html("");
                                                                            if (event.target == this) {
                                                                                var selectedarrayval = $(this).val();
                                                                                $(".taxbooking_id" +<?php echo $unique_id?>).val(selectedarrayval);
                                                                                $(".netamount" +<?php echo $unique_id?>).val('');
                                                                                var tax_id = selectedarrayval;
                                                                                var nightcharge = $(".nightcharge" +<?php echo $unique_id?>).val();
                                                                                var staynight = $(".staynight" +<?php echo $unique_id?>).val();
                                                                                var discount = $(".discount" +<?php echo $unique_id?>).val();
                                                                                if (staynight == '') {
                                                                                    $.ajax({
                                                                                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                                                                                        type: 'POST',
                                                                                        data: {
                                                                                            'remove_id': <?php echo $unique_id?>,
                                                                                            'comparechild': 1
                                                                                        },
                                                                                        dataType: 'JSON',
                                                                                        success: function (data) {
                                                                                            $(".error").html("");
                                                                                            $(".error").append("<span>No of Nights Stay cannot be empty in " + data[0].roomtitle + " </span>");
                                                                                            return false;
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    if (isNaN(staynight)) {
                                                                                        $.ajax({
                                                                                            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                                                                                            type: 'POST',
                                                                                            data: {
                                                                                                'remove_id': <?php echo $unique_id?>,
                                                                                                'comparechild': 1
                                                                                            },
                                                                                            dataType: 'JSON',
                                                                                            success: function (data) {
                                                                                                $(".error").html("");
                                                                                                $(".error").append("<span>Please Use Numeric value Instead " + staynight + " in " + data[0].roomtitle + " </span>");
                                                                                                return false;
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                    else {
                                                                                        $(".error").html("");
                                                                                        $.ajax({
                                                                                            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                                                                                            type: 'POST',
                                                                                            data: {
                                                                                                'tax_id': tax_id,
                                                                                                'gettaxrate': 1
                                                                                            },
                                                                                            dataType: 'JSON',
                                                                                            success: function (data) {
                                                                                                $(".error").html("");
                                                                                                //totalnetamount without vat
                                                                                                if (discount == '') {
                                                                                                    discount = 0;
                                                                                                    var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                                                                                                    var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                                                                                                    var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                                                                                                    var taxamount = parseFloat(data[0].taxrates);
                                                                                                    var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                                                                                                    var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                                                                                                    $(".dicount_amount" +<?php echo $unique_id?>).val(getdiscountamount.toFixed(2));
                                                                                                    $(".vat_amount" +<?php echo $unique_id?>).val(getvatamount.toFixed(2));
                                                                                                    $(".netamount" +<?php echo $unique_id?>).val(totalnet.toFixed(2));
                                                                                                } else {
                                                                                                    var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                                                                                                    var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                                                                                                    var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                                                                                                    var taxamount = parseFloat(data[0].taxrates);
                                                                                                    var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                                                                                                    var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                                                                                                    $(".dicount_amount" +<?php echo $unique_id?>).val(0);
                                                                                                    $(".vat_amount" +<?php echo $unique_id?>).val(getvatamount.toFixed(2));
                                                                                                    $(".netamount" +<?php echo $unique_id?>).val(totalnet.toFixed(2));
                                                                                                }
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                }
                                                                            }
                                                                        });
                                                                        $("#unit_id" +<?php echo $unique_id?>).change(function (event) {
                                                                            $(".error").html();
                                                                            if (event.target == this) {
                                                                                var selectedarrayval = $(this).val();
                                                                                $(".unit_id" +<?php echo $unique_id?>).val(selectedarrayval);
                                                                            }
                                                                        });
                                                                    </script>
                                                                </td>
                                                                <td><input type="text"
                                                                           name="netamount[]"
                                                                           class="netamount<?php echo $roomdata['room_id']; ?> totalbook"
                                                                           disabled
                                                                           value="<?php echo $roomdata['booking_net_amount']; ?>"
                                                                           size="4"/></td>
                                                                <td>
                                                                    <a class="btn btn-xs delete-record"
                                                                       data-id="<?php echo $room_id; ?>"><i
                                                                                class="glyphicon glyphicon-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                        <?php
                                                    }
                                                }//end of if editform
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>


                </form>
            </div>

            <div id="services" class="tab-pane fade services">
                <!-- function service_tab   -->
                <form id="service_form">
                    <div class="row">
                        <div class="col-md-4 mb10">
                            <h5 class="data_success" style="color: green;font-size:small;"></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 mb10">
                            <h5 class="errorservices" style="color: red;font-size:small;"></h5>
                        </div>
                    </div>

                    <div class="row">
                        <input type="button" name="save" onclick="add_row();" value="Add Services"
                               class="btn btn-success mt15 floatbtn mb10">
                    </div>

                    <div class="row mb10">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered heading" id="tbl_posts">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th style="width: 20%;">Title</th>
                                        <th style="width: 20%;">Unit</th>
                                        <th>Quantity</th>
                                        <th>Charges</th>
                                        <th>Discount(%)</th>
                                        <th style="width: 17%;">Tax Rate(%)</th>
                                        <th style="width: 10%;">Net Amount</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbl_posts_body" class="services_data">
                                    <?php
                                    if ($editformid != 0) {
                                        $get_service = $db->query("select * from pm_packages_services where packages_parent_id = $id")->fetchAll();
                                        foreach ($get_service as $service) {
                                            ?>
                                            <tr id="rec-1">
                                                <td><select name="servicedropdown[]" id="servicedropdownid_0<?php echo $service['id']; ?>" required class="limitedNumbSelectedSingle_0<?php echo $service['id']; ?> getservicedropdown_0<?php echo $service['id']; ?>"><?php echo getSelectedDropDownDataspecialpurpose('pm_service', 'servicetitle', 'Service', $db, $service['id']) ?></select>
                                                </td>
                                                <td><select name="serviceunit[]" id="serviceunitdropdownid_0<?php echo $service['id']; ?>" required class="limitedNumbSelectedSingle_0<?php echo $service['id']; ?> serviceunitdropdown_0<?php echo $service['id']; ?>"><?php echo getDropDownDataUnit('Unit', $service['unit_id']) ?></select>
                                                </td>
                                                <td><input type="text" class="servicequantity_0<?php echo $service['id']; ?>" name="servicequantity[]" value="<?php echo $service['quantity']; ?>" size="4" onkeyup="updatenetpriceserviceeditrow(<?php echo $service['id']; ?>)"/>
                                                </td>
                                                <td><input type="text"
                                                           class="servicecharges_0<?php echo $service['id']; ?>" name="servicecharges[]" value="<?php echo $service['charges']; ?>" size="4" onkeyup="updatenetpriceserviceeditrow(<?php echo $service['id']; ?>)"/>
                                                </td>
                                                <td><input type="text" class="servicediscount_0<?php echo $service['id']; ?>" name="servicediscount[]" value="<?php echo $service['discount']; ?>" size="4" onkeyup="updatenetpriceserviceeditrow(<?php echo $service['id']; ?>)"/>
                                                </td>
                                                <td><select name="taxdropdown[]" id="taxdropdownid_0<?php echo $service['id']; ?>" required class="limitedNumbSelectedSingle_0<?php echo $service['id']; ?>  taxdropdownservice_0<?php echo $service['id']; ?>" onchange="taxserviceeditchange(<?php echo $service['id']; ?>)"><?php echo getSelectedDropDownDataspecialpurpose('pm_tax', 'taxname', 'Tax', $db, $service['tax_rate']) ?></select>
                                                </td>
                                                <td><input type="text" class="servicenetamount_0<?php echo $service['id']; ?>" name="servicenetamount[]" value="<?php echo $service['net_amount']; ?>" size="8" disabled/>
                                                </td>

                                            </tr>
                                            <script type="text/javascript">
                                                $(".limitedNumbSelectedSingle_0<?php echo $service['id'];?>").select2({
                                                    width: '180px',
                                                    dropdownAutoWidth: true
                                                });
                                            </script>
                                        <?php }
                                    }//end of if ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div id="activities" class="tab-pane fade">
                <form id="activity_form">
                    <div class="row">
                        <div class="col-md-4 mb10">
                            <h5 class="erroractivities" style="color: red;font-size:small;"></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 mb10">
                            <h5 class="data_success" style="color: green;font-size:small;"></h5>
                        </div>
                    </div>
                    <div class="row">
                        <input type="button" name="save" onclick="add_rowactivities();"
                               value="Add Activities"
                               class="btn btn-success mt15 floatbtn mb10">
                    </div>
                    <div class="row mb10">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered heading" id="activities_tbl_posts">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Adult</th>
                                        <th>Children</th>
                                        <th>Unit</th>
                                        <th>Quantity</th>
                                        <th>Charges</th>
                                        <th>Discount(%)</th>
                                        <th>Tax Rate(%)</th>
                                        <th>Net Amount</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbl_posts_body_activities"
                                           class="activities_data">
                                    <?php
                                    if ($editformid != 0) {

                                        $get_activity = $db->query("select * from pm_packages_activities where packages_parent_id = $id")->fetchAll();
                                        foreach ($get_activity as $activity) {
                                            ?>
                                            <tr id="rec-1">
                                                <td><input type="hidden" value="<?php echo $activity['id'] ?>"
                                                           name="activity_id"><input type="text"
                                                                                          class="activitiesid_0<?php echo $activity['id']; ?>"
                                                                                          name="activitiesid[]"
                                                                                          value="<?php echo $activity['activity_id']; ?>"
                                                                                          size="4" disabled/></td>
                                                <input type="hidden"
                                                       class="taxactivitiesid_0<?php echo $activity['id']; ?>"
                                                       name="taxactivitiesid[]"
                                                       value="<?php echo $activity['tax_rate']; ?>"/>
                                                <input type="hidden"
                                                       class="activitiesunitid_0<?php echo $activity['id']; ?>"
                                                       name="activitiesunitid[]"
                                                       value="<?php echo $activity['unit_id']; ?>"/>
                                                <input type="hidden" name="activity_package_id[]"
                                                       value="<?php echo $activity['id']; ?>"/>
                                                <input type="hidden" name="activitiesdiscount_amount[]"
                                                       value="<?php echo $activity['discount_amount']; ?>"
                                                       class="activitiesdiscount_amount_0<?php echo $activity['id']; ?>"/>
                                                <input type="hidden" name="activitiesvat_amount[]"
                                                       value="<?php echo $activity['vat_amount']; ?>"
                                                       class="activitiesvat_amount_0<?php echo $activity['id']; ?>"/>
                                                <td><select name="activities[]"
                                                            id="activitiesdropdownid_0<?php echo $activity['id']; ?>"
                                                            required
                                                            class="limitedNumbSelectedSingleactivities getactivitiesdropdown_0<?php echo $activity['id']; ?>"><?php echo getSelectedDropDownDataspecialpurpose('pm_activity', 'activityname', 'Activity', $db, $activity['id']) ?></select>
                                                <td><input type="text"
                                                           class="activitiesadult_0<?php echo $activity['id']; ?>"
                                                           name="activitiesadult[]"
                                                           value="<?php echo $activity['adult']; ?>"
                                                           onkeyup="findadultact(<?php echo $activity['id']; ?>)"
                                                           size="4"/></td>
                                                <td><input type="text"
                                                           class="activitieschild_0<?php echo $activity['id']; ?>"
                                                           name="activitieschild[]"
                                                           value="<?php echo $activity['child']; ?>"
                                                           onkeyup="findchildact(<?php echo $activity['id']; ?>)"
                                                           size="4"/></td>
                                                <td><select name="activitiesunit[]"
                                                            id="activitiesunitdropdownid_0<?php echo $activity['id']; ?>"
                                                            required
                                                            class="limitedNumbSelectedSingleactivitiesunit test_0<?php echo $activity['id']; ?>"><?php echo getDropDownDataUnit('Unit', $activity['unit_id']) ?></select>
                                                </td>
                                                <td><input type="text"
                                                           class="activitiesquantity_0<?php echo $activity['id']; ?>"
                                                           name="activitiesquantity[]"
                                                           value="<?php echo $activity['quantity']; ?>" size="4"
                                                           onkeyup="updatenetpriceactivitieseditrow(0<?php echo $activity['id']; ?>)"/>
                                                </td>
                                                <td><input type="text"
                                                           class="activitiescharges_0<?php echo $activity['id']; ?>"
                                                           name="activitiescharges[]"
                                                           value="<?php echo $activity['charges']; ?>" size="4"
                                                           onkeyup="updatenetpriceactivitieseditrow(0<?php echo $activity['id']; ?>)"/>
                                                </td>
                                                <td><input type="text"
                                                           class="activitiesdiscount_0<?php echo $activity['id']; ?>"
                                                           name="activitiesdiscount[]"
                                                           value="<?php echo $activity['discount']; ?>" size="4"
                                                           onkeyup="updatenetpriceactivitieseditrow(0<?php echo $activity['id']; ?>)"/>
                                                </td>
                                                <td><select name="taxdropdownactivities[]"
                                                            id="taxdropdownidactivities_0<?php echo $activity['id']; ?>"
                                                            required
                                                            class="limitedNumbSelectedSingleactivitiestax taxdropdownactivities_<?php echo $activity['id']; ?>"
                                                            onchange="taxactivitieseditchange(<?php echo $activity['id']; ?>)"><?php echo getSelectedDropDownDataspecialpurpose('pm_tax', 'taxname', 'Tax', $db, $activity['tax_rate']) ?></select>
                                                </td>
                                                <td><input type="text"
                                                           class="activitiesnetamount_0<?php echo $activity['id']; ?>"
                                                           name="activitiesnetamount[]"
                                                           value="<?php echo $activity['net_amount']; ?>" size="8"
                                                           disabled/>
                                                </td>
                                                <td>
                                                    <a class="btn btn-xs delete_recordactivities_0<?php echo $activity['id']; ?>"
                                                       data-id="1"><i
                                                                class="glyphicon glyphicon-trash trash-rowactivities_0<?php echo $activity['id']; ?> remove_rowactivities_0<?php echo $activity['id']; ?>"
                                                                onclick="deleteactivity(<?php echo $activity['id']; ?>);"></i></a>
                                                </td>
                                            </tr>
                                        <?php }
                                    }//end of if edit id
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </form>
            </div>
            <div id="addOn" class="tab-pane fade">
                <div class="row">
                    <div class="col-md-4 mb10">
                        <h5 class="data_success" style="color: green;font-size:small;"></h5>
                    </div>
                </div>
                <?php
                } ?>
            </div>
        </div>
    </div>
</div>

<!--//end copy-->
</div>
<!--end cotainer-fluid-->

</form>


<?php
} ?>
</div>
</div>
</form>
<?php
} else echo '<p>' . $texts['ACCESS_DENIED'] . '</p>'; ?>
</div>
<?php

?>

</body>
</html>
<?php
if (empty($_SESSION['msg_error'])) recursive_rmdir(SYSBASE . 'medias/' . MODULE . '/tmp/' . $_SESSION['token']);
$_SESSION['redirect'] = false;
$_SESSION['msg_error'] = array();
$_SESSION['msg_success'] = array();
$_SESSION['msg_notice'] = array(); ?>

<!---->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
<script type="text/javascript">
    $(document).ready(function () {
        $(".limitedNumbChosenSinglebooking").select2({
            width: '100%',
            dropdownAutoWidth: true
        });
        $(".limitedNumbSelectedSingleactivities").select2({width: '100px', dropdownAutoWidth: true});
        $(".limitedNumbSelectedSingleactivitiestax").select2({width: '100px', dropdownAutoWidth: true});
        $(".limitedNumbSelectedSingleactivitiesunit").select2({width: '100px', dropdownAutoWidth: true});

    });
    $(".delete-record").click(function () {
        var booking_id = $(".booking_id").val();
        var remove_id = $(this).attr("data-id");
        $(this).closest('tr').remove();
        //this request will remove the room id from temp booking table
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'booking_id': booking_id,
                'delete_room': 1
            },
            dataType: 'JSON',
            success: function (data) {

            }

        });
    });

    function deleteservice(id) {
        var service_id = $(".delete_record_0" + id).val();
        $(this).closest('tr').remove();
        //this request will remove the room id from temp booking table
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'service_id': service_id,
                'delete_service': 1
            },
            dataType: 'JSON',
            success: function (data) {

            }

        });

    }

    function deleteactivity(id) {
        var activity_id = $(".remove_activities_0" + id).val();
        $(this).closest('tr').remove();
        //this request will remove the room id from temp booking table
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'activity_id': activity_id,
                'delete_activity': 1
            },
            dataType: 'JSON',
            success: function (data) {

            }

        });

    }

    function findadult(id) {
        $(".error").html("");
        var maxadults = $(".maxadults" + id).val();
        if (maxadults != '') {
            if (isNaN(maxadults)) {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'remove_id': id,
                        'comparechild': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        if (parseFloat(maxadults) > parseFloat(data[0].maxadults)) {
                            $(".error").html("");
                            $(".error").append("<span><strong>Adults cannot be alphabets in " + data[0].roomtitle + "</strong></span>");
                        }
                    }
                });
            }
            else {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'remove_id': id,
                        'compareadults': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        if (parseFloat(maxadults) > parseFloat(data[0].maxadults)) {
                            $(".error").html("");
                            $(".error").append("<span><strong>Adults cannot be more than " + data[0].maxadults + " in " + data[0].roomtitle + "</strong></span>");
                        }
                    }
                });

            }
        }
    }

    function findchild(id) {
        $(".error").html("");
        var maxchild = $(".maxchild" + id).val();
        if (maxchild != '') {
            if (isNaN(maxchild)) {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'remove_id': id,
                        'comparechild': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        if (parseFloat(maxchild) > parseFloat(data[0].maxchild)) {
                            $(".error").html("");
                            $(".error").append("<span><strong>Childs cannot be alphabets in " + data[0].roomtitle + " </strong></span>");
                        }

                    }

                });
            } else {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'remove_id': id,
                        'comparechild': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        if (parseFloat(maxchild) > parseFloat(data[0].maxchild)) {
                            $(".error").html("");
                            $(".error").append("<span><strong>Childs cannot be more than " + data[0].maxchild + " in " + data[0].roomtitle + "</strong></span>");
                        }

                    }

                });
            }
        }

    }

    function updatenetprice(id) {
        $(".booking_save").prop('disabled', true);
        $(".netamount" + id).val('');
        var nightcharge = $(".nightcharge" + id).val();
        var staynight = $(".staynight" + id).val();
        var tax_id = $("#tax_id" + id).val();
        if (isNaN(staynight)) {
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'remove_id': id,
                    'comparechild': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".error").html("");
                    $(".error").append("<span>Please Use Numeric value Instead " + staynight + " in " + data[0].roomtitle + " </span>");
                    return false;
                }
            });
        } else {
            $(".error").html("");
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'tax_id': tax_id,
                    'gettaxrate': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".error").html("");
                    //totalnetamount without vat
                    $(".booking_save").prop('disabled', false);
                    var discount = 0;
                    var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                    var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                    var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                    var taxamount = parseFloat(data[0].taxrates);
                    var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                    $(".discount_amount" + id).val((getdiscountamount).toFixed(2));
                    $(".vat_amount" + id).val((getvatamount).toFixed(2));
                    var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                    $(".netamount" + id).val(totalnet.toFixed(2));
                }
            });
        }
    }

    function updatenetpricewithdiscount(id, event) {
        $(".booking_save").prop('disabled', true);
        $(".netamount" + id).val('');
        var nightcharge = $(".nightcharge" + id).val();
        var staynight = $(".staynight" + id).val();
        var discount = $(".discount" + id).val();
        var tax_id = $("#tax_id" + id).val();
        if (staynight != '') {
            $(".error").html("");
            if (discount == '') {
                discount = 0;
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'tax_id': tax_id,
                        'gettaxrate': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        $(".booking_save").prop('disabled', false);
                        var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                        var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                        var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                        var taxamount = parseFloat(data[0].taxrates);
                        var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                        $(".discount_amount" + id).val((getdiscountamount).toFixed(2));
                        $(".vat_amount" + id).val((getvatamount).toFixed(2));
                        var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                        $(".netamount" + id).val(totalnet.toFixed(2));
                    }
                });
            } else {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'tax_id': tax_id,
                        'gettaxrate': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        $(".booking_save").prop('disabled', false);
                        var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                        var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                        var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                        var taxamount = parseFloat(data[0].taxrates);
                        var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                        var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                        $(".discount_amount" + id).val((getdiscountamount).toFixed(2));
                        $(".vat_amount" + id).val((getvatamount).toFixed(2));
                        $(".netamount" + id).val(totalnet.toFixed(2));
                    }
                });
            }
        } else {
            $(".error").html("");
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'remove_id': id,
                    'comparechild': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".error").html("");
                    $(".error").append("<span>Number of nights cannot be empty in " + data[0].roomtitle + " </span>");
                    return false;
                }
            });
        }
    }

    function getuser() {
        var customer = $("#customername").val();
        var customername = [];
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'customer': customer,
                'get_user': 1
            },
            dataType: 'JSON',
            success: function (data) {
                var customername = data;
                $(".firstname").val(data[0].label);
                $(".lastname").val(data[0].lastname);
                $(".email").val(data[0].email);
                $(".country").val(data[0].country);
                $(".company").val(data[0].company);
                $(".city").val(data[0].city);
                $(".mobile").val(data[0].mobile);
                $(".phone").val(data[0].phone);
                $(".address").val(data[0].address);
                $("#customername").autocomplete({
                    minLength: 0,
                    source: customername,
                    focus: function (event, ui) {
                        $("#customername").val(ui.item.label);
                        return false;
                    },
                    select: function (event, ui) {
                        $("#customername").val(ui.item.label);
                        $("#customername-id").val(ui.item.value);
                        return false;
                    }
                })
                    .data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li>")
                        .append("<a>" + item.label + "</a>")
                        .appendTo(ul);
                };
            }

        });
    }

    function service_tab() {
        $(".data_success").html("");
        $(".delete_record_0").show();
        $(".errorservices").html("");
    }

    function updatenetpriceservice(id) {
        $(".errorservices").html("");
        $(".servicenetamount_00").val('');
        $(".service_save").prop('disabled', true);
        var servicecharges = $(".servicecharges_00").val();
        var servicequantity = $(".servicequantity_00").val();
        var servicetaxid = $(".taxdropdown_00").val();
        var servicediscount = $(".servicediscount_00").val();
        var selected_service_id = $('.serviceid_00').val();
        if (servicecharges <= 0 || servicecharges == '') {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use value Greater in charges than " + servicecharges + " For ID " + selected_service_id + "</strong></span>");
            return false;
        }
        if (isNaN(servicecharges)) {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use Numeric value in charges Instead " + servicecharges + " For ID " + selected_service_id + " </strong></span>");
            return false;
        }
        if (servicequantity <= 0 || servicequantity == '') {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use value Greater in quantity than " + servicequantity + " For ID " + selected_service_id + "</strong></span>");
            return false;
        }
        if (isNaN(servicequantity)) {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use Numeric value in quantity Instead " + servicequantity + " For ID " + selected_service_id + " </strong></span>");
            return false;
        }
        if (servicecharges != '' && servicequantity != '' && !isNaN(servicequantity) && !isNaN(servicecharges)) {
            $(".errorservices").html("");
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'tax_id': servicetaxid,
                    'gettaxrate': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".errorservices").html("");
                    //totalnetamount without vat
                    if (servicediscount != '') {
                        if (servicediscount <= 0) {
                            $(".errorservices").html("");
                            $(".errorservices").append("<strong><span>Please Use value Greater in discount than " + servicediscount + " For ID " + selected_service_id + " </strong></span>");
                            return false;
                        } else if (isNaN(servicediscount)) {
                            $(".errorservices").html("");
                            $(".errorservices").append("<strong><span>Please Use Numeric value in discount Instead " + servicediscount + " For ID " + selected_service_id + " </strong></span>");
                            return false;
                        } else {
                            $(".service_save").prop('disabled', false);
                            var totalnetamountvalservices = (parseFloat(servicecharges)) * (parseFloat(servicequantity));
                            var getdiscountamount = ((totalnetamountvalservices) * (servicediscount / 100 ));
                            var taxamount = parseFloat(data[0].taxrates);
                            var getvatamountservices = ((getdiscountamount) * (taxamount / 100 ));
                            $(".servicenetamount_00").val(getvatamountservices.toFixed(2));
                        }
                    } else {
                        $(".service_save").prop('disabled', false);
                        var totalnetamountvalservices = (parseFloat(servicecharges)) * (parseFloat(servicequantity));
                        var taxamount = parseFloat(data[0].taxrates);
                        var getvatamountservices = ((totalnetamountvalservices) * (taxamount / 100 ));
                        var totalnetservices = (totalnetamountvalservices + getvatamountservices);
                        $(".servicenetamount_00").val(totalnetservices.toFixed(2));
                    }
                }
            });
        }
    }

    var i = 0;

    function add_room() {
        debugger;
        $("#tbl_posts_body").append('<tr id="rec_' + i + '"><td>' +
            '<input type="hidden" class="roomid' + i + '" name="room_id[]"><input type="hidden" class="taxbooking_id' + i + '" name="taxbooking_id[]"><input type="hidden" class="discount_amount' + i + '" name="discount_amount[]"><input type="hidden" class="vat_amount' + i + '" name="vat_amount[]">' +
            '<input type="hidden" name="room_book_id[]"><span class="roomcode' + i + '" name="roomcode[]"></span></td>' +
            '                            <td><select name="room[]" id="roomdropdownid_' + i + '" required class="roomdropdown_' + i + ' room_' + i + '"><?php echo getDropDownDataspecialpurpose('pm_room', 'roomtitle', 'Room', $db) ?></select></td>' +
            '                            <td><span class="roomtitle' + i + '" name="roomtitle[]"></span></td>' +
            '                            <td><select name="roomunit[]" id="roomunitdropdownid_' + i + '" required class="roomunitdropdown_' + i + ' roomunit_' + i + '"><?php echo getDropDownDataUnit('Unit', '') ?></select></td>' +
            '                            <td><input type="text" class="nightcharge' + i + '" disabled name="nightcharge[]" value=""size="4"/></td>' +
            '                            <td><input type="text" name="adults[]" class="maxadults' + i + '"" value="" size="4"/></td>' +
            '                            <td><input type="text" name="children[]" class="maxchild' + i + '" value="" size="4"/></td>' +
            '                            <td><input type="text" name="night[]" class="staynight' + i + '" value="" size="4"/></td>' +
            '                            <td><input type="text" class="discount' + i + '" name="discount[]" value="" size="4"/></td>' +
            '                            <td><select name="taxdropdownroom[]" id="taxdropdownroom_' + i + '" required class="taxdropdownroom_' + i + '"><?php echo getSelectedDropDownDataspecialpurpose('pm_tax', 'taxname', 'Tax', $db, '') ?></select></td>' +
            '                            <td><input type="text" class="roomnetamount_' + i + '" name="netamount[]" value="" size="6" disabled/></td>' +
            '                            <td><a class="btn btn-xs delete_record_' + i + '" data-id="1"><i class="glyphicon glyphicon-trash trash-row_' + i + ' remove_row_' + i + '" "></i></a></td></tr>');
        $(".taxdropdownroom_" + i).select2({
            width: '100%',
            dropdownAutoWidth: true
        });
        $(".roomunitdropdown_" + i).select2({
            width: '100%',
            dropdownAutoWidth: true
        });
        $(".roomdropdown_" + i).select2({
            width: '100%',
            dropdownAutoWidth: true
        }).change(function (evt) {
            var roomid = $(evt.target).val();
            $("room_id" + i).val(roomid);
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'roomid': roomid,
                    'getroomdata': 1
                },
                dataType: 'JSON',
                success: function (data) {

                    $(".roomid" + i).val(data[0].room_id);
                    $(".roomcode" + i).text(data[0].roomcode);
                    $(".roomtitle" + i).text(data[0].roomtitle);
                    $(".nightcharge" + i).val(data[0].price);
                    $(".maxadults" + i).val(data[0].maxadults);
                    $(".maxchild" + i).val(data[0].maxchild);
                    $(".taxdropdownroom_" + i).select2().val(data[0].taxrates);
                    $(".taxdropdownroom_" + i).select2({
                        width: '100%',
                        dropdownAutoWidth: true
                    });
                }
            });
        });

        $(".delete_record_" + i).click(function () {
            $(this).closest('tr').remove();
        });
        $(".maxadults" + i).on('keyup', function () {
            $(".error").html("");
            var id = $(this).closest('tr').find(".roomid" + i).val();
            var maxadults = $(".maxadults" + i).val();
            if (maxadults != '') {
                if (isNaN(maxadults)) {
                    $.ajax({
                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                        type: 'POST',
                        data: {
                            'remove_id': id,
                            'comparechild': 1
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            if (parseFloat(maxadults) > parseFloat(data[0].maxadults)) {
                                $(".error").html("");
                                $(".error").append("<span><strong>Adults cannot be alphabets in " + data[0].roomtitle + "</strong></span>");
                            }
                        }
                    });
                }
                else {
                    $.ajax({
                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                        type: 'POST',
                        data: {
                            'remove_id': id,
                            'compareadults': 1
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            if (parseFloat(maxadults) > parseFloat(data[0].maxadults)) {
                                $(".error").html("");
                                $(".error").append("<span><strong>Adults cannot be more than " + data[0].maxadults + " in " + data[0].roomtitle + "</strong></span>");
                            }
                        }
                    });

                }
            }

        });
        $(".maxchild" + i).on('keyup', function () {
            $(".error").html("");
            var maxchild = $(".maxchild" + i).val();
            var id = $(this).closest('tr').find(".roomid" + i).val();
            if (maxchild != '') {
                if (isNaN(maxchild)) {
                    $.ajax({
                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                        type: 'POST',
                        data: {
                            'remove_id': id,
                            'comparechild': 1
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            if (parseFloat(maxchild) > parseFloat(data[0].maxchild)) {
                                $(".error").html("");
                                $(".error").append("<span><strong>Childs cannot be alphabets in " + data[0].roomtitle + " </strong></span>");
                            }

                        }

                    });
                } else {
                    $.ajax({
                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                        type: 'POST',
                        data: {
                            'remove_id': id,
                            'comparechild': 1
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            if (parseFloat(maxchild) > parseFloat(data[0].maxchild)) {
                                $(".error").html("");
                                $(".error").append("<span><strong>Childs cannot be more than " + data[0].maxchild + " in " + data[0].roomtitle + "</strong></span>");
                            }

                        }

                    });
                }
            }

        });
        $(".staynight" + i).on('keyup', function () {

            $(".booking_save").prop('disabled', true);
            $(".roomnetamount" + i).val('');
            var id = $(this).closest('tr').find(".roomid" + i).val();
            var nightcharge = $(".nightcharge" + i).val();
            var staynight = $(".staynight" + i).val();
            var tax_id = $(".taxdropdownroom_" + i).val();
            $(".taxbooking_id" + i).val(tax_id);
            if (isNaN(staynight)) {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'remove_id': id,
                        'comparechild': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        $(".error").html("");
                        $(".error").append("<span>Please Use Numeric value Instead " + staynight + " in " + data[0].roomtitle + " </span>");
                        return false;
                    }
                });
            } else {
                $(".error").html("");
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'tax_id': tax_id,
                        'gettaxrate': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        $(".error").html("");
                        //totalnetamount without vat
                        $(".booking_save").prop('disabled', false);
                        var discount = 0;
                        var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                        var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                        var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                        var taxamount = parseFloat(data[0].taxrates);
                        var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                        var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                        $(".discount_amount" + i).val(getdiscountamount.toFixed(2));
                        $(".vat_amount" + i).val(getvatamount.toFixed(2));
                        $(".roomnetamount_" + i).val(totalnet.toFixed(2));
                    }
                });
            }
        });
        $(".discount" + i).on('keyup', function () {
            $(".booking_save").prop('disabled', true);
            $(".roomnetamount_" + i).val('');
            var id = $(this).closest('tr').find(".roomid" + i).val();
            var nightcharge = $(".nightcharge" + i).val();
            var staynight = $(".staynight" + i).val();
            var discount = $(".discount" + i).val();
            var tax_id = $(".taxdropdownroom_" + i).val();
            $(".taxbooking_id" + i).val(tax_id);
            if (staynight != '') {
                $(".error").html("");
                if (discount == '') {
                    discount = 0;
                    $.ajax({
                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                        type: 'POST',
                        data: {
                            'tax_id': tax_id,
                            'gettaxrate': 1
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            $(".booking_save").prop('disabled', false);
                            var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                            var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                            var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                            var taxamount = parseFloat(data[0].taxrates);
                            var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                            var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                            $(".discount_amount" + i).val(getdiscountamount.toFixed(2));
                            $(".vat_amount" + i).val(getvatamount.toFixed(2));
                            $(".roomnetamount_" + i).val(totalnet.toFixed(2));
                        }
                    });
                } else {
                    $.ajax({
                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                        type: 'POST',
                        data: {
                            'tax_id': tax_id,
                            'gettaxrate': 1
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            $(".booking_save").prop('disabled', false);
                            var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                            var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                            var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                            var taxamount = parseFloat(data[0].taxrates);
                            var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                            var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                            $(".discount_amount" + i).val(getdiscountamount.toFixed(2));
                            $(".vat_amount" + i).val(getvatamount.toFixed(2));
                            $(".roomnetamount_" + i).val(totalnet.toFixed(2));
                        }
                    });
                }
            } else {
                $(".error").html("");
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'remove_id': id,
                        'comparechild': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        $(".error").html("");
                        $(".error").append("<span>Number of nights cannot be empty in " + data[0].roomtitle + " </span>");
                        return false;
                    }
                });
            }


        });
        $(".taxdropdownroom_" + i).change(function (event) {
            $(".error").html("");
            if (event.target == this) {
                var selectedarrayval = $(this).val();
                $(".taxdropdownroom_" + i).val(selectedarrayval);
                $(".taxbooking_id" + i).val(selectedarrayval);
                $(".roomnetamount_" + i).val('');
                var tax_id = selectedarrayval;
                var nightcharge = $(".nightcharge" + i).val();
                var staynight = $(".staynight" + i).val();
                var discount = $(".discount" + i).val();
                var id = $(this).closest('tr').find(".roomid" + i).val();
                if (staynight == '') {
                    $.ajax({
                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                        type: 'POST',
                        data: {
                            'remove_id': id,
                            'comparechild': 1
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            $(".error").html("");
                            $(".error").append("<span>No of Nights Stay cannot be empty in " + data[0].roomtitle + " </span>");
                            return false;
                        }
                    });
                } else {
                    if (isNaN(staynight)) {
                        $.ajax({
                            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                            type: 'POST',
                            data: {
                                'remove_id': id,
                                'comparechild': 1
                            },
                            dataType: 'JSON',
                            success: function (data) {
                                $(".error").html("");
                                $(".error").append("<span>Please Use Numeric value Instead " + staynight + " in " + data[0].roomtitle + " </span>");
                                return false;
                            }
                        });
                    }
                    else {
                        $(".error").html("");
                        $.ajax({
                            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                            type: 'POST',
                            data: {
                                'tax_id': tax_id,
                                'gettaxrate': 1
                            },
                            dataType: 'JSON',
                            success: function (data) {
                                $(".error").html("");
                                //totalnetamount without vat
                                if (discount == '') {
                                    discount = 0;
                                    var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                                    var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                                    var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                                    var taxamount = parseFloat(data[0].taxrates);
                                    var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                                    var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                                    $(".discount_amount" + i).val(getdiscountamount.toFixed(2));
                                    $(".vat_amount" + i).val(getvatamount.toFixed(2));
                                    $(".roomnetamount_" + i).val(totalnet.toFixed(2));
                                } else {
                                    var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                                    var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                                    var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                                    var taxamount = parseFloat(data[0].taxrates);
                                    var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                                    var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                                    $(".discount_amount" + i).val(getdiscountamount.toFixed(2));
                                    $(".vat_amount" + i).val(getvatamount.toFixed(2));
                                    $(".roomnetamount_" + i).val(totalnet.toFixed(2));
                                }
                            }
                        });
                    }
                }
            }
        });
        $(".roomunitdropdown" + i).change(function (event) {
            $(".error").html();
            if (event.target == this) {
                var selectedarrayval = $(this).val();
                $(".roomunitdropdown" + i).val(selectedarrayval);
            }
        });


    }

    function add_row() {
        $(".delete_record_00").hide();
        $(".errorservices").html("");
        //  $(".delete_plus_"+i).hide();
        i++;
        $('.services_data').append('<tr id="rec-1">' +
            '                                                             <td><input type="text" class="serviceid_' + i + '" name="serviceid[]" value="" size="4" disabled/></td>' +
            '                                                            <input type="hidden" class="taxserviceid_' + i + '" name="taxservice[]"/>' +
            '                                                            <input type="hidden" class="serviceunitid_' + i + '" name="serviceunitid[]"/>' +
            '                                                            <input type="hidden" name="servicediscount_amount[]" class="servicediscount_amount_' + i + '"/>' +
            '                                                            <input type="hidden" name="service_package_id[]"/>' +
            '                                                            <input type="hidden" name="servicevat_amount[]"class="servicevat_amount_' + i + '"/>' +
            '                                                            <td><select name="servicedropdown[]" id="servicedropdownid_' + i + '" required class="limitedNumbSelectedSingle_' + i + ' getservicedropdown_' + i + '" "><?php echo getDropDownDataspecialpurpose('pm_service', 'servicetitle', 'Service', $db) ?>' + '</select>' +
            '                                                            </td>' +
            '                                                            <td><select name="serviceunit[]" id="serviceunitdropdownid_' + i + '" required class="limitedNumbSelectedSingle_' + i + ' serviceunitdropdown_' + i + '"><?php echo getDropDownDataUnit('Unit', '') ?>' + '</select>' +
            '                                                            </td>' +
            '                                                            <td><input type="text" class="servicequantity_' + i + '" name="servicequantity[]" value="" size="4" onkeyup="updatenetpriceserviceaddrow(' + i + ')"/>' +
            '                                                            </td>' +
            '                                                            <td><input type="text" class="servicecharges_' + i + '" name="servicecharges[]" value="" size="4" onkeyup="updatenetpriceserviceaddrow(' + i + ')"/>' +
            '                                                            </td>' +
            '                                                            <td><input type="text" class="servicediscount_' + i + '" name="servicediscount[]" value="" size="4" onkeyup="updatenetpriceserviceaddrow(' + i + ')"/></td>' +
            '                                                            <td><select name="taxdropdown[]" id="taxdropdownid_' + i + '" required class="limitedNumbSelectedSingle_' + i + ' taxdropdown_' + i + '"><?php echo getDropDownDataspecialpurpose('pm_tax', 'taxname', 'Tax', $db) ?>' + '</select>' + '                                                            </td>' +
            '                                                            <td><input type="text" class="servicenetamount_' + i + '" name="servicenetamount[]" value="" size="8" disabled/>' +
            '                                                            </td>' +
            '                                                            <td><a class="btn btn-xs delete_record_' + i + '" data-id="1"><i class="glyphicon glyphicon-trash trash-row_' + i + ' remove_row_' + i + '" "></i></a>' +
            '                                                            </td>' +
            '                                                        </tr>');

        $(".limitedNumbSelectedSingle_" + i).select2({
            dropdownAutoWidth: true
        });
        $(".getservicedropdown_" + i).select2({
            dropdownAutoWidth: true
        }).bind('change', function (evt) {
            $(".errorservices").html("");
            var serviceid = $(evt.target).val();
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'serviceid': serviceid,
                    'getservicedata': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".serviceid_" + i).val(data[0].id);
                    $(".serviceunitdropdown_" + i).select2();
                    $(".serviceunitdropdown_" + i).val(data[0].servicechargesunit);
                    $(".servicecharges_" + i).val(data[0].servicecharges);
                    $(".taxdropdown_" + i).select2().val(data[0].taxes);
                    $(".taxserviceid_" + i).val(data[0].taxes);
                    $(".serviceunitid_" + i).val(data[0].servicechargesunit);
                    updatenetpriceserviceaddrow(i);
                }
            });
        });
        $(".taxdropdown_" + i).change(function (event) {
            $(".errorservices").html("");
            if (event.target == this) {
                var selectedarrayval = $(this).val();
                $(".taxserviceid_" + i).val(selectedarrayval);
            }
            updatenetpriceserviceaddrow(i);
        }).change();

        $(".serviceunitdropdown_" + i).change(function (event) {
            $(".errorservices").html("");
            if (event.target == this) {
                var selectedarrayval = $(this).val();
                $(".serviceunitid_" + i).val(selectedarrayval);
            }
            updatenetpriceserviceaddrow(i);
        }).change();
        $(".delete_record_" + i).click(function () {
            $(this).closest('tr').remove();
        });


    }

    function updatenetpriceserviceaddrow(id) {
        $(".service_save").prop('disabled', true);
        $(".errorservices").html("");
        $(".servicenetamount_" + id).val('');
        var servicecharges = $(".servicecharges_" + id).val();
        var servicequantity = $(".servicequantity_" + id).val();
        var servicetaxid = $(".taxdropdown_" + id).val();
        var servicediscount = $(".servicediscount_" + id).val();
        var selected_service_id = $(".serviceid_" + id).val();
        if (servicecharges <= 0 || servicecharges == '') {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use value Greater in charges than " + servicecharges + " For ID " + selected_service_id + "</strong></span>");
            return false;
        }
        if (isNaN(servicecharges)) {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use Numeric value in charges Instead " + servicecharges + " For ID " + selected_service_id + " </strong></span>");
            return false;
        }
        if (servicequantity <= 0 || servicequantity == '') {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use value Greater in quantity than " + servicequantity + " For ID " + selected_service_id + "</span>");
            return false;
        }
        if (isNaN(servicequantity)) {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use Numeric value in quantity Instead " + servicequantity + " For ID " + selected_service_id + " </strong></span>");
            return false;
        }
        if (servicecharges != '' && servicequantity != '' && !isNaN(servicequantity) && !isNaN(servicecharges)) {
            $(".errorservices").html("");
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'tax_id': servicetaxid,
                    'gettaxrate': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".errorservices").html("");
                    //totalnetamount without vat
                    if (servicediscount != '') {
                        if (servicediscount <= 0) {
                            $(".errorservices").html("");
                            $(".errorservices").append("<span>Please Use value Greater in discount than " + servicediscount + " For ID <strong>" + selected_service_id + " </strong></span>");
                            return false;
                        } else if (isNaN(servicediscount)) {
                            $(".errorservices").html("");
                            $(".errorservices").append("<span>Please Use Numeric value in discount Instead " + servicediscount + " For ID <strong>" + selected_service_id + " </strong></span>");
                            return false;
                        } else {
                            $(".service_save").prop('disabled', false);
                            var totalnetamountvalservices = (parseFloat(servicecharges)) * (parseFloat(servicequantity));
                            var getdiscountamount = ((totalnetamountvalservices) * (servicediscount / 100 ));
                            var amountafterdiscoount = (totalnetamountvalservices - getdiscountamount);
                            var taxamount = parseFloat(data[0].taxrates);
                            var getvatamountservices = ((amountafterdiscoount) * (taxamount / 100 ));
                            $(".servicediscount_amount_" + id).val((getdiscountamount).toFixed(2));
                            $(".servicevat_amount_" + id).val((getvatamountservices).toFixed(2));
                            var totalnetservices = (amountafterdiscoount + getvatamountservices);
                            $(".servicenetamount_" + id).val(totalnetservices.toFixed(2));
                        }
                    } else {
                        $(".service_save").prop('disabled', false);
                        var totalnetamountvalservices = (parseFloat(servicecharges)) * (parseFloat(servicequantity));
                        var taxamount = parseFloat(data[0].taxrates);
                        var getvatamountservices = ((totalnetamountvalservices) * (taxamount / 100 ));
                        $(".servicediscount_amount_" + id).val(0);
                        $(".servicevat_amount_" + id).val((getvatamountservices ).toFixed(2));
                        var totalnetservices = (totalnetamountvalservices + getvatamountservices);
                        $(".servicenetamount_" + id).val(totalnetservices.toFixed(2));
                    }
                }
            });
        }
    }

    function taxserviceeditchange(id) {
        $(".errorservices").html("");
        updatenetpriceserviceeditrow(id);
    }

    function updatenetpriceserviceeditrow(id) {
        $(".service_save").prop('disabled', true);
        $(".errorservices").html("");
        $(".servicenetamount_" + id).val('');
        var servicecharges = $(".servicecharges_0" + id).val();
        var servicequantity = $(".servicequantity_0" + id).val();
        var servicetaxid = $(".taxdropdownservice_0" + id).val();
        var servicediscount = $(".servicediscount_0" + id).val();
        var selected_service_id = $(".serviceid_0" + id).val();
        if (servicecharges <= 0 || servicecharges == '') {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use value Greater in charges than " + servicecharges + " For ID " + selected_service_id + "</strong></span>");
            return false;
        }
        if (isNaN(servicecharges)) {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use Numeric value in charges Instead " + servicecharges + " For ID " + selected_service_id + " </strong></span>");
            return false;
        }
        if (servicequantity <= 0 || servicequantity == '') {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use value Greater in quantity than " + servicequantity + " For ID " + selected_service_id + "</span>");
            return false;
        }
        if (isNaN(servicequantity)) {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use Numeric value in quantity Instead " + servicequantity + " For ID " + selected_service_id + " </strong></span>");
            return false;
        }
        if (servicecharges != '' && servicequantity != '' && !isNaN(servicequantity) && !isNaN(servicecharges)) {
            $(".errorservices").html("");
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'tax_id': servicetaxid,
                    'gettaxrate': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".errorservices").html("");
                    //totalnetamount without vat
                    if (servicediscount != '') {
                        if (servicediscount <= 0) {
                            $(".errorservices").html("");
                            $(".errorservices").append("<span>Please Use value Greater in discount than " + servicediscount + " For ID <strong>" + selected_service_id + " </strong></span>");
                            return false;
                        } else if (isNaN(servicediscount)) {
                            $(".errorservices").html("");
                            $(".errorservices").append("<span>Please Use Numeric value in discount Instead " + servicediscount + " For ID <strong>" + selected_service_id + " </strong></span>");
                            return false;
                        } else {
                            $(".service_save").prop('disabled', false);
                            var totalnetamountvalservices = (parseFloat(servicecharges)) * (parseFloat(servicequantity));
                            var getdiscountamount = ((totalnetamountvalservices) * (servicediscount / 100 ));
                            var amountafterdiscoount = (totalnetamountvalservices - getdiscountamount);
                            var taxamount = parseFloat(data[0].taxrates);
                            var getvatamountservices = ((amountafterdiscoount) * (taxamount / 100 ));
                            $(".servicediscount_amount_0" + id).val((getdiscountamount).toFixed(2));
                            $(".servicevat_amount_0" + id).val((getvatamountservices).toFixed(2));
                            var totalnetservices = (amountafterdiscoount + getvatamountservices);
                            $(".servicenetamount_0" + id).val(totalnetservices.toFixed(2));
                        }
                    } else {
                        $(".service_save").prop('disabled', false);
                        var totalnetamountvalservices = (parseFloat(servicecharges)) * (parseFloat(servicequantity));
                        var taxamount = parseFloat(data[0].taxrates);
                        var getvatamountservices = ((totalnetamountvalservices) * (taxamount / 100 ));
                        $(".servicediscount_amount_0" + id).val(0);
                        $(".servicevat_amount_0" + id).val((getvatamountservices ).toFixed(2));
                        var totalnetservices = (totalnetamountvalservices + getvatamountservices);
                        $(".servicenetamount_0" + id).val(totalnetservices.toFixed(2));
                    }
                }
            });
        }
    }

    function activity_tab() {
        $(".data_success").html("");
        $(".delete_record_0").show();
        $(".erroractivities").html("");

    }

    var j = 0;

    function add_rowactivities() {
        $(".delete_recordactivities_00").hide();
        $(".erroractivities").html("");
        //  $(".delete_plus_"+i).hide();
        $(".limitedNumbSelectedSingleactivities_" + j).select2({
            width: '100%',
            dropdownAutoWidth: true
        });
        j++;
        $('.activities_data').append('<tr id="rec-1">' +
            '                                                            <td><input type="text" class="activitiesid_' + j + '" name="activitiesid[]" value="" size="4" disabled/></td>' +
            '                                                            <input type="hidden" class="taxactivitiesid_' + j + '" name="taxactivitiesid[]" />' +
            '                                                            <input type="hidden" class="activitiesunitid_' + j + '" name="activitiesunitid[]" />' +
            '                                                            <input type="hidden" name="activitiesdiscount_amount[]"class="activitiesdiscount_amount_' + j + '"/>' +
            '                                                            <input type="hidden" name="activity_package_id[]"/>' +
            '                                                            <input type="hidden" name="activitiesvat_amount[]" class="activitiesvat_amount_' + j + '"/>' +
            '                                                            <td><select name="activities[]" id="activitiesdropdownid_' + j + '" required class="limitedNumbSelectedSingleactivities_' + j + ' getactivitiesdropdown_' + j + '" "><?php echo getDropDownDataspecialpurpose('pm_activity', 'activityname', 'Activity', $db) ?>' + '</select>' +
            '                                                            <td><input type="text" class="activitiesadult_' + j + '" name="activitiesadult[]" value="" size="4"/></td>' +
            '                                                            <td><input type="text" class="activitieschild_' + j + '" name="activitieschild[]" value="" size="4"/>' +
            '                                                            </td>' +
            '                                                            <td><select name="activitiesunit[]" id="activitiesunitdropdownid_' + j + '" required class="activitiesunitdropdown_' + j + ' test_' + j + '"><?php echo getDropDownDataUnit('Unit', '') ?>' + '</select>' +
            '                                                            </td>' +
            '                                                            <td><input type="text" class="activitiesquantity_' + j + '" name="activitiesquantity[]" value="" size="4" onkeyup="updatenetpriceactivitiesaddrow(' + j + ')"/>' +
            '                                                            </td>' +
            '                                                            <td><input type="text" class="activitiescharges_' + j + '" name="activitiescharges[]" value="" size="4" onkeyup="updatenetpriceactivitiesaddrow(' + j + ')"/>' +
            '                                                            </td>' +
            '                                                            <td><input type="text" class="activitiesdiscount_' + j + '" name="activitiesdiscount[]" value="" size="4" onkeyup="updatenetpriceactivitiesaddrow(' + j + ')"/></td>' +
            '                                                            <td><select name="taxdropdownactivities[]" id="taxdropdownidactivities_' + j + '" required class="limitedNumbSelectedSingleactivities_' + j + ' taxdropdownactivities_' + j + '"><?php echo getDropDownDataspecialpurpose('pm_tax', 'taxname', 'Tax', $db) ?>' + '</select>' + '                                                            </td>' +
            '                                                            <td><input type="text" class="activitiesnetamount_' + j + '" name="activitiesnetamount[]" value="" size="8" disabled/>' +
            '                                                            </td>' +
            '                                                            <td><a class="btn btn-xs delete_recordactivities_' + j + '" data-id="1"><i class="glyphicon glyphicon-trash trash-rowactivities_' + j + ' remove_rowactivities_' + j + '" "></i></a>' +
            '                                                            </td>' +
            '                                                        </tr>');

        $(".limitedNumbSelectedSingleactivities_" + j).select2({width: '100%'});
        $('.test_' + j).select2({width: '100%', dropdownAutoWidth: true});
        $(".getactivitiesdropdown_" + j).select2().bind('change', function (evt) {
            $(".erroractivities").html("");
            var activityid = $(evt.target).val();
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'activityid': activityid,
                    'getactivitydata': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".activitiesid_" + j).val(data[0].id);
                    $(".activitiesadult_" + j).val(data[0].maxadults);
                    $(".activitieschild_" + j).val(data[0].maxchild);
                    $(".activitiesunitdropdown_" + j).select2({width: '85%'});
                    $(".activitiesunitdropdown_" + j).val(data[0].activityunit);
                    $(".activitiescharges_" + j).val(data[0].price);
                    $(".taxdropdownactivities_" + j).select2({width: '85%'}).val(data[0].taxrates);
                    $(".taxactivitiesid_" + j).val(data[0].taxrates);
                    $(".activitiesunitid_" + j).val(data[0].activityunit);
                    updatenetpriceactivitiesaddrow(j);

                }
            });
        }).select2({width: '100%', dropdownAutoWidth: true});
        $(".taxdropdownactivities_" + j).change(function (evt) {
            $(".erroractivities").html("");
            if (evt.target == this) {
                var selectedarrayval = $(this).val();
                $(".taxactivitiesid_" + j).val(selectedarrayval);
            }
            updatenetpriceactivitiesaddrow(j);

        }).change();
        $(".activitiesunitdropdown_" + j).change(function (evt) {
            if (evt.target == this) {
                var selectedarrayval = $(this).val();
                $(".activitiesunitid_" + j).val(selectedarrayval);
            }
            updatenetpriceservice();
        }).change();
        $(".delete_recordactivities_" + j).click(function () {
            $(this).closest('tr').remove();
        });


    }

    function updatenetpriceactivitiesaddrow(id) {
        $(".activity_save").prop('disabled', true);
        $(".erroractivities").html("");
        $(".activitiesnetamount_" + id).val('');
        var activitiescharges = $(".activitiescharges_" + id).val();
        var activitiesquantity = $(".activitiesquantity_" + id).val();
        var activitiestaxid = $(".taxdropdownactivities_" + id).val();
        var activitiesdiscount = $(".activitiesdiscount_" + id).val();
        var selected_activities_id = $(".activitiesid_" + id).val();
        if (activitiescharges <= 0 || activitiescharges == '') {
            $(".erroractivities").html("");
            $(".erroractivities").append("<strong><span>Please Use value Greater in charges than " + activitiescharges + " For ID " + selected_activities_id + "</strong></span>");
            return false;
        }
        if (isNaN(activitiescharges)) {
            $(".erroractivities").html("");
            $(".erroractivities").append("<strong><span>Please Use Numeric value in charges Instead " + activitiescharges + " For ID " + selected_activities_id + " </strong></span>");
            return false;
        }
        if (activitiesquantity <= 0 || activitiesquantity == '') {
            $(".erroractivities").html("");
            $(".erroractivities").append("<strong><span>Please Use value Greater in quantity than " + activitiesquantity + " For ID " + selected_activities_id + "</span>");
            return false;
        }
        if (isNaN(activitiesquantity)) {
            $(".erroractivities").html("");
            $(".erroractivities").append("<strong><span>Please Use Numeric value in quantity Instead " + activitiesquantity + " For ID " + selected_activities_id + " </strong></span>");
            return false;
        }
        if (activitiescharges != '' && activitiesquantity != '' && !isNaN(activitiesquantity) && !isNaN(activitiescharges)) {
            $(".erroractivities").html("");
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'tax_id': activitiestaxid,
                    'gettaxrate': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".erroractivities").html("");
                    //totalnetamount without vat
                    if (activitiesdiscount != '') {
                        if (activitiesdiscount <= 0) {
                            $(".erroractivities").html("");
                            $(".erroractivities").append("<span>Please Use value Greater in discount than " + activitiesdiscount + " For ID <strong>" + selected_activities_id + " </strong></span>");
                            return false;
                        } else if (isNaN(activitiesdiscount)) {
                            $(".erroractivities").html("");
                            $(".erroractivities").append("<span>Please Use Numeric value in discount Instead " + activitiesdiscount + " For ID <strong>" + selected_activities_id + " </strong></span>");
                            return false;
                        } else {
                            $(".activity_save").prop('disabled', false);
                            var totalnetamountvalactivities = (parseFloat(activitiescharges)) * (parseFloat(activitiesquantity));
                            var getdiscountamountactivities = ((totalnetamountvalactivities) * (activitiesdiscount / 100 ));
                            $(".activitiesdiscount_amount_" + id).val((getdiscountamountactivities).toFixed(2));
                            var amountafterdiscoountactivities = (totalnetamountvalactivities - getdiscountamountactivities);
                            var taxamountactivities = parseFloat(data[0].taxrates);
                            var getvatamountactivities = ((amountafterdiscoountactivities) * (taxamountactivities / 100 ));
                            $(".activitiesvat_amount_" + id).val(( getvatamountactivities).toFixed(2));
                            var totalnetactivities = ( amountafterdiscoountactivities + getvatamountactivities);
                            $(".activitiesnetamount_" + id).val(totalnetactivities.toFixed(2));
                        }
                    } else {
                        var totalnetamountvalactivities = (parseFloat(activitiescharges)) * (parseFloat(activitiesquantity));
                        var taxamountactivities = parseFloat(data[0].taxrates);
                        var getvatamountactivities = ((totalnetamountvalactivities) * (taxamountactivities / 100 ));
                        var totalnetactivities = (totalnetamountvalactivities + getvatamountactivities);
                        $(".activitiesdiscount_amount_" + id).val(0);
                        $(".activitiesvat_amount_" + id).val((getvatamountactivities).toFixed(2));
                        $(".activity_save").prop("disabled", false);
                        $(".activitiesnetamount_" + id).val(totalnetactivities.toFixed(2));
                    }
                }
            });
        }
    }

    function taxactivitieseditchange(id) {
        $(".errorservices").html("");
        updatenetpriceactivitieseditrow(id);
    }

    function updatenetpriceactivitieseditrow(id) {
        $(".activity_save").prop('disabled', true);
        $(".erroractivities").html("");
        $(".activitiesnetamount_0" + id).val('');
        var activitiescharges = $(".activitiescharges_0" + id).val();
        var activitiesquantity = $(".activitiesquantity_0" + id).val();
        var activitiestaxid = $(".taxdropdownactivities_" + id).val();
        var activitiesdiscount = $(".activitiesdiscount_0" + id).val();
        var selected_activities_id = $(".activitiesid_0" + id).val();
        if (activitiescharges <= 0 || activitiescharges == '') {
            $(".erroractivities").html("");
            $(".erroractivities").append("<strong><span>Please Use value Greater in charges than " + activitiescharges + " For ID " + selected_activities_id + "</strong></span>");
            return false;
        }
        if (isNaN(activitiescharges)) {
            $(".erroractivities").html("");
            $(".erroractivities").append("<strong><span>Please Use Numeric value in charges Instead " + activitiescharges + " For ID " + selected_activities_id + " </strong></span>");
            return false;
        }
        if (activitiesquantity <= 0 || activitiesquantity == '') {
            $(".erroractivities").html("");
            $(".erroractivities").append("<strong><span>Please Use value Greater in quantity than " + activitiesquantity + " For ID " + selected_activities_id + "</span>");
            return false;
        }
        if (isNaN(activitiesquantity)) {
            $(".erroractivities").html("");
            $(".erroractivities").append("<strong><span>Please Use Numeric value in quantity Instead " + activitiesquantity + " For ID " + selected_activities_id + " </strong></span>");
            return false;
        }
        if (activitiescharges != '' && activitiesquantity != '' && !isNaN(activitiesquantity) && !isNaN(activitiescharges)) {
            $(".erroractivities").html("");
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'tax_id': activitiestaxid,
                    'gettaxrate': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".erroractivities").html("");
                    //totalnetamount without vat
                    if (activitiesdiscount != '') {
                        if (activitiesdiscount <= 0) {
                            $(".erroractivities").html("");
                            $(".erroractivities").append("<span>Please Use value Greater in discount than " + activitiesdiscount + " For ID <strong>" + selected_activities_id + " </strong></span>");
                            return false;
                        } else if (isNaN(activitiesdiscount)) {
                            $(".erroractivities").html("");
                            $(".erroractivities").append("<span>Please Use Numeric value in discount Instead " + activitiesdiscount + " For ID <strong>" + selected_activities_id + " </strong></span>");
                            return false;
                        } else {
                            $(".activity_save").prop('disabled', false);
                            var totalnetamountvalactivities = (parseFloat(activitiescharges)) * (parseFloat(activitiesquantity));
                            var getdiscountamountactivities = ((totalnetamountvalactivities) * (activitiesdiscount / 100 ));
                            $(".activitiesdiscount_amount_0" + id).val((getdiscountamountactivities).toFixed(2));
                            var amountafterdiscoountactivities = (totalnetamountvalactivities - getdiscountamountactivities);
                            var taxamountactivities = parseFloat(data[0].taxrates);
                            var getvatamountactivities = ((amountafterdiscoountactivities) * (taxamountactivities / 100 ));
                            $(".activitiesvat_amount_0" + id).val(( getvatamountactivities).toFixed(2));
                            var totalnetactivities = ( amountafterdiscoountactivities + getvatamountactivities);
                            $(".activitiesnetamount_0" + id).val(totalnetactivities.toFixed(2));
                        }
                    } else {
                        var totalnetamountvalactivities = (parseFloat(activitiescharges)) * (parseFloat(activitiesquantity));
                        var taxamountactivities = parseFloat(data[0].taxrates);
                        var getvatamountactivities = ((totalnetamountvalactivities) * (taxamountactivities / 100 ));
                        var totalnetactivities = (totalnetamountvalactivities + getvatamountactivities);
                        $(".activitiesdiscount_amount_0" + id).val(0);
                        $(".activitiesvat_amount_0" + id).val((getvatamountactivities).toFixed(2));
                        $(".activity_save").prop("disabled", false);
                        $(".activitiesnetamount_0" + id).val(totalnetactivities.toFixed(2));
                    }
                }
            });
        }
    }

    function booking_save() {
        var booking_id = $('input[name="packageid[]"]').map(function () {
            return this.value;
        }).get();
        var room_book_id = $('input[name="room_book_id[]"]').map(function () {
            return this.value;
        }).get();
//        var booking_date = $('input[name="booking_date[]"]').map(function () {
//            return this.value;
//        }).get();
//        var CheckIn_date = $('input[name="CheckIn_date[]"]').map(function () {
//            return this.value;
//        }).get();
//        var CheckOut_date = $('input[name="CheckOut_date[]"]').map(function () {
//            return this.value;
//        }).get();
        var room_id = $('input[name="room_id[]"]').map(function () {
            return this.value;
        }).get();
        var unit_id = $('input[name="unit_id[]"]').map(function () {
            return this.value;
        }).get();
        var nightcharge = $('input[name="nightcharge[]"]').map(function () {
            return this.value;
        }).get();
        var adults = $('input[name="adults[]"]').map(function () {
            return this.value;
        }).get();
        var children = $('input[name="children[]"]').map(function () {
            return this.value;
        }).get();
        var night = $('input[name="night[]"]').map(function () {
            return this.value;
        }).get();
        var discount = $('input[name="discount[]"]').map(function () {
            return this.value;
        }).get();
        var taxbooking_id = $('input[name="taxbooking_id[]"]').map(function () {
            return this.value;
        }).get();
        var netamount = $('input[name="netamount[]"]').map(function () {
            return this.value;
        }).get();
        var discount_amount = $('input[name="discount_amount[]"]').map(function () {
            return this.value;
        }).get();
        var vat_amount = $('input[name="vat_amount[]"]').map(function () {
            return this.value;
        }).get();
        var sum = 0;
        netamount.toString().split(',').map(function (n) {
            if (!n) return;
            sum += parseFloat(n);
            return sum;
        });
        $(".total_room_charges").val(sum);
//        if (CheckIn_date == '' || CheckOut_date == '') {
//            $(".date_error").html("");
//            $(".date_error").append("<span>Check In or Check Out date cannot be empty.</span>");
//        }
//        if (CheckIn_date != '' || CheckOut_date != '') {
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'booking_id[]': booking_id,
                'room_book_id[]': room_book_id,
                'room_id[]': room_id,
                'nightcharge[]': nightcharge,
                'adults[]': adults,
                'children[]': children,
                'night[]': night,
                'discount[]': discount,
                'taxbooking_id[]': taxbooking_id,
                'netamount[]': netamount,
                'unit_id[]': unit_id,
                'discount_amount[]': discount_amount,
                'vat_amount[]': vat_amount,
                'getpackageform': 1
            },
            success: function (data) {
                if (data == "success") {
                }
            }
        });
//        }
    }

    $(".customer_save").click(function () {
        $(".errorcustomer").val("");
        var booking_id = $(".booking_id").val();
        var customername = $("#customer-name_id").val();
        var firstname = $(".firstname").val();
        var email = $(".email").val();
        var phone = $(".phone").val();
        var mobile = $(".mobile").val();
        if (customername == '' || customername == 'undefined') {
            $(".errorcustomer").html("");
            $(".errorcustomer").append("<strong><span>Customer Name required</strong></span>");
        }
        if (firstname == '' || firstname == 'undefined') {
            $(".errorcustomer").html("");
            $(".errorcustomer").append("<strong><span>First Name required</strong></span>");
        }
        if (email == '' || email == 'undefined') {
            $(".errorcustomer").html("");
            $(".errorcustomer").append("<strong><span>Email is required</strong></span>");
        }
        if (phone == '' || phone == 'undefined') {
            $(".errorcustomer").html("");
            $(".errorcustomer").append("<strong><span>Phone No required</strong></span>");
        }
        if (mobile == '' || mobile == 'undefined') {
            $(".errorcustomer").html("");
            $(".errorcustomer").append("<strong><span>Mobile No required</strong></span>");
        }

    });

    function customer_save() {
        var booking_id = $('input[name="booking_id[]"]').map(function () {
            return this.value;
        }).get();
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'customerdata': $("#customer_form").serialize(),
                'booking_id': booking_id,
                'getcustomerform': 1
            },
            success: function (data) {
            }
        });
    }

    function service_save() {
        var booking_id = $('input[name="packageid[]"]').map(function () {
            return this.value;
        }).get();
        var serviceid = $('input[name="serviceid[]"]').map(function () {
            return this.value;
        }).get();
        var service_package_id = $('input[name="service_package_id[]"]').map(function () {
            return this.value;
        }).get();
        var servicedropdown = $('input[name="servicedropdown[]"]').map(function () {
            return this.value;
        }).get();
        var serviceunit = $('input[name="serviceunit[]"]').map(function () {
            return this.value;
        }).get();
        var servicequantity = $('input[name="servicequantity[]"]').map(function () {
            return this.value;
        }).get();
        var servicecharges = $('input[name="servicecharges[]"]').map(function () {
            return this.value;
        }).get();
        var servicediscount = $('input[name="servicediscount[]"]').map(function () {
            return this.value;
        }).get();
        var taxservice = $('input[name="taxservice[]"]').map(function () {
            return this.value;
        }).get();
        var servicenetamount = $('input[name="servicenetamount[]"]').map(function () {
            return this.value;
        }).get();
        var serviceunitid = $('input[name="serviceunitid[]"]').map(function () {
            return this.value;
        }).get();
        var servicediscount_amount = $('input[name="servicediscount_amount[]"]').map(function () {
            return this.value;
        }).get();
        var servicevat_amount = $('input[name="servicevat_amount[]"]').map(function () {
            return this.value;
        }).get();

        var sum = 0;
        servicenetamount.toString().split(',').map(function (n) {
            if (!n) return;
            sum += parseFloat(n);
            return sum;
        });
        $(".total_service_charges").val(sum);
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'booking_id[]': booking_id,
                'serviceid[]': serviceid,
                'service_package_id[]': service_package_id,
                'servicedropdown[]': servicedropdown,
                'serviceunit[]': serviceunit,
                'servicequantity[]': servicequantity,
                'servicecharges[]': servicecharges,
                'servicediscount[]': servicediscount,
                'taxservice[]': taxservice,
                'servicenetamount[]': servicenetamount,
                'serviceunitid[]': serviceunitid,
                'servicediscount_amount[]': servicediscount_amount,
                'servicevat_amount[]': servicevat_amount,
                'getpackagesserviceform': 1
            },
            success: function (data) {
            }
        });

    }

    function packageform_save() {
        var id= $(".form_id").val();
        var package_id = $('input[name="packageid[]"]').val();
        var package_name = $('input[name="packname').val();
        var package_descr = $('#descr').val();
        var package_fromdate = $('#fromdate').val();
        var package_todate = $('#todate').val();
        var package_meals = getcheckvalues;
        var package_applyondays = getcheckvaluesdays;
        var package_maxpeople = $('input[name="maxpeople"]').val();
        var package_maxheight = $('input[name="maxheight"]').val();
        var package_totalpackcharge = $('input[name="totalpackcharge"]').val();
        var package_packchargenight = $('input[name="packchargenight"]').val();
        var package_taxrates = $('#taxrates').val();
        var package_taxinclusive = $('#taxinclusive').val();
        var package_ecnightadult = $('input[name="ecnightadult"]').val();
        var package_ecnightchild = $('#ecnightchild').val();
        var package_extrafood = $('input[name="extrafood"]').val();
        var package_status = $('#package_status').val();
        var package_allowpkgedit = get_allowedit();



        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'id': id,
                'package_id[]': package_id,
                'package_name': package_name,
                'package_descr': package_descr,
                'package_fromdate': package_fromdate,
                'package_todate': package_todate,
                'package_meals[]': package_meals,
                'package_applyondays[]': package_applyondays,
                'package_maxpeople': package_maxpeople,
                'package_maxheight': package_maxheight,
                'package_totalpackcharge': package_totalpackcharge,
                'package_packchargenight': package_packchargenight,
                'package_taxrates': package_taxrates,
                'package_taxinclusive': package_taxinclusive,
                'package_ecnightadult': package_ecnightadult,
                'package_ecnightchild': package_ecnightchild,
                'package_extrafood': package_extrafood,
                'package_status': package_status,
                'package_allowpkgedit': package_allowpkgedit,
                'savepackageform': 1
            },
            success: function (data) {
            }
        });

    }

    function getcheckvalues() {
        var chkArray = [];
        $("input:checkbox[name='meals[]']:checked").each(function () {
            chkArray.push($(this).val());
        });
        var selected;
        selected = chkArray.join(',');
        return selected;
    }
    function getcheckvaluesdays() {
        var chkArray = [];
        $("input:checkbox[name='applyondays[]']:checked").each(function () {
            chkArray.push($(this).val());
        });
        var selected;
        selected = chkArray.join(',');
        return selected;
    }
    function get_allowedit() {
        var value='';
        if($('#allowpkgedit').is(':checked')== true){
            value=1;
        }else{
            value=0;
        }
        return value;
    }
    function activity_save() {
        var booking_id = $('input[name="packageid[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesid = $('input[name="activitiesid[]"]').map(function () {
            return this.value;
        }).get();
        var activities = $('input[name="activities[]"]').map(function () {
            return this.value;
        }).get();
        var activity_package_id = $('input[name="activity_package_id[]"]').map(function () {
            return this.value;
        }).get();
//        var activity_id = $('input[name="activity_id[]"]').map(function () {
//            return this.value;
//        }).get();
        var activitiesadult = $('input[name="activitiesadult[]"]').map(function () {
            return this.value;
        }).get();
        var activitieschild = $('input[name="activitieschild[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesunit = $('input[name="activitiesunit[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesquantity = $('input[name="activitiesquantity[]"]').map(function () {
            return this.value;
        }).get();
        var activitiescharges = $('input[name="activitiescharges[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesdiscount = $('input[name="activitiesdiscount[]"]').map(function () {
            return this.value;
        }).get();
        var taxdropdownactivities = $('input[name="taxdropdownactivities[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesnetamount = $('input[name="activitiesnetamount[]"]').map(function () {
            return this.value;
        }).get();
        var taxactivitiesid = $('input[name="taxactivitiesid[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesunitid = $('input[name="activitiesunitid[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesdiscount_amount = $('input[name="activitiesdiscount_amount[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesvat_amount = $('input[name="activitiesvat_amount[]"]').map(function () {
            return this.value;
        }).get();

        var sum = 0;
        activitiesnetamount.toString().split(',').map(function (n) {
            if (!n) return;
            sum += parseFloat(n);
            return sum;
        });
        $(".total_activity_charges").val(sum);
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'booking_id[]': booking_id,
                'activitiesid[]': activitiesid,
                'activities[]': activities,
                'activity_package_id[]': activity_package_id,
                'activitiesadult[]': activitiesadult,
                'activitieschild[]': activitieschild,
                'activitiesunit[]': activitiesunit,
                'activitiesquantity[]': activitiesquantity,
                'activitiescharges[]': activitiescharges,
                'activitiesdiscount[]': activitiesdiscount,
                'taxactivitiesid[]': taxactivitiesid,
                'activitiesunitid[]': activitiesunitid,
                'activitiesnetamount[]': activitiesnetamount,
                'activitiesdiscount_amount[]': activitiesdiscount_amount,
                'activitiesvat_amount[]': activitiesvat_amount,
                'getpackagesactivityform': 1
            },
            success: function (data) {
            }
        });

    }

    $(".save_all").click(function () {
       // debugger;
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();
        var current_date = d.getFullYear() + '-' +((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day;
        var pkg_startdate = $('#fromdate').val();
        var pkg_enddate = $('#todate').val();
        var packagename = $('input[name="packname"]').val();
        var Mealsinpackage =getcheckvalues();
        var applyondays = getcheckvaluesdays();
        var maxpeople = $('input[name="maxpeople"]').val();
        var maxnights = $('input[name="maxheight"]').val();
        var packageCharges = $('input[name="totalpackcharge"]').val();
        var taxrates = $('#taxrates').val();
        var packageChargesperNight = $('input[name="packchargenight"]').val();
        var extraChargesperNightAdult = $('input[name="ecnightadult"]').val();
        var extraChargesperNightChild = $('input[name="ecnightchild"]').val();
        var extraFoodSupplimentCharges = $('input[name="extrafood"]').val();

        if (packagename== '' ){
            $(".errordate").html('');
            $(".errordate").addClass("alert alert-danger");
            $(".errordate").append('<p style="color:red;">Package Name Cannot be Empty.</p>');
            return false;
        }
        if(pkg_startdate < current_date){
            $(".errordate").html('');
            $(".errordate").addClass("alert alert-danger");
            $(".errordate").append('<p style="color:red;">Start Date Cannot be Less than todays date.</p>');
            return false;
        }
        if(pkg_enddate < current_date){
            $(".errordate").html('');
            $(".errordate").addClass("alert alert-danger");
            $(".errordate").append('<p style="color:red;">End Date Cannot be Less than todays date.</p>');
            return false;
        }

        if (pkg_startdate == '' || pkg_enddate == ''){
            $(".errordate").html('');
            $(".errordate").addClass("alert alert-danger");
            $(".errordate").append('<p style="color:red;">Start Date and CheckOut Date Cannot be Empty.</p>');
            return false;
        }
        if (pkg_startdate > pkg_enddate) {
            $(".errordate").html('');
            $(".errordate").addClass("alert alert-danger");
            $(".errordate").append('<p style="color:red;">End Date Cannot be greater than CheckOut Date.</p>');
            return false;
        }
        if (Mealsinpackage== '' ){
            $(".errordate").html('');
            $(".errordate").addClass("alert alert-danger");
            $(".errordate").append('<p style="color:red;">Meals included in package Cannot be Empty.</p>');
            return false;
        }
        if ( applyondays== '' ){
            $(".errordate").html('');
            $(".errordate").addClass("alert alert-danger");
            $(".errordate").append('<p style="color:red;">Apply on Days Cannot be Empty.</p>');
            return false;
        }
        if ( maxpeople== '' ){
            $(".errordate").html('');
            $(".errordate").addClass("alert alert-danger");
            $(".errordate").append('<p style="color:red;">Max No. of People Cannot be Empty.</p>');
            return false;
        }
        if ( maxnights== '' ){
            $(".errordate").html('');
            $(".errordate").addClass("alert alert-danger");
            $(".errordate").append('<p style="color:red;">Max No. of Nights Cannot be Empty.</p>');
            return false;
        }
        if ( packageCharges== '' ){
            $(".errordate").html('');
            $(".errordate").addClass("alert alert-danger");
            $(".errordate").append('<p style="color:red;">Package Charges Cannot be Empty.</p>');
            return false;
        }
        if ( taxrates== '' ){
            $(".errordate").html('');
            $(".errordate").addClass("alert alert-danger");
            $(".errordate").append('<p style="color:red;">Applicable Tax Rates Cannot be Empty.</p>');
            return false;
        }
        if ( packageChargesperNight== '' ){
            $(".errordate").html('');
            $(".errordate").addClass("alert alert-danger");
            $(".errordate").append('<p style="color:red;">Package Charges Per Night Cannot be Empty.</p>');
            return false;
        }
        if ( extraChargesperNightAdult== '' ){
            $(".errordate").html('');
            $(".errordate").addClass("alert alert-danger");
            $(".errordate").append('<p style="color:red;">Extra Charges per Night(Adult) Cannot be Empty.</p>');
            return false;
        }
        if (extraChargesperNightChild== '' ){
            $(".errordate").html('');
            $(".errordate").addClass("alert alert-danger");
            $(".errordate").append('<p style="color:red;">Extra Charges per Night(Child) Cannot be Empty.</p>');
            return false;
        }
      if ( extraFoodSupplimentCharges== '' ){
            $(".errordate").html('');
            $(".errordate").addClass("alert alert-danger");
            $(".errordate").append('<p style="color:red;">Extra Food Suppliment Charges Cannot be Empty.</p>');
            return false;
        }

        if($('input[name="room_id[]"]').map(function () {
                return this.value;
            }).get()==''){
            $(".errordate").html('');
            $(".errordate").addClass("alert alert-danger");
            $(".errordate").append('<p style="color:red;">Rooms are not attatched in the package</p>');
            return false;
        }
    else{
        packageform_save();
        booking_save();
        service_save();
        activity_save();
        }


      //window.location  = '<?php echo $base . ADMIN_FOLDER ?>/modules/booking/package/index.php?view=list';
    });

    $(".save_all_print").click(function () {
        booking_save();
        customer_save();
        service_save();
        activity_save();
        var booking_id = $('input[name="booking_id[]"]').map(function () {
            return this.value;
        }).get();
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'savepayment': $("#save_payment").serialize(),
                'booking_id': booking_id,
                'getsavepaymentformprint': 1
            },
            success: function (data) {
                $(".save_all").prop("disabled", true);
                $(".save_all_print").prop("disabled", true);
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/printbill.php',
                    type: 'POST',
                    data: {
                        'booking_id': booking_id,
                        'getbbokingid': 1
                    },
                    success: function (data) {
                        $(".data_success").append("<strong><span>Data has been saved successfully</span><strong>");
                    }
                });

            }
        });
    });

    function payment_tab() {
        var netamount = $('input[name="netamount[]"]').map(function () {
            return this.value;
        }).get();
        var sum1 = 0;
        netamount.toString().split(',').map(function (n) {
            if (!n) return;
            sum1 += parseFloat(n);
            return sum1;
        });
        var servicenetamount = $('input[name="servicenetamount[]"]').map(function () {
            return this.value;
        }).get();
        var sum2 = 0;
        servicenetamount.toString().split(',').map(function (n) {
            if (!n) return;
            sum2 += parseFloat(n);
            return sum2;
        });
        var activitiesnetamount = $('input[name="activitiesnetamount[]"]').map(function () {
            return this.value;
        }).get();
        var sum3 = 0;
        activitiesnetamount.toString().split(',').map(function (n) {
            if (!n) return;
            sum3 += parseFloat(n);
            return sum3;
        });
        var booking_id = $('input[name="booking_id[]"]').map(function () {
            return this.value;
        }).get();
        $(".total_service_charges").val(sum2);
        $(".total_room_charges").val(sum1);
        $(".total_activity_charges").val(sum3);
        $(".data_success").html("");
        if ($(".total_room_charges").val() == '') {
            $(".save_all").prop("disabled", true);
            $(".save_all_print").prop("disabled", true);
        } else {
            $(".save_all_print").prop("disabled", false);
            $(".save_all").prop("disabled", false);
            var total_room_charges = $(".total_room_charges").val();
            var total_activity_charges = $(".total_activity_charges").val();
            var total_service_charges = $(".total_service_charges").val();
            if (total_activity_charges == '') {
                total_activity_charges = 0;
            }
            if (total_room_charges == '') {
                total_room_charges = 0;
            }
            if (total_service_charges == 0) {
                total_service_charges = 0;
            }
            var plus = (parseFloat(total_room_charges) + parseFloat(total_activity_charges) + parseFloat(total_service_charges) );
            $(".totalnetpayable").val(plus.toFixed(2));
        }
    }

    function customer_click() {
        $(".data_success").html('');
    }

    $(".total_down_payment").keyup(function () {
        $(".errorpayment").html("");
        var downpayment = $(".total_down_payment").val();
        if (downpayment > 0 && downpayment != '') {
            $(".totalnetpayable").val('');
            var total_room_charges = $(".total_room_charges").val();
            var total_activity_charges = $(".total_activity_charges").val();
            var total_service_charges = $(".total_service_charges").val();
            if (total_activity_charges == '') {
                total_activity_charges = 0;
            }
            if (total_room_charges == '') {
                total_room_charges = 0;
            }
            if (total_service_charges == 0) {
                total_service_charges = 0;
            }
            var plus = (parseFloat(total_room_charges) + parseFloat(total_activity_charges) + parseFloat(total_service_charges) );
            var subtractdown = (parseFloat(plus) - parseFloat(downpayment));
            $(".totalnetpayable").val(subtractdown.toFixed(2));
            if (isNaN(downpayment)) {
                $(".errorpayment").html("");
                $(".errorpayment").append("<strong>Down Payment must be numeric.</strong>");
            } else {
                $(".totalnetpayable").val('');
                var total_room_charges = $(".total_room_charges").val();
                var total_activity_charges = $(".total_activity_charges").val();
                var total_service_charges = $(".total_service_charges").val();
                if (total_activity_charges == '') {
                    total_activity_charges = 0;
                }
                if (total_room_charges == '') {
                    total_room_charges = 0;
                }
                if (total_service_charges == 0) {
                    total_service_charges = 0;
                }
                var plus = (parseFloat(total_room_charges) + parseFloat(total_activity_charges) + parseFloat(total_service_charges) );
                var subtractdown = (parseFloat(plus) - parseFloat(downpayment));
                $(".totalnetpayable").val(subtractdown.toFixed(2));
            }

        } else {
            $(".errorpayment").html("");
            $(".errorpayment").append("<strong>Down Payment cannot be empty or must be greater than zero.</strong>");

            $(".totalnetpayable").val('');
            var total_room_charges = $(".total_room_charges").val();
            var total_activity_charges = $(".total_activity_charges").val();
            var total_service_charges = $(".total_service_charges").val();
            if (total_activity_charges == '') {
                total_activity_charges = 0;
            }
            if (total_room_charges == '') {
                total_room_charges = 0;
            }
            if (total_service_charges == 0) {
                total_service_charges = 0;
            }
            var plus = (parseFloat(total_room_charges) + parseFloat(total_activity_charges) + parseFloat(total_service_charges) );
            $(".totalnetpayable").val(plus.toFixed(2));
        }
    });


</script>