
<?php
/**
 * Template of the module form
 */
debug_backtrace() || die ('Direct access not permitted');

// Item ID
if(isset($_GET['id']) && is_numeric($_GET['id'])) $id = $_GET['id'];
elseif(isset($_POST['id']) && is_numeric($_POST['id'])) $id = $_POST['id'];
else{
    header('Location: index.php?view=list');
    exit();
}

// Item ID to delete
$id_file = (isset($_GET['file']) && is_numeric($_GET['file'])) ? $_GET['file'] : 0;
$id_row = (isset($_GET['row']) && is_numeric($_GET['row'])) ? $_GET['row'] : 0;


// Action to perform
$back = false;
$action = (isset($_GET['action'])) ? htmlentities($_GET['action'], ENT_QUOTES, 'UTF-8') : '';
if(isset($_POST['edit']) || isset($_POST['edit_back'])){
    $action = 'edit';
    if(isset($_POST['edit_back'])) $back = true;
}
if(isset($_POST['add']) || isset($_POST['add_back'])){
    $action = 'add';
    $id = 0;
    if(isset($_POST['add_back'])) $back = true;
}
if($action != '' && defined('DEMO') && DEMO == 1){
    $action = '';
    $_SESSION['msg_error'][] = 'This action is disabled in the demo mode';
}

// Initializations
$file = array();
$img = array();
$img_label = array();
$file_label = array();
$fields_checked = true;
$total_lang = 1;
$rank = 0;
$old_rank = 0;
$home = 0;
$checked = 0;
$add_date = time();
$edit_date = time();
$publish_date = time();
$unpublish_date = null;
$users = array($_SESSION['user']['id']);
$referer = DIR.'index.php?view=form';

// Messages
if(NB_FILES > 0) $_SESSION['msg_notice'][] = $texts['EXPECTED_IMAGES_SIZE'].' '.MAX_W_BIG.' x '.MAX_H_BIG.'px<br>';

// Creation of the unique token for uploadifive
if(!isset($_SESSION['uniqid'])) $_SESSION['uniqid'] = uniqid();
if(!isset($_SESSION['timestamp'])) $_SESSION['timestamp'] = time();
if(!isset($_SESSION['token'])) $_SESSION['token'] = md5('sessid_'.$_SESSION['uniqid'].$_SESSION['timestamp']);

// Getting languages
if(MULTILINGUAL && $db != false){
    $result_lang = $db->query('SELECT id, title FROM pm_lang WHERE checked = 1 ORDER BY CASE main WHEN 1 THEN 0 ELSE 1 END, rank');
    if($result_lang !== false){
        $total_lang = $db->last_row_count();
        $langs = $result_lang->fetchAll(PDO::FETCH_ASSOC);
    }
}

// Last rank selection
if(RANKING && $db != false){
    $result_rank = $db->query('SELECT rank FROM pm_'.MODULE.' ORDER BY rank DESC LIMIT 1');
    $rank = ($result_rank !== false && $db->last_row_count() > 0) ? $result_rank->fetchColumn(0) + 1 : 1;
}

// Inclusions
require_once(SYSBASE.ADMIN_FOLDER.'/includes/fn_form.php');
//here fnform giving the fields from XML File
$fields = getFields($db);
if(is_null($fields)) $fields = array();

// Getting datas in the database
if($db !== false){
    $result = $db->query('SELECT * FROM pm_'.MODULE.' WHERE id = '.$id);

    if($result !== false){
        // Datas of the module
        foreach($result as $row){
            $id_lang = (MULTILINGUAL) ? $row['lang'] : 0;
            foreach($fields[MODULE]['fields'] as $fieldName => $field){
                if($field->getType() != 'separator'){
                    $field->setValue($row[$fieldName], 0, $id_lang);

                }
            }

            if($id_lang == DEFAULT_LANG || $id_lang == 0){
                if(HOME) $home = $row['home'];
                if(VALIDATION) $checked = $row['checked'];
                if(RANKING) $old_rank = $row['rank'];
                if(DATES) $add_date = $row['add_date'];
                if(RELEASE){
                    $publish_date = $row['publish_date'];
                    $unpublish_date = $row['unpublish_date'];
                }
                if(db_column_exists($db, 'pm_'.MODULE, 'users')){
                    $users = explode(',', $row['users']);
                    if(!in_array($_SESSION['user']['type'], array('administrator', 'manager', 'editor')) && !in_array($_SESSION['user']['id'], $users)){
                        header('Location: index.php?view=list');
                        exit();
                    }
                }
            }
        }
    }

    // Datas of the module's tables

    foreach($fields as $tableName => $fields_table){

        if($tableName != MODULE){


            $result = $db->query('SELECT * FROM pm_'.$tableName.' WHERE '.$fields_table['table']['fieldRef'].' = '.$id);
            if($result !== false){

                foreach($result as $i => $row){

                    $id_lang = ($fields_table['table']['multi'] == 1 && isset($row['lang'])) ? $row['lang'] : 0;

                    foreach($fields_table['fields'] as $fieldName => $field){
                        if($field->getType() != 'separator'){
                            $field->setValue($row[$fieldName], $i, $id_lang);
                        }
                    }
                }
            }
        }
    }

    // Insersion / update
    if(in_array('add', $permissions) || in_array('edit', $permissions) || in_array('all', $permissions)){
        if((($action == 'add') || ($action == 'edit')) && check_token($referer, 'form', 'post')){

            $files = array();

            // Getting POST values
            for($i = 0; $i < $total_lang; $i++){

                $id_lang = (MULTILINGUAL) ? $langs[$i]['id'] : 0;

                foreach($fields as $tableName => $fields_table){

                    if($tableName == MODULE || $id_lang == DEFAULT_LANG || $fields_table['table']['multi'] == 1){

                        foreach($fields_table['fields'] as $fieldName => $field){
                            $fieldName = $tableName.'_'.$fieldName.'_';

                            if($tableName == MODULE)
                                $fieldName .= (MULTILINGUAL && !$field->isMultilingual()) ? DEFAULT_LANG : $id_lang;
                            else{
                                $id_lang = ($fields_table['table']['multi'] == 1) ? $langs[$i]['id'] : 0;
                                $fieldName .= ($fields_table['table']['multi'] == 1 && !$field->isMultilingual()) ? DEFAULT_LANG : $id_lang;
                            }

                            if(isset($_POST[$fieldName])){

                                foreach($_POST[$fieldName] as $index => $value){

                                    switch($field->getType()){
                                        case 'date' :
                                            $date = isset($_POST[$fieldName][$index]['date']) ? $_POST[$fieldName][$index]['date'] : '';
                                            if(!empty($date)) $date = gm_strtotime($date.' 00:00:00');
                                            if(is_numeric($date) && $date !== false)
                                                $field->setValue($date, $index, $id_lang);
                                            else
                                                $field->setValue(NULL, $index, $id_lang);
                                            break;
                                        case 'datetime' :
                                            $date = isset($_POST[$fieldName][$index]['date']) ? $_POST[$fieldName][$index]['date'] : '';
                                            $hour = isset($_POST[$fieldName][$index]['hour']) ? $_POST[$fieldName][$index]['hour'] : '';
                                            $minute = isset($_POST[$fieldName][$index]['minute']) ? $_POST[$fieldName][$index]['minute'] : 0;
                                            if(!empty($date) && is_numeric($hour) && is_numeric($minute)) $date = strtotime($date.' '.$hour.':'.$minute.':00');
                                            if(is_numeric($date) && $date !== false)
                                                $field->setValue($date, $index, $id_lang);
                                            else
                                                $field->setValue(NULL, $index, $id_lang);
                                            break;
                                        case 'password' :
                                            $value = ($value != '') ? md5($value) : '';
                                            if($value == '') $value = $field->getValue(false, $index, $id_lang);
                                            $field->setValue($value, $index, $id_lang);
                                            break;
                                        case 'checkbox' :
                                        case 'multiselect' :
                                            $value = (isset($_POST[$fieldName][$index]) && is_array($_POST[$fieldName][$index])) ? implode(',', $_POST[$fieldName][$index]) : '';
                                            $field->setValue($value, $index, $id_lang);
                                            break;
                                        case 'alias' :
                                            $value = text_format($_POST[$fieldName][$index]);
                                            $field->setValue($value, $index, $id_lang);
                                            break;
                                        default :
                                            $value = isset($_POST[$fieldName][$index]) ? $_POST[$fieldName][$index] : '';
                                            $field->setValue($value, $index, $id_lang);
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Remove row if (all fields = empty) and if (tableName != MODULE)

            foreach($fields as $tableName => $fields_table){
                if($tableName != MODULE){

                    $default_lang = ($fields_table['table']['multi'] == 1) ? DEFAULT_LANG : 0;

                    $numRows = getNumMaxRows($fields, $tableName);
                    for($index = 0; $index < $numRows; $index++){

                        $empty = true;
                        $id_row = 0;
                        if(isset($_POST[$tableName.'_id_'.$default_lang][$index]))
                            $id_row = $_POST[$tableName.'_id_'.$default_lang][$index];

                        if($id_row == 0 || $id_row == ''){

                            foreach($fields_table['fields'] as $fieldName => $field){
                                $value = $field->getValue(false, $index, $default_lang);
                                if(!empty($value)) $empty = false;
                            }
                            if($empty){
                                foreach($fields_table['fields'] as $fieldName => $field){
                                    $field->removeValue($index);
                                }
                            }
                        }
                    }
                }
            }

            if(VALIDATION && isset($_POST['checked']) && is_numeric($_POST['checked'])) $checked = $_POST['checked'];
            if(HOME && isset($_POST['home']) && is_numeric($_POST['home'])) $home = $_POST['home'];
            if(DATES && (!is_numeric($add_date) || $add_date == 0)) $add_date = time();
            if(RELEASE){
                $day = (isset($_POST['publish_date_day'])) ? $_POST['publish_date_day'] : '';
                $month = (isset($_POST['publish_date_month'])) ? $_POST['publish_date_month'] : '';
                $year = (isset($_POST['publish_date_year'])) ? $_POST['publish_date_year'] : '';
                $hour = (isset($_POST['publish_date_hour'])) ? $_POST['publish_date_hour'] : '';
                $minute = (isset($_POST['publish_date_minute'])) ? $_POST['publish_date_minute'] : '';
                if(is_numeric($day) && is_numeric($month) && is_numeric($year) && is_numeric($hour) && is_numeric($minute))
                    $publish_date = mktime($hour, $minute, 0, $month, $day, $year);
                else
                    $publish_date = NULL;

                $day = (isset($_POST['unpublish_date_day'])) ? $_POST['unpublish_date_day'] : '';
                $month = (isset($_POST['unpublish_date_month'])) ? $_POST['unpublish_date_month'] : '';
                $year = (isset($_POST['unpublish_date_year'])) ? $_POST['unpublish_date_year'] : '';
                $hour = (isset($_POST['unpublish_date_hour'])) ? $_POST['unpublish_date_hour'] : '';
                $minute = (isset($_POST['unpublish_date_minute'])) ? $_POST['unpublish_date_minute'] : '';
                if(is_numeric($day) && is_numeric($month) && is_numeric($year) && is_numeric($hour) && is_numeric($minute))
                    $unpublish_date = mktime($hour, $minute, 0, $month, $day, $year);
                else
                    $unpublish_date = NULL;
            }
            if(isset($_POST['users'])) $users = $_POST['users'];
            if(!is_array($users)) $users = explode(',', $users);

            if(checkFields($db, $fields, $id)){

                for($i = 0; $i < $total_lang; $i++){

                    $id_lang = (MULTILINGUAL) ? $langs[$i]['id'] : 0;

                    // Add / Edit item in the table of the module
                    $data = array();
                    $data['id'] = $id;
                    $data['lang'] = $id_lang;
                    $data['rank'] = $rank;
                    $data['home'] = $home;
                    $data['checked'] = $checked;
                    $data['add_date'] = $add_date;
                    $data['edit_date'] = $edit_date;
                    $data['publish_date'] = $publish_date;
                    $data['unpublish_date'] = $unpublish_date;
                    $data['users'] = implode(',', $users);

                    foreach($fields[MODULE]['fields'] as $fieldName => $field)
                        $data[$fieldName] = $field->getValue(false, 0, $id_lang);

                    if($action == 'add' && (in_array('add', $permissions) || in_array('all', $permissions))){

                        $result_insert = db_prepareInsert($db, 'pm_'.MODULE, $data);

                        add_item($db, MODULE, $result_insert, $id_lang);

                    }elseif($action == 'edit' && (in_array('edit', $permissions) || in_array('all', $permissions))){

                        $query_exist = 'SELECT * FROM pm_'.MODULE.' WHERE id = '.$id;
                        if(MULTILINGUAL) $query_exist .= ' AND lang = '.$id_lang;
                        $result_exist = $db->query($query_exist);

                        $data['rank'] = $old_rank;

                        if($result_exist !== false){
                            if($db->last_row_count() > 0){

                                $result_update = db_prepareUpdate($db, 'pm_'.MODULE, $data);

                                edit_item($db, MODULE, $result_update, $id, $id_lang);
                            }else{
                                $result_insert = db_prepareInsert($db, 'pm_'.MODULE, $data);

                                add_item($db, MODULE, $result_insert, $id_lang);
                            }
                        }
                    }

                    // Add / Edit items in other tables
                    if(empty($_SESSION['msg_error']) && $id > 0){

                        foreach($fields as $tableName => $fields_table){
                            if($tableName != MODULE){
                                $numRows = getNumMaxRows($fields, $tableName);
                                for($index = 0; $index < $numRows; $index++){

                                    if($fields_table['table']['multi'] == 0) $id_lang = 0;

                                    $id_row = $fields_table['fields']['id']->getValue(false, $index, $id_lang);

                                    $data = array();
                                    $data['lang'] = $id_lang;
                                    $data[$fields_table['table']['fieldRef']] = $id;

                                    foreach($fields_table['fields'] as $fieldName => $field)
                                        $data[$fieldName] = $field->getValue(false, $index, $id_lang);

                                    if($id_row == 0 && (in_array('add', $permissions) || in_array('all', $permissions))){

                                        $result_insert = db_prepareInsert($db, 'pm_'.$tableName, $data);
                                        if($result_insert->execute() !== false){
                                            $fields_table['fields']['id']->setValue($db->lastInsertId(), $index, $id_lang);
                                        }

                                    }elseif($id_row > 0 && (in_array('edit', $permissions) || in_array('all', $permissions))){

                                        $query_exist = 'SELECT * FROM pm_'.$tableName.' WHERE id = '.$id_row;
                                        if($fields_table['table']['multi'] == 1) $query_exist .= ' AND lang = '.$id_lang;
                                        $result_exist = $db->query($query_exist);

                                        if($result_exist !== false){
                                            if($db->last_row_count() > 0){

                                                $result_update = db_prepareUpdate($db, 'pm_'.$tableName, $data);
                                                $result_update->execute();

                                            }else{
                                                $result_insert = db_prepareInsert($db, 'pm_'.$tableName, $data);
                                                if($result_insert->execute() !== false){
                                                    $fields_table['fields']['id']->setValue($db->lastInsertId(), $index, $id_lang);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }else
                $_SESSION['msg_error'][] = $texts['FORM_ERRORS'];
        }
    }

    if(($back === true) && empty($_SESSION['msg_error']) && !empty($_SESSION['msg_success'])){
        header('Location: index.php?view=list');
        exit();
    }

    if(in_array('edit', $permissions) || in_array('all', $permissions)){
        // Row deletion
        if($action == 'delete_row' && $id_row > 0 && isset($_GET['table']) && isset($_GET['fieldref']) && check_token($referer, 'form', 'get'))
            delete_row($db, $id, $id_row, 'pm_'.$_GET['table'], $_GET['fieldref']);

        // File deletion
        if($action == 'delete_file' && $id_file > 0 && check_token($referer, 'form', 'get'))
            delete_file($db, $id_file);

        if($action == 'delete_multi_file' && isset($_POST['multiple_file']) && check_token($referer, 'form', 'get'))
            delete_multi_file($db, $_POST['multiple_file'], $id);

        // File activation/deactivation
        if($action == 'check_file' && $id_file > 0 && check_token($referer, 'form', 'get'))
            check($db, 'pm_'.MODULE.'_file', $id_file, 1);

        if($action == 'uncheck_file' && $id_file > 0 && check_token($referer, 'form', 'get'))
            check($db, 'pm_'.MODULE.'_file', $id_file, 2);

        if($action == 'check_multi_file' && isset($_POST['multiple_file']) && check_token($referer, 'form', 'get'))
            check_multi($db, 'pm_'.MODULE.'_file', 1, $_POST['multiple_file']);

        if($action == 'uncheck_multi_file' && isset($_POST['multiple_file']) && check_token($referer, 'form', 'get'))
            check_multi($db, 'pm_'.MODULE.'_file', 2, $_POST['multiple_file']);

        // Files displayed in homepage
        if($action == 'display_home_file' && $id_file > 0 && check_token($referer, 'form', 'get'))
            display_home($db, 'pm_'.MODULE.'_file', $id_file, 1);

        if($action == 'remove_home_file' && $id_file > 0 && check_token($referer, 'form', 'get'))
            display_home($db, 'pm_'.MODULE.'_file', $id_file, 0);

        if($action == 'display_home_multi_file' && isset($_POST['multiple_file']) && check_token($referer, 'form', 'get'))
            display_home_multi($db, 'pm_'.MODULE.'_file', 1, $_POST['multiple_file']);

        if($action == 'remove_home_multi_file' && isset($_POST['multiple_file']) && check_token($referer, 'form', 'get'))
            display_home_multi($db, 'pm_'.MODULE.'_file', 0, $_POST['multiple_file']);
    }
}

// File download
if($action == 'download' && isset($_GET['type'])){
    $type = $_GET['type'];
    if($id_file > 0){
        if($type == 'image' || $type == 'other'){
            $query_file = 'SELECT file FROM pm_'.MODULE.'_file WHERE id = '.$id_file;
            if(MULTILINGUAL) $query_file .= ' AND lang = '.DEFAULT_LANG;
            $result_file = $db->query($query_file);
            if($result_file !== false && $db->last_row_count() > 0){
                $file = $result_file->fetchColumn(0);

                if($type == 'image'){
                    if(is_file(SYSBASE.'medias/'.MODULE.'/big/'.$id_file.'/'.$file))
                        $filepath = SYSBASE.'medias/'.MODULE.'/big/'.$id_file.'/'.$file;
                    elseif(is_file(SYSBASE.'medias/'.MODULE.'/medium/'.$id_file.'/'.$file))
                        $filepath = SYSBASE.'medias/'.MODULE.'/medium/'.$id_file.'/'.$file;
                    elseif(is_file(SYSBASE.'medias/'.MODULE.'/small/'.$id_file.'/'.$file))
                        $filepath = SYSBASE.'medias/'.MODULE.'/small/'.$id_file.'/'.$file;
                }elseif($type == 'other' && is_file(SYSBASE.'medias/'.MODULE.'/other/'.$id_file.'/'.$file))
                    $filepath = SYSBASE.'medias/'.MODULE.'/other/'.$id_file.'/'.$file;
                if(isset($filepath)){
                    $mime = getFileMimeType($filepath);
                    if(strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE') == false){
                        header('Content-disposition: attachment; filename='.$file);
                        header('Content-Type: '.$mime);
                        header('Content-Transfer-Encoding: '.$mime."\n");
                        header('Content-Length: '.filesize($filepath));
                        header('Pragma: no-cache');
                        header('Cache-Control: must-revalidate, post-check=0, pre-check=0, public');
                        header('Expires: 0');
                    }
                    readfile($filepath);
                }
            }
        }
    }
}

//getting data from table to edit
$oldquery='SELECT * FROM pm_' . $tableName . ' WHERE ' . $fields_table['table']['fieldRef'] . ' = ' . $id;
$newquery='SELECT * FROM pm_' . $tableName . ' WHERE  id = ' . $id;
$result = $db->query($newquery);
$singlerow = $result->fetch();

$csrf_token = get_token('form'); ?>
<!DOCTYPE html>
<head>
    <?php include(SYSBASE.ADMIN_FOLDER.'/includes/inc_header_form.php'); ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


</head>
<body>



<div id="overlay"><div id="loading"></div></div>
<div id="wrapper">



    <?php
    include(SYSBASE.ADMIN_FOLDER.'/includes/inc_top.php');

    if(!in_array('no_access', $permissions)){
    include(SYSBASE.ADMIN_FOLDER.'/includes/inc_library.php'); ?>

    <form id="form" class="form-horizontal" role="form" action="index.php?view=form" method="post" enctype="multipart/form-data">
        <div id="page-wrapper">
            <div class="page-header">

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="pull-left"><i class="fa fa-cog"></i> <?php echo TITLE_ELEMENT; ?></h4>
                        </div>

                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="alert-container removealert">
                    <div class="alert alert-success alert-dismissable"></div>
                    <!-- <div class="alert alert-warning alert-dismissable"></div> -->
                    <div class="alert alert-danger alert-dismissable"></div>
                </div>
                <div class="panel panel-default">
                    <div class="error">
                    </div>

                    <div class="container">
                        <form action="" method="post">
                            <div class="row mb10">

                                <label class="col-md-2" style="padding-top:7px;">
                                    Transaction ID
                                </label>
                                <div class="col-md-4">
                                    <input class="form-control" type="text" disabled value="<?php if ($id==0){echo get_numbering("pm_roomshift",$db);} if ($id>0){echo $id;}?>" name="transid">
                                </div>
                                <label class="col-md-2" style="padding-top:7px;">
                                    Shift Date
                                </label>
                                <div class="col-md-4">
                                    <input class="form-control" type="date" value="<?php if($id>0){echo $singlerow['add_date'];}?>" id="shiftdate" name="shiftdate">
                                </div>
                            </div>

                            <div class="row mb10">
                                <label class="col-md-2" style="padding-top:7px;">
                                    Customer ID/Name
                                </label>
                                <div class="col-md-4">
                                    <?php
                                    if($id==0){echo getDropDownDataID_customer('pm_booking_parent', 'id_user', 'customer_id', 'customer_name', 'Customer ID', 'required', $db);}
                                    if($id!=0){echo getSelectedDropDownDataID_customer('pm_booking_parent', 'id_user', 'customer_id', 'customer_name', 'Customer ID', 'required',$id ,$db);}
                                    ?>
                                </div>
                                <label class="col-md-2" style="padding-top:7px;">
                                    Booking ID
                                </label>
                                <div class="col-md-4">
                                    <?php
                                    if($id==0){echo getDropDownDataID('pm_booking', 'bookingid', 'booking_id', 'id', 'Booking ID', 'required', $db);}
                                    if($id!=0){echo getSelectedDropDownDataID_booking('pm_booking', 'bookingid', 'booking_id', 'id', 'Booking ID', 'required', $id,$db);}
                                    ?>
                                </div>
                            </div>
                            <div class="errorsearch">

                            </div>

                            <div class="row">
                                <div class="col-md-6 clearfix pb15 text-right" style="margin-left: 43%;margin-top: -14px;">
                                    <button type="button" id="roomshiftsearch" name="search" class="btn btn-primary mt15 text-left">
                                        Search
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

    </form>

    <!--        <form id="form" class="form-horizontal" role="form" action="index.php?view=form" method="post" enctype="multipart/form-data">-->

</div>
<!--end of container-->
<div class="container">


    <div class="row">
        <div class="col-md-5">
            <div class="row">
                <label class="col-md-5 control-label">
                    <h4 style=" font-weight: bold; font-size: 20px">Shifting Details<h4>
                </label>
            </div>
        </div>
    </div>
    <hr style="border-top: dotted 1px;" />
    <?php $roomdata='';?>
    <div class="row">
        <strong class="col-md-4 mb10">
            <strong<h5 class="error" style="color: red;font-size:small;"></h5>
        </strong>
    </div>
    <div class="row mb10">

        <div class="col-md-6">
            <div class="row">
                <label class="col-md-5 control-label">
                    <?php echo 'Current Room/Rooms'; ?>
                </label>
                <div class="col-md-7">
                    <select class="form-control" id="currentroom" type="text" name="currentroom">

                    </select>
                    <input type="hidden" id="hiddenbookingparent" name="hiddenbookingparent" value="">
                    <input type="hidden" id="hiddenunit" name="hiddenunit" value="">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <label class="col-md-5 control-label">
                    Shift to Room
                </label>
                <div class="col-md-7">
                    <?php echo getDropDownData_shifttoroom('pm_booking', 'shifttoroom', 'shifttoroom', 'room_id', '', 'required', $db);?>
                </div>
            </div>
        </div>

    </div>
    <div class="row mb10">
        <div class="col-md-6">
            <div class="row">
                <label class="col-md-5 control-label">
                    <?php echo 'Room Type'; ?>
                </label>
                <div class="col-md-7 dropdowndsc">
                    <?php echo getDropDownData_roomtype('pm_roomtypes', 'roomtypeft', 'roomtype_id_current', 'roomtype', 'Room', 'required', $db) ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <label class="col-md-5 control-label">
                    <?php echo 'Room Type'; ?>
                </label>
                <div class="col-md-7">
                    <?php echo getDropDownData_roomtype('pm_roomtypes', 'roomtypeft', 'roomtype_id_shift', 'roomtype', 'Room type', 'required', $db) ?>
                </div>
            </div>
        </div>

    </div>

    <div class="row mb10">
        <div class="col-md-6">
            <div class="row">
                <label class="col-md-5 control-label">
                    <?php echo 'Room Capacity'; ?>
                </label>
                <div class="col-md-7">
                    <input class="form-control" type="text" value="" id="roomcapacitycnt" name="roomcapacitycnt">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <label class="col-md-5 control-label">
                    <?php echo 'Room Capacity'; ?>
                </label>
                <div class="col-md-7">
                    <input class="form-control" type="text" value=""  id="roomcapacityft" name="roomcapacityft">
                </div>
            </div>
        </div>

    </div>

    <div class="row mb10">
        <div class="col-md-6">
            <div class="row">
                <label class="col-md-5 control-label">
                    <?php echo 'Room Charges/Night'; ?>
                </label>
                <div class="col-md-7">
                    <input class="form-control" type="text" id="roomchargescnt"   value="" name="roomchargescnt">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <label class="col-md-5 control-label">
                    <?php echo 'Room Charges/Night'; ?>
                </label>
                <div class="col-md-7">
                    <input class="form-control roomchargesft" type="text" id="roomchargesft" value="" name="roomchargesft">
                </div>
            </div>
        </div>

    </div>

    <div class="row mb10">
        <div class="col-md-6">
            <div class="row">
                <label class="col-md-5 control-label">
                    <?php echo 'Adults'; ?>
                </label>
                <div class="col-md-7">
                    <input class="form-control adultscnt" type="text"  id="adultscnt"  name="adultscnt">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <label class="col-md-5 control-label">
                    <?php echo 'Adults'; ?>
                </label>
                <div  class="col-md-7">
                    <input type="text" class="form-control adults" name="adults[]" onkeyup="findadult()" value="">
                </div>
            </div>
        </div>

    </div>

    <div class="row mb10">
        <div class="col-md-6">
            <div class="row">
                <label class="col-md-5 control-label">
                    <?php echo 'Children'; ?>
                </label>
                <div class="col-md-7">
                    <input class="form-control" type="text" value="" id="childrencnt"  name="childrencnt">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <label class="col-md-5 control-label">
                    <?php echo 'Children'; ?>
                </label>
                <div class="col-md-7">
                    <input  type="text" name="children[]" class="maxchild form-control" onkeyup="findchild()" value=""/>
                </div>
            </div>
        </div>

    </div>

    <div class="row mb10">
        <div class="col-md-6">
            <table id="chargingdetails">

                <tr>
                    <td>Current Room Charges/Night</td>
                    <td><input class="form-control" type="text" value="" id="currentroomchargespernight"  name="currentroomchargespernight"></td>

                </tr>
                <tr>
                    <td>New Room Charges/Night</td>
                    <td><input class="form-control" type="text" value="" id="newroomchargespernight"  name="newroomchargespernight"></td>


                </tr>
                <tr>
                    <td>Charges Difference Payable/Receivable</td>
                    <td><input class="form-control" type="text" value="" id="chargesdifferencepayable"  name="chargesdifferencepayable"></td>
                </tr>
                <tr>
                    <td>Estimated Charges Payable/Receivable</td>
                    <td><input class="form-control" type="text" value="" id="estiamteddifferencepayable"  name="estimateddifferencepayable"></td>
                </tr>


            </table>
        </div>
        <div class="col-md-6">
            <div class="row mb10">
                <label class="col-md-5 control-label">
                    <?php echo 'No. of Nights Stay'; ?>
                </label>
                <div class="col-md-7">
                    <input type="text" name="night[]"
                           class="form-control staynight"
                           onkeyup="updatenetprice()"
                           value="">
                </div>
            </div>
            <div class="row mb10">
                <label class="col-md-5 control-label">
                    <?php echo 'Discount'; ?>
                </label>
                <div class="col-md-7">
                    <input type="text" class="discount form-control" name="discount[]"  onkeyup="updatenetpricewithdiscount()" value="0"/>
                </div>
            </div>
            <div class="row mb10">
                <label class="col-md-5 control-label">
                    <?php echo 'Tax Rates'; ?>
                </label>
                <div class="col-md-7">
                    <!--                            <input type="text" id="tax_id" class="tax_id form-control" name="taxrate"  value="0">-->
                    <?php
                    echo getDropDownData_taxrates('pm_tax', 'taxrate', 'tax_id', 'taxname', '', 'required', $db);
                    ?>
                </div>
            </div>
            <div class="row mb10">
                <label class="col-md-5 control-label">
                    <?php echo 'Price'; ?>
                </label>
                <div class="col-md-7">
                    <input type="text" name="netamount[]" class="netamount form-control" value="" >
                </div>
            </div>
            <div class="row mb10">
                <label class="col-md-5 control-label">
                    <?php echo 'Reason to Shift'; ?>
                </label>
                <div class="col-md-7">
                    <input type="text" class="form-control" value="" id="reasonft" name="reasonft">
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-6 clearfix pb15 text-right" style="margin-left: 47%;">
                <button id="saveshiftroom" type="button" name="save" class="btn btn-success mt15">
                    <i class="fa fa-save"></i>Save
                </button>
                <button  id="savenexit"  type="button" name="Save and Exit" class="btn btn-success mt15">
                    Save and Exit
                </button>
                <a href="index.php?view=list&id=0">
                    <button id="Backtolist" type="button" name="Back to List" class="btn btn-success mt15">
                        Back to List
                    </button></a>
            </div>
        </div>

<!--    </div>-->
    <!--Container END-->
</div>



<!--        </form>-->





<?php
}else echo '<p>'.$texts['ACCESS_DENIED'].'</p>'; ?>
</div>

<?php

//echo "<Pre>";
//
//   print_r( $_POST );
//
//exit;

?>
</body>
</html>
<?php
if(empty($_SESSION['msg_error'])) recursive_rmdir(SYSBASE.'medias/'.MODULE.'/tmp/'.$_SESSION['token']);
$_SESSION['redirect'] = false;
$_SESSION['msg_error'] = array();
$_SESSION['msg_success'] = array();
$_SESSION['msg_notice'] = array(); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
<script type="text/javascript">
    $(document).ready(function () {
        $(".shifttoroom").select2({
            width : '100px'
        });

        $("#roomshiftsearch").click(function () {
            var shiftdate = $("#shiftdate").val();
            var booking_id = $("#booking_id").val();
            var customer_id = $("#customer_id").val();
            //var hiddenbookingparent = $("#hiddenbookingparent").val();

            if(shiftdate=='' ){

                $(".error").html('');
                $(".error").append('<p style="color:red;">Shift Date Cannot be Empty</p>');
                return false;
            } if(booking_id=="" ){

                $(".error").html('');
                $(".error").append('<p style="color:red;">Booking ID Cannot be Empty</p>');
                return false;
            } if(customer_id==""){

                $(".error").html('');
                $(".error").append('<p style="color:red;">Customer ID Cannot be Empty</p>');
                return false;
            }


            if (shiftdate != '' && booking_id != '' && customer_id != '' ) {
                $.ajax({
                    url: '<?php echo $base.ADMIN_FOLDER ?>/modules/customroomshift/roomshiftajax.php',
                    type: 'POST',
                    data: {
                        'shiftdate': shiftdate,
                        'booking_id': booking_id,
                        'customer_id': customer_id,
                        'getbookings': 1
                    },
                    dataType: 'json',
                    success: function (data) {
                        $.each(data, function() {
                            $.each(this, function(k, v) {

                                if(k == 'ROOM_ID') {

                                    $('#currentroom').append($('<option>', {
                                        value: v,
                                        text: v,
                                    }));
                                    currentroompopulate(v);


                                }
                            });






                            $.each(this, function(k, v) {
                                if(k == 'bookingparentid'){
                                    $("#hiddenbookingparent").val(v);
                                }
                            });
                            $.each(this, function(k, v) {
                                if(k == 'unit'){
                                    $("#hiddenunit").val(v);
                                }
                            });


                        });
                        // $("#roomtype_id").val(data[0].roomtype);
                        //  $("#roomchargescnt").val(data[0].RoomCharges);
                        $("#roomchargescnt").prop('disabled', true);

                        //  $("#adultscnt").val(data[0].Adults);
                        $("#adultscnt").prop('disabled', true);
                        //  $("#childrencnt").val(data[0].Children);
                        $("#childrencnt").prop('disabled', true);

                    }
                });
            } else {



            }



        });

        $("#saveshiftroom").click(function () {
            var bookingidid = $("#booking_id option:selected"). val();
            var currentroom = $("#currentroom").val();
            var hiddenbookingparent = $("#hiddenbookingparent").val();
            var hiddenunit = $("#hiddenunit").val();
            var shiftdate = $("#shiftdate").val();

            var transid= $("input:text[name='transid']").val();
            var customer= $("#customer_id").val();


            var shiftroomid = $("#shifttoroom option:selected"). val();
            var roomtype_id_shift = $("#roomtype_id_shift option:selected"). val();
            var roomchargesft = $("#roomchargesft").val();
            var adults = $(".adults").val();
            var maxchild  = $(".maxchild").val();
            var staynight = $(".staynight").val();
            var discount = $(".discount").val();
            var tax_id  = $("#tax_id").val();
            var netamount  = $(".netamount").val();


            if (currentroom == ''||currentroom == null) {
                $(".errorsearch").html('');
                $(".errorsearch").addClass("alert alert-danger");
                $(".errorsearch").append('<p style="color:red;">Provided Data is Incomplete.</p>');
                return false;
            }
            if (shiftroomid == ''||shiftroomid == null) {
                $(".errorsearch").html('');
                $(".errorsearch").addClass("alert alert-danger");
                $(".errorsearch").append('<p style="color:red;">Provided Data is Incomplete.</p>');
                return false;
            }
            if (netamount == ''||netamount == null) {
                $(".errorsearch").html('');
                $(".errorsearch").addClass("alert alert-danger");
                $(".errorsearch").append('<p style="color:red;">Provided Data is Incomplete.</p>');
                return false;
            }


            $.ajax({
                url: '<?php echo $base.ADMIN_FOLDER ?>/modules/customroomshift/roomshiftajax.php',
                type: 'POST',
                data: {
                    bookingidid:bookingidid,
                    currentroom:currentroom,
                    hiddenbookingparent:hiddenbookingparent,
                    hiddenunit:hiddenunit,
                    shiftdate:shiftdate,
                    shiftroomid:shiftroomid,
                    roomtype_id_shift:roomtype_id_shift,
                    roomchargesft:roomchargesft,
                    adults:adults,
                    maxchild:maxchild,
                    staynight:staynight,
                    discount:discount,
                    tax_id:tax_id,
                    netamount:netamount,
                    transid:transid,
                    customer:customer,

                    saveshiftdata:1

                },
                dataType: 'json',
                success: function (data) {
                    if(data==1){




                    }



                }//end of success
            });
        });
        $("#currentroom").click(function (event) {

            if(event.target == this){
                var roomID = $(this).val();
                $.ajax({
                    url: '<?php echo $base.ADMIN_FOLDER ?>/modules/customroomshift/roomshiftajax.php',
                    type: 'POST',
                    data: {
                        roomID:roomID,
                        getRoomType:1
                    },
                    dataType: 'json',
                    success: function (data) {
                        var bookingid = $("#booking_id option:selected"). val();
                        $("#roomtype_id_current").val(data[0].id_roomtype).trigger('chosen:updated');
                        $('#roomtype_id_current').prop('disabled', true).trigger("chosen:updated");
                        $("#roomcapacitycnt").val(data[0].maxpeople);
                        $("#roomcapacitycnt").prop('disabled', true);

                        $("#roomchargescnt").val(data[0].price);
                        $("#roomchargescnt").prop('disabled', true);

                        $("#adultscnt").val(data[0].maxadults);
                        $("#adultscnt").prop('disabled', true);
                        $("#childrencnt").val(data[0].maxchild);
                        $("#childrencnt").prop('disabled', true);
                        $.ajax({
                            url: '<?php echo $base.ADMIN_FOLDER ?>/modules/customroomshift/roomshiftajax.php',
                            type: 'POST',
                            data: {
                                bookingid:bookingid,
                                roomcharges:1
                            },
                            dataType: 'json',
                            success: function (data) {


                                $("#currentroomchargespernight").val(data[0].currentroomcharges);
                                $('#currentroomchargespernight').prop('disabled',true);



                            } //end of success

                        });





                    }//end of success
                });

            }


        });

        function currentroompopulate(roomID){
            $.ajax({
                url: '<?php echo $base.ADMIN_FOLDER ?>/modules/customroomshift/roomshiftajax.php',
                type: 'POST',
                data: {
                    roomID:roomID,
                    getRoomType:1
                },
                dataType: 'json',
                success: function (data) {
                    var bookingid = $("#booking_id option:selected"). val();
                    $("#roomtype_id_current").val(data[0].id_roomtype).trigger('chosen:updated');
                    $('#roomtype_id_current').prop('disabled', true).trigger("chosen:updated");
                    $("#roomcapacitycnt").val(data[0].maxpeople);
                    $("#roomcapacitycnt").prop('disabled', true);

                    $("#roomchargescnt").val(data[0].price);
                    $("#roomchargescnt").prop('disabled', true);

                    $("#adultscnt").val(data[0].maxadults);
                    $("#adultscnt").prop('disabled', true);
                    $("#childrencnt").val(data[0].maxchild);
                    $("#childrencnt").prop('disabled', true);
                    $.ajax({
                        url: '<?php echo $base.ADMIN_FOLDER ?>/modules/customroomshift/roomshiftajax.php',
                        type: 'POST',
                        data: {
                            bookingid:bookingid,
                            roomcharges:1
                        },
                        dataType: 'json',
                        success: function (data) {


                            $("#currentroomchargespernight").val(data[0].currentroomcharges);
                            $('#currentroomchargespernight').prop('disabled',true);



                        } //end of success

                    });





                }//end of success
            });
        }


        $("#shifttoroom").change(function (event) {
            if(event.target == this){
                var emptyroomid = $(this).val();
                $.ajax({
                    url: '<?php echo $base.ADMIN_FOLDER ?>/modules/customroomshift/roomshiftajax.php',
                    type: 'POST',
                    data: {
                        emptyroomid:emptyroomid,
                        getEmptyRoom:2
                    },
                    dataType: 'json',
                    success: function (data) {
                        $("#roomtype_id_shift").val(data[0].id_roomtype).trigger('chosen:updated');
                        $('#roomtype_id_shift').prop('disabled', true).trigger("chosen:updated");
                        $("#roomcapacityft").val(data[0].maxpeople);
                        $("#roomcapacityft").prop('disabled', true);
                        $("#roomchargesft").val(data[0].price);
                        $("#roomchargesft").prop('disabled', true);
                        $(".adults").val(data[0].maxadults);
                        $(".maxchild").val(data[0].maxchild);
                        $("#tax_id").val(data[0].taxrates).trigger('chosen:updated');
                        $('#tax_id').prop('disabled', true).trigger("chosen:updated");
                        $("#childrenft").prop('disabled', true);
                    }//end of success
                });

            }


        });






        $("#booking_id").change(function (event) {
            if(event.target == this){
                var bookingid = $(this).val();
                $.ajax({
                    url: '<?php echo $base.ADMIN_FOLDER ?>/modules/customroomshift/roomshiftajax.php',
                    type: 'POST',
                    data: {
                        bookingid:bookingid,
                        customerbookinglink:1
                    },
                    dataType: 'json',
                    success: function (data) {
                        $("#customer_id").val(data[0].id).trigger('chosen:updated');

                    }//end of success
                });

            }


        });


        $("#customer_id").change(function (event) {
            if(event.target == this){
                var customerid = $(this).val();
                $.ajax({
                    url: '<?php echo $base.ADMIN_FOLDER ?>/modules/customroomshift/roomshiftajax.php',
                    type: 'POST',
                    data: {
                        customerid:customerid,
                        customerbookinglink:2
                    },
                    dataType: 'json',
                    success: function (data) {
                        $("#booking_id").val(data[0].id).trigger('chosen:updated');

                    }//end of success
                });

            }


        });





    });


    //    =============New Functions Added===============

    function findadult() {

        var id = $("#shifttoroom option:selected"). val();
        $(".error").html("");
        var maxadults = $(".adults" ).val();
        if (maxadults != '') {
            if (isNaN(maxadults)) {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'remove_id': id,
                        'compareadults': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        if (maxadults > data[0].maxadults) {
                            $(".error").html("");
                            $(".error").append("<span>Adults cannot be alphabets in " + data[0].roomtitle + " </span>");
                        }
                    }

                });

            } else {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'remove_id': id,
                        'compareadults': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        if (maxadults > data[0].maxadults) {
                            $(".error").html("");
                            $(".error").append("<span>Adults cannot be more than " + data[0].maxadults + " in " + data[0].roomtitle + " </span>");
                        }
                    }

                });

            }
        }
    }

    function findchild() {
        var id = $("#shifttoroom option:selected"). val();
        $(".error").html("");
        var maxchild = $(".maxchild" ).val();
        if (maxchild != '') {
            if (isNaN(maxchild)) {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'remove_id': id,
                        'comparechild': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        if (maxchild > data[0].maxchild) {
                            $(".error").html("");
                            $(".error").append("<span>Childs cannot be alphabets in " + data[0].roomtitle + " </span>");
                        }

                    }

                });
            } else {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'remove_id': id,
                        'comparechild': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        if (maxchild > data[0].maxchild) {
                            $(".error").html("");
                            $(".error").append("<span>Childs cannot be more than " + data[0].maxchild + " in " + data[0].roomtitle + " </span>");
                        }

                    }

                });
            }
        }

    }
    //staynight updateprice
    function updatenetprice() {
        var id = $("#shifttoroom option:selected"). val();
        $(".netamount").val('');
        var nightcharge = $(".roomchargesft" ).val();
        var staynight = $(".staynight" ).val();
        var tax_id = $("#tax_id").val();
        if (isNaN(staynight)) {
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'remove_id': id,
                    'comparechild': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".error").html("");
                    $(".error").append("<span>Please Use Numeric value Instead " + staynight + " in " + data[0].roomtitle + " </span>");
                    return false;
                }
            });
        } else {
            $(".error").html("");
            $(".error").html("");
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'tax_id': tax_id,
                    'gettaxrate': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".error").html("");
                    //totalnetamount without vat
                    var discount = 0;
                    var totalnetamountval = ((parseFloat(nightcharge))*(parseFloat(staynight)));
                    var getdiscountamount = (((totalnetamountval) * (discount / 100 )));
                    var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                    var taxamount = parseFloat(data[0].taxrates);
                    var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                    var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                    $(".netamount").val(totalnet.toFixed(2));
                    var est = (parseFloat(totalnet)/parseFloat(staynight));
                    $("#newroomchargespernight").val(est);
                    chargedifference();


                }
            });
        }
    }

    function chargedifference(){
        var a=$("#currentroomchargespernight").val();
        var b=$("#newroomchargespernight").val();
        var c=b-a;
        var d=$(".staynight").val()*c;
        $("#chargesdifferencepayable").val(c);
        $("#estiamteddifferencepayable").val(d);
    }

    function updatenetpricewithdiscount() {

        $(".netamount").val('');
        var nightcharge = $(".roomchargesft" ).val();
        var staynight = $(".staynight").val();
        var discount = $(".discount").val();
        var tax_id = $("#tax_id" ).val();
        if (staynight != '') {
            $(".error").html("");
            if (discount == '') {
                discount = 0;
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'tax_id': tax_id,
                        'gettaxrate': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                        var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                        var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                        var taxamount = parseFloat(data[0].taxrates);
                        var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                        var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                        var estimate=totalnet/staynight;
                        $("#estiamteddifferencepayable").val(estimate);
                        $(".netamount").val(totalnet.toFixed(2));
                    }
                });
            } else {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'tax_id': tax_id,
                        'gettaxrate': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {

                        var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                        var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                        var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                        var taxamount = parseFloat(data[0].taxrates);
                        var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                        var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                        $(".netamount").val(totalnet.toFixed(2));
                    }
                });
            }
        } else {
            $(".error").html("");
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'remove_id': id,
                    'comparechild': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".error").html("");
                    $(".error").append("<span>Number of nights cannot be empty in " + data[0].roomtitle + " </span>");
                    return false;
                }
            });
        }
    }
    //    =============New Functions Added Ended===============






</Script>