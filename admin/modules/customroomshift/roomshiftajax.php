<?php
/**
 * Created by PhpStorm.
 * User: ARfat ali
 * Date: 30/7/2018
 * Time: 12:30 PM
 */
define('ADMIN', true);

define('SYSBASE', str_replace('\\', '/', realpath(dirname(__FILE__) . '/../../../') . '/'));

require_once(SYSBASE . 'common/lib.php');
require_once(SYSBASE . 'common/define.php');


if (isset($_POST['getbookings'])) {
    if ($_POST['getbookings'] == 1) {

        $shiftdate = $_POST['shiftdate'];
        $booking_id = $_POST['booking_id'];
        $customer_id = $_POST['customer_id'];
        $getbookingdata = $db->query("SELECT  unit,booking_parent_id as bookingparentid,room_id as ROOM_ID
                                             FROM pm_booking WHERE id=$booking_id")->fetchAll();
        echo json_encode($getbookingdata);
    }


}
if (isset($_POST['getRoomType'])) {
    if ($_POST['getRoomType'] == 1) {
    $roomid=$_POST['roomID'];
    $roomtype = $db->query("Select id_roomtype,price,maxpeople,maxadults,maxchild from pm_room WHERE id=$roomid and lang = 2;")->fetchAll();
        echo json_encode($roomtype);
    }
}

if (isset($_POST['getEmptyRoom'])) {
    if ($_POST['getEmptyRoom'] == 1) {
        $result = $db->query("Select
  pm_booking.room_id,
  pm_room.roomtitle as RoomTitle
from
  pm_booking
  INNER Join pm_room ON pm_room.id = pm_booking.room_id
where
  booking_status IN ('empty', 'cancel')
  and pm_room.lang = 2")->fetchAll();
        echo json_encode($result);
    }
    if ($_POST['getEmptyRoom'] == 2) {
       $emptyroomid=$_POST['emptyroomid'];
       $q="Select * from pm_room where id=$emptyroomid and lang=2 ";
        $result = $db->query($q)->fetchAll();
        echo json_encode($result);
    }
}


if (isset($_POST['savedata'])) {
    if ($_POST['savedata'] == 1) {

        $shift_room = "INSERT INTO pm_roomshift (booking_id,room_id) VALUES ($booking_id,$room_id)";
        $db->exec($shift_room);
        echo 1;
    }
}

if (isset($_POST['customerbookinglink'])) {
    if ($_POST['customerbookinglink'] == 1) {
        $bookingID=$_POST['bookingid'];
        $q= "Select id from pm_booking_customer where id=$bookingID";
        $getcustomerselect=$db->query($q)->fetchAll();;
        echo json_encode($getcustomerselect);
    }
    if ($_POST['customerbookinglink'] == 2) {

        $customerID=$_POST['customerid'];
        $q= "Select id from pm_booking_customer where booking_id=$customerID";
        $getbookingselect=$db->query($q)->fetchAll();;
        echo json_encode($getbookingselect);
    }
}


if (isset($_POST['saveshiftdata'])) {
    if ($_POST['saveshiftdata'] == 1) {
        $currentroomid=$_POST['currentroom'];
        $bookingidid=$_POST['bookingidid'];
        $hiddenbookingparent=$_POST['hiddenbookingparent'];
        $hiddenunit=$_POST['hiddenunit'];
        $shiftdate=$_POST['shiftdate'];

        $shiftroomid=$_POST['shiftroomid'];
        $roomchargesft=$_POST['roomchargesft'];

        $adults=$_POST['adults'];
        $maxchild=$_POST['maxchild'];
        $staynight=$_POST['staynight'];
        $discount=$_POST['discount'];
        $tax_id=$_POST['tax_id'];
        $netamount=$_POST['netamount'];

        $transid=$_POST['transid'];
        $customer=$_POST['customer'];
        $people=$adults+$maxchild;

        $shiftroomstatus = "UPDATE pm_booking SET booking_status = 'shifted' WHERE id=$bookingidid";
        $db->exec($shiftroomstatus);
        $insertbooking = "INSERT INTO `pm_booking` 
            (`add_date`, 
             `edit_date`, 
             `nights`, 
             `adults`, 
             `children`, 
             `discount`, 
             `booking_parent_id`, 
             `nightcharge`, 
             `tax_rate`, 
             `unit`, 
             `room_id`, 
             `booking_net_amount`, 
             `booking_status`) 
VALUES      ($shiftdate, 
             '', 
             $staynight, 
             $adults, 
             $maxchild, 
              $discount, 
             $hiddenbookingparent, 
             $roomchargesft, 
             $tax_id, 
             $hiddenunit, 
             $shiftroomid, 
             $netamount, 
             'reserved' )";
        $db->exec($insertbooking);
        $updateshiftroom = "INSERT INTO pm_roomshift(lang,add_date,booking_id,transid,customer,roomfrom,roomto,people,charges) VALUES ('2','$shiftdate','$bookingidid','$transid','$customer','$currentroomid','$shiftroomid','$people','$roomchargesft')";
        $db->exec($updateshiftroom);
        echo 1;


    }
}

if (isset($_POST['roomcharges'])) {
    if ($_POST['roomcharges'] == 1) {

        $bookingid = $_POST['bookingid'];

        $currentroomchargespernight = $db->query("SELECT booking_net_amount/nights as currentroomcharges FROM `pm_booking` WHERE  id=$bookingid")->fetchAll();
        echo json_encode($currentroomchargespernight);
    }
    if ($_POST['roomcharges'] == 2) {

        $bookingid = $_POST['bookingid'];

        $currentroomchargespernight = $db->query("SELECT booking_net_amount/nights as currentroomcharges FROM `pm_booking` WHERE  id=$bookingid")->fetchAll();
        echo json_encode($currentroomchargespernight);
    }


}
