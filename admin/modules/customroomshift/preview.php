<?php

define('ADMIN', true);
define('SYSBASE', str_replace('\\', '/', realpath(dirname(__FILE__) . '/../../../') . '/'));
require_once(SYSBASE . 'common/lib.php');
require_once(SYSBASE . 'common/define.php');
?>
<!DOCTYPE html>
<html>
<head>

    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<style type="text/css">
    .invoice-title h2, .invoice-title h3 {
        display: inline-block;
    }
    .table > tbody > tr > .no-line {
        border-top: none;
    }
    .table > thead > tr > .no-line {
        border-bottom: none;
    }
    .table > tbody > tr > .thick-line {
        border-top: 2px solid;
    }

</style>
<body>
<div class="container-fluid print_bill">
    <input type="hidden" class="booking_id_for_print">
    <div class="row">
        <div class="col-md-12">
            <div class="invoice-title">
                <h2>MHSSCO</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <?php
            //error_reporting(E_ALL);
            $newbook_id= '';
            $parentbookingid='';
            if(isset($_GET['id'])){
                $newbook_id = $_GET['id'];
            }
            $firstquery = $db->query("SELECT booking_parent_id from pm_booking where id=$newbook_id");
            foreach($firstquery as $data){
                $parentbookingid=$data['booking_parent_id'];
            }
            $query = $db->query("SELECT pm_roomshift.transid, 
		pm_roomshift.add_date, 
		pm_booking_customer.customer_name 
FROM   `pm_booking` 
       LEFT JOIN pm_roomshift 
              ON pm_roomshift.booking_id = pm_booking.booking_parent_id
       LEFT JOIN pm_booking_customer 
              ON pm_booking.booking_parent_id = pm_booking_customer.booking_id 
WHERE  pm_booking.booking_parent_id =$parentbookingid
       AND booking_status = 'reserved'");
            foreach($query as $data){ ?>
            <div class="table">
                <div class="col-md-4 col-xs-4">
                    <div class="row">
                        <div class="col-md-7 col-xs-7">
                            <label>Transaction ID</label>
                        </div>
                        <div class="col-md-5 col-xs-5">
                            <?php echo $data['transid'] ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-xs-5">
                            <label>Customer ID/Name</label>
                        </div>
                        <div class="col-md-7 col-xs-7">
                           <?php echo $data['customer_name']  ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-xs-4">
                    <div class="row">
                        <div class="col-sm-7 col-xs-7">
                            <label>Shift Date</label>
                        </div>
                        <div class="col-sm-5 col-xs-5">
                          <?php echo $data['add_date'] ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-5 col-xs-5">
                            <label>Booking ID</label>
                        </div>
                        <div class="col-sm-7 col-xs-7">
                           <?php echo $parentbookingid;  ?>
                        </div>
                    </div>

                </div>

                <?php }?>

            </div>
        </div>
    </div>
    <?php
    $shiftoroomid='';
    $shiftprice='';
    $shiftmaxpeople='';
    $shiftmaxadults='';
    $shiftmaxchild='';
    $shiftroomnightstay='';
    $shiftroomtype='';
    $shiftdiscount='';
    $shifttaxrates='';

    $shiftnights='';
    $shiftdiscount='';
    $shifttaxname='';
    $shiftnetamount='';
    $shiftoroomreason='';



    $roomid='';
    $price='';
    $maxpeople='';
    $maxadults='';
    $maxchild='';
    $roomtype='';
    $query = $db->query("SELECT room_id from pm_booking where id=$newbook_id");
    foreach ($query as $data){
        $roomid=$data['room_id'];
    }
    $currentroomquery = $db->query("SELECT maxpeople,price,maxadults,maxchild,pm_roomtypes.roomtype from pm_room LEFT JOIN pm_roomtypes on pm_room.id_roomtype=pm_roomtypes.id Where pm_roomtypes.lang=2 AND pm_room.lang=2 AND pm_room.id=$roomid");
    foreach ($currentroomquery as $data){
        $maxpeople=$data['maxpeople'];
        $price=$data['price'];
        $maxadults=$data['maxadults'];
        $maxchild=$data['maxchild'];
        $roomtype=$data['roomtype'];
    }


    $shiftquery = $db->query("SELECT reason,roomto from pm_roomshift where booking_id=$parentbookingid ");

    foreach ($shiftquery as $data){
        $shiftoroomid=$data['roomto'];
        $shiftoroomreason=$data['reason'];
    }
    $shiftroomquery = $db->query("SELECT maxpeople,price,maxadults,maxchild,pm_roomtypes.roomtype from pm_room LEFT JOIN pm_roomtypes on pm_room.id_roomtype=pm_roomtypes.id Where pm_roomtypes.lang=2 AND pm_room.lang=2 AND pm_room.id=$shiftoroomid");
    foreach ($shiftroomquery as $datashiftroom){
        $shiftmaxpeople=$datashiftroom['maxpeople'];
        $shiftprice=$datashiftroom['price'];
        $shiftmaxadults=$datashiftroom['maxadults'];
        $shiftmaxchild=$datashiftroom['maxchild'];
        $shiftroomtype=$datashiftroom['roomtype'];
    }



    $shiftroomquery2 = $db->query("SELECT booking_net_amount,nights,discount,pm_tax.taxname from pm_booking LEFT JOIN pm_tax ON pm_tax.id=pm_booking.tax_rate WHERE pm_tax.lang=2 and pm_booking.id=$newbook_id");

    foreach ($shiftroomquery2 as $data2){

        $shiftnetamount= $data2['booking_net_amount'];
        $shiftnights= $data2['nights'];
        $shiftdiscount=$data2['discount'];
        $shifttaxname= $data2['taxname'];
    }









    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <strong>Shifting Details</strong>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <tbody>
                                <tr>
                                    <td class="text-left">Current Room/Rooms</td>
                                    <td class="text-left"><?php echo $roomid?></td>
                                    <td class="text-left">Shift to Room</td>
                                    <td class="text-left"><?php echo $shiftoroomid?></td>

                                </tr>
                                    <tr>
                                        <td class="text-left">Room Type</td>
                                        <td class="text-left"><?php echo $roomtype?></td>
                                        <td class="text-left">Room Type</td>
                                        <td class="text-left"><?php echo $shiftroomtype?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Room Capacity</td>
                                        <td class="text-left"><?php echo $maxpeople?></td>
                                        <td class="text-left">Room Capacity</td>
                                        <td class="text-left"><?php echo $shiftmaxpeople?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Room Charges/Night</td>
                                        <td class="text-left"><?php echo $price?></td>
                                        <td class="text-left">Room Charges/Night</td>
                                        <td class="text-left"><?php echo $shiftprice?></td>
                                    </tr>
                                <tr>
                                        <td class="text-left">Adults</td>
                                        <td class="text-left"><?php echo $maxadults?></td>
                                        <td class="text-left">Adults</td>
                                        <td class="text-left"><?php echo $shiftmaxadults?></td>
                                    </tr>
                                <tr>
                                        <td class="text-left">Children</td>
                                        <td class="text-left"><?php echo $maxchild?></td>
                                        <td class="text-left">Children</td>
                                        <td class="text-left"><?php echo $shiftmaxchild; ?></td>
                                    </tr>
                                <tr>
                                    <td class="text-left">
                                            Current Room Charges/Night</td>
                                        <td class="text-left"></td>
                                        <td class="text-left">No. of Nights Stay</td>
                                        <td class="text-left"><?php echo $shiftnights; ?></td>
                                    </tr>
                                <tr>
                                        <td class="text-left">
                                            New Room Charges/Night</td>
                                        <td class="text-left"></td>
                                        <td class="text-left">Discount</td>
                                        <td class="text-left"><?php echo $shiftdiscount;?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            Charges Difference Payable/Receivable</td>
                                        <td class="text-left"></td>
                                        <td class="text-left">Tax Rates</td>
                                        <td class="text-left"><?php echo $shifttaxname; ?></td>
                                    </tr>
                                <tr>
                                        <td class="text-left">
                                            Estimated Charges Payable/Receivable</td>
                                        <td class="text-left"></td>
                                        <td class="text-left">Price</td>
                                        <td class="text-left"><?php echo $shiftnetamount?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                          </td>
                                        <td class="text-left"></td>
                                        <td class="text-left">  Reason to Shift</td>
                                        <td class="text-left"><?php echo $shiftoroomreason;?></td>
                                    </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>