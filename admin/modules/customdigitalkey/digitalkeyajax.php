<?php
/**
 * Created by PhpStorm.
 * User: Arfaat Ali
 * Date: 10/25/2018
 * Time: 11:39 AM
 */

define('ADMIN', true);
define('SYSBASE', str_replace('\\', '/', realpath(dirname(__FILE__).'/../../../').'/'));
require_once(SYSBASE.'common/lib.php');
require_once(SYSBASE.'common/define.php');
include SYSBASE.ADMIN_FOLDER.'/includes/BarcodeGenerator.php';
include SYSBASE.ADMIN_FOLDER.'/includes/BarcodeGeneratorHTML.php';
include SYSBASE.ADMIN_FOLDER.'/includes/BarcodeGeneratorJPG.php';
include SYSBASE.ADMIN_FOLDER.'/includes/BarcodeGeneratorPNG.php';
include SYSBASE.ADMIN_FOLDER.'/includes/BarcodeGeneratorSVG.php';
$generatorPNG = new Picqer\Barcode\BarcodeGeneratorPNG();
$generatorSVG = new Picqer\Barcode\BarcodeGeneratorSVG();
$generatorHTML = new Picqer\Barcode\BarcodeGeneratorHTML();
//echo $generatorHTML->getBarcode('uioui', $generatorPNG::TYPE_CODE_128);
//echo $generatorSVG->getBarcode('081231723897', $generatorPNG::TYPE_EAN_13);
//echo '<img src="data:image/png;base64,' . base64_encode($generatorPNG->getBarcode('081231723897', $generatorPNG::TYPE_CODE_128)) . '">';

//


if (isset($_POST['barcodegenerator'])) {
    if ($_POST['barcodegenerator'] == 1) {
    $keycode=$_POST['keycode'];
    $bar_code_type=$_POST['bar_code_type'];
    if($bar_code_type==1){
        echo json_encode($generatorHTML->getBarcode($keycode, $generatorPNG::TYPE_CODE_128));
    }
    if($bar_code_type==2){
       echo json_encode($generatorSVG->getBarcode($keycode, $generatorPNG::TYPE_EAN_13));
}
    }


}

if (isset($_POST['add']) || (isset($_POST['id']) && $_POST['id'] == 0)) {
    $keycode = $_POST['key_code'];
    $bar_code_type = $_POST['bar_code_type_val'];
    $valid_from = $_POST['valid_from'];
    $valid_to = $_POST['valid_to'];
    $status = $_POST['status'];
    $description = $_POST['description'];
    $svg = '';
    if($bar_code_type == 1){
        $svg = '<img src="data:image/png;base64,' . base64_encode($generatorPNG->getBarcode($keycode, $generatorPNG::TYPE_CODE_128)) . '">';
    }
    if($bar_code_type == 2){
        $svg = '<img src="data:image/png;base64,' . base64_encode($generatorPNG->getBarcode($keycode, $generatorPNG::TYPE_EAN_13)) . '">';
    }
    $query="INSERT INTO `pm_digitalkey`(`lang`,`key_code`, `bar_code_type`, `valid_from`, `valid_to`, `status`, `description`) VALUES ('2','$keycode',$bar_code_type,'$valid_from','$valid_to','$status','$description')";
    $db->exec($query);
    $adminfolder=ADMIN_FOLDER;
    header("Location: ".$_SERVER['BASE']."admin/modules/digitalkey/index.php?view=list");
}

if(isset($_POST['id']) && $_POST['id'] > 0){
    $keycode = $_POST['key_code'];
    $bar_code_type = $_POST['bar_code_type_val'];
    $valid_from = $_POST['valid_from'];
    $valid_to = $_POST['valid_to'];
    $status = $_POST['status'];
    $description = $_POST['description'];
    $svg = '';
    if($bar_code_type == 1){
        $svg = '<img src="data:image/png;base64,' . base64_encode($generatorPNG->getBarcode($keycode, $generatorPNG::TYPE_CODE_128)) . '">';
    }
    if($bar_code_type == 2){
        $svg = '<img src="data:image/png;base64,' . base64_encode($generatorPNG->getBarcode($keycode, $generatorPNG::TYPE_EAN_13)) . '">';
    }
    $id = $_POST['id'];
    $query="UPDATE `pm_digitalkey`set `lang` = 2,`key_code` = '$keycode', `bar_code_type` = $bar_code_type, `valid_from`='$valid_from', `valid_to`='$valid_to', `status`='$status', `description` ='$description' where id = $id";
    $db->exec($query);
    $adminfolder=ADMIN_FOLDER;
    header("Location: ".$_SERVER['BASE']."admin/modules/digitalkey/index.php?view=list");

}

if(isset($_POST['checkkecodeduplication'])) {
    if ($_POST['checkkecodeduplication'] == 1) {

        $keycode = $_POST['keycode'];
        $query = "SELECT * from pm_digitalkey where key_code='$keycode'";
        $result = $db->query($query)->fetchAll();
        if ($result) {
            echo 1;
            exit;
        }
       else {
            $keycode = $_POST['keycode'];
            $bar_code_type = $_POST['bar_code_type'];
            $valid_from = $_POST['valid_from'];
            $valid_to = $_POST['valid_to'];
            $status = $_POST['status'];
            $description = $_POST['description'];
            $query = "INSERT INTO `pm_digitalkey`(`lang`,`key_code`, `bar_code_type`, `valid_from`, `valid_to`, `status`, `description`) VALUES (2,$keycode,$bar_code_type,'$valid_from','$valid_to','$status','$description')";
           $db->exec($query);
           echo 2;
           exit;
        }

    }
}

if(isset($_POST['id']) && $_POST['id'] > 0){
        $keycode = $_POST['key_code'];
        $bar_code_type = $_POST['bar_code_type_val'];
        $valid_from = $_POST['valid_from'];
        $valid_to = $_POST['valid_to'];
        $status = $_POST['status'];
        $description = $_POST['description'];
        $svg = '';
            if($bar_code_type == 1){
                $svg = '<img src="data:image/png;base64,' . base64_encode($generatorPNG->getBarcode($keycode, $generatorPNG::TYPE_CODE_128)) . '">';
            }
            if($bar_code_type == 2){
                $svg = '<img src="data:image/png;base64,' . base64_encode($generatorPNG->getBarcode($keycode, $generatorPNG::TYPE_EAN_13)) . '">';
            }
        $id = $_POST['id'];
        $query="UPDATE `pm_digitalkey`set `lang` = 2,`key_code` = '$keycode', `bar_code_type` = $bar_code_type, `valid_from`='$valid_from', `valid_to`='$valid_to', `status`='$status', `description` ='$description' where id = $id";
        $db->exec($query);
        $adminfolder=ADMIN_FOLDER;
        header("Location: ".$_SERVER['BASE']."admin/modules/digitalkey/index.php?view=list");


}

if (isset($_POST['savereplicateform'])) {

    if ($_POST['savereplicateform']=='1') {
        $keycode = $_POST['key_code'];
        $bar_code_type = $_POST['bar_code_type_val'];
        $valid_from = $_POST['valid_from'];
        $valid_to = $_POST['valid_to'];
        $status = $_POST['status'];
        $description = $_POST['description'];
        $query = "Select * from pm_digitalkey where key_code=$keycode";
        if ($query != null) {
            echo "1";

        } else {
            $query = "Update pm_digitalkey SET key_code ='$keycode' ,bar_code_type='$bar_code_type',valid_from='$valid_from',valid_to='$valid_to',status='$status',description='$description' WHERE id=$id";
            $db->exec($query);

        }


        header("Location: /modules/digitalkey/index.php?view=list");
//        echo SYSBASE.ADMIN_FOLDER;
//        move_uploaded_file($svg,'http://localhost/hotelMS/admin/images/test.svg');


    }
}
