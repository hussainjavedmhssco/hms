<?php

define('ADMIN', true);
define('SYSBASE', str_replace('\\', '/', realpath(dirname(__FILE__) . '/../../../') . '/'));
require_once(SYSBASE . 'common/lib.php');
require_once(SYSBASE . 'common/define.php');
?>
<!DOCTYPE html>
<html>
<head>

    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<style type="text/css">
    .invoice-title h2, .invoice-title h3 {
        display: inline-block;
    }
    .table > tbody > tr > .no-line {
        border-top: none;
    }
    .table > thead > tr > .no-line {
        border-bottom: none;
    }
    .table > tbody > tr > .thick-line {
        border-top: 2px solid;
    }

</style>
<body>
<div class="container-fluid print_bill">
    <input type="hidden" class="booking_id_for_print">
    <div class="row">
        <div class="col-md-12">
            <div class="invoice-title">
                <h2>MHSSCO</h2>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12 col-md-12">
            <?php
            //error_reporting(E_ALL);
            $pm_sellepackage_id= '';
            $parentbookingid='';
            if(isset($_GET['id'])){
                $pm_sellepackage_id = $_GET['id'];
            }
            $firstquery = $db->query("SELECT *,packname as PackageName from pm_sellpackage JOIN pm_package ON pm_sellpackage.packagetitleid=pm_package.id where pm_sellpackage.id=$pm_sellepackage_id");

//            $query = $db->query("SELECT  pm_sellpackage_room.*,
//                                                      pm_packages.id as sellpackage_parent_id,
//                                                        pm_room.roomcode,
//                                                        pm_room.roomtitle,
//                                                        pm_packages.nightcharge,
//                                                        pm_packages.adults,
//                                                        pm_packages.nights,
//                                                        pm_packages.discount,
//                                                        pm_packages.children,
//                                                        (
//                                                          SELECT
//                                                            pm_room.id_roomtype
//                                                          FROM
//                                                            pm_room
//                                                          where
//                                                            pm_room.id = pm_sellpackage_room.room_id
//                                                            and pm_room.lang = 2
//                                                        ) as room_type_id,
//                                                        (
//                                                          SELECT
//                                                            pm_roomtypes.roomtype
//                                                          from
//                                                            pm_roomtypes
//                                                          where
//                                                            pm_roomtypes.id = pm_room.id_roomtype
//                                                            and pm_roomtypes.lang = 2
//                                                        ) as room_type_name
//                                                      FROM
//                                                        pm_sellpackage_room
//                                                        INNER JOIN pm_room ON pm_sellpackage_room.room_id = pm_room.id
//                                                      WHERE
//                                                        pm_sellpackage_room.sellpackage_parent_id = $newbook_id
//                                                        and pm_room.lang = 2");
            foreach($firstquery as $data){ ?>
            <div class="table">
                <div class="col-md-4 col-xs-4">
                    <div class="row">
                        <div class="col-md-7 col-xs-7">
                            <label>Package ID</label>
                        </div>
                        <div class="col-md-5 col-xs-5">
                            <?php echo $data['packagetitleid'] ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-xs-5">
                            <label>Package Title</label>
                        </div>
                        <div class="col-md-7 col-xs-7">
                            <?php echo $data['PackageName']  ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-xs-4">
                    <div class="row">
                        <div class="col-sm-7 col-xs-7">
                            <label>Booking Date</label>
                        </div>
                        <div class="col-sm-5 col-xs-5">
                            <?php echo $data['bookingdate'] ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-5 col-xs-5">
                            <label>Check In Date</label>
                        </div>
                        <div class="col-sm-7 col-xs-7">
                            <?php echo $data['checkIndate']  ?>
                        </div>
                    </div>

                </div>

                <div class="col-md-4 col-xs-4">
                    <div class="row">
                        <div class="col-sm-7 col-xs-7">
                            <label>Check Out Date</label>
                        </div>
                        <div class="col-sm-5 col-xs-5">
                            <?php echo $data['checkOutdate'] ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-5 col-xs-5">
                            <label>Valid From</label>
                        </div>
                        <div class="col-sm-7 col-xs-7">
                            <?php echo $data['validefrom'] ?>
                        </div>
                    </div>

                </div>

                <div class="col-md-4 col-xs-4">
                    <div class="row">
                        <div class="col-sm-7 col-xs-7">
                            <label>Valid To</label>
                        </div>
                        <div class="col-sm-5 col-xs-5">
                            <?php echo $data['validto'] ?>
                        </div>
                    </div>


                </div>

                <?php }?>

            </div>
        </div>
    </div>
    <?php

    $roomsquery = $db->query("SELECT  pm_sellpackage_room.*,
                                                    pm_room.roomcode,
                                                        pm_room.roomtitle,
                                                        pm_sellpackage_room.nightcharge,
                                                        pm_sellpackage_room.adults,
                                                        pm_sellpackage_room.nights,
                                                        pm_sellpackage_room.discount,
                                                        pm_sellpackage_room.children,
                                                        (
                                                          SELECT
                                                            pm_room.id_roomtype
                                                          FROM
                                                            pm_room
                                                          where
                                                            pm_room.id = pm_sellpackage_room.room_id
                                                            and pm_room.lang = 2
                                                        ) as room_type_id,
                                                        (
                                                          SELECT
                                                            pm_roomtypes.roomtype
                                                          from
                                                            pm_roomtypes
                                                          where
                                                            pm_roomtypes.id = pm_room.id_roomtype
                                                            and pm_roomtypes.lang = 2
                                                        ) as room_type_name
                                                      FROM
                                                        pm_sellpackage_room
                                                        INNER JOIN pm_room ON pm_sellpackage_room.room_id = pm_room.id
                                                      WHERE
                                                        pm_sellpackage_room.sellpackage_parent_id = $pm_sellepackage_id
                                                        and pm_room.lang = 2");
    ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <strong>Rooms Services and Activities</strong>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <tbody>
                            <tr>
                                <th>Room Code</th>
                                <th>Room Title</th>
                                <th>Room Type</th>
                                <th>Unit</th>
                                <th>Night Charges</th>
                                <th>Adults</th>
                                <th>Children</th>
                                <th>No. of Nights</th>
                                <th>Discount(%)</th>
                                <th>Tax Rate(%)</th>
                                <th>Net Amount</th>
                                <th>Actions</th>
                            </tr>
                            <?php
                            foreach($roomsquery as $data){ ?>
                            <tr id="rec_<?php echo $data['id']; ?>">
                                <td>
                                    <span class="sn"><?php echo $data['roomcode']; ?></span>
                                </td>
                                <td>
                                    <span class="sn"><?php echo $data['roomtitle']; ?></span>
                                </td>
                                <td>
                                    <span class="sn"><?php echo $data['room_type_name']; ?></span>
                                </td>
                                <td>
                                    <?php echo getDropDownDataUnitroom('unit_id', 'unit') ?>
                                </td>
                                <td><input type="text" name="nightcharge[]" value="<?php echo $data['nightcharge']; ?>" size="4"/></td>
                                <td><input type="text" name="adults[]"  value="<?php echo $data['adults']; ?>" size="4"/>
                                </td>
                                <td><input type="text" name="children[]" value="<?php echo $data['children']; ?>" size="4"/></td>
                                <td><input type="text" name="night[]"  value="<?php echo $data['nights']; ?>" size="4"/>
                                </td>
                                <td><input type="text"  value="<?php echo $data['discount']; ?>" size="4"/></td>
                                <td>
                                    <?php
                                    echo getSelectedDropDownData('pm_tax', 'taxrate', 'tax_id' . $data['room_id'], 'taxname', 'Tax', 'required', $data['tax_rate'], $db); ?>

                                </td>
                                <td><input type="text" name="netamount[]"  value="<?php echo $data['booking_net_amount']; ?>" /></td>

                            </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <th style="width: 15%;">Title</th>
                                <th style="width: 15%;">Unit</th>
                                <th>Quantity</th>
                                <th>Charges</th>
                                <th>Discount(%)</th>
                                <th style="width: 17%;">Tax Rate(%)</th>
                                <th style="width: 10%;">Net Amount</th>

                            </tr>
                            <?php
                            $servicequery = $db->query("select * from pm_sellpackage_services where sellpackage_parent_id = $pm_sellepackage_id")->fetchAll();
                            foreach($servicequery as $data){ ?>
                                <tr id="rec-1">
                                    <td><input type="hidden" name="service_package_id"
                                               value="<?php echo $service['id']; ?>"><input type="text" class="serviceid_0<?php echo $service['id']; ?>" name="serviceid[]" value="<?php echo $service['service_id']; ?>" size="4" disabled/></td>

                                    <td><select name="servicedropdown[]"  <?php echo getSelectedDropDownDataspecialpurpose('pm_service', 'servicetitle', 'Service', $db, $service['id']) ?></select>
                                    </td>
                                    <td><select name="serviceunit[]" id="serviceunitdropdownid_0<?php echo $service['id']; ?>" required <?php echo getDropDownDataUnit('Unit', $service['unit_id']) ?></select>
                                    </td>
                                    <td><input type="text" name="servicequantity[]" value="<?php echo $service['quantity']; ?>" size="4" "/>
                                    </td>
                                    <td><input type="text"  name="servicecharges[]" value="<?php echo $service['charges']; ?>" size="4" "/>
                                    </td>
                                    <td><input type="text" name="servicediscount[]" value="<?php echo $service['discount']; ?>" size="4" "/>
                                    </td>
                                    <td><select name="taxdropdown[]" id="taxdropdownid_0<?php echo $service['id']; ?>"  <?php echo getSelectedDropDownDataspecialpurpose('pm_tax', 'taxname', 'Tax', $db, $service['tax_rate']) ?></select>
                                    </td>
                                    <td><input type="text"name="servicenetamount[]" value="<?php echo $service['net_amount']; ?>" size="8" disabled/></td>

                                </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Adult</th>
                                <th>Children</th>
                                <th>Unit</th>
                                <th>Quantity</th>
                                <th>Charges</th>
                                <th>Discount(%)</th>
                                <th>Tax Rate(%)</th>
                                <th>Net Amount</th>
                                <th>Actions</th>
                            </tr>
                            <?php
                            $activityquery = $db->query("select * from pm_sellpackage_activities where sellpackage_parent_id = $pm_sellepackage_id")->fetchAll();
                            foreach($activityquery as $activity){ ?>
                                <tr id="rec-1">
                                    <td><select name="activities[]" id="activitiesdropdownid_0<?php echo $activity['id']; ?>" required class="limitedNumbSelectedSingleactivities getactivitiesdropdown_0<?php echo $activity['id']; ?>"><?php echo getSelectedDropDownDataspecialpurpose('pm_activity', 'activityname', 'Activity', $db, $activity['id']) ?></select>
                                    <td><input type="text" class="activitiesadult_0<?php echo $activity['id']; ?>" name="activitiesadult[]" value="<?php echo $activity['adult']; ?>" onkeyup="findadultact(<?php echo $activity['id']; ?>)" size="4"/></td>
                                    <td><input type="text" class="activitieschild_0<?php echo $activity['id']; ?>" name="activitieschild[]" value="<?php echo $activity['child']; ?>" onkeyup="findchildact(<?php echo $activity['id']; ?>)" size="4"/></td>
                                    <td><select name="activitiesunit[]" id="activitiesunitdropdownid_0<?php echo $activity['id']; ?>" required class="limitedNumbSelectedSingleactivitiesunit test_0<?php echo $activity['id']; ?>"><?php echo getDropDownDataUnit('Unit', $activity['unit_id']) ?></select>
                                    </td>
                                    <td><input type="text" class="activitiesquantity_0<?php echo $activity['id']; ?>" name="activitiesquantity[]" value="<?php echo $activity['quantity']; ?>" size="4" onkeyup="updatenetpriceactivitieseditrow(0<?php echo $activity['id']; ?>)"/>
                                    </td>
                                    <td><input type="text" class="activitiescharges_0<?php echo $activity['id']; ?>" name="activitiescharges[]" value="<?php echo $activity['charges']; ?>" size="4" onkeyup="updatenetpriceactivitieseditrow(0<?php echo $activity['id']; ?>)"/>
                                    </td>
                                    <td><input type="text" class="activitiesdiscount_0<?php echo $activity['id']; ?>" name="activitiesdiscount[]" value="<?php echo $activity['discount']; ?>" size="4" onkeyup="updatenetpriceactivitieseditrow(0<?php echo $activity['id']; ?>)"/>
                                    </td>
                                    <td><select name="taxdropdownactivities[]" id="taxdropdownidactivities_0<?php echo $activity['id']; ?>" required class="limitedNumbSelectedSingleactivitiestax taxdropdownactivities_<?php echo $activity['id']; ?>" onchange="taxactivitieseditchange(<?php echo $activity['id']; ?>)"><?php echo getSelectedDropDownDataspecialpurpose('pm_tax', 'taxname', 'Tax', $db, $activity['tax_rate']) ?></select>
                                    </td>
                                    <td><input type="text" class="activitiesnetamount_0<?php echo $activity['id']; ?>" name="activitiesnetamount[]" value="<?php echo $activity['net_amount']; ?>" size="8" disabled/>
                                    </td>

                                </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>


</body>
</html>