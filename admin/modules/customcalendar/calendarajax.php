<?php
define('ADMIN', true);
define('SYSBASE', str_replace('\\', '/', realpath(dirname(__FILE__) . '/../../../') . '/'));
require_once(SYSBASE . 'common/lib.php');
require_once(SYSBASE . 'common/define.php');

//we will change the lang type 2 to any other for other language show

if (isset($_POST['getrooms'])) {
    if ($_POST['getrooms'] == 1) {
        $checkIn = $_POST['checkIn'];
        $checkOut = $_POST['checkOut'];
        $roomtype = $_POST['roomtype'];
        $staynight = $_POST['staynight'];
        $adult = $_POST['adult'];
        $children = $_POST['children'];
        $dCheckIn = date_create($checkIn);
        $dCheckOut = date_create($checkOut);
        $dateCheckIn = date_format($dCheckIn, "Y-m-d");
        $dateCheckOut = date_format($dCheckOut, "Y-m-d");
        $checkindatetime = strtotime($dateCheckIn);
        $checkoutdatetime = strtotime($dateCheckOut);
        $html = "";
//        if ($roomtype == '') {
//            $html .= "<div class='alert alert-danger'><strong><span><h5 style='color: red;font-weight: 700;text-align: center;'>Please Select Room Type.</h5></span></strong></div>";
//        } else {
            $getfloordata = $db->query("SELECT
              pm_roomtypes.id as ROOMTYPE_ID,
              pm_room.id as ROOM_ID,
              pm_room.roomtitle as ROOM_NAME,
               (select pm_floor.id from pm_floor where pm_room.id_floor = pm_floor.id)  as FLOOR_ID,
			       (select pm_floor.floortitle from pm_floor where pm_floor.id = FLOOR_ID)  as FLOOR_NAME
FROM   pm_roomtypes 
       LEFT JOIN pm_room 
              ON pm_roomtypes.id = pm_room.id_roomtype 
       LEFT JOIN pm_floor 
              ON pm_roomtypes.roomtype = pm_floor.id
              WHERE pm_room.lang = 2 and pm_roomtypes.lang = 2 and pm_floor.lang = 2 and pm_roomtypes.id = $roomtype and 
              (pm_room.checkIndate >= $checkindatetime and pm_room.checkOutdate <= $checkoutdatetime) or (pm_room.booked = 'empty' or pm_room.booked = '') and
               pm_room.maxadults >= $adult
               group by pm_floor.id")->fetchAll();

            if (empty($getfloordata)) {
                $html .= "<div class='alert alert-info'><strong><span><h5 style='color: grey;font-weight: 700;text-align: center;'>No Record Found.</h5></span></strong></div>";
            } else {
                $html = "";
                $html .= "<form method='post' action='http://localhost/HotelMS/admin/modules/booking/booking/index.php?view=form&id=0'>
                      <div class='container-fluid book_html'>
                        <div class='row'>
                            <div class='col-md-12'>
                                <div class='pull-left'><button type='submit' class='btn btn-success' >Book Room</button>
                                </div>
                        </div>
                      </div>
                      <input type='hidden' class='checkIn_date' name='checkIn_date'>
                      <input type='hidden' class='checkOut_date' name='checkOut_date'>
                      <input type='hidden' class='adult_count' name='adult_count'>
                      <input type='hidden' class='child_count' name='child_count'>
                      <input type='hidden' class='night_stay' name='night_stay'>
                      <input type='hidden' class='room_type' name='room_type'>
                      </div>
                      <div class='booking-table'>
                      </div>";
                $html .= "<div class='container-fluid floor_html'>
                    <div class='row'>
                    <div class='col-md-12'>";
                foreach ($getfloordata as $datafloor) {
                    if($datafloor['ROOM_ID'] != '') {
                        if($datafloor['FLOOR_NAME'] != '') {
                            $html .= " <div class='floor'>
                                    <h3>" . $datafloor['FLOOR_NAME'] . "</h3>
                                    </div>";
                            $floorid = $datafloor['FLOOR_ID'];
                            $getroomdata = $db->query("SELECT
              pm_roomtypes.id as ROOMTYPE_ID,
              pm_room.id as ROOM_ID,
              pm_room.roomtitle as ROOM_NAME,
              pm_room.roomcode as ROOM_CODE,
              pm_floor.id as FLOOR_ID,
              pm_floor.floortitle as FLOOR_NAME
              FROM pm_roomtypes
                LEFT JOIN pm_room ON pm_roomtypes.id = pm_room.id_roomtype
                LEFT JOIN pm_floor ON pm_room.id_floor = pm_floor.id
              WHERE pm_room.lang = 2 and pm_roomtypes.lang = 2 and pm_floor.lang = 2 and pm_roomtypes.id = $roomtype and pm_floor.id = $floorid 
              and (pm_room.checkIndate >= $checkindatetime and pm_room.checkOutdate <= $checkoutdatetime || pm_room.checkIndate >= 0 and pm_room.checkOutdate <= 0) and (pm_room.booked = 'empty' or pm_room.booked = '')
              and pm_room.maxadults >= $adult
              and (pm_room.booked = 'empty' or pm_room.booked = '')")->fetchAll();
                            foreach ($getroomdata as $room) {
                                $html .= "<div class='col-md-1 getselected selected" . $room['ROOM_ID'] . "'>
                      <div class='booking-rows'>
                      <div class='booking'>" . $room['ROOM_CODE'] . "</div>
                      <div class='available'>
                      <input type='hidden' class='selected_room_id" . $room['ROOM_ID'] . "' name='selected_room_id[]'>
                      <table>
                            <tr>
                                <td>
                                    <input type='hidden' class='room_id' value=" . $room['ROOM_ID'] . ">
                                        <button class='btn btn-default book_room'>
                                         <i class='fa fa-check'></i>
                                        </button>
                                </td>
                            </tr>
                      </table>
                      </div>
                      </div>";
               $html .= "<span class='crossbox" . $room['ROOM_ID'] . " crossremove" . $room['ROOM_ID'] . "'></span></div>";
                            }

//        $html .= "<div class='col-md-1'><div class='booking-rows'><div class='booking'>" . $dataroom['ROOM_NAME'] . "</div><div class='cross'>";
//        $html .= "<a href='#'><i class='fa fa-check'></i></a></div></div></div>";
                        }
                        }
                }
                $html .= "</div></div>";
                $html .= "</div></form>";
            }
        }
   // }

    echo $html;
}
if (isset($_POST['delete_room'])) {
    $remove_id = $_POST['remove_id'];
    $booking_id = $_POST['booking_id'];
    $query = "DELETE FROM pm_booking_temp where booking_id = $booking_id and room_id = $remove_id";
    $db->exec($query);
}
if (isset($_POST['delete_service'])) {
    $remove_id = $_POST['remove_id'];
    $query = "DELETE FROM pm_booking_services where id = $remove_id";
    $db->exec($query);
}
if (isset($_POST['delete_activity'])) {
    $remove_id = $_POST['remove_id'];
    $query = "DELETE FROM pm_booking_activities where id = $remove_id";
    $db->exec($query);
}
if (isset($_POST['compareadults'])) {
    if ($_POST['compareadults'] == 1) {
        $remove_id = $_POST['remove_id'];
        $query = $db->query("SELECT maxadults,roomtitle  FROM pm_room WHERE id = $remove_id and lang = 2")->fetchAll();
        echo json_encode($query);
    }
}
if (isset($_POST['comparechild'])) {
    if ($_POST['comparechild'] == 1) {
        $remove_id = $_POST['remove_id'];
        $query = $db->query("SELECT maxchild,roomtitle FROM pm_room WHERE id = $remove_id and lang = 2")->fetchAll();
        echo json_encode($query);
    }
}
if (isset($_POST['gettaxrate'])) {
    if ($_POST['gettaxrate'] == 1) {
        $tax_id = $_POST['tax_id'];
        $query = $db->query("SELECT taxrates FROM pm_tax WHERE id = $tax_id and lang = 2")->fetchAll();
        echo json_encode($query);

    }
}
if (isset($_POST['get_user'])) {
    if ($_POST['get_user'] == 1) {
        $customer = $_POST['customer'];
        $query = $db->query("SELECT id as value, customer_name as label,last_name,email,country,company_name,city,phone_no FROM pm_booking_customer WHERE customer_name like '%$customer%' ")->fetchAll();
        echo json_encode($query);

    }
}
if (isset($_POST['getservicedata'])) {
    if ($_POST['getservicedata'] == 1) {
        $serviceid = $_POST['serviceid'];
        $query = $db->query("SELECT id,servicechargesunit,servicecharges,taxes FROM pm_service WHERE id = $serviceid and lang = 2")->fetchAll();
        echo json_encode($query);

    }
}
if (isset($_POST['getactivitydata'])) {
    if ($_POST['getactivitydata'] == 1) {
        $activityid = $_POST['activityid'];
        $query = $db->query("SELECT id,price,maxadults,maxchild,taxrates,activityunit FROM pm_activity WHERE id = $activityid and lang = 2")->fetchAll();
        echo json_encode($query);

    }
}
$lastInsertedId = '';
$current_date = date("Y-m-d H:i:s");

if (isset($_POST['getbookingform'])) {
    if ($_POST['getbookingform'] == 1) {
        $getbooking_id = $db->query("select id from pm_booking where booking_parent_id = ".$_POST['booking_id'][0]." order by id desc limit 1")->fetchAll();
        if($getbooking_id){
           echo "1";
        }else{
            $bookingdate = $_POST["booking_date"][0];
            $checkindate = $_POST['CheckIn_date'][0];
            $checkindateval = '';
            $checkoutdate = $_POST['CheckOut_date'][0];
            $checkoutdateval = '';
            if(isset($checkindate) && $checkindate != ''){
                $checkindateval = $checkindate;
            }else{
                $checkindateval = date("Y-m-d H:i:s");
            }
            if(isset($checkoutdate) && $checkoutdate != ''){
                $checkoutdateval = $checkoutdate;
            }else{
                $checkoutdateval = date("Y-m-d H:i:s");
            }
            $insert_booking_parent = "INSERT INTO pm_booking_parent (booking_date,checkin_date,checkout_date) VALUES ('$current_date','$checkindateval','$checkoutdateval')";
            $db->exec($insert_booking_parent);
            $lastInsertedId = $db->lastInsertId();
        }
                    $discountbooking = '';
                    $unit= 1;
                    for($i=0; $i<sizeof($_POST['room_book_id']); $i++){
                        if($_POST['room_book_id'][$i] == ''){
                            if (isset($_POST['discount'][$i])) {
                                if ($_POST['discount'][$i] != '') {
                                    $discountbooking = $_POST['discount'][$i];
                                } else {
                                    $discountbooking = 0;
                                }
                                $insert_booking = "INSERT INTO pm_booking (add_date,room_id,nightcharge,adults,children,nights,discount,tax_rate,booking_net_amount,booking_parent_id,unit,discount_amount,vat_amount)
        VALUES ('$current_date'," . $_POST['room_id'][$i] . "," . $_POST['nightcharge'][$i] . "," . $_POST['adults'][$i] . "," . $_POST['children'][$i] . "," . $_POST['night'][$i] . "," . $discountbooking . "," . $_POST['taxbooking_id'][$i] . "," . $_POST['netamount'][$i] . "," . $_POST['booking_id'][0] . ",1," . $_POST['discount_amount'][$i] . "," . $_POST['vat_amount'][$i] . ")";
                                $db->exec($insert_booking);
                                //print_r($insert_booking);
                                $updatestatus = "UPDATE pm_booking set booking_status = 'checkin' WHERE booking_parent_id = ".$_POST['booking_id'][0]." " ;
                                $db->exec($updatestatus);

                            }
                        }else{
                            if (isset($_POST['discount'][$i])) {
                                if ($_POST['discount'][$i] != '') {
                                    $discountbooking = $_POST['discount'][$i];
                                } else {
                                    $discountbooking = 0;
                                }
                                $update_booking = "UPDATE pm_booking set edit_date = '$current_date',room_id = " . $_POST['room_id'][$i] . ",nightcharge = " . $_POST['nightcharge'][$i] . ",adults = " . $_POST['adults'][$i] . ",children = " . $_POST['children'][$i] . ",nights=" . $_POST['night'][$i] . ",discount=" . $discountbooking . ",tax_rate=" . $_POST['taxbooking_id'][$i] . ",booking_net_amount=" . $_POST['netamount'][$i] . ",booking_parent_id= " . $_POST['booking_id'][0] . ",unit= ".$unit.",discount_amount=" . $_POST['discount_amount'][$i] . ",vat_amount = " . $_POST['vat_amount'][$i] . " where id= " . $_POST['room_book_id'][$i] . " ";
                                $db->exec($update_booking);
                                $updatestatus = "UPDATE pm_booking set booking_status = 'checkin' WHERE booking_parent_id = ".$_POST['booking_id'][0]." " ;
                                $db->exec($updatestatus);

                            }
                        }
                }

        echo "success";
    } else {
        echo "error";
    }

}
//query for package form
if (isset($_POST['getpackageform'])) {
    if ($_POST['getpackageform'] == 1) {
                 $discountpackages = '';
                    $unit= 1;
                    for($i=0; $i<sizeof($_POST['room_book_id']); $i++){
                        if($_POST['room_book_id'][$i] == ''){
                            if (isset($_POST['discount'][$i])) {
                                if ($_POST['discount'][$i] != '') {
                                    $discountpackages = $_POST['discount'][$i];
                                } else {
                                    $discountpackages = 0;
                                }
                                $insert_booking = "INSERT INTO pm_packages (add_date,room_id,nightcharge,adults,children,nights,discount,tax_rate,booking_net_amount,packages_parent_id,unit,discount_amount,vat_amount)
        VALUES ('$current_date'," . $_POST['room_id'][$i] . "," . $_POST['nightcharge'][$i] . "," . $_POST['adults'][$i] . "," . $_POST['children'][$i] . "," . $_POST['night'][$i] . "," . $discountpackages . "," . $_POST['taxbooking_id'][$i] . "," . $_POST['netamount'][$i] . "," . $_POST['booking_id'][0] . ",1," . $_POST['discount_amount'][$i] . "," . $_POST['vat_amount'][$i] . ")";
                                $db->exec($insert_booking);
                               print_r($insert_booking);
                                $updatestatus = "UPDATE pm_packages set booking_status = 'checkin' WHERE packages_parent_id = ".$_POST['booking_id'][0]." " ;
                               $db->exec($updatestatus);

                            }
                        }else{
                            if (isset($_POST['discount'][$i])) {
                                if ($_POST['discount'][$i] != '') {
                                    $discountpackages = $_POST['discount'][$i];
                                } else {
                                    $discountpackages = 0;
                                }
                                $update_booking = "UPDATE pm_packages set edit_date = '$current_date',room_id = " . $_POST['room_id'][$i] . ",nightcharge = " . $_POST['nightcharge'][$i] . ",adults = " . $_POST['adults'][$i] . ",children = " . $_POST['children'][$i] . ",nights=" . $_POST['night'][$i] . ",discount=" . $discountbooking . ",tax_rate=" . $_POST['taxbooking_id'][$i] . ",booking_net_amount=" . $_POST['netamount'][$i] . ",packages_parent_id= " . $_POST['booking_id'][0] . ",unit= ".$unit.",discount_amount=" . $_POST['discount_amount'][$i] . ",vat_amount = " . $_POST['vat_amount'][$i] . " where id= " . $_POST['room_book_id'][$i] . " ";
                               $db->exec($update_booking);
                                $updatestatus = "UPDATE pm_packages set booking_status = 'checkin' WHERE packages_parent_id = ".$_POST['booking_id'][0]." " ;
                                $db->exec($updatestatus);

                            }
                        }
                }

        echo "success";
    } else {
        echo "error";
    }

}



if (isset($_POST['getcustomerform'])) {
    if ($_POST['getcustomerform'] == 1) {
        $params = array();
        parse_str($_POST['customerdata'],$params);
        $lastname = '';
        $country = '';
        $city = '';
        $company_name = '';
        $phoneno = '';
        $postcode = '';
        $comments = '';
        $cnic = '';
        $passport = '';
        $address = '';
        if( $params['lastname'] == ''){
            $lastname = 'null';
        }if( $params['lastname'] != ''){
            $lastname = $params['lastname'];
        }if($params['country'] == ''){
            $country = 'null';
        }if($params['country'] != ''){
            $country = $params['country'];
        }if($params['city'] == ''){
            $city = 'null';
        }else{
            $city = $params['city'];
        }if($params['companyname'] == ''){
            $company_name = 'null';
        }else{
            $company_name = $params['companyname'];
        }if($params['phone'] == ''){
            $phoneno = 'null';
        }else{
            $phoneno = $params['phone'];
        }if($params['postcode'] == ''){
            $postcode = 'null';
        }else{
            $postcode = $params['postcode'];
        }if($params['comments'] == ''){
            $comments = 'null';
        }else{
            $comments = $params['comments'];
        }if($params['cnic'] == ''){
            $cnic = 'null';
        }else{
            $cnic = $params['cnic'];
        }if($params['passport'] == ''){
            $passport = 'null';
        }else{
            $passport = $params['passport'];
        }if($params['address'] == ''){
            $address = 'null';
        }else{
            $address = $params['address'];
        }
        if(isset($_POST['customer_book_id'])){
            $update_customer = "Update pm_booking_customer set edit_date='$current_date',customer_name='".$params['customer']."',first_name='".$params['firstname']."',last_name='".$lastname."',country='".$country."',city='".$city."',company_name='".$company_name."',email='".$params['email']."',phone_no='".$phoneno."',post_code='".$postcode."',mobile_no='".$params['mobile']."',comments='".$comments."',cnic='".$cnic."',passport='".$passport."',booking_id=".$_POST['booking_id'][0].",address='".$address."' where id= ".$_POST['customer_book_id']." ";
            $db->exec($update_customer);

        }else{
            $insert_customer = "INSERT INTO pm_booking_customer (add_date,customer_name,first_name,last_name,country,city,company_name,email,phone_no,post_code,mobile_no,comments,cnic,passport,booking_id,address)
        VALUES ('$current_date','" . $params['customer'] . "','" . $params['firstname'] . "','" .$lastname. "','" . $country . "','" . $city . "','" . $company_name . "','" . $params['email'] . "','" . $phoneno. "','" . $postcode. "','" . $params['mobile']. "','" . $comments. "','" . $cnic. "','" . $passport. "'," . $_POST['booking_id'][0] . ",'".$address."')";
            $db->exec($insert_customer);
            print_r($insert_customer);
        }
                echo "success";
    }else{
        echo "error";
    }
}
if (isset($_POST['getserviceform'])) {
    if ($_POST['getserviceform'] == 1) {
        for ($i = 0; $i < sizeof($_POST['service_book_id']); $i++) {
            $discountservice = '';
            if($_POST['service_book_id'][$i] == ''){
                if (isset($_POST['servicediscount'][$i])) {
                    if ($_POST['servicediscount'][$i] != '') {
                        $discountservice = $_POST['servicediscount'][$i];
                    } else {
                        $discountservice = 0;
                    }
                    $insert_service = "INSERT INTO pm_booking_services (add_date,service_id,unit_id,quantity,charges,discount,tax_rate,net_amount,booking_parent_id,discount_amount,vat_amount) 
        VALUES ('$current_date'," . $_POST['serviceid'][$i] . "," . $_POST['serviceunitid'][$i] . "," . $_POST['servicequantity'][$i] . "," . $_POST['servicecharges'][$i] . "," . $discountservice . "," . $_POST['taxservice'][$i] . "," . $_POST['servicenetamount'][$i] . "," . $_POST['booking_id'][0] . "," . $_POST['servicediscount_amount'][$i] . "," . $_POST['servicevat_amount'][$i] . ")";
                    $db->exec($insert_service);
                    print_R($insert_service);
                }
            }else{
                if (isset($_POST['servicediscount'][$i])) {
                    if ($_POST['servicediscount'][$i] != '') {
                        $discountservice = $_POST['servicediscount'][$i];
                    } else {
                        $discountservice = 0;
                    }
                    $update_service = "update pm_booking_services set edit_date='$current_date',service_id=".$_POST['serviceid'][$i].",unit_id=".$_POST['serviceunitid'][$i].",quantity=".$_POST['servicequantity'][$i].",charges=".$_POST['servicecharges'][$i].",discount=".$discountservice.",tax_rate=".$_POST['taxservice'][$i].",net_amount=".$_POST['servicenetamount'][$i].",booking_parent_id=".$_POST['booking_id'][0].",discount_amount=".$_POST['servicediscount_amount'][$i].",vat_amount=".$_POST['servicevat_amount'][$i]." where id = ".$_POST['service_book_id'][$i]." ";
                    print_R($update_service);
                    $db->exec($update_service);
                }

            }
        }
        echo "success";
    }        else {
            echo "error";
    }
}
//query for package form
if (isset($_POST['getpackagesserviceform'])) {
    //print_r($_POST);exit;
    if ($_POST['getpackagesserviceform'] == 1) {
        for ($i = 0; $i < sizeof($_POST['service_package_id']); $i++) {
            $discountservice = '';
            if($_POST['service_package_id'][$i] == ''){
                if (isset($_POST['servicediscount'][$i])) {
                    if ($_POST['servicediscount'][$i] != '') {
                        $discountservice = $_POST['servicediscount'][$i];
                    } else {
                        $discountservice = 0;
                    }
                    $insert_service = "INSERT INTO pm_packages_services (add_date,service_id,unit_id,quantity,charges,discount,tax_rate,net_amount,packages_parent_id,discount_amount,vat_amount) 
        VALUES ('$current_date'," . $_POST['serviceid'][$i] . "," . $_POST['serviceunitid'][$i] . "," . $_POST['servicequantity'][$i] . "," . $_POST['servicecharges'][$i] . "," . $discountservice . "," . $_POST['taxservice'][$i] . "," . $_POST['servicenetamount'][$i] . "," . $_POST['booking_id'][0] . "," . $_POST['servicediscount_amount'][$i] . "," . $_POST['servicevat_amount'][$i] . ")";
                   $db->exec($insert_service);
                    //print_R($insert_service);
                }
            }else{
                if (isset($_POST['servicediscount'][$i])) {
                    if ($_POST['servicediscount'][$i] != '') {
                        $discountservice = $_POST['servicediscount'][$i];
                    } else {
                        $discountservice = 0;
                    }
                    $update_service = "update pm_packages_services set edit_date='$current_date',service_id=".$_POST['serviceid'][$i].",unit_id=".$_POST['serviceunitid'][$i].",quantity=".$_POST['servicequantity'][$i].",charges=".$_POST['servicecharges'][$i].",discount=".$discountservice.",tax_rate=".$_POST['taxservice'][$i].",net_amount=".$_POST['servicenetamount'][$i].",packages_parent_id=".$_POST['booking_id'][0].",discount_amount=".$_POST['servicediscount_amount'][$i].",vat_amount=".$_POST['servicevat_amount'][$i]." where id = ".$_POST['service_package_id'][$i]." ";
                    //print_R($update_service);
                    $db->exec($update_service);
                }

            }
        }
        echo "success";
    }        else {
            echo "error";
    }
}
if (isset($_POST['getactivityform'])) {
    if ($_POST['getactivityform'] == 1) {
        for ($i = 0; $i < sizeof($_POST['activity_book_id']); $i++) {
            $discountact = '';
            if($_POST['activity_book_id'][$i] == ''){
                if (isset($_POST['activitiesdiscount'][$i])) {
                    if ($_POST['activitiesdiscount'][$i] != '') {
                        $discountact = $_POST['activitiesdiscount'][$i];
                    }else{
                        $discountact = 0;
                    }
                    $insert_activity = "INSERT INTO pm_booking_activities (add_date,activity_id,adult,child,unit_id,quantity,charges,discount,tax_rate,net_amount,booking_parent_id,discount_amount,vat_amount) 
        VALUES ('$current_date'," . $_POST['activitiesid'][$i] . "," . $_POST['activitiesadult'][$i] . "," . $_POST['activitieschild'][$i] . "," . $_POST['activitiesunitid'][$i] . ", " . $_POST['activitiesquantity'][$i] . "," . $_POST['activitiescharges'][$i] . "," . $discountact . "," . $_POST['taxactivitiesid'][$i] . "," . $_POST['activitiesnetamount'][$i] . "," . $_POST['booking_id'][0] . "," . $_POST['activitiesdiscount_amount'][$i] . "," . $_POST['activitiesvat_amount'][$i] . ")";
                    $db->exec($insert_activity);
                    //print_r($insert_activity);
                }
            }else{
                if (isset($_POST['activitiesdiscount'][$i])) {
                    if ($_POST['activitiesdiscount'][$i] != '') {
                        $discountact = $_POST['activitiesdiscount'][$i];
                    }else{
                        $discountact = 0;
                    }
                    $update_activity = "UPDATE pm_booking_activities set edit_date='$current_date',activity_id=".$_POST['activitiesid'][$i].",adult=".$_POST['activitiesadult'][$i].",child=".$_POST['activitieschild'][$i].",unit_id=".$_POST['activitiesunitid'][$i].",quantity=".$_POST['activitiesquantity'][$i].",charges=".$_POST['activitiescharges'][$i].",discount=".$discountact.",tax_rate=".$_POST['taxactivitiesid'][$i].",net_amount=".$_POST['activitiesnetamount'][$i].",booking_parent_id=".$_POST['booking_id'][0].",discount_amount=".$_POST['activitiesdiscount_amount'][$i].",vat_amount=".$_POST['activitiesvat_amount'][$i]." where id= ".$_POST['activity_book_id'][$i]." ";
                    $db->exec($update_activity);
                }
            }
        }
        echo "success";
    }else {
            echo "error";
        }
}
//query for package form
if (isset($_POST['getpackagesactivityform'])) {
  //print_r($_POST);exit;
    if ($_POST['getpackagesactivityform'] == 1) {
        for ($i = 0; $i < sizeof($_POST['activity_package_id']); $i++) {
            $discountact = '';
            if($_POST['activity_package_id'][$i] == ''){
                if (isset($_POST['activitiesdiscount'][$i])) {
                    if ($_POST['activitiesdiscount'][$i] != '') {
                        $discountact = $_POST['activitiesdiscount'][$i];
                    }else{
                        $discountact = 0;
                    }
                    $insert_activity = "INSERT INTO pm_packages_activities (add_date,activity_id,adult,child,unit_id,quantity,charges,discount,tax_rate,net_amount,packages_parent_id,discount_amount,vat_amount) 
        VALUES ('$current_date'," . $_POST['activitiesid'][$i] . "," . $_POST['activitiesadult'][$i] . "," . $_POST['activitieschild'][$i] . "," . $_POST['activitiesunitid'][$i] . ", " . $_POST['activitiesquantity'][$i] . "," . $_POST['activitiescharges'][$i] . "," . $discountact . "," . $_POST['taxactivitiesid'][$i] . "," . $_POST['activitiesnetamount'][$i] . "," . $_POST['booking_id'][0] . "," . $_POST['activitiesdiscount_amount'][$i] . "," . $_POST['activitiesvat_amount'][$i] . ")";
                   $db->exec($insert_activity);
                   // print_r($insert_activity);
                }
            }else{
                if (isset($_POST['activitiesdiscount'][$i])) {
                    if ($_POST['activitiesdiscount'][$i] != '') {
                        $discountact = $_POST['activitiesdiscount'][$i];
                    }else{
                        $discountact = 0;
                    }
                    $update_activity = "UPDATE pm_packages_activities set edit_date='$current_date',activity_id=".$_POST['activitiesid'][$i].",adult=".$_POST['activitiesadult'][$i].",child=".$_POST['activitieschild'][$i].",unit_id=".$_POST['activitiesunitid'][$i].",quantity=".$_POST['activitiesquantity'][$i].",charges=".$_POST['activitiescharges'][$i].",discount=".$discountact.",tax_rate=".$_POST['taxactivitiesid'][$i].",net_amount=".$_POST['activitiesnetamount'][$i].",booking_packages_id=".$_POST['booking_id'][0].",discount_amount=".$_POST['activitiesdiscount_amount'][$i].",vat_amount=".$_POST['activitiesvat_amount'][$i]." where id= ".$_POST['activity_package_id'][$i]." ";
                $db->exec($update_activity);
                   //print_r($update_activity);
                }
            }
        }
        echo "success";
    }else {
            echo "error";
        }
}
//query for package form
if(isset($_POST['savepackageform'])) {
//print_r($_POST);exit;
    $editid=$_POST['id'];
    $id=$_POST['package_id']['0'];
    $package_name=$_POST['package_name'];
    $package_descr=$_POST['package_descr'];
    $package_fromdate=$_POST['package_fromdate'];
    $package_todate=$_POST['package_todate'];
    $package_meals=$_POST['package_meals']['0'];
    $package_applyondays=$_POST['package_applyondays']['0'];
    $package_maxpeople=$_POST['package_maxpeople'];
    $package_taxrates=$_POST['package_taxrates'];
    $package_taxinclusive=$_POST['package_taxinclusive'];
    $package_maxheight=$_POST['package_maxheight'];
    $package_totalpackcharge=$_POST['package_totalpackcharge'];
    $package_packchargenight=$_POST['package_packchargenight'];
    $package_ecnightadult=$_POST['package_ecnightadult'];
    $package_ecnightchild=$_POST['package_ecnightchild'];
    $package_extrafood=$_POST['package_extrafood'];
    $package_status=$_POST['package_status'];
    $package_allowpkgedit=$_POST['package_allowpkgedit'];

    if($editid==0)
    {
      $query="INSERT INTO pm_package(packageid,packname,descr,fromdate,todate,applyondays,meals,totalpackcharges,maxpeople,maxheight,packcharge,taxrates,taxinclusive,ecnightadult,ecnightchild,extrafood,status,allow_edit,lang) VALUES ($id,'$package_name','$package_descr','$package_fromdate','$package_todate','$package_applyondays','$package_meals',$package_totalpackcharge,$package_maxpeople,$package_maxheight,$package_packchargenight,$package_taxrates,$package_taxinclusive,$package_ecnightadult,$package_ecnightchild,$package_extrafood,$package_status,$package_allowpkgedit,2)";

     $query = $db->exec($query);

    }else{
        $updatestatus = "UPDATE pm_package set packageid=$id, packname = '$package_name',descr='$package_descr',fromdate='$package_fromdate',todate='$package_todate',applyondays='$package_applyondays',meals='$package_meals',totalpackcharges=$package_totalpackcharge,maxpeople=$package_maxpeople,maxheight=$package_maxheight,packcharge=$package_packchargenight,taxrates=$package_taxrates,taxinclusive=$package_taxinclusive,ecnightadult=$package_ecnightadult,ecnightchild=$package_ecnightchild,extrafood=$package_extrafood,status=$package_status,allow_edit=$package_allowpkgedit WHERE id = ".$editid." " ;
        $db->exec($updatestatus);
    }
}

if (isset($_POST['getsavepaymentform'])) {
    if ($_POST['getsavepaymentform'] == 1) {
        $getpayment_id = $db->query("select id from pm_booking_payment where booking_id = ".$_POST['booking_id'][0]." order by id desc limit 1")->fetchAll();
        if(!empty($getpayment_id)){
            $params = array();
            parse_str($_POST['savepayment'],$params);
            $activitypayment='';
            $servicepayment='';
            $downpayments = '';
            if($params['total_down_payment']  == ''){
                $downpayments = 0;
            }else{
                $downpayments = $params['total_down_payment'];
            }
            if($params['total_activity_charges']  == ''){
                $activitypayment = 0;
            }else{
                $activitypayment = $params['total_activity_charges'];
            }
            if($params['total_service_charges']  == ''){
                $servicepayment = 0;
            }else{
                $servicepayment = $params['total_service_charges'];
            }
            $getdiff = $params['totalnetpayable']- $downpayments;
            $status = "";
            if($getdiff > 0){
                $status = 'Paid/Partial';
            }
            if($getdiff == 0){
                $status = 'Completed';
            }
            $update_payment = "UPDATE pm_booking_payment set edit_date='$current_date',total_room_charge=".$params['total_room_charges'].",total_activity_charge=".$activitypayment.",total_service_charge=".$servicepayment.",net_payable=".$params['totalnetpayable'].",payment_mode='".$params['paymentmode']."' where booking_id = " . $_POST['booking_id'][0]." ";
            $db->exec($update_payment);
            $updatestatus = "UPDATE pm_booking set booking_status = $status WHERE booking_parent_id = ".$_POST['booking_id'][0]." " ;
            $db->exec($updatestatus);

        }else{
            $params = array();
            parse_str($_POST['savepayment'],$params);
            $activitypayment='';
            $servicepayment='';
            $downpayments = '';
            if($params['total_down_payment']  == ''){
                $downpayments = 0;
            }else{
                $downpayments = $params['total_down_payment'];
            }
            if($params['total_activity_charges']  == ''){
                $activitypayment = 0;
            }else{
                $activitypayment = $params['total_activity_charges'];
            }
            if($params['total_service_charges']  == ''){
                $servicepayment = 0;
            }else{
                $servicepayment = $params['total_service_charges'];
            }
            $getdiff = $params['totalnetpayable']- $downpayments;
            $status = "";
            if($getdiff > 0){
                $status = 'Paid/Partial';
            }
            if($getdiff == 0){
                $status = 'Completed';
            }
            $insert_payment = "INSERT INTO pm_booking_payment (add_date,total_room_charge,total_activity_charge,total_service_charge,down_payment,net_payable,payment_mode,booking_id)
        VALUES ('$current_date'," . $params['total_room_charges'] . "," . $activitypayment . "," . $servicepayment . "," . $downpayments . "," . $params['totalnetpayable'] . ",'" . $params['paymentmode'] . "'," . $_POST['booking_id'][0] . ")";
            $db->exec($insert_payment);
            $updatestatus = "UPDATE pm_booking set booking_status = $status WHERE booking_parent_id = ".$_POST['booking_id'][0]." " ;
            $db->exec($updatestatus);

        }
        echo "success";
    }else{
        echo "error";
    }
}
if (isset($_POST['getsavepaymentformprint'])) {
    if ($_POST['getsavepaymentformprint'] == 1) {

        $params = array();
        parse_str($_POST['savepayment'],$params);
        $activitypayment='';
        $servicepayment='';
        $downpayments = '';
        if($params['total_down_payment']  == ''){
            $downpayments = 0;
        }else{
            $downpayments = $params['total_down_payment'];
        }
        if($params['total_activity_charges']  == ''){
            $activitypayment = 0;
        }else{
            $activitypayment = $params['total_activity_charges'];
        }
        if($params['total_service_charges']  == ''){
            $servicepayment = 0;
        }else{
            $servicepayment = $params['total_service_charges'];
        }
        $getdiff = $params['totalnetpayable']- $downpayments;
        $status = "";
        if($getdiff > 0){
            $status = 'Paid/Partial';
        }
        if($getdiff == 0){
            $status = 'Completed';
        }
        $insert_customer = "INSERT INTO pm_booking_payment (add_date,total_room_charge,total_activity_charge,total_service_charge,down_payment,net_payable,payment_mode,booking_id)
        VALUES ('$current_date'," . $params['total_room_charges'] . "," . $activitypayment . "," . $servicepayment . "," . $downpayments . "," . $params['totalnetpayable'] . ",'" . $params['paymentmode'] . "'," . $_POST['booking_id'][0] . ")";
        $db->exec($insert_customer);
        $updatestatus = "UPDATE pm_booking set booking_status = $status WHERE booking_parent_id = ".$_POST['booking_id'][0]." " ;
        $db->exec($updatestatus);
        echo "success";
    }else{
      echo  "error";
    }

}
if(isset($_POST['cancel_book'])) {
    $id = $_POST['id'];
    $update_status = "update pm_booking set booking_status = 'cancel' WHERE booking_parent_id = $id";
    $db->exec($update_status);
    echo "1";
}
if(isset($_POST['getroomdata'])) {
    $id = $_POST['roomid'];
    $query = $db->query("SELECT pm_room.id as room_id,pm_room.roomcode,pm_room.roomtitle,pm_room.maxadults,pm_room.maxchild,pm_room.price,pm_roomtypes.roomtype,pm_room.taxrates
                        FROM pm_room
                        INNER JOIN pm_roomtypes
                        ON pm_room.id_roomtype = pm_roomtypes.id
                        WHERE pm_room.id = $id and pm_room.lang = 2 and pm_roomtypes.lang = 2")->fetchAll();
    echo json_encode($query);

}
if(isset($_POST['receivecash'])) {
    $id = $_POST['id'];
    $query = $db->query("SELECT 
                        pm_booking_payment.down_payment,
                        pm_booking_payment.id as pm_booking_payment_id,
                        pm_booking_payment.net_payable,
                        pm_booking_payment.payment_mode,
                        pm_booking_customer.id,
                        pm_booking_customer.cnic,
                        (select COALESCE(sum(pm_booking_sub_payment.payment_amount),0) from pm_booking_sub_payment where pm_booking_payment_id = $id) as payment_received,
                        (select COALESCE(sum(pm_booking.discount_amount),0)  from pm_booking where booking_parent_id = $id) as room_discount,
                        (select COALESCE(sum(pm_booking_services.discount_amount),0)  from pm_booking_services where booking_parent_id = $id) as service_discount,
                        (select COALESCE(sum(pm_booking_activities.discount_amount),0) from pm_booking_activities where booking_parent_id = $id) as activity_discount
                        FROM pm_booking_payment
                        LEFT JOIN 
                        pm_booking_customer ON
                        pm_booking_payment.booking_id = pm_booking_customer.booking_id
                        LEFT JOIN 
                        pm_booking ON
                        pm_booking_payment.booking_id = pm_booking.booking_parent_id
                        LEFT JOIN 
                        pm_booking_services ON
                        pm_booking_payment.booking_id = pm_booking_services.booking_parent_id
                        LEFT JOIN pm_booking_activities ON
                        pm_booking_payment.booking_id = pm_booking_activities.booking_parent_id
                        LEFT JOIN pm_booking_sub_payment 
                        ON pm_booking_payment.booking_id = pm_booking_sub_payment.pm_booking_payment_id 
                        where pm_booking_payment.booking_id = $id group by pm_booking_payment.booking_id ")->fetchAll();
    echo json_encode($query);

}
if(isset($_POST['savesubpayment']) && $_POST['savesubpayment']== 1){
    $adjusted_discount = '';
    $comment = '';
    $check_ref = '';
    $coupon = '';
    $cnic = '';
    if($_POST['adjusted_discount'] == ''){
        $adjusted_discount = 0;
    }else{
        $adjusted_discount = $_POST['adjusted_discount'];
    }
    if($_POST['check_ref'] == ''){
        $check_ref = '';
    }else{
        $check_ref = $_POST['check_ref'];
    }
    if($_POST['comments'] == ''){
        $comment = '';
    }else{
        $comment = $_POST['comments'];
    }
    if($_POST['coupon'] == ''){
        $coupon = 0;
    }else{
        $coupon = $_POST['coupon'];
    }
    if($_POST['customer_cnic'] == '' || $_POST['customer_cnic'] == null){
        $cnic = 0;
    }else{
        $cnic = $_POST['customer_cnic'];
    }
    $insert_sub_payment = "INSERT INTO pm_booking_sub_payment (add_date,booking_id,customer_account,customer_cnic,payment_method,payment_amount,check_ref,comments,total_balance,down_payment,discount_credit,coupon,payment_received,balance_amount,pm_booking_payment_id,adjusted_discount) 
    VALUES ('".$_POST['add_date']."',".$_POST['booking_id'].",".$_POST['customer_account'].",'$cnic','".$_POST['payment_method']."',".$_POST['payment_amount'].",'$check_ref','$comment',".$_POST['total_balance'].",".$_POST['down_payment'].",".$_POST['discount_credit'].",$coupon,".$_POST['payment_received'].",".$_POST['balance_amount'].",".$_POST['pm_booking_parent_id'].",$adjusted_discount)";
    $db->exec($insert_sub_payment);
    if($_POST['checkbox'] == 1){
        $updatestatus = "UPDATE pm_booking set booking_status = 'checkout' WHERE booking_parent_id = ".$_POST['booking_id']." " ;
        $db->exec($updatestatus);
    }
    echo "1";


}


//query for Sell package new form
if (isset($_POST['getroomserviceactivtysellpackage'])) {

    if ($_POST['getroomserviceactivtysellpackage'] == 1) {
        $id=$_POST['packageid'];
    //$query="SELECT * FROM `pm_packages` WHERE packages_parent_id=$id";
   //$roomsinPackages= $db->query("SELECT *,taxname as TaxName ,roomtitle as RoomTitle FROM `pm_packages` LEFT JOIN pm_tax on pm_packages.tax_rate=pm_tax.id LEFT JOIN pm_room on pm_packages.room_id=pm_room.id WHERE packages_parent_id=$id and pm_tax.lang=2 and pm_room.lang=2;")->fetchAll();
   $roomsinPackages= $db->query("SELECT  pm_packages.*,
                                                      pm_packages.id as room_book_id,
                                                        pm_room.roomcode,
                                                        pm_room.roomtitle,
                                                        pm_packages.nightcharge,
                                                        pm_packages.adults,
                                                        pm_packages.nights,
                                                        pm_packages.discount,
                                                        pm_packages.children,
                                                        (
                                                          SELECT
                                                            pm_room.id_roomtype
                                                          FROM
                                                            pm_room
                                                          where
                                                            pm_room.id = pm_packages.room_id
                                                            and pm_room.lang = 2
                                                        ) as room_type_id,
                                                        (
                                                          SELECT
                                                            pm_roomtypes.roomtype
                                                          from
                                                            pm_roomtypes
                                                          where
                                                            pm_roomtypes.id = pm_room.id_roomtype
                                                            and pm_roomtypes.lang = 2
                                                        ) as room_type_name
                                                      FROM
                                                        pm_packages
                                                        INNER JOIN pm_room ON pm_packages.room_id = pm_room.id
                                                      WHERE
                                                        pm_packages.packages_parent_id = $id
                                                        and pm_room.lang = 2")->fetchAll();
    echo json_encode($roomsinPackages);
    }
    if ($_POST['getroomserviceactivtysellpackage'] == 2) {
        $id=$_POST['packageid'];
    //$query="SELECT * FROM `pm_packages` WHERE packages_parent_id=$id";
   //$roomsinPackages= $db->query("SELECT *,taxname as TaxName ,roomtitle as RoomTitle FROM `pm_packages` LEFT JOIN pm_tax on pm_packages.tax_rate=pm_tax.id LEFT JOIN pm_room on pm_packages.room_id=pm_room.id WHERE packages_parent_id=$id and pm_tax.lang=2 and pm_room.lang=2;")->fetchAll();
   $serviceinPackages= $db->query("select * from pm_packages_services where packages_parent_id = $id")->fetchAll();

        echo json_encode($serviceinPackages);
    }
    if ($_POST['getroomserviceactivtysellpackage'] == 3) {
        $id=$_POST['packageid'];
   $activityinPackages=$db->query("select * from pm_packages_activities where packages_parent_id = $id")->fetchAll();
        echo json_encode($activityinPackages);
    }

}

//query for sell package save


if (isset($_POST['sellpackagesave'])) {
    if ($_POST['sellpackagesave'] == 1) {

        $packagetitle_id=$_POST['packagetitle_id'];
        $bookingdate=$_POST['bookingdate'];
        $validfrom=$_POST['validfrom'];
        $validto=$_POST['validto'];
        $checkindate=$_POST['checkindate'];
        $checkoutdate=$_POST['checkoutdate'];


//        if($editid==0)
//        {
            $query="INSERT INTO pm_sellpackage(packagetitleid,bookingdate,checkIndate,checkOutdate,validefrom,validto,lang) VALUES ($packagetitle_id,'$bookingdate','$checkindate','$checkoutdate','$validfrom','$validto',2)";
            //print_r($query);
            //$packageformdata = $db->query("$query")->fetchAll();
            //echo json_encode($packageformdata);

//        }else{
           // $updatestatus = "UPDATE pm_package set packageid=$id, packname = '$package_name',descr='$package_descr',fromdate='$package_fromdate',todate='$package_todate',applyondays='$package_applyondays',meals='$package_meals',totalpackcharges=$package_totalpackcharge,maxpeople=$package_maxpeople,maxheight=$package_maxheight,packcharge=$package_packchargenight,taxrates=$package_taxrates,taxinclusive=$package_taxinclusive,ecnightadult=$package_ecnightadult,ecnightchild=$package_ecnightchild,extrafood=$package_extrafood,status=$package_status,allow_edit=$package_allowpkgedit WHERE id = ".$editid." " ;
            // print_r($updatestatus);
           // $db->exec($updatestatus);
//        }

        echo "success";
    }
    if ($_POST['sellpackagesave'] == 2) {

        $discountpackages = '';
        $unit= 1;
        for($i=0; $i<sizeof($_POST['room_package_id']); $i++){
  if($_POST['room_package_id'][$i] == ''){
                if (isset($_POST['discount'][$i])) {
                    if ($_POST['discount'][$i] != '') {
                        $discountpackages = $_POST['discount'][$i];
                    } else {
                        $discountpackages = 0;
                    }
                    $insert_booking = "INSERT INTO pm_sellpackage (add_date,room_id,nightcharge,adults,children,nights,discount,tax_rate,booking_net_amount,sellpackage_parent_id,unit,discount_amount,vat_amount)
        VALUES ('$current_date'," . $_POST['room_id'][$i] . "," . $_POST['nightcharge'][$i] . "," . $_POST['adults'][$i] . "," . $_POST['children'][$i] . "," . $_POST['night'][$i] . "," . $discountpackages . "," . $_POST['taxbooking_id'][$i] . "," . $_POST['netamount'][$i] . "," . $_POST['package_id'][0] . ",1," . $_POST['discount_amount'][$i] . "," . $_POST['vat_amount'][$i] . ")";
                    $db->exec($insert_booking);
                    print_r($insert_booking);
                    $updatestatus = "UPDATE pm_sellpackage set booking_status = 'checkin' WHERE sellpackage_parent_id = ".$_POST['package_id'][0]." " ;
                    $db->exec($updatestatus);

                }
            }else{
                if (isset($_POST['discount'][$i])) {
                    if ($_POST['discount'][$i] != '') {
                        $discountpackages = $_POST['discount'][$i];
                    } else {
                        $discountpackages = 0;
                    }
                    $update_booking = "UPDATE pm_packages set edit_date = '$current_date',room_id = " . $_POST['room_id'][$i] . ",nightcharge = " . $_POST['nightcharge'][$i] . ",adults = " . $_POST['adults'][$i] . ",children = " . $_POST['children'][$i] . ",nights=" . $_POST['night'][$i] . ",discount=" . $discountbooking . ",tax_rate=" . $_POST['taxbooking_id'][$i] . ",booking_net_amount=" . $_POST['netamount'][$i] . ",	sellpackage_parent_id= " . $_POST['package_id'][0] . ",unit= ".$unit.",discount_amount=" . $_POST['discount_amount'][$i] . ",vat_amount = " . $_POST['vat_amount'][$i] . " where id= " . $_POST['room_package_id'][$i] . " ";
                    $db->exec($update_booking);
                    $updatestatus = "UPDATE pm_packages set booking_status = 'checkin' WHERE sellpackage_parent_id = ".$_POST['package_id'][0]." " ;
                    $db->exec($updatestatus);

                }
            }
        }

        echo "success";
    }
    if ($_POST['sellpackagesave'] == 3) {

        for ($i = 0; $i < sizeof($_POST['service_package_id']); $i++) {
            $discountservice = '';
      if($_POST['service_package_id'][$i] == ''){
                if (isset($_POST['servicediscount'][$i])) {
                    if ($_POST['servicediscount'][$i] != '') {
                        $discountservice = $_POST['servicediscount'][$i];
                    } else {
                        $discountservice = 0;
                    }
                    $insert_service = "INSERT INTO pm_sellpackage_services (add_date,service_id,unit_id,quantity,charges,discount,tax_rate,net_amount,	sellpackage_parent_id,discount_amount,vat_amount) 
        VALUES ('$current_date'," . $_POST['serviceid'][$i] . "," . $_POST['serviceunitid'][$i] . "," . $_POST['servicequantity'][$i] . "," . $_POST['servicecharges'][$i] . "," . $discountservice . "," . $_POST['taxservice'][$i] . "," . $_POST['servicenetamount'][$i] . "," . $_POST['package_id'][0] . "," . $_POST['servicediscount_amount'][$i] . "," . $_POST['servicevat_amount'][$i] . ")";
                    print_R($insert_service);
                    $db->exec($insert_service);

                }
            }else{
                if (isset($_POST['servicediscount'][$i])) {
                    if ($_POST['servicediscount'][$i] != '') {
                        $discountservice = $_POST['servicediscount'][$i];
                    } else {
                        $discountservice = 0;
                    }
                    $update_service = "update pm_sellpackage_services set edit_date='$current_date',service_id=".$_POST['serviceid'][$i].",unit_id=".$_POST['serviceunitid'][$i].",quantity=".$_POST['servicequantity'][$i].",charges=".$_POST['servicecharges'][$i].",discount=".$discountservice.",tax_rate=".$_POST['taxservice'][$i].",net_amount=".$_POST['servicenetamount'][$i].",	sellpackage_parent_id=".$_POST['package_id'][0].",discount_amount=".$_POST['servicediscount_amount'][$i].",vat_amount=".$_POST['servicevat_amount'][$i]." where id = ".$_POST['service_package_id'][$i]." ";
                    //print_R($update_service);
                    $db->exec($update_service);
                }

            }
        }
        echo "success";
    }
    if ($_POST['sellpackagesave'] == 4) {
        for ($i = 0; $i < sizeof($_POST['activity_package_id']); $i++) {
            $discountact = '';
            if($_POST['activity_package_id'][$i] == ''){
                if (isset($_POST['activitiesdiscount'][$i])) {
                    if ($_POST['activitiesdiscount'][$i] != '') {
                        $discountact = $_POST['activitiesdiscount'][$i];
                    }else{
                        $discountact = 0;
                    }
                    $insert_activity = "INSERT INTO pm_sellpackages_activities (add_date,activity_id,adult,child,unit_id,quantity,charges,discount,tax_rate,net_amount,	sellpackage_parent_id,discount_amount,vat_amount) 
        VALUES ('$current_date'," . $_POST['activitiesid'][$i] . "," . $_POST['activitiesadult'][$i] . "," . $_POST['activitieschild'][$i] . "," . $_POST['activitiesunitid'][$i] . ", " . $_POST['activitiesquantity'][$i] . "," . $_POST['activitiescharges'][$i] . "," . $discountact . "," . $_POST['taxactivitiesid'][$i] . "," . $_POST['activitiesnetamount'][$i] . "," . $_POST['packageid'][0] . "," . $_POST['activitiesdiscount_amount'][$i] . "," . $_POST['activitiesvat_amount'][$i] . ")";
                    print_r($insert_activity);
                    $db->exec($insert_activity);

                }
            }else{
                if (isset($_POST['activitiesdiscount'][$i])) {
                    if ($_POST['activitiesdiscount'][$i] != '') {
                        $discountact = $_POST['activitiesdiscount'][$i];
                    }else{
                        $discountact = 0;
                    }
                    $update_activity = "UPDATE pm_sellpackages_activities set edit_date='$current_date',activity_id=".$_POST['activitiesid'][$i].",adult=".$_POST['activitiesadult'][$i].",child=".$_POST['activitieschild'][$i].",unit_id=".$_POST['activitiesunitid'][$i].",quantity=".$_POST['activitiesquantity'][$i].",charges=".$_POST['activitiescharges'][$i].",discount=".$discountact.",tax_rate=".$_POST['taxactivitiesid'][$i].",net_amount=".$_POST['activitiesnetamount'][$i].",sellpackage_parent_id=".$_POST['packageid'][0].",discount_amount=".$_POST['activitiesdiscount_amount'][$i].",vat_amount=".$_POST['activitiesvat_amount'][$i]." where id= ".$_POST['activity_package_id'][$i]." ";
                    $db->exec($update_activity);
                    print_r($update_activity);
                }
            }
        }

    }


}
