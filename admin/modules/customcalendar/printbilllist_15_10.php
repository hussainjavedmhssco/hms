<?php
/**
 * Created by PhpStorm.
 * User: Hussain
 * Date: 2/10/2018
 * Time: 10:05 AM
 */

define('ADMIN', true);
define('SYSBASE', str_replace('\\', '/', realpath(dirname(__FILE__) . '/../../../') . '/'));
require_once(SYSBASE . 'common/lib.php');
require_once(SYSBASE . 'common/define.php');
?>
<!DOCTYPE html>
<html>
<head>

    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.6.0/jQuery.print.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.6.0/jQuery.print.min.js"></script>
</head>

<style type="text/css">
    .invoice-title h2, .invoice-title h3 {
        display: inline-block;
    }
    .table > tbody > tr > .no-line {
        border-top: none;
    }
    .table > thead > tr > .no-line {
        border-bottom: none;
    }
    .table > tbody > tr > .thick-line {
        border-top: 2px solid;
    }


</style>
<body>
<div class="container-fluid print_bill">
    <input type="hidden" class="booking_id_for_print">
    <div class="row">
        <div class="col-md-12">
            <div class="invoice-title">
                <h2>MHSSCO</h2>
                <h3 class="pull-right">INVOICE</h3>
            </div>
            <?php
            error_reporting(E_ALL);
            $newbook_id= '';
            if(isset($_GET['id'])){
                $newbook_id = $_GET['id'];
            }
            $query = $db->query("select 
                                                                        pm_booking_customer.id,
                                                                        pm_booking_customer.customer_name,
                                                                        pm_booking_customer.company_name,
                                                                        pm_booking_customer.post_code,
                                                                        pm_booking_customer.address,
                                                                        pm_booking_customer.phone_no,
                                                                        pm_booking_customer.email,
                                                                        pm_booking_customer.city,
                                                                        pm_booking_parent.id as invoice_no,
                                                                        pm_booking_parent.booking_date,
                                                                        pm_booking_parent.checkin_date,
                                                                        pm_booking_parent.checkout_date,
                                                                        (select COALESCE(sum(pm_booking.nights),0) from pm_booking where booking_parent_id = $newbook_id) as total_stay,
                                                                        (select COALESCE(sum(pm_booking.nightcharge),0) from pm_booking where booking_parent_id = $newbook_id) as per_day,
                                                                        (select COALESCE(sum(pm_booking.adults),0) from pm_booking where booking_parent_id = $newbook_id) as adults,
                                                                        (select COALESCE(sum(pm_booking.children),0) from pm_booking where booking_parent_id = $newbook_id) as children,
                                                                        (select count(pm_booking.room_id) from pm_booking where booking_parent_id = 4)as no_of_rooms
                                                                        FROM pm_booking_parent
                                                                        LEFT JOIN pm_booking ON
                                                                        pm_booking_parent.id = pm_booking.booking_parent_id
                                                                        LEFT JOIN pm_booking_customer ON 
                                                                        pm_booking_parent.id = pm_booking_customer.booking_id
                                                                        where pm_booking_parent.id = $newbook_id GROUP BY pm_booking_parent.id");
            foreach($query as $data){ ?>
            <div class="row">
                <div class="col-md-6 col-xs-6">
                    <address>
                        ADDRESS:
                        <?php echo $data['address'];?><br>
                        Phone: <?php echo $data['phone_no'];?><br>
                        Email: <?php echo $data['email']?>
                    </address>
                </div>
                <div class="col-md-6 col-xs-6 text-right">
                    <address>
                        INVOICE NO: <?php echo $data['invoice_no'];?><br />
                        DATE:   <?php $date = date_create($data['booking_date']); echo date_format($date, "Y-m-d");?><br />
                    </address>
                </div>
            </div>
            <label>Billing Information</label>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="table">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="row">
                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <label>Check In Date</label>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <?php $datein = date_create($data['checkin_date']); echo date_format($datein, "Y-m-d");?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7 col-xs-7">
                            <label>Check Out Date</label>
                        </div>
                        <div class="col-md-5 col-xs-5">
                            <?php $dateout = date_create($data['checkout_date']);  echo date_format($dateout, "Y-m-d");?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-7 col-xs-7">
                            <label>Total No. Of Night</label>
                        </div>
                        <div class="col-md-5 col-xs-5">
                            <?php echo $data['total_stay'];?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7 col-xs-7">
                            <label>Rate Per Night/Room</label>
                        </div>
                        <div class="col-md-5 col-xs-5">
                            <?php echo $data['per_day'];?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7 col-xs-7">
                            <label>No. Of Adults</label>
                        </div>
                        <div class="col-md-5 col-xs-5">
                            <?php echo $data['adults'];?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7 col-xs-7">
                            <label>No. Of Children</label>
                        </div>
                        <div class="col-md-5 col-xs-5">
                            <?php echo $data['children'];?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-7 col-xs-7">
                            <label>No. Of Room</label>
                        </div>
                        <div class="col-sm-5 col-xs-5">
                            <?php echo $data['no_of_rooms'];?>
                        </div>
                    </div>
                    <!--                    <div class="row">-->
                    <!--                        <div class="col-sm-5">-->
                    <!--                            <label>Other</label>-->
                    <!--                        </div>-->
                    <!--                        <div class="col-sm-7">-->
                    <!--                            0-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                </div>

                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="row">
                        <div class="col-md-5 col-xs-5">
                            <label>CUSTOMER ID:</label>
                        </div>
                        <div class="col-md-7 col-xs-7">
                            <?php echo $data['id'];?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-5 col-xs-5">
                            <label>Bill To</label>
                        </div>
                        <div class="col-sm-7 col-xs-7">
                            <?php echo $data['customer_name'];?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-5 col-xs-5">
                            <label>Company Name</label>
                        </div>
                        <div class="col-sm-7 col-xs-7">
                            <?php echo $data['company_name'];?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-5">
                            <label>Address</label>
                        </div>
                        <div class="col-sm-7 col-xs-7">
                            <?php echo $data['address'];?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-5">
                            <label>City & Postcode</label>
                        </div>
                        <div class="col-sm-7 col-xs-7">
                            <?php echo $data['city']?>,<?php echo $data['post_code'];?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-5">
                            <label>Phone No:</label>
                        </div>
                        <div class="col-sm-7 col-xs-7">
                            <?php echo $data['phone_no'];?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-5">
                            <label>Email:</label>
                        </div>
                        <div class="col-sm-7 col-xs-7">
                            <?php echo $data['email']; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php    }
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <strong>Order Summary</strong>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive col-12 col-sm-12 col-lg-12">
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <td class="bg-primary"><strong>Room</strong></td>
                                <td class="bg-primary text-center col-xs-2"><strong>Unit</strong></td>
                                <td class="bg-primary text-center col-xs-2"><strong>No of Night</strong></td>
                                <td class="bg-primary text-center col-xs-2"><strong>Per Night</strong></td>
                                <td class="bg-primary text-center col-xs-2"><strong>Discount(%)</strong></td>
                                <td class="bg-primary text-center col-xs-2"><strong>Tax Rate(%)</strong></td>
                                <td class="bg-primary text-center col-xs-2"><strong>Amount</strong></td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $query_room = $db->query("SELECT pm_booking.nights,pm_booking.nightcharge,pm_booking.discount,pm_booking.booking_net_amount,pm_room.roomtitle,pm_tax.`taxrates`,
                                                (CASE WHEN pm_booking.unit = 1 THEN 'Night'            
                                                 WHEN pm_booking.unit = 2 THEN 'Person'
                                                 WHEN pm_booking.unit = 3 THEN 'Person/Night'
                                                 WHEN pm_booking.unit = 4 THEN 'Fixed Price'
                                                 WHEN pm_booking.unit = 5 THEN 'Quantity'
                                                 WHEN pm_booking.unit = 6 THEN 'Quantity/Night'
                                                 WHEN pm_booking.unit = 7 THEN 'Quantity/Night/Person'
                                                            ELSE 'notfound' END) AS unit_name
                                                FROM `pm_booking`
                                                LEFT JOIN pm_room ON pm_booking.room_id = pm_room.id
                                                LEFT JOIN pm_tax ON pm_booking.tax_rate = pm_tax.id
                                                WHERE pm_booking.booking_parent_id = $newbook_id and pm_room.lang = 2 OR pm_tax.lang = 2 GROUP BY pm_booking.id")->fetchAll();
                            $booking_room_total = 0;
                            foreach ($query_room as $roomdata){
                                ?>
                                <tr>
                                    <?php $booking_room_total += $roomdata['booking_net_amount'] ;?>
                                    <td class="text-center"><?php echo $roomdata['roomtitle'];?></td>
                                    <td class="text-center"><?php echo $roomdata['unit_name'];?></td>
                                    <td class="text-center"><?php echo $roomdata['nights'];?></td>
                                    <td class="text-center"><?php echo $roomdata['nightcharge'];?></td>
                                    <td class="text-center"><?php echo $roomdata['discount'];?></td>
                                    <td class="text-center"><?php echo $roomdata['taxrates'];?></td>
                                    <td class="text-center"><?php echo $roomdata['booking_net_amount'];?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-center"><b><?php echo round($booking_room_total,2); ?></b></td>
                            </tr>

                            <tr>
                                <td><strong>Services</strong></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php $query_service = $db->query("SELECT pm_booking_services.quantity,pm_booking_services.charges,pm_booking_services.discount,pm_booking_services.net_amount,pm_service.servicetitle,pm_tax.`taxrates`,
                                                (CASE WHEN pm_booking_services.unit_id = 1 THEN 'Night'            
                                                 WHEN pm_booking_services.unit_id = 2 THEN 'Person'
                                                 WHEN pm_booking_services.unit_id = 3 THEN 'Person/Night'
                                                 WHEN pm_booking_services.unit_id = 4 THEN 'Fixed Price'
                                                 WHEN pm_booking_services.unit_id = 5 THEN 'Quantity'
                                                 WHEN pm_booking_services.unit_id = 6 THEN 'Quantity/Night'
                                                 WHEN pm_booking_services.unit_id = 7 THEN 'Quantity/Night/Person'
                                                            ELSE 'notfound' END) AS unit_name
                                                FROM `pm_booking_services`
                                                LEFT JOIN pm_service ON pm_booking_services.service_id = pm_service.id
                                                LEFT JOIN pm_tax ON pm_booking_services.tax_rate = pm_tax.id
                                                WHERE pm_booking_services.booking_parent_id = $newbook_id and pm_service.lang = 2  OR pm_tax.lang = 2 GROUP BY pm_booking_services.id")->fetchAll();
                            $booking_service_total = 0;
                            if($query_service){
                                foreach ($query_service as $service_data){
                                    ?>
                                    <tr>
                                        <?php $booking_service_total += $service_data['net_amount'];?>
                                        <td class="text-center"><?php echo $service_data['servicetitle']?></td>
                                        <td class="text-center"><?php echo $service_data['unit_name']?></td>
                                        <td class="text-center"><?php echo $service_data['quantity']?></td>
                                        <td class="text-center"><?php echo $service_data['charges']?></td>
                                        <td class="text-center"><?php echo $service_data['discount']?></td>
                                        <td class="text-center"><?php echo $service_data['taxrates']?></td>
                                        <td class="text-center"><?php echo $service_data['net_amount']?></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-center"><b><?php echo round($booking_service_total,2);?></b></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><strong>Activities</strong></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php $query_activity = $db->query("SELECT pm_booking_activities.quantity,pm_booking_activities.charges,pm_booking_activities.discount,pm_booking_activities.net_amount,pm_activity.activityname,pm_tax.taxrates,
                                                (CASE WHEN pm_booking_activities.unit_id = 1 THEN 'Night'            
                                                 WHEN pm_booking_activities.unit_id = 2 THEN 'Person'
                                                 WHEN pm_booking_activities.unit_id = 3 THEN 'Person/Night'
                                                 WHEN pm_booking_activities.unit_id = 4 THEN 'Fixed Price'
                                                 WHEN pm_booking_activities.unit_id = 5 THEN 'Quantity'
                                                 WHEN pm_booking_activities.unit_id = 6 THEN 'Quantity/Night'
                                                 WHEN pm_booking_activities.unit_id = 7 THEN 'Quantity/Night/Person'
                                                            ELSE 'notfound' END) AS unit_name
                                                FROM `pm_booking_activities`
                                                LEFT JOIN pm_activity ON pm_booking_activities.activity_id = pm_activity.id
                                                LEFT JOIN pm_tax ON pm_booking_activities.tax_rate = pm_tax.id
                                                WHERE pm_booking_activities.booking_parent_id = $newbook_id  OR pm_tax.lang = 2 GROUP BY pm_booking_activities.id")->fetchAll();
                            $booking_activity_total = 0;
                            if($query_activity){
                                foreach ($query_activity as $activity_data){
                                    ?>
                                    <tr>
                                        <?php $booking_activity_total += $activity_data['net_amount'];?>
                                        <td class="text-center"><?php echo $activity_data['activityname'];?></td>
                                        <td class="text-center"><?php echo $activity_data['unit_name'];?></td>
                                        <td class="text-center"><?php echo $activity_data['quantity'];?></td>
                                        <td class="text-center"><?php echo $activity_data['charges'];?></td>
                                        <td class="text-center"><?php echo $activity_data['discount'];?></td>
                                        <td class="text-center"><?php echo $activity_data['taxrates'];?></td>
                                        <td class="text-center"><?php echo $activity_data['net_amount'];?></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-center"><b><?php echo round($booking_activity_total,2); ?></b></td>
                                </tr>
                            <?php } ?>
                            <!--                                            <tr>-->
                            <!--                                                <td><strong>Add On</strong></td>-->
                            <!--                                                <td></td>-->
                            <!--                                                <td></td>-->
                            <!--                                                <td></td>-->
                            <!--                                                <td></td>-->
                            <!--                                                <td></td>-->
                            <!--                                            </tr>-->
                            <!--                                            <tr>-->
                            <!--                                                <td class="text-center">A</td>-->
                            <!--                                                <td class="text-center">No</td>-->
                            <!--                                                <td class="text-center">2</td>-->
                            <!--                                                <td class="text-center">450</td>-->
                            <!--                                                <td class="text-center">0</td>-->
                            <!--                                                <td class="text-center">900</td>-->
                            <!--                                            </tr>-->
                            <!--                                            <tr>-->
                            <!--                                                <td></td>-->
                            <!--                                                <td></td>-->
                            <!--                                                <td></td>-->
                            <!--                                                <td></td>-->
                            <!--                                                <td></td>-->
                            <!--                                                <td class="text-center"><b>2325</b></td>-->
                            <!--                                            </tr>-->
                            <?php  $query_payment = $db->query("SELECT payment_mode,round((COALESCE(sum(total_room_charge),0)) + (COALESCE(sum(total_activity_charge),0)) + (COALESCE(sum(total_service_charge),0)),2) as sum_total_net,
                                                    round((COALESCE(sum(down_payment),0)),2) as sum_downpayment ,
                                                    round((COALESCE(sum(total_room_charge),0)) + (COALESCE(sum(total_activity_charge),0)) + (COALESCE(sum(total_service_charge),0)) - (COALESCE(sum(down_payment),0)),2) as balance
                                                    FROM `pm_booking_payment` where booking_id = $newbook_id")->fetchAll();
                            $subpayment = $db->query("SELECT payment_method,
                                                        round((COALESCE(sum(payment_amount),0)),2) as sub_payment
                                                        FROM `pm_booking_sub_payment`
                                                        where booking_id = $newbook_id")->fetchAll();
                            foreach($query_payment as $payment){
                                ?>
                                <tr>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line text-right col-12 col-sm-12 col-lg-12"><strong>(Including tax)Net Total.</strong></td>
                                    <td class="thick-line text-center col-12 col-sm-12 col-lg-12"><?php echo $payment['sum_total_net'];?></td>
                                </tr>
                                <!--                                            <tr>-->
                                <!--                                                <td class="no-line"></td>-->
                                <!--                                                <td class="no-line"></td>-->
                                <!--                                                <td class="no-line"></td>-->
                                <!--                                                <td class="no-line"></td>-->
                                <!--                                                <td class="no-line text-right"><strong>Tax Total.</strong></td>-->
                                <!--                                                <td class="no-line text-center">+275</td>-->
                                <!--                                            </tr>-->
                                <!--                                            <tr>-->
                                <!--                                                <td class="no-line"></td>-->
                                <!--                                                <td class="no-line"></td>-->
                                <!--                                                <td class="no-line"></td>-->
                                <!--                                                <td class="no-line"></td>-->
                                <!--                                                <td class="no-line text-right"><strong>Net Payable</strong></td>-->
                                <!--                                                <td class="no-line text-center">7000</td>-->
                                <!--                                            </tr>-->
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-right col-12 col-sm-12 col-lg-12"><strong>Down Payment (<?php echo $payment['payment_mode'];?>)</strong></td>
                                    <td class="no-line text-center col-12 col-sm-12 col-lg-12"><?php echo $payment['sum_downpayment'];?></td>
                                </tr>
                                <!--                                <tr>-->
                                <!--                                    <td class="no-line"></td>-->
                                <!--                                    <td class="no-line"></td>-->
                                <!--                                    <td class="no-line"></td>-->
                                <!--                                    <td class="no-line"></td>-->
                                <!--                                    <td class="no-line"></td>-->
                                <!--                                    <td class="no-line text-right"><strong>Payment (--><?php //echo $payment['payment_mode'];?><!--)</strong></td>-->
                                <!--                                    <td class="no-line text-center">--><?php //echo $payment['sum_total_net'];?><!--</td>-->
                                <!--                                </tr>-->
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-right col-12 col-sm-12 col-lg-12"><strong>Payment Received Total </strong></td>
                                    <td class="no-line text-center"><?php echo $subpayment[0]['sub_payment'];?></td>
                                </tr>
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-right col-12 col-sm-12 col-lg-12"><strong>Balance</strong></td>
                                    <td class="no-line text-center"><strong><?php echo $payment['balance']-$subpayment[0]['sub_payment']; ?></strong></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    $(document).ready(function(){
        $(".print_bill").print();
    });
</script>