<?php
/**
 * Template of the module listing
 */
debug_backtrace() || die ('Direct access not permitted');


// Action to perform
$action = (isset($_GET['action'])) ? htmlentities($_GET['action'], ENT_QUOTES, 'UTF-8') : '';

// Item ID
$id = (isset($_GET['id']) && is_numeric($_GET['id'])) ? $_GET['id'] : 0;

// current page
if (isset($_GET['offset']) && is_numeric($_GET['offset'])) $offset = $_GET['offset'];
elseif (isset($_SESSION['offset']) && isset($_SESSION['module_referer']) && $_SESSION['module_referer'] == MODULE) $offset = $_SESSION['offset'];
else $offset = 0;

// Items per page
if (isset($_GET['limit']) && is_numeric($_GET['limit'])) {
    $limit = $_GET['limit'];
    $offset = 0;
} elseif (isset($_SESSION['limit']) && isset($_SESSION['module_referer']) && $_SESSION['module_referer'] == MODULE) $limit = $_SESSION['limit'];
else $limit = 20;

$_SESSION['limit'] = $limit;

$_SESSION['offset'] = $offset;

// Inclusions
require_once(SYSBASE . ADMIN_FOLDER . '/includes/fn_list.php');
?>

<?php
if ($db !== false) {
    // Initializations
    $cols = getCols();
    $filters = getFilters($db);
    if (is_null($cols)) $cols = array();
    if (is_null($filters)) $filters = array();
    $total = 0;
    $total_page = 0;
    $q_search = '';
    $result_lang = false;
    $total_lang = 1;
    $result = false;
    $referer = DIR . 'index.php?view=list';

    // Sort order
    if (isset($_GET['order'])) $order = htmlentities($_GET['order'], ENT_QUOTES, 'UTF-8');
    elseif (isset($_SESSION['order']) && $_SESSION['order'] != '' && isset($_SESSION['module_referer']) && $_SESSION['module_referer'] == MODULE) $order = $_SESSION['order'];
    else $order = getOrder();

    if (isset($_GET['sort'])) $sort = htmlentities($_GET['sort'], ENT_QUOTES, 'UTF-8');
    elseif (isset($_SESSION['sort']) && $_SESSION['sort'] != '' && isset($_SESSION['module_referer']) && $_SESSION['module_referer'] == MODULE) $sort = $_SESSION['sort'];
    else $sort = 'asc';

    if (strpos($order, ' ') !== false) {
        $sort = strtolower(substr($order, strpos($order, ' ') + 1));
        $order = substr($order, 0, strpos($order, ' '));
    }

    $_SESSION['order'] = $order;
    $_SESSION['sort'] = $sort;

    $rsort = ($sort == 'asc') ? 'desc' : 'asc';

    // Getting languages
    if (MULTILINGUAL) {
        $result_lang = $db->query('SELECT id, title FROM pm_lang WHERE id != ' . DEFAULT_LANG . ' AND checked = 1');
        if ($result_lang !== false)
            $total_lang = $db->last_row_count();
    }

    // Getting filters values
    if (isset($_SESSION['module_referer']) && $_SESSION['module_referer'] !== MODULE) {
        unset($_SESSION['filters']);
        unset($_SESSION['q_search']);
    }
    if (isset($_POST['search'])) {
        foreach ($filters as $filter) {
            $fieldName = $filter->getName();
            $value = (isset($_POST[$fieldName])) ? htmlentities($_POST[$fieldName], ENT_QUOTES, 'UTF-8') : '';
            $filter->setValue($value);
        }
        $q_search = htmlentities($_POST['q_search'], ENT_QUOTES, 'UTF-8');
        $_SESSION['filters'] = serialize($filters);
        $_SESSION['q_search'] = $q_search;
        $offset = 0;
        $_SESSION['offset'] = $offset;
    } else {
        if (isset($_SESSION['filters'])) $filters = unserialize($_SESSION['filters']);
        if (isset($_SESSION['q_search'])) $q_search = $_SESSION['q_search'];
    }

    // Getting items in the database
    $condition = '';

    if (MULTILINGUAL) $condition .= ' lang = ' . DEFAULT_LANG;

    foreach ($filters as $filter) {
        $fieldName = $filter->getName();
        $fieldValue = $filter->getValue();
        if ($fieldValue != '') {
            if ($condition != '') $condition .= ' AND';
            $condition .= ' ' . $fieldName . ' = ' . $db->quote($fieldValue);
        }
    }

    if (!in_array($_SESSION['user']['type'], array('administrator', 'manager', 'editor')) && db_column_exists($db, 'pm_' . MODULE, 'users')) {
        if ($condition != '') $condition .= ' AND';
        $condition .= ' users REGEXP \'(^|,)' . $_SESSION['user']['id'] . '(,|$)\'';
    }

    $query_search = db_getRequestSelect($db, 'pm_' . MODULE, getSearchFieldsList($cols), $q_search, $condition, $order . ' ' . $sort);

    $result_total = $db->query($query_search);
    if ($result_total !== false)
        $total = $db->last_row_count();

    if ($limit > 0) $query_search .= ' LIMIT ' . $limit . ' OFFSET ' . $offset;

    $result = $db->query($query_search);
    if ($result !== false)
        $total_page = $db->last_row_count();

    if (in_array('edit', $permissions) || in_array('all', $permissions)) {

        // Setting main item
        if ($action == 'define_main' && $id > 0 && check_token($referer, 'list', 'get'))
            define_main($db, 'pm_' . MODULE, $id, 1);

        if ($action == 'remove_main' && $id > 0 && check_token($referer, 'list', 'get'))
            define_main($db, 'pm_' . MODULE, $id, 0);

        // Items displayed in homepage
        if ($action == 'display_home' && $id > 0 && check_token($referer, 'list', 'get'))
            display_home($db, 'pm_' . MODULE, $id, 1);

        if ($action == 'remove_home' && $id > 0 && check_token($referer, 'list', 'get'))
            display_home($db, 'pm_' . MODULE, $id, 0);

        if ($action == 'display_home_multi' && isset($_POST['multiple_item']) && check_token($referer, 'list', 'get'))
            display_home_multi($db, 'pm_' . MODULE, 1, $_POST['multiple_item']);

        if ($action == 'remove_home_multi' && isset($_POST['multiple_item']) && check_token($referer, 'list', 'get'))
            display_home_multi($db, 'pm_' . MODULE, 0, $_POST['multiple_item']);

        // Item activation/deactivation
        if ($action == 'check' && $id > 0 && check_token($referer, 'list', 'get'))
            check($db, 'pm_' . MODULE, $id, 1);

        if ($action == 'uncheck' && $id > 0 && check_token($referer, 'list', 'get'))
            check($db, 'pm_' . MODULE, $id, 2);

        if ($action == 'archive' && $id > 0 && check_token($referer, 'list', 'get'))
            check($db, 'pm_' . MODULE, $id, 3);

        if ($action == 'check_multi' && isset($_POST['multiple_item']) && check_token($referer, 'list', 'get'))
            check_multi($db, 'pm_' . MODULE, 1, $_POST['multiple_item']);

        if ($action == 'uncheck_multi' && isset($_POST['multiple_item']) && check_token($referer, 'list', 'get'))
            check_multi($db, 'pm_' . MODULE, 2, $_POST['multiple_item']);

        if ($action == 'archive_multi' && isset($_POST['multiple_item']) && check_token($referer, 'list', 'get'))
            check_multi($db, 'pm_' . MODULE, 3, $_POST['multiple_item']);
    }

    if (in_array('delete', $permissions) || in_array('all', $permissions)) {

        // Item deletion
        if ($action == 'delete' && $id > 0 && check_token($referer, 'list', 'get'))
            delete_item($db, $id);

        if ($action == 'delete_multi' && isset($_POST['multiple_item']) && check_token($referer, 'list', 'get'))
            delete_multi($db, $_POST['multiple_item']);
    }

    if (in_array('all', $permissions)) {

        // Languages completion
        if (MULTILINGUAL && isset($_POST['complete_lang']) && isset($_POST['languages']) && check_token($referer, 'list', 'post')) {
            foreach ($_POST['languages'] as $id_lang) {
                complete_lang_module($db, 'pm_' . MODULE, $id_lang);
                if (NB_FILES > 0) complete_lang_module($db, 'pm_' . MODULE . '_file', $id_lang, true);
            }
        }
    }
}

$_SESSION['module_referer'] = MODULE;
$csrf_token = get_token('list'); ?>
<!DOCTYPE html>
<head>
    <?php include(SYSBASE . ADMIN_FOLDER . '/includes/inc_header_list.php'); ?>
</head>
<body>
<div id="wrapper">
    <?php include(SYSBASE . ADMIN_FOLDER . '/includes/inc_top.php'); ?>
    <div id="page-wrapper">
        <div class="page-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 clearfix">
                        <div class="col-md-6">
                            <div class="pull-left">
                                <h1><i class="fa fa-<?php echo ICON; ?>"></i>Schedule</h1>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <?php
        if ($db !== false) {
            if (!in_array('no_access', $permissions)) { ?>
                <input type="hidden" name="csrf_token" value="<?php echo $csrf_token; ?>"/>
                <div class="panel-body">

                    <div class="table-responsive">
                        <div class="error">
                        </div>
                        <form id="search_form" class="form-horizontal" role="form" method="post"
                              enctype="multipart/form-data">

                            <div class="filter-bar">
                                <div class="container-fluid">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="col-md-4">
                                                    <div class="date-lable">
                                                        <label>Check In</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-8">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                        <input id="" type="date" placeholder="Calendar"
                                                               class="checkIn form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="col-md-4">
                                                    <div class="date-lable">
                                                        <label>Check Out</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                        <input id="" type="date" placeholder="Calendar"
                                                               class="checkOut form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <div class="date-lable">
                                                            <label>Room Type</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-inline roomtype_dropdown">
                                                            <?php echo getDropDownDataroomactive('pm_roomtypes', 'roomtype', 'roomtype_id', 'roomtype', 'Room', 'required', $db) ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="filter-bar">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="col-md-4">
                                                <div class="date-lable">
                                                    <label>No.Of Nights stay</label>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="number" class="form-control" name="staynight_id" id="staynight_id">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="col-md-4">
                                                <div class="date-lable">
                                                    <label>Adult</label>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="number" class="form-control" name="adult_id" id="adult_id">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="col-md-4">
                                                <div class="date-lable">
                                                    <label>Children</label>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="number" class="form-control" name="children_id" id="children_id">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="filter-bar">
                                <div class="container-fluid">

                                    <div class="row">
                                        <div class="col-md-9"></div>
                                        <div class="col-md-1">
                                            <input type="button" class="btn btn-success refresh_search" id="refresh_search" readonly style="color:lightgrey;"
                                                   name="refresh" value="Refresh">
                                        </div>
                                        <div class="col-md-1">
                                            <input type="button" class="btn btn-success search" id="search"
                                                   name="search" value="Check">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <hr />
                    </div>
                    <div class="floorhtml">
                    </div>
                </div>
                <?php
            } else echo '<p>' . $texts['ACCESS_DENIED'] . '</p>';
        } ?>
    </div>
</div>
</div>
</body>
</html>
<script type="text/javascript">
    $(document).ready(function () {
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap'
        });
    });

    $(document).ready(function () {
        $('#datepickers').datepicker({
            uiLibrary: 'bootstrap'
        });
    });
</script>

