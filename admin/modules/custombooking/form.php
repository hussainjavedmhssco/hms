<style>
    .pl10 {
        padding-left: 10px;
    }

    .heading th {
        font-size: 15px !important;
        font-weight: normal;
        color: #4B4C4D;
    }

    #tbl_posts_body {
        background-color: #E3E3E3;
    }

    .floatbtn {
        float: right;
        margin-right: 1%;
    }

    .invoice-title h2, .invoice-title h3 {
        display: inline-block;
    }

    .table > tbody > tr > .no-line {
        border-top: none;
    }

    .table > thead > tr > .no-line {
        border-bottom: none;
    }

    .table > tbody > tr > .thick-line {
        border-top: 2px solid;
    }

</style>
<?php
/**
 * Template of the module form
 */
debug_backtrace() || die ('Direct access not permitted');
// Item ID
if(isset($_GET['id']) && is_numeric($_GET['id'])) $id = $_GET['id'];
elseif(isset($_POST['id']) && is_numeric($_POST['id'])) $id = $_POST['id'];
else{
    header('Location: index.php?view=list');
    exit();
}
// Inclusions
require_once(SYSBASE . ADMIN_FOLDER . '/includes/fn_form.php');
// Getting datas in the database
if ($db !== false) {
    $result = $db->query('SELECT * FROM pm_' . MODULE . ' WHERE id = ' . $id);



}

$csrf_token = get_token('form'); ?>
<!DOCTYPE html>
<head>
    <?php include(SYSBASE . ADMIN_FOLDER . '/includes/inc_header_form.php'); ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">

</head>
<body>
<div id="overlay">
    <div id="loading"></div>
</div>

<?php if($id > 0){ ?>
    <div id="wrapper">
        <?php
        include(SYSBASE . ADMIN_FOLDER . '/includes/inc_top.php');

        if (!in_array('no_access', $permissions)) {
            include(SYSBASE . ADMIN_FOLDER . '/includes/inc_library.php'); ?>
            <div id="page-wrapper">
                <div class="page-header">

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-6 col-md-6 col-sm-8 clearfix">
                                <h1 class="pull-left"><i class="fa fa-cog"></i> <?php echo TITLE_ELEMENT; ?></h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="panel panel-default">
                        <ul class="nav nav-tabs pt5" id="tabs">
                            <li class="active booking_active"><a data-toggle="tab" href="#BD"><i
                                            class="fas fa-book"></i> <?php echo "BookingDetails"; ?></a></li>
                            <li class="customer_active" onclick="customer_click();"><a data-toggle="tab"
                                                                                       href="#customerinfo"><i
                                            class="fas fa-info-circle"></i> <?php echo 'CustomerInfo'; ?></a></li>
                            <li class="services_active" onclick="service_tab();"><a data-toggle="tab" href="#services"><i
                                            class="fas fa-wrench"></i> <?php echo 'Services'; ?></a></li>
                            <li class="activity_active" onclick="activity_tab();"><a data-toggle="tab" href="#activities"><i
                                            class="fa-ticket"></i> <?php echo 'Activity'; ?>
                                </a></li>
                            <li onclick="payment_tab();"><a data-toggle="tab" href="#payment"><i
                                            class="fa-usd"></i> <?php echo 'Payment'; ?></a>
                            </li>
                        </ul>

                        <div class="panel-body">
                            <div class="tab-content">
                                <?php
                                if ($_SESSION['user']['type'] == 'administrator') { ?>
                                <div id="BD" class="tab-pane fade in active booking_content">
                                    <div class="row mb10">
                                        <div class="row">
                                            <div class="col-md-4 mb10">
                                                <h5 class="date_error" style="color: red;font-size:small;"></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 mb10">
                                                <h5 class="data_success" style="color: green;font-size:small;"></h5>
                                            </div>
                                        </div>
                                        <form id="booking_form" method="POST">
                                            <?php        $get_booking = $db->query("SELECT id,date(booking_date) as booking_date,date(checkin_date) as checkin_date,date(checkout_date) as checkout_date from pm_booking_parent where id = $id")->fetchAll(); ?>
                                            <div class="row">
                                                <div class="col-md-2 ">
                                                    <label class="control-label pl10">
                                                        <?php echo 'Book ID'; ?>
                                                    </label>
                                                </div>
                                                <div class="col-md-4 mb10">
                                                    <?php $booking_id = get_numbering("pm_booking_parent", $db); ?>
                                                    <input class="form-control booking_id" type="text" disabled
                                                           value="<?php echo $get_booking[0]['id'];?>"
                                                           name="booking_id[]">
                                                </div>
                                            </div>
                                            <!--                                            <div class="row">-->
                                            <!--                                                <div class="col-md-2 ">-->
                                            <!--                                                    <label class="control-label pl10">-->
                                            <!--                                                        --><?php //echo 'Package ID'; ?>
                                            <!--                                                    </label>-->
                                            <!--                                                </div>-->
                                            <!--                                                <div class="col-md-4 mb10">-->
                                            <!--                                                    <input class="form-control" type="text" value="" name="package-id"-->
                                            <!--                                                           placeholder="Package ID">-->
                                            <!--                                                </div>-->
                                            <!--                                            </div>-->
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label class="control-label pl10">
                                                        <?php echo 'Booking Date'; ?>
                                                    </label>
                                                </div>
                                                <div class="col-md-4 mb10">
                                                    <input class="form-control" type="date"
                                                           value="<?php
                                                           echo $get_booking[0]['booking_date']; ?>"
                                                           name="booking_date[]" disabled>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label class="control-label pl10">
                                                        <?php echo 'Check In Date'; ?>
                                                    </label>
                                                </div>
                                                <div class="col-md-4 mb10">
                                                    <input class="form-control" type="date"
                                                           value="<?php
                                                               echo $get_booking[0]['checkin_date'];
                                                            ?>"
                                                           name="CheckIn_date[]" disabled>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label class="control-label pl10">
                                                        <?php echo 'Check Out Date'; ?>
                                                    </label>
                                                </div>
                                                <div class="col-md-4 mb10">
                                                    <input class="form-control" type="date"
                                                           value="<?php
                                                               echo $get_booking[0]['checkout_date'];
                                                            ?>"
                                                           name="CheckOut_date[]">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 mb10">
                                                    <strong><h5 class="error" style="color: red;font-size:small;"></h5></strong>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="row">
                                        <input name="addroom" value="Add Room" type="button" style="margin-bottom: 12px;"
                                               class="btn btn-success floatbtn add_room" onclick="add_room();">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered heading" id="tbl_posts_book">
                                                <thead>
                                                <tr>
                                                    <th>Room Code</th>
                                                    <th>Room Title</th>
                                                    <th>Room Type</th>
                                                    <th>Unit</th>
                                                    <th>Night Charges</th>
                                                    <th>Adults</th>
                                                    <th>Children</th>
                                                    <th>No. of Nights</th>
                                                    <th>Discount(%)</th>
                                                    <th>Tax Rate(%)</th>
                                                    <th>Net Amount</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbl_posts_body_edit">

                                                <?php
                                                        //get data from temp table
                                                        $get_booking_id = $db->query("SELECT pm_booking.*,pm_booking.id as room_book_id,pm_room.roomcode,pm_room.roomtitle,pm_booking.nightcharge,pm_booking.adults,
                                                                    pm_booking.nights,pm_booking.discount,pm_booking.children,
                                                                    (SELECT pm_room.id_roomtype
                                                                     FROM pm_room 
                                                                     where pm_room.id = pm_booking.room_id and pm_room.lang = 2) as room_type_id,
                                                                    (SELECT pm_roomtypes.roomtype from pm_roomtypes where pm_roomtypes.id = pm_room.id_roomtype and pm_roomtypes.lang = 2) as room_type_name
                                                                    FROM pm_booking
                                                                    INNER JOIN pm_room
                                                                    ON  pm_booking.room_id = pm_room.id
                                                                    WHERE pm_booking.booking_parent_id = $id and pm_room.lang = 2")->fetchAll();
                                                        if ($get_booking_id) {
                                                                foreach ($get_booking_id as $roomdata) { ?>
                                                                    <input type="hidden" name="room_id[]"
                                                                           value="<?php echo $roomdata['room_id']; ?>"/>
                                                                    <input type="hidden" name="room_book_id[]"
                                                                           value="<?php echo $roomdata['room_book_id']; ?>"/>
                                                                    <input type="hidden" name="taxbooking_id[]"
                                                                           class="taxbooking_id<?php echo $roomdata['room_id']; ?>"
                                                                           value="<?php echo $roomdata['tax_rate']; ?>"/>
                                                                    <input type="hidden" name="unit_id[]"
                                                                           class="unit_id<?php echo $roomdata['id']; ?>"/>
                                                                    <input type="hidden" name="discount_amount[]"
                                                                           value="<?php echo $roomdata['discount_amount']; ?>" class="discount_amount<?php echo $roomdata['id']; ?>"/>
                                                                    <input type="hidden" name="vat_amount[]"
                                                                           value="<?php echo $roomdata['vat_amount']; ?>" class="vat_amount<?php echo $roomdata['id']; ?>"/>
                                                                    <tr id="rec_<?php echo $roomdata['id']; ?>">
                                                                        <td>
                                                                            <span class="sn"><?php echo $roomdata['roomcode']; ?></span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="sn"><?php echo $roomdata['roomtitle']; ?></span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="sn"><?php echo $roomdata['room_type_name']; ?></span>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo getDropDownDataUnitroom('unit_id', 'unit') ?>
                                                                        </td>
                                                                        <td><input type="text"
                                                                                   class="nightcharge<?php echo $roomdata['room_id']; ?>"
                                                                                   disabled
                                                                                   name="nightcharge[]"
                                                                                   value="<?php echo $roomdata['nightcharge']; ?>"
                                                                                   size="4"/></td>
                                                                        <td><input type="text" name="adults[]"
                                                                                   class="maxadults<?php echo $roomdata['room_id']; ?>"
                                                                                   onkeyup="findadult(<?php echo $roomdata['room_id']; ?>)"
                                                                                   value="<?php echo $roomdata['adults']; ?>"
                                                                                   size="4"/>
                                                                        </td>
                                                                        <td><input type="text" name="children[]"
                                                                                   class="maxchild<?php echo $roomdata['room_id']; ?>"
                                                                                   onkeyup="findchild(<?php echo $roomdata['room_id']; ?>)"
                                                                                   value="<?php  echo $roomdata['children'];?>"
                                                                                   size="4"/></td>
                                                                        <td><input type="text" name="night[]"
                                                                                   class="staynight<?php echo $roomdata['room_id']; ?>"
                                                                                   onkeyup="updatenetprice(<?php echo $roomdata['room_id']; ?>)"
                                                                                   value="<?php echo $roomdata['nights']; ?>"
                                                                                   size="4"/>
                                                                        </td>
                                                                        <td><input type="text"
                                                                                   class="discount<?php echo $roomdata['room_id']; ?>"
                                                                                   name="discount[]"
                                                                                   onkeyup="updatenetpricewithdiscount(<?php echo $roomdata['room_id']; ?>)"
                                                                                   value="<?php echo $roomdata['discount']; ?>"
                                                                                   size="4"/></td>
                                                                        <td>
                                                                            <?php $tax_id = $roomdata['tax_rate'];
                                                                            $unique_id = $roomdata['room_id'];
                                                                            echo getSelectedDropDownDatatax('pm_tax', 'taxrate', 'tax_id' . $roomdata['room_id'], 'taxname', 'Tax', 'required', $tax_id, $db) ?>
                                                                            <script>
                                                                                $("#tax_id" +<?php echo $unique_id;?>).change(function (event) {
                                                                                    $(".error").html("");
                                                                                    if (event.target == this) {
                                                                                        var selectedarrayval = $(this).val();
                                                                                    $(".taxbooking_id" +<?php echo $unique_id?>).val(selectedarrayval);
                                                                                    $(".netamount" +<?php echo $unique_id?>).val('');
                                                                                    var tax_id = selectedarrayval;
                                                                                    var nightcharge = $(".nightcharge" +<?php echo $unique_id?>).val();
                                                                                    var staynight = $(".staynight" +<?php echo $unique_id?>).val();
                                                                                    var discount = $(".discount" +<?php echo $unique_id?>).val();
                                                                                    if (staynight == '') {
                                                                                        $.ajax({
                                                                                            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                                                                                            type: 'POST',
                                                                                            data: {
                                                                                                'remove_id': <?php echo $unique_id?>,
                                                                                                'comparechild': 1
                                                                                            },
                                                                                            dataType: 'JSON',
                                                                                            success: function (data) {
                                                                                                $(".error").html("");
                                                                                                $(".error").append("<span>No of Nights Stay cannot be empty in " + data[0].roomtitle + " </span>");
                                                                                                return false;
                                                                                            }
                                                                                        });
                                                                                    } else {
                                                                                        if (isNaN(staynight)) {
                                                                                            $.ajax({
                                                                                                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                                                                                                type: 'POST',
                                                                                                data: {
                                                                                                    'remove_id': <?php echo $unique_id?>,
                                                                                                    'comparechild': 1
                                                                                                },
                                                                                                dataType: 'JSON',
                                                                                                success: function (data) {
                                                                                                    $(".error").html("");
                                                                                                    $(".error").append("<span>Please Use Numeric value Instead " + staynight + " in " + data[0].roomtitle + " </span>");
                                                                                                    return false;
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                        else {
                                                                                            $(".error").html("");
                                                                                            $.ajax({
                                                                                                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                                                                                                type: 'POST',
                                                                                                data: {
                                                                                                    'tax_id': tax_id,
                                                                                                    'gettaxrate': 1
                                                                                                },
                                                                                                dataType: 'JSON',
                                                                                                success: function (data) {
                                                                                                    $(".error").html("");
                                                                                                    //totalnetamount without vat
                                                                                                    if (discount == '') {
                                                                                                        discount = 0;
                                                                                                        var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                                                                                                        var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                                                                                                        var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                                                                                                        var taxamount = parseFloat(data[0].taxrates);
                                                                                                        var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                                                                                                        var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                                                                                                        $(".dicount_amount" +<?php echo $unique_id?>).val(getdiscountamount.toFixed(2));
                                                                                                        $(".vat_amount" +<?php echo $unique_id?>).val(getvatamount.toFixed(2));
                                                                                                        $(".netamount" +<?php echo $unique_id?>).val(totalnet.toFixed(2));
                                                                                                    } else {
                                                                                                        var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                                                                                                        var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                                                                                                        var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                                                                                                        var taxamount = parseFloat(data[0].taxrates);
                                                                                                        var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                                                                                                        var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                                                                                                        $(".dicount_amount" +<?php echo $unique_id?>).val(0);
                                                                                                        $(".vat_amount" +<?php echo $unique_id?>).val(getvatamount.toFixed(2));
                                                                                                        $(".netamount" +<?php echo $unique_id?>).val(totalnet.toFixed(2));
                                                                                                    }
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    }
                                                                                    }
                                                                                });
                                                                                $("#unit_id" +<?php echo $unique_id?>).change(function (event) {
                                                                                    $(".error").html();
                                                                                    if (event.target == this) {
                                                                                        var selectedarrayval = $(this).val();
                                                                                        $(".unit_id" +<?php echo $unique_id?>).val(selectedarrayval);
                                                                                    }
                                                                                });
                                                                            </script>
                                                                        </td>
                                                                        <td><input type="text"
                                                                                   name="netamount[]"
                                                                                   class="netamount<?php echo $roomdata['room_id']; ?> totalbook"
                                                                                   disabled
                                                                                   value="<?php echo $roomdata['booking_net_amount'];?>"
                                                                                   size="4"/></td>
                                                                        <td>
                                                                            <a class="btn btn-xs delete-record"
                                                                               data-id="<?php echo $room_id; ?>"><i
                                                                                        class="glyphicon glyphicon-trash"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            <?php
                                                        }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <input type="hidden" class="nettotal">
                                    </form>
                                </div>
                                <style>
                                    .icon {
                                        padding: 10px;
                                        background: dodgerblue;
                                        color: white;
                                        min-width: 45px;
                                        text-align: center;
                                        font-size: 14px;
                                    }

                                    .input-group-addon {
                                        margin: 0;
                                        padding: 0;
                                    }
                                </style>
                                <div id="customerinfo" class="tab-pane fade customer_content">
                                    <?php $getuser_data = $db->query("select * from pm_booking_customer where booking_id = $id")->fetchAll();?>
                                    <div class="row">
                                        <div class="col-md-4 mb10">
                                            <h5 class="data_success" style="color: green;font-size:small;"></h5>
                                        </div>
                                    </div>
                                    <form id="customer_form">
                                        <input type="hidden" name="customer_book_id" value="<?php echo $getuser_data[0]['id'];?>">
                                        <div class="row">
                                            <div class="col-md-4 mb10">
                                                <h5 class="errorcustomer" style="color: red;font-size:small;"></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2" id="project-label">
                                                <label class="control-label  pl10">
                                                    <?php echo 'Customer-Name'; ?>
                                                </label>
                                            </div>
                                            <div class="col-md-4" id="project-label">
                                                <div class="field-adjust">
                                                    <div class="input-group">
                                                        <div class="field-adjust">
                                                            <input id="customername" class="form-control customername"
                                                                   type="text"
                                                                   value="<?php echo $getuser_data[0]['customer_name'];?>" name="customer" onkeyup="getuser();">
                                                            <input type="hidden" id="customername-id"/></div>
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-mobile icon"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <label class="control-label  pl10">
                                                    First Name
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <input class="form-control firstname" type="text" value="<?php if(isset($getuser_data[0]['first_name'])){ echo $getuser_data[0]['first_name']; }?>"
                                                       name="firstname">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label  pl10">
                                                    Country
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <input class="form-control country" type="text" value="<?php if(isset($getuser_data[0]['country'])){ echo $getuser_data[0]['country']; }?>" name="country">
                                            </div>
                                            <div class="col-md-2">
                                                <label class="control-label  pl10">
                                                    Last Name
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <input class="form-control lastname" type="text" value="<?php if(isset($getuser_data[0]['last_name'])){ echo $getuser_data[0]['last_name']; }?>"
                                                       name="lastname">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label  pl10">
                                                    Company Name
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <input class="form-control companyname" type="text" value="<?php if(isset($getuser_data[0]['company_name'])){ echo $getuser_data[0]['company_name']; }?>"
                                                       name="companyname">
                                            </div>
                                            <div class="col-md-2">
                                                <label class="control-label  pl10">
                                                    Email
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <input class="form-control email" type="text" value="<?php if(isset($getuser_data[0]['email'])){ echo $getuser_data[0]['email']; }?>" name="email">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label  pl10">
                                                    Address
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <input class="form-control address" type="text" value="<?php if(isset($getuser_data[0]['address'])){ echo $getuser_data[0]['address']; }?>" name="address">
                                            </div>
                                            <div class="col-md-2">
                                                <label class="control-label  pl10">
                                                    Phone-No
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <input class="form-control phone" type="text" value="<?php if(isset($getuser_data[0]['phone_no'])){ echo $getuser_data[0]['phone_no']; }?>" name="phone">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label  pl10">
                                                    PostCode
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <input class="form-control postcode" type="text" value="<?php if(isset($getuser_data[0]['post_code'])){ echo $getuser_data[0]['post_code']; }?>"
                                                       name="postcode">
                                            </div>
                                            <div class="col-md-2">
                                                <label class="control-label  pl10">
                                                    Mobile-No
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <input class="form-control mobile" type="text" value="<?php if(isset($getuser_data[0]['first_name'])){ echo $getuser_data[0]['first_name']; }?>" name="mobile">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label  pl10">
                                                    City
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <input class="form-control city" type="text" value="<?php if(isset($getuser_data[0]['city'])){ echo $getuser_data[0]['city']; }?>" name="city">
                                            </div>
                                            <div class="col-md-2">
                                                <label class="control-label  pl10">
                                                    CNIC/EID
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <input class="form-control cnic" type="text" value="<?php if(isset($getuser_data[0]['cnic'])){ echo $getuser_data[0]['cnic']; }?>" name="cnic">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label  pl10">
                                                    Comments
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <input class="form-control comments" type="text" value="<?php if(isset($getuser_data[0]['comment'])){ echo $getuser_data[0]['comment']; }?>"
                                                       name="comments">
                                            </div>
                                            <div class="col-md-2">
                                                <label class="control-label  pl10">
                                                    Passport
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <input class="form-control passport" type="text" value="<?php if(isset($getuser_data[0]['passport'])){ echo $getuser_data[0]['passport']; }?>"
                                                       name="passport">
                                            </div>
                                        </div>
                                      </div>
                                <div id="services" class="tab-pane fade services service_content">
                                    <!--                                        function service_tab   -->
                                    <form id="service_form">
                                        <div class="row">
                                            <div class="col-md-4 mb10">
                                                <h5 class="data_success" style="color: green;font-size:small;"></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 mb10">
                                                <h5 class="errorservices" style="color: red;font-size:small;"></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <input type="button" name="save" onclick="add_row();" value="Add Services"
                                                   class="btn btn-success mt15 floatbtn mb10">
                                        </div>
                                        <div class="row mb10">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-bordered heading" id="tbl_posts">
                                                        <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th style="width: 20%;">Title</th>
                                                            <th style="width: 20%;">Unit</th>
                                                            <th>Quantity</th>
                                                            <th>Charges</th>
                                                            <th>Discount(%)</th>
                                                            <th style="width: 17%;">Tax Rate(%)</th>
                                                            <th style="width: 10%;">Net Amount</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="tbl_posts_body" class="services_data">
                                                        <?php $get_service = $db->query("select * from pm_booking_services where booking_parent_id = $id")->fetchAll();
                                                        foreach ($get_service as $service){?>
                                                        <tr id="rec-1">
                                                                                                                        <td><input type="hidden" name="service_book_id[]" value="<?php echo $service['id'];?>"><input type="text" class="serviceid_0<?php echo $service['id'];?>" name="serviceid[]" value="<?php echo $service['service_id'];?>" size="4" disabled/></td>
                                                                                                                        <input type="hidden" class="taxserviceid_0<?php echo $service['id'];?>" name="taxservice[]" value="<?php echo $service['tax_rate'];?>"/>
                                                                                                                        <input type="hidden" name="servicediscount_amount[]" value="<?php echo $service['discount_amount'];?>" class="servicediscount_amount_0<?php echo $service['id'];?>"/>
                                                                                                                        <input type="hidden" name="servicevat_amount[]" value="<?php echo $service['vat_amount'];?>" class="servicevat_amount_0<?php echo $service['id'];?>"/>
                                                                                                                        <input type="hidden" class="serviceunitid_0<?php echo $service['id'];?>" value="<?php echo $service['unit_id'];?>" name="serviceunitid[]"/>
                                                                                                                        <td><select name="servicedropdown[]" id="servicedropdownid_0<?php echo $service['id'];?>" required class="limitedNumbSelectedSingle_0<?php echo $service['id'];?> getservicedropdown_0<?php echo $service['id'];?>"><?php echo getSelectedDropDownDataspecialpurpose('pm_service', 'servicetitle', 'Service', $db, $service['id']) ?></select>
                                                                                                                            </td>
                                                                                                                        <td><select name="serviceunit[]" id="serviceunitdropdownid_0<?php echo $service['id'];?>" required class="limitedNumbSelectedSingle_0<?php echo $service['id'];?> serviceunitdropdown_0<?php echo $service['id'];?>"><?php echo getDropDownDataUnit('Unit', $service['unit_id']) ?></select>
                                                                                                                            </td>
                                                                                                                        <td><input type="text" class="servicequantity_0<?php echo $service['id'];?>" name="servicequantity[]" value="<?php echo $service['quantity'];?>" size="4" onkeyup="updatenetpriceserviceeditrow(<?php echo $service['id'];?>)"/>
                                                                                                                            </td>
                                                                                                                        <td><input type="text" class="servicecharges_0<?php echo $service['id'];?>" name="servicecharges[]" value="<?php echo $service['charges'];?>" size="4" onkeyup="updatenetpriceserviceeditrow(<?php echo $service['id'];?>)"/>
                                                                                                                            </td>
                                                                                                                        <td><input type="text" class="servicediscount_0<?php echo $service['id'];?>" name="servicediscount[]" value="<?php echo $service['discount'];?>" size="4" onkeyup="updatenetpriceserviceeditrow(<?php echo $service['id'];?>)"/></td>
                                                                                                                        <td><select name="taxdropdown[]" id="taxdropdownid_0<?php echo $service['id'];?>" required class="limitedNumbSelectedSingle_0<?php echo $service['id'];?>  taxdropdownservice_0<?php echo $service['id'];?>" onchange="taxserviceeditchange(<?php echo $service['id'];?>)"><?php echo getSelectedDropDownDataspecialpurposetax('pm_tax', 'taxname', 'Tax', $db, $service['tax_rate']) ?></select></td>
                                                                                                                       <td><input type="text" class="servicenetamount_0<?php echo $service['id'];?>" name="servicenetamount[]" value="<?php echo $service['net_amount'];?>" size="8" disabled/>
                                                                                                                            </td>
                                                                                                                        <td><a class="btn btn-xs delete_record_0<?php echo $service['id'];?>" data-id="1"><i class="glyphicon glyphicon-trash trash-row_0<?php echo $service['id'];?> remove_row_0<?php echo $service['id'];?>" onclick="deleteservice(<?php echo $service['id'];?>);"></i></a>
                                                                                                                            </td>
                                                                                                                    </tr>
                                                        <script type="text/javascript">
                                                            $(".limitedNumbSelectedSingle_0<?php echo $service['id'];?>").select2({width : '180px', dropdownAutoWidth : true});
                                                        </script>
                                                        <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        </form>
                                </div>
                                <div id="activities" class="tab-pane fade activity_content">
                                    <form id="activity_form">
                                        <div class="row">
                                            <div class="col-md-4 mb10">
                                                <h5 class="erroractivities" style="color: red;font-size:small;"></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 mb10">
                                                <h5 class="data_success" style="color: green;font-size:small;"></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <input type="button" name="save" onclick="add_rowactivities();"
                                                   value="Add Activities"
                                                   class="btn btn-success mt15 floatbtn mb10">
                                        </div>
                                        <div class="row mb10">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-bordered heading" id="activities_tbl_posts">
                                                        <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th style="width:11%;">Title</th>
                                                            <th >Adult</th>
                                                            <th>Children</th>
                                                            <th style="width:11%;">Unit</th>
                                                            <th>Quantity</th>
                                                            <th>Charges</th>
                                                            <th>Discount(%)</th>
                                                            <th style="width:11%;">Tax Rate(%)</th>
                                                            <th>Net Amount</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="tbl_posts_body_activities"
                                                               class="activities_data">
                                    <?php $get_activity = $db->query("select * from pm_booking_activities where booking_parent_id = $id")->fetchAll();
                                    foreach ($get_activity as $activity){?>
                                        <tr id="rec-1">
                                                                                                                        <td><input type="hidden" value="<?php echo $activity['id']?>" name="activity_book_id"><input type="text" class="activitiesid_0<?php echo $activity['id'];?>" name="activitiesid[]" value="<?php echo $activity['activity_id'];?>" size="4" disabled/></td>
                                                                                                                        <input type="hidden" class="taxactivitiesid_0<?php echo $activity['id'];?>" name="taxactivitiesid[]" value="<?php echo $activity['tax_rate'];?>"/>
                                                                                                                        <input type="hidden" class="activitiesunitid_0<?php echo $activity['id'];?>" name="activitiesunitid[]" value="<?php echo $activity['unit_id'];?>" />
                                                                                                                        <input type="hidden"  name="activity_book_id[]" value="<?php echo $activity['id'];?>" />
                                                                                                                        <input type="hidden" name="activitiesdiscount_amount[]" value="<?php echo $activity['discount_amount'];?>" class="activitiesdiscount_amount_0<?php echo $activity['id'];?>"/>
                                                                                                                        <input type="hidden" name="activitiesvat_amount[]" value="<?php echo $activity['vat_amount'];?>" class="activitiesvat_amount_0<?php echo $activity['id'];?>"/>
                                                                                                                        <td><select name="activities[]" id="activitiesdropdownid_0<?php echo $activity['id'];?>" required class="limitedNumbSelectedSingleactivities getactivitiesdropdown_0<?php echo $activity['id'];?>"><?php echo getSelectedDropDownDataspecialpurpose('pm_activity', 'activityname', 'Activity', $db, $activity['id']) ?></select>
                                                                                                                        <td><input type="text" class="activitiesadult_0<?php echo $activity['id'];?>" name="activitiesadult[]" value="<?php echo $activity['adult'];?>" onkeyup="findadultact(<?php echo $activity['id'];?>)" size="4"/></td>
                                                                                                                        <td><input type="text" class="activitieschild_0<?php echo $activity['id'];?>" name="activitieschild[]" value="<?php echo $activity['child'];?>" onkeyup="findchildact(<?php echo $activity['id'];?>)" size="4"/></td>
                                                                                                                        <td><select name="activitiesunit[]" id="activitiesunitdropdownid_0<?php echo $activity['id'];?>" required class="limitedNumbSelectedSingleactivitiesunit test_0<?php echo $activity['id'];?>"><?php echo getDropDownDataUnit('Unit', $activity['unit_id']) ?></select></td>
                                                                                                                        <td><input type="text" class="activitiesquantity_0<?php echo $activity['id'];?>" name="activitiesquantity[]" value="<?php echo $activity['quantity'];?>" size="4" onkeyup="updatenetpriceactivitieseditrow(0<?php echo $activity['id'];?>)"/>
                                                                                                                        </td>
                                                                                                                        <td><input type="text" class="activitiescharges_0<?php echo $activity['id'];?>" name="activitiescharges[]" value="<?php echo $activity['charges'];?>" size="4" onkeyup="updatenetpriceactivitieseditrow(0<?php echo $activity['id'];?>)"/>
                                                                                                                        </td>
                                                                                                                        <td><input type="text" class="activitiesdiscount_0<?php echo $activity['id'];?>" name="activitiesdiscount[]" value="<?php echo $activity['discount'];?>" size="4" onkeyup="updatenetpriceactivitieseditrow(0<?php echo $activity['id'];?>)"/></td>
                                                                                                                        <td><select name="taxdropdownactivities[]" id="taxdropdownidactivities_0<?php echo $activity['id'];?>" required class="limitedNumbSelectedSingleactivitiestax taxdropdownactivities_<?php echo $activity['id'];?>" onchange="taxactivitieseditchange(<?php echo $activity['id'];?>)"><?php echo getSelectedDropDownDataspecialpurposetax('pm_tax', 'taxname', 'Tax', $db,$activity['tax_rate'])?></select>                                                            </td>
                                                                                                                        <td><input type="text" class="activitiesnetamount_0<?php echo $activity['id'];?>" name="activitiesnetamount[]" value="<?php echo $activity['net_amount'];?>" size="8" disabled/>
                                                                                                                         </td>
                                                                                                                      <td><a class="btn btn-xs delete_recordactivities_0<?php echo $activity['id'];?>" data-id="1"><i class="glyphicon glyphicon-trash trash-rowactivities_0<?php echo $activity['id'];?> remove_rowactivities_0<?php echo $activity['id'];?>" onclick="deleteactivity(<?php echo $activity['id'];?>);"></i></a>
                                                                                                                            </td>
                                                                                                                    </tr>
                                    <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div id="payment" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col-md-4 mb10">
                                            <h5 class="data_success" style="color: green;font-size:small;"></h5>
                                        </div>
                                    </div>
                                    <form id="save_payment">
                                        <div class="col-md-6 mb10">
                                            <div class="row">
                                                <div class="col-md-4 mb10">
                                                    <h5 class="errorpayment" style="color: red;font-size:small;"></h5>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5 mb10">
                                                    <label>Total Room Charges.</label>
                                                </div>
                                                <div class="col-sm-7">
                                                    <div class="input-group mb10">
                                                        <input type="text" name="total_room_charges" value=""
                                                               class="form-control total_room_charges" readonly>
                                                        <div class="input-group-addon"><i class="fa fa-dollar-sign"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5 mb10">
                                                    <label>Total Service Charges.</label>
                                                </div>
                                                <div class="col-sm-7">
                                                    <div class="input-group mb10">
                                                        <input class="form-control total_service_charges" type="text"
                                                               name="total_service_charges"
                                                               value="" placeholder="" readonly/>
                                                        <div class="input-group-addon"><i class="fa fa-dollar-sign"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5 mb10">
                                                    <label>Total Activities Charges.</label>
                                                </div>
                                                <div class="col-sm-7">
                                                    <div class="input-group mb10">
                                                        <input class="form-control total_activity_charges" type="text"
                                                               name="total_activity_charges"
                                                               value="" placeholder="" readonly/>
                                                        <div class="input-group-addon"><i class="fa fa-dollar-sign"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5 mb10">
                                                    <label>Down Payment.</label>
                                                </div>
                                                <?php  $get_payment = $db->query("select down_payment from pm_booking_payment where booking_id = $id")->fetchAll(); ?>
                                                <div class="col-sm-7">
                                                    <div class="input-group mb10">
                                                        <input class="form-control total_down_payment_edit" type="text"
                                                               name="total_down_payment" value="<?php
                                                        foreach ($get_payment as $p){
                                                            echo $p['down_payment'];
                                                        } ?>"
                                                               placeholder="" disabled/>
                                                        <div class="input-group-addon"><i class="fa fa-dollar-sign"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5 mb10">
                                                    <label>Net Payable Amount.</label>
                                                </div>
                                                <div class="col-sm-7">
                                                    <div class="input-group mb10">
                                                        <input class="form-control totalnetpayable" type="text"
                                                               name="totalnetpayable" value=""
                                                               placeholder="" readonly/>
                                                        <div class="input-group-addon"><i class="fa fa-dollar-sign"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb10">

                                            <div class="row">
                                                <div class="col-sm-5 mb10">
                                                    <label>Discount Coupon Code.</label>
                                                </div>
                                                <div class="col-sm-4 mb10">
                                                    <input class="form-control couponcode" type="text" name="couponcode"
                                                           value=""
                                                           placeholder=""/>
                                                    <i>Discount Coupon Name:</i>
                                                    <input class="form-control couponname" type="text" name="couponname"
                                                           value=""
                                                           placeholder="" disabled/>
                                                    <i>Discount Amount/(%):</i>
                                                    <input class="form-control discountamount" type="text"
                                                           name="discountamount" value=""
                                                           placeholder="" disabled/>
                                                </div>
                                                <div class="col-md-3 ">
                                                    <button type="button" class="btn btn-info">Validate</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5 mb10">
                                                    <label>Payment Mode.</label>
                                                </div>
                                                <div class="col-sm-7 mb10">
                                                    <select class="form-control paymentmode" name="paymentmode">
                                                        <option value="cash" selected>Cash</option>
                                                        <option value="bank">Bank</option>
                                                        <option value="card">Card</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <input type="hidden"
                                                       value="<?php echo get_numbering("pm_booking_parent", $db) ?>"
                                                       name="bookingid_all">
                                                <input type="button" name="save" value="Update"
                                                       class="btn btn-success mt15 floatbtn save_all">
                                            </div>
                                        </div>
                                    </form>
                                    <?php
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </form>


            <?php
        } else echo '<p>' . $texts['ACCESS_DENIED'] . '</p>'; ?>
    </div>
<?php } ?>
<?php if($id == 0){ ?>
<div id="wrapper">
    <?php
    include(SYSBASE . ADMIN_FOLDER . '/includes/inc_top.php');

    if (!in_array('no_access', $permissions)) {
        include(SYSBASE . ADMIN_FOLDER . '/includes/inc_library.php'); ?>
        <div id="page-wrapper">
            <div class="page-header">

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-6 col-md-6 col-sm-8 clearfix">
                            <h4 class="pull-left"><i class="fa fa-cog"></i> <?php echo TITLE_ELEMENT; ?></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="alert-container removealert">
                    <div class="alert alert-success alert-dismissable"></div>
                    <!--  <div class="alert alert-warning alert-dismissable"></div> -->
                    <div class="alert alert-danger alert-dismissable"></div>
                </div>
                <div class="panel panel-default">
                    <ul class="nav nav-tabs pt5" id="tabs">
                        <li class="active booking_active"><a data-toggle="tab" href="#BD"><i
                                        class="fas fa-book"></i> <?php echo "BookingDetails"; ?></a></li>
                        <li class="customer_active" onclick="customer_click();"><a data-toggle="tab"
                                                                                   href="#customerinfo"><i
                                        class="fas fa-info-circle"></i> <?php echo 'CustomerInfo'; ?></a></li>
                        <li class="services_active" onclick="service_tab();"><a data-toggle="tab" href="#services"><i
                                        class="fas fa-wrench"></i> <?php echo 'Services'; ?></a></li>
                        <li class="activity_active" onclick="activity_tab();"><a data-toggle="tab" href="#activities"><i
                                        class="fa-ticket"></i> <?php echo 'Activity'; ?>
                            </a></li>
                        <li onclick="payment_tab();"><a data-toggle="tab" href="#payment"><i
                                        class="fa-usd"></i> <?php echo 'Payment'; ?></a>
                        </li>
                    </ul>

                    <div class="panel-body">
                        <div class="tab-content">
                            <?php
                            if ($_SESSION['user']['type'] == 'administrator') { ?>
                            <div id="BD" class="tab-pane fade in active booking_content">
                                <div class="row mb10">
                                    <div class="row">
                                        <div class="col-md-4 mb10">
                                            <h5 class="date_error" style="color: red;font-size:small;"></h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 mb10">
                                            <h5 class="data_success" style="color: green;font-size:small;"></h5>
                                        </div>
                                    </div>
                                    <form id="booking_form" method="POST">
                                        <div class="row">
                                            <div class="col-md-2 ">
                                                <label class="control-label pl10">
                                                    <?php echo 'Book ID'; ?>
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <?php $booking_id = get_numbering("pm_booking_parent", $db); ?>
                                                <input class="form-control booking_id" type="text" disabled
                                                       value="<?php echo get_numbering("pm_booking_parent", $db) ?>"
                                                       name="booking_id[]">
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label pl10">
                                                    <?php echo 'Booking Date'; ?>
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <input class="form-control" type="text"
                                                       value="<?php $booking_date = date('d/m/Y');
                                                       echo $booking_date; ?>"
                                                       name="booking_date[]" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label pl10">
                                                    <?php echo 'Check In Date'; ?>
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <input class="form-control" type="date"
                                                       value="<?php if (isset($_POST['checkIn_date'])) {
                                                           $checkdate = date_create($_POST['checkIn_date']);
                                                           echo date_format($checkdate, 'Y-m-d');
                                                       } ?>"
                                                       name="CheckIn_date[]">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label pl10">
                                                    <?php echo 'Check Out Date'; ?>
                                                </label>
                                            </div>
                                            <div class="col-md-4 mb10">
                                                <input class="form-control" type="date"
                                                       value="<?php if (isset($_POST['checkOut_date'])) {
                                                           $checkout = date_create($_POST['checkOut_date']);
                                                           echo date_format($checkout, 'Y-m-d');
                                                       } ?>"
                                                       name="CheckOut_date[]">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <strong class="col-md-4 mb10">
                                                <strong<h5 class="error" style="color: red;font-size:small;"></h5>
                                            </strong>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-bordered heading" id="tbl_posts_book">
                                            <thead>
                                            <tr>
                                                <th>Room Code</th>
                                                <th>Room Title</th>
                                                <th>Room Type</th>
                                                <th>Unit</th>
                                                <th>Night Charges</th>
                                                <th>Adults</th>
                                                <th>Children</th>
                                                <th>No. of Nights</th>
                                                <th>Discount(%)</th>
                                                <th>Tax Rate(%)</th>
                                                <th>Net Amount</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody id="tbl_posts_body">

    
                                            <?php
                                            if (isset($_POST['selected_room_id'])) { ?>

                                                <?php if (isset($_POST['selected_room_id'])) {
                                                    $delete_room = "DELETE FROM pm_booking_temp";
                                                    $db->exec($delete_room);
                                                    $roomarray = array_filter($_POST['selected_room_id']);
                                                    foreach ($roomarray as $key => $room_id) {
                                                        if (!empty($room_id)) {
                                                            ////here we will add room id's to temp table in case of users want to select more and hold the selection
                                                            $insert_room = "INSERT INTO pm_booking_temp (booking_id,room_id) VALUES ($booking_id,$room_id)";
                                                            $db->exec($insert_room);
                                                            $checkindate = strtotime($_POST['checkIn_date']);
                                                            $checkoutdate = strtotime($_POST['checkOut_date']);
                                                            // $update_room_table = "update pm_room set checkIndate= $checkindate, checkoutdate=$checkoutdate, booked= 'booked' where id= $room_id";
                                                            // $db->exec($update_room_table);
                                                        }
                                                    }
                                                    //get data from temp table
                                                    $get_room_id = $db->query("SELECT room_id FROM pm_booking_temp WHERE booking_id = $booking_id ORDER BY room_id ASC")->fetchAll();
                                                    if ($get_room_id) {
                                                        foreach ($get_room_id as $room_id_temp) {
                                                            $room_temp = $room_id_temp['room_id'];
                                                            $query = $db->query("SELECT pm_room.*,pm_roomtypes.roomtype as room_type_name 
                                                                                    FROM pm_roomtypes
                                                                                    LEFT JOIN
                                                                                    pm_room ON 
                                                                                    pm_roomtypes.id = pm_room.id_roomtype
                                                                                    where pm_room.id = $room_temp and pm_room.lang = 2 and pm_roomtypes.lang = 2")->fetchAll();
                                                            foreach ($query as $roomdata) { ?>
                                                                <input type="hidden" name="room_id[]"
                                                                       value="<?php echo $roomdata['id']; ?>"/>
                                                                <input type="hidden" name="taxbooking_id[]"
                                                                       class="taxbooking_id<?php echo $roomdata['id']; ?>"
                                                                       value="<?php echo $roomdata['taxrates']; ?>"/>
                                                                <input type="hidden" name="unit_id[]"
                                                                       class="unit_id<?php echo $roomdata['id']; ?>"/>
                                                                <input type="hidden" name="room_book_id[]"/>
                                                                <input type="hidden" name="discount_amount[]"
                                                                       class="discount_amount<?php echo $roomdata['id']; ?>"/>
                                                                <input type="hidden" name="vat_amount[]"
                                                                       class="vat_amount<?php echo $roomdata['id']; ?>"/>

                                                                <tr id="rec_<?php echo $roomdata['id']; ?>">
                                                                    <td>
                                                                        <span class="sn"><?php echo $roomdata['roomcode']; ?></span>
                                                                    </td>
                                                                    <td>
                                                                        <span class="sn"><?php echo $roomdata['roomtitle']; ?></span>
                                                                    </td>
                                                                    <td>
                                                                        <span class="sn"><?php echo $roomdata['room_type_name']; ?></span>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo getDropDownDataUnitroom('unit_id', 'unit') ?>
                                                                    </td>
                                                                    <td><input type="text"
                                                                               class="nightcharge<?php echo $roomdata['id']; ?>"
                                                                               disabled
                                                                               name="nightcharge[]"
                                                                               value="<?php echo $roomdata['price']; ?>"
                                                                               size="4"/></td>
                                                                    <td><input type="text" name="adults[]"
                                                                               class="maxadults<?php echo $roomdata['id']; ?>"
                                                                               onkeyup="findadult(<?php echo $roomdata['id']; ?>)"
                                                                               value="<?php if (isset($_POST['adult_count'])) { echo $_POST['adult_count']; } ?>"
                                                                               size="4"/>
                                                                    </td>
                                                                    <td><input type="text" name="children[]"
                                                                               class="maxchild<?php echo $roomdata['id']; ?>"
                                                                               onkeyup="findchild(<?php echo $roomdata['id']; ?>)"
                                                                               value="<?php if (isset($_POST['child_count'])) { echo $_POST['child_count']; } else { echo "0";} ?>"
                                                                               size="4"/></td>
                                                                    <td><input type="text" name="night[]"
                                                                               class="staynight<?php echo $roomdata['id']; ?>"
                                                                               onkeyup="updatenetprice(<?php echo $roomdata['id']; ?>)"
                                                                               value="<?php if (isset($_POST['night_stay'])) { echo $_POST['night_stay']; } else { echo "0";} ?>"
                                                                               size="4"/>
                                                                    </td>
                                                                    <td><input type="text"
                                                                               class="discount<?php echo $roomdata['id']; ?>"
                                                                               name="discount[]"
                                                                               onkeyup="updatenetpricewithdiscount(<?php echo $roomdata['id']; ?>)"
                                                                               value="0"
                                                                               size="4"/></td>
                                                                    <td>
                                                                        <?php $tax_id = $roomdata['taxrates'];
                                                                        $unique_id = $roomdata['id'];
                                                                        echo getSelectedDropDownDatatax('pm_tax', 'taxrate', 'tax_id' . $roomdata['id'], 'taxname', 'Tax', 'required', $tax_id, $db) ?>
                                                                        <script>
                                                                            $("#tax_id" +<?php echo $unique_id?>).change(function (event) {
                                                                                $(".error").html();
                                                                                if (event.target == this) {
                                                                                    var selectedarrayval = $(this).val();
                                                                                    $(".taxbooking_id" +<?php echo $unique_id?>).val(selectedarrayval);
                                                                                }
                                                                                $(".netamount" +<?php echo $unique_id?>).val('');
                                                                                var tax_id = $("#tax_id" +<?php echo $unique_id?>).val();
                                                                                var nightcharge = $(".nightcharge" +<?php echo $unique_id?>).val();
                                                                                var staynight = $(".staynight" +<?php echo $unique_id?>).val();
                                                                                var discount = $(".discount" +<?php echo $unique_id?>).val();
                                                                                if (staynight == '') {
                                                                                    $.ajax({
                                                                                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                                                                                        type: 'POST',
                                                                                        data: {
                                                                                            'remove_id': <?php echo $unique_id?>,
                                                                                            'comparechild': 1
                                                                                        },
                                                                                        dataType: 'JSON',
                                                                                        success: function (data) {
                                                                                            $(".error").html("");
                                                                                            $(".error").append("<span>No of Nights Stay cannot be empty in " + data[0].roomtitle + " </span>");
                                                                                            return false;
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    if (isNaN(staynight)) {
                                                                                        $.ajax({
                                                                                            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                                                                                            type: 'POST',
                                                                                            data: {
                                                                                                'remove_id': <?php echo $unique_id?>,
                                                                                                'comparechild': 1
                                                                                            },
                                                                                            dataType: 'JSON',
                                                                                            success: function (data) {
                                                                                                $(".error").html("");
                                                                                                $(".error").append("<span>Please Use Numeric value Instead " + staynight + " in " + data[0].roomtitle + " </span>");
                                                                                                return false;
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                    else {
                                                                                        $(".error").html("");
                                                                                        $.ajax({
                                                                                            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                                                                                            type: 'POST',
                                                                                            data: {
                                                                                                'tax_id': tax_id,
                                                                                                'gettaxrate': 1
                                                                                            },
                                                                                            dataType: 'JSON',
                                                                                            success: function (data) {
                                                                                                $(".error").html("");
                                                                                                //totalnetamount without vat
                                                                                                if (discount == '') {
                                                                                                    discount = 0;
                                                                                                    var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                                                                                                    var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                                                                                                    var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                                                                                                    var taxamount = parseFloat(data[0].taxrates);
                                                                                                    $(".discount_amount"+<?php echo $unique_id?>).val((getdiscountamount).toFixed(2));
                                                                                                    var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                                                                                                    $(".vat_amount"+<?php echo $unique_id?>).val((getvatamount).toFixed(2));
                                                                                                    var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                                                                                                    $(".netamount" +<?php echo $unique_id?>).val(totalnet.toFixed(2));
                                                                                                } else {
                                                                                                    var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                                                                                                    var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                                                                                                    var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                                                                                                    var taxamount = parseFloat(data[0].taxrates);
                                                                                                    $(".discount_amount"+<?php echo $unique_id?>).val((getdiscountamount).toFixed(2));
                                                                                                    var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                                                                                                    $(".vat_amount"+<?php echo $unique_id?>).val((getvatamount).toFixed(2));
                                                                                                    var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                                                                                                    $(".netamount" +<?php echo $unique_id?>).val(totalnet.toFixed(2));
                                                                                                }
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                }

                                                                            });
                                                                            $("#unit_id" +<?php echo $unique_id?>).change(function (event) {
                                                                                $(".error").html();
                                                                                if (event.target == this) {
                                                                                    var selectedarrayval = $(this).val();
                                                                                    $(".unit_id" +<?php echo $unique_id?>).val(selectedarrayval);
                                                                                }
                                                                            });
                                                                        </script>
                                                                    </td>
                                                                    <td><input type="text"
                                                                               name="netamount[]"
                                                                               class="netamount<?php echo $roomdata['id']; ?> totalbook"
                                                                               disabled
                                                                               value="<?php echo($roomdata['taxrates'] + $roomdata['price']); ?>"
                                                                               size="4"/></td>
                                                                    <td>
                                                                        <a class="btn btn-xs delete-record"
                                                                           data-id="<?php echo $room_id; ?>"><i
                                                                                    class="glyphicon glyphicon-trash"></i></a>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        <?php }
                                                    } else {
                                                        $query = $db->query("SELECT pm_room.*,pm_roomtypes.roomtype as room_type_name 
                                                                                    FROM pm_roomtypes
                                                                                    LEFT JOIN
                                                                                    pm_room ON 
                                                                                    pm_roomtypes.id = pm_room.id_roomtype
                                                                                    where pm_room.id = $room_id and pm_room.lang = 2 and pm_roomtypes.lang = 2")->fetchAll();
                                                        ?>
                                                        <?php foreach ($query as $roomdata) { ?>
                                                            <input type="hidden" name="room_id[]"
                                                                   value="<?php echo $roomdata['id']; ?>"/>
                                                            <input type="hidden" name="taxbooking_id[]"
                                                                   class="taxbooking_id<?php echo $roomdata['id']; ?>"
                                                                   value="<?php echo $roomdata['taxrates']; ?>"/>
                                                            <input type="hidden" name="room_book_id[]"/>
                                                            <input type="hidden" name="unit_id[]"
                                                                   class="unit_id<?php echo $roomdata['id']; ?>"/>
                                                            <input type="hidden" name="discount_amount[]"
                                                                   class="discount_amount<?php echo $roomdata['id']; ?>"/>
                                                            <input type="hidden" name="vat_amount[]"
                                                                   class="vat_amount<?php echo $roomdata['id']; ?>"/>

                                                            <tr id="rec_<?php echo $roomdata['id']; ?>">
                                                                <td>
                                                                    <span class="sn"><?php echo $roomdata['roomcode']; ?></span>
                                                                </td>
                                                                <td>
                                                                    <span class="sn"><?php echo $roomdata['roomtitle']; ?></span>
                                                                </td>
                                                                <td>
                                                                    <span class="sn"><?php echo $roomdata['room_type_name']; ?></span>
                                                                </td>
                                                                <td>
                                                                    <?php echo getDropDownDataUnitroom('unit_id', 'unit') ?>
                                                                </td>
                                                                <td><input type="text" disabled
                                                                           class="nightcharge<?php echo $roomdata['id']; ?>"
                                                                           name="nightcharge[]"
                                                                           value="<?php echo $roomdata['price']; ?>"
                                                                           size="4"/></td>
                                                                <td><input type="text" name="adults[]"
                                                                           class="maxadults<?php echo $roomdata['id']; ?>"
                                                                           onkeyup="findadult(<?php echo $roomdata['id']; ?>)"
                                                                           value="<?php if (isset($_POST['adult_count'])) { echo $_POST['adult_count']; } ?>"
                                                                           size="4"/>
                                                                </td>
                                                                <td><input type="text" name="children[]"
                                                                           class="maxchild<?php echo $roomdata['id']; ?>"
                                                                           onkeyup="findchild(<?php echo $roomdata['id']; ?>)"
                                                                           value="<?php if (isset($_POST['child_count'])) { echo $_POST['child_count']; } else { echo "0";} ?>"
                                                                           size="4"/></td>
                                                                <td><input type="text" name="night[]"
                                                                           class="staynight<?php echo $roomdata['id']; ?>"
                                                                           onkeyup="updatenetprice(<?php echo $roomdata['id']; ?>)"
                                                                           value="<?php if (isset($_POST['night_stay'])) { echo $_POST['night_stay']; } else { echo "0";} ?>"
                                                                           size="4"/>
                                                                </td>
                                                                <td><input type="text"
                                                                           class="discount<?php echo $roomdata['id']; ?>"
                                                                           name="discount[]"
                                                                           onkeyup="updatenetpricewithdiscount(<?php echo $roomdata['id']; ?>)"
                                                                           value="0"
                                                                           size="4"/></td>
                                                                <td>
                                                                    <?php $tax_id = $roomdata['taxrates'];
                                                                    $unique_id = $roomdata['id'];
                                                                    echo getSelectedDropDownData('pm_tax', 'taxrate', 'tax_id' . $roomdata['id'], 'taxname', 'Tax', 'required', $tax_id, $db) ?>
                                                                    <script>
                                                                        $("#tax_id" +<?php echo $unique_id?>).change(function (event) {
                                                                            $(".error").html();
                                                                            if (event.target == this) {
                                                                                var selectedarrayval = $(this).val();
                                                                                $(".taxbooking_id" +<?php echo $unique_id?>).val(selectedarrayval);
                                                                            }
                                                                            $(".netamount" +<?php echo $unique_id?>).val('');
                                                                            var tax_id = $("#tax_id" +<?php echo $unique_id?>).val();
                                                                            var nightcharge = $(".nightcharge" +<?php echo $unique_id?>).val();
                                                                            var staynight = $(".staynight" +<?php echo $unique_id?>).val();
                                                                            var discount = $(".discount" +<?php echo $unique_id?>).val();
                                                                            if (staynight == '') {
                                                                                $.ajax({
                                                                                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                                                                                    type: 'POST',
                                                                                    data: {
                                                                                        'remove_id': <?php echo $unique_id?>,
                                                                                        'comparechild': 1
                                                                                    },
                                                                                    dataType: 'JSON',
                                                                                    success: function (data) {
                                                                                        $(".error").html("");
                                                                                        $(".error").append("<span>No of Nights Stay cannot be empty in " + data[0].roomtitle + " </span>");
                                                                                        return false;
                                                                                    }
                                                                                });
                                                                            } else {
                                                                                if (isNaN(staynight)) {
                                                                                    $.ajax({
                                                                                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                                                                                        type: 'POST',
                                                                                        data: {
                                                                                            'remove_id': <?php echo $unique_id?>,
                                                                                            'comparechild': 1
                                                                                        },
                                                                                        dataType: 'JSON',
                                                                                        success: function (data) {
                                                                                            $(".error").html("");
                                                                                            $(".error").append("<span>Please Use Numeric value Instead " + staynight + " in " + data[0].roomtitle + " </span>");
                                                                                            return false;
                                                                                        }
                                                                                    });
                                                                                }
                                                                                else {
                                                                                    $(".error").html("");
                                                                                    $.ajax({
                                                                                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                                                                                        type: 'POST',
                                                                                        data: {
                                                                                            'tax_id': tax_id,
                                                                                            'gettaxrate': 1
                                                                                        },
                                                                                        dataType: 'JSON',
                                                                                        success: function (data) {
                                                                                            $(".error").html("");
                                                                                            //totalnetamount without vat
                                                                                            if (discount == '') {
                                                                                                discount = 0;
                                                                                                var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                                                                                                var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                                                                                                var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                                                                                                var taxamount = parseFloat(data[0].taxrates);
                                                                                                $(".discount_amount"+<?php echo $unique_id?>).val((getdiscountamount).toFixed(2));
                                                                                                var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                                                                                                $(".vat_amount"+<?php echo $unique_id?>).val(( getvatamount).toFixed(2));
                                                                                                var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                                                                                                $(".netamount" +<?php echo $unique_id?>).val(totalnet.toFixed(2));
                                                                                            } else {
                                                                                                var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                                                                                                var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                                                                                                var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                                                                                                var taxamount = parseFloat(data[0].taxrates);
                                                                                                $(".discount_amount"+<?php echo $unique_id?>).val((getdiscountamount).toFixed(2));
                                                                                                var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                                                                                                $(".vat_amount"+<?php echo $unique_id?>).val((getvatamount).toFixed(2));
                                                                                                var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                                                                                                $(".netamount" +<?php echo $unique_id?>).val(totalnet.toFixed(2));
                                                                                            }
                                                                                        });
                                                                                }
                                                                            }

                                                                        });
                                                                        $("#unit_id" +<?php echo $unique_id?>).change(function (event) {
                                                                            $(".error").html();
                                                                            if (event.target == this) {
                                                                                var selectedarrayval = $(this).val();
                                                                                $(".unit_id" +<?php echo $unique_id?>).val(selectedarrayval);
                                                                            }
                                                                        });
                                                                    </script>
                                                                </td>
                                                                <td><input type="text" name="netamount[]"
                                                                           class="netamount<?php echo $roomdata['id']; ?> totalbook"
                                                                           disabled
                                                                           value="<?php echo($roomdata['taxrates'] + $roomdata['price']); ?>"
                                                                           size="4"/></td>
                                                                <td><a class="btn btn-xs delete-record"
                                                                       data-id="<?php echo $room_id; ?>"><i
                                                                                class="glyphicon glyphicon-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                        <?php }
                                                    }


                                                }
                                            } else {
                                                //get data from temp table
                                                $get_room_id = $db->query("SELECT room_id FROM pm_booking_temp WHERE booking_id = $booking_id GROUP BY room_id ORDER BY room_id ASC")->fetchAll();
                                                if ($get_room_id) {
                                                    foreach ($get_room_id as $room_id_temp) {
                                                        $room_temp = $room_id_temp['room_id'];
                                                        $query = $db->query("SELECT pm_room.*,pm_roomtypes.roomtype as room_type_name 
                                                                                    FROM pm_roomtypes
                                                                                    LEFT JOIN
                                                                                    pm_room ON 
                                                                                    pm_roomtypes.id = pm_room.id_roomtype
                                                                                    where pm_room.id = $room_temp and pm_room.lang = 2 and pm_roomtypes.lang = 2")->fetchAll();
                                                        ?>

                                                        <?php foreach ($query as $roomdata) { ?>
                                                            <input type="hidden" name="room_id[]"
                                                                   value="<?php echo $roomdata['id']; ?>"/>
                                                            <input type="hidden" name="taxbooking_id[]"
                                                                   class="taxbooking_id<?php echo $roomdata['id']; ?>"
                                                                   value="<?php echo $roomdata['taxrates']; ?>"/>
                                                            <input type="hidden" name="unit_id[]"
                                                                   class="unit_id<?php echo $roomdata['id']; ?>"
                                                                   value="<?php echo $roomdata['taxrates']; ?>"/>
                                                            <input type="hidden" name="room_book_id[]"/>
                                                            <input type="hidden" name="discount_amount[]"
                                                                   class="discount_amount<?php echo $roomdata['id']; ?>"/>
                                                            <input type="hidden" name="vat_amount[]"
                                                                   class="vat_amount<?php echo $roomdata['id']; ?>"/>

                                                            <tr id="rec_<?php echo $roomdata['id']; ?>">
                                                                <td>
                                                                    <span class="sn"><?php echo $roomdata['roomcode']; ?></span>
                                                                </td>
                                                                <td>
                                                                    <span class="sn"><?php echo $roomdata['roomtitle']; ?></span>
                                                                </td>
                                                                <td>
                                                                    <span class="sn"><?php echo $roomdata['room_type_name']; ?></span>
                                                                </td>
                                                                <td>
                                                                    <?php echo getDropDownDataUnitroom('unit_id', 'unit') ?>
                                                                </td>
                                                                <td><input type="text" disabled name="nightcharge[]"
                                                                           value="<?php echo $roomdata['price']; ?>"
                                                                           class="nightcharge<?php echo $roomdata['id']; ?>"
                                                                           size="4"/></td>
                                                                <td><input type="text" name="adults[]"
                                                                           class="maxadults<?php echo $roomdata['id']; ?>"
                                                                           onkeyup="findadult(<?php echo $roomdata['id']; ?>)"
                                                                           value="<?php if (isset($_POST['adult_count'])) { echo $_POST['adult_count']; } ?>"
                                                                           size="4"/>
                                                                </td>
                                                                <td><input type="text" name="children[]"
                                                                           class="maxchild<?php echo $roomdata['id']; ?>"
                                                                           onkeyup="findchild(<?php echo $roomdata['id']; ?>)"
                                                                           value="<?php if (isset($_POST['child_count'])) { echo $_POST['child_count']; } else { echo "0";} ?>"
                                                                           size="4"/></td>
                                                                <td><input type="text" name="night[]"
                                                                           class="staynight<?php echo $roomdata['id']; ?>"
                                                                           onkeyup="updatenetprice(<?php echo $roomdata['id']; ?>)"
                                                                           value="<?php if (isset($_POST['night_stay'])) { echo $_POST['night_stay']; } else { echo "0";} ?>"
                                                                           size="4"/>
                                                                </td>
                                                                <td><input type="text"
                                                                           class="discount<?php echo $roomdata['id']; ?>"
                                                                           name="discount[]"
                                                                           onkeyup="updatenetpricewithdiscount(<?php echo $roomdata['id']; ?>)"
                                                                           value="0"
                                                                           size="4"/></td>
                                                                <td>
                                                                    <?php $tax_id = $roomdata['taxrates'];
                                                                    $unique_id = $roomdata['id'];
                                                                    echo getSelectedDropDownDatatax('pm_tax', 'taxrate', 'tax_id' . $roomdata['id'], 'taxname', 'Tax', 'required', $tax_id, $db) ?>
                                                                    <script>
                                                                        $("#tax_id" +<?php echo $unique_id?>).change(function (event) {
                                                                            $(".error").html();
                                                                            if (event.target == this) {
                                                                                var selectedarrayval = $(this).val();
                                                                                $(".taxbooking_id" +<?php echo $unique_id?>).val(selectedarrayval);
                                                                            }
                                                                            $(".netamount" +<?php echo $unique_id?>).val('');
                                                                            var tax_id = $("#tax_id" +<?php echo $unique_id?>).val();
                                                                            var nightcharge = $(".nightcharge" +<?php echo $unique_id?>).val();
                                                                            var staynight = $(".staynight" +<?php echo $unique_id?>).val();
                                                                            var discount = $(".discount" +<?php echo $unique_id?>).val();
                                                                            if (staynight == '') {
                                                                                $.ajax({
                                                                                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                                                                                    type: 'POST',
                                                                                    data: {
                                                                                        'remove_id': <?php echo $unique_id?>,
                                                                                        'comparechild': 1
                                                                                    },
                                                                                    dataType: 'JSON',
                                                                                    success: function (data) {
                                                                                        $(".error").html("");
                                                                                        $(".error").append("<span>No of Nights Stay cannot be empty in " + data[0].roomtitle + " </span>");
                                                                                        return false;
                                                                                    }
                                                                                });
                                                                            } else {
                                                                                if (isNaN(staynight)) {
                                                                                    $.ajax({
                                                                                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                                                                                        type: 'POST',
                                                                                        data: {
                                                                                            'remove_id': <?php echo $unique_id?>,
                                                                                            'comparechild': 1
                                                                                        },
                                                                                        dataType: 'JSON',
                                                                                        success: function (data) {
                                                                                            $(".error").html("");
                                                                                            $(".error").append("<span>Please Use Numeric value Instead " + staynight + " in " + data[0].roomtitle + " </span>");
                                                                                            return false;
                                                                                        }
                                                                                    });
                                                                                }
                                                                                else {
                                                                                    $(".error").html("");
                                                                                    $.ajax({
                                                                                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                                                                                        type: 'POST',
                                                                                        data: {
                                                                                            'tax_id': tax_id,
                                                                                            'gettaxrate': 1
                                                                                        },
                                                                                        dataType: 'JSON',
                                                                                        success: function (data) {
                                                                                            $(".error").html("");
                                                                                            //totalnetamount without vat
                                                                                            if (discount == '') {
                                                                                                discount = 0;
                                                                                                var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                                                                                                var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                                                                                                var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                                                                                                var taxamount = parseFloat(data[0].taxrates);
                                                                                                $(".discount_amount"+<?php echo $unique_id?>).val((getdiscountamount).toFixed(2));
                                                                                                var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                                                                                                $(".vat_amount"+<?php echo $unique_id?>).val((getvatamount).toFixed(2));
                                                                                                var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                                                                                                $(".netamount" +<?php echo $unique_id?>).val(totalnet.toFixed(2));
                                                                                            } else {
                                                                                                var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                                                                                                var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                                                                                                var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                                                                                                var taxamount = parseFloat(data[0].taxrates);
                                                                                                $(".discount_amount"+<?php echo $unique_id?>).val((getdiscountamount).toFixed(2));
                                                                                                var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                                                                                                $(".vat_amount"+<?php echo $unique_id?>).val((getvatamount).toFixed(2));
                                                                                                var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                                                                                                $(".netamount" +<?php echo $unique_id?>).val(totalnet.toFixed(2));
                                                                                            }
                                                                                        }
                                                                                    });
                                                                                }
                                                                            }
                                                                        });
                                                                        $("#unit_id" +<?php echo $unique_id?>).change(function (event) {
                                                                            $(".error").html();
                                                                            if (event.target == this) {
                                                                                var selectedarrayval = $(this).val();
                                                                                $(".unit_id" +<?php echo $unique_id?>).val(selectedarrayval);
                                                                            }
                                                                        });
                                                                    </script>
                                                                </td>
                                                                <td><input type="text" name="netamount[]"
                                                                           class="netamount<?php echo $roomdata['id'] ?> totalbook"
                                                                           disabled value=""
                                                                           size="4"/></td>
                                                                <td><a class="btn btn-xs delete-record"
                                                                       data-id="<?php echo $room_id; ?>"><i
                                                                                class="glyphicon glyphicon-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                        <?php }
                                                    }
                                                }

                                            }

                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <input type="hidden" class="nettotal">
                                </form>
                            </div>
                            <style>
                                .icon {
                                    padding: 10px;
                                    background: dodgerblue;
                                    color: white;
                                    min-width: 45px;
                                    text-align: center;
                                    font-size: 14px;
                                }

                                .input-group-addon {
                                    margin: 0;
                                    padding: 0;
                                }
                            </style>
                            <div id="customerinfo" class="tab-pane fade customer_content">
                                <div class="row">
                                    <div class="col-md-4 mb10">
                                        <h5 class="data_success" style="color: green;font-size:small;"></h5>
                                    </div>
                                </div>
                                <form id="customer_form">
                                    <div class="row">
                                        <div class="col-md-4 mb10">
                                            <h5 class="errorcustomer" style="color: red;font-size:small;"></h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2" id="project-label">
                                            <label class="control-label  pl10">
                                                <?php echo 'Customer-Name'; ?>
                                            </label>
                                        </div>
                                        <div class="col-md-4" id="project-label">
                                            <div class="field-adjust">
                                                <div class="input-group">
                                                    <div class="field-adjust">
                                                        <input id="customername" class="form-control customername"
                                                               type="text"
                                                               value="" name="customer" onkeyup="getuser();">
                                                        <input type="hidden" id="customername-id"/></div>
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-mobile icon"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label  pl10">
                                                First Name
                                            </label>
                                        </div>
                                        <div class="col-md-4 mb10">
                                            <input class="form-control firstname" type="text" value=""
                                                   name="firstname">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label class="control-label  pl10">
                                                Country
                                            </label>
                                        </div>
                                        <div class="col-md-4 mb10">
                                            <input class="form-control country" type="text" value="" name="country">
                                        </div>
                                        <div class="col-md-2">
                                            <label class="control-label  pl10">
                                                Last Name
                                            </label>
                                        </div>
                                        <div class="col-md-4 mb10">
                                            <input class="form-control lastname" type="text" value=""
                                                   name="lastname">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label class="control-label  pl10">
                                                Company Name
                                            </label>
                                        </div>
                                        <div class="col-md-4 mb10">
                                            <input class="form-control companyname" type="text" value=""
                                                   name="companyname">
                                        </div>
                                        <div class="col-md-2">
                                            <label class="control-label  pl10">
                                                Email
                                            </label>
                                        </div>
                                        <div class="col-md-4 mb10">
                                            <input class="form-control email" type="text" value="" name="email">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label class="control-label  pl10">
                                                Address
                                            </label>
                                        </div>
                                        <div class="col-md-4 mb10">
                                            <input class="form-control address" type="text" value="" name="address">
                                        </div>
                                        <div class="col-md-2">
                                            <label class="control-label  pl10">
                                                Phone-No
                                            </label>
                                        </div>
                                        <div class="col-md-4 mb10">
                                            <input class="form-control phone" type="text" value="" name="phone">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label class="control-label  pl10">
                                                PostCode
                                            </label>
                                        </div>
                                        <div class="col-md-4 mb10">
                                            <input class="form-control postcode" type="text" value=""
                                                   name="postcode">
                                        </div>
                                        <div class="col-md-2">
                                            <label class="control-label  pl10">
                                                Mobile-No
                                            </label>
                                        </div>
                                        <div class="col-md-4 mb10">
                                            <input class="form-control mobile" type="text" value="" name="mobile">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label class="control-label  pl10">
                                                City
                                            </label>
                                        </div>
                                        <div class="col-md-4 mb10">
                                            <input class="form-control city" type="text" value="" name="city">
                                        </div>
                                        <div class="col-md-2">
                                            <label class="control-label  pl10">
                                                CNIC/EID
                                            </label>
                                        </div>
                                        <div class="col-md-4 mb10">
                                            <input class="form-control cnic" type="text" value="" name="cnic">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label class="control-label  pl10">
                                                Comments
                                            </label>
                                        </div>
                                        <div class="col-md-4 mb10">
                                            <input class="form-control comments" type="text" value=""
                                                   name="comments">
                                        </div>
                                        <div class="col-md-2">
                                            <label class="control-label  pl10">
                                                Passport
                                            </label>
                                        </div>
                                        <div class="col-md-4 mb10">
                                            <input class="form-control passport" type="text" value=""
                                                   name="passport">
                                        </div>
                                    </div>
                            </div>
                            <div id="services" class="tab-pane fade services service_content">
                                <!--                                        function service_tab   -->
                                <form id="service_form">
                                    <div class="row">
                                        <div class="col-md-4 mb10">
                                            <h5 class="data_success" style="color: green;font-size:small;"></h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 mb10">
                                            <h5 class="errorservices" style="color: red;font-size:small;"></h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <input type="button" name="save" onclick="add_row();" value="Add Services"
                                               class="btn btn-success mt15 floatbtn mb10">
                                    </div>
                                    <div class="row mb10">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-bordered heading" id="tbl_posts">
                                                    <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th style="width: 20%;">Title</th>
                                                        <th style="width: 20%;">Unit</th>
                                                        <th>Quantity</th>
                                                        <th>Charges</th>
                                                        <th>Discount(%)</th>
                                                        <th style="width: 17%;">Tax Rate(%)</th>
                                                        <th style="width: 10%;">Net Amount</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="tbl_posts_body" class="services_data"></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="activities" class="tab-pane fade activity_content">
                                <form id="activity_form">
                                    <div class="row">
                                        <div class="col-md-4 mb10">
                                            <h5 class="erroractivities" style="color: red;font-size:small;"></h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 mb10">
                                            <h5 class="data_success" style="color: green;font-size:small;"></h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <input type="button" name="save" onclick="add_rowactivities();"
                                               value="Add Activities"
                                               class="btn btn-success mt15 floatbtn mb10">
                                    </div>
                                    <div class="row mb10">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-bordered heading" id="activities_tbl_posts">
                                                    <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Title</th>
                                                        <th>Adult</th>
                                                        <th>Children</th>
                                                        <th>Unit</th>
                                                        <th>Quantity</th>
                                                        <th>Charges</th>
                                                        <th>Discount(%)</th>
                                                        <th>Tax Rate(%)</th>
                                                        <th>Net Amount</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="tbl_posts_body_activities"
                                                           class="activities_data"></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="payment" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-4 mb10">
                                        <h5 class="data_success" style="color: green;font-size:small;"></h5>
                                    </div>
                                </div>
                                <form id="save_payment">
                                    <input type="hidden" name="payment_id" class="payment_id">
                                    <div class="col-md-6 mb10">
                                        <div class="row">
                                            <div class="col-md-4 mb10">
                                                <h5 class="errorpayment" style="color: red;font-size:small;"></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5 mb10">
                                                <label>Total Room Charges.</label>
                                            </div>
                                            <div class="col-sm-7">
                                                <div class="input-group mb10">
                                                    <input type="text" name="total_room_charges" value=""
                                                           class="form-control total_room_charges" readonly>
                                                    <div class="input-group-addon"><i class="fa fa-dollar-sign"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5 mb10">
                                                <label>Total Service Charges.</label>
                                            </div>
                                            <div class="col-sm-7">
                                                <div class="input-group mb10">
                                                    <input class="form-control total_service_charges" type="text"
                                                           name="total_service_charges"
                                                           value="" placeholder="" readonly/>
                                                    <div class="input-group-addon"><i class="fa fa-dollar-sign"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5 mb10">
                                                <label>Total Activities Charges.</label>
                                            </div>
                                            <div class="col-sm-7">
                                                <div class="input-group mb10">
                                                    <input class="form-control total_activity_charges" type="text"
                                                           name="total_activity_charges"
                                                           value="" placeholder="" readonly/>
                                                    <div class="input-group-addon"><i class="fa fa-dollar-sign"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--                                        <div class="row">-->
                                        <!--                                            <div class="col-sm-5 mb10">-->
                                        <!--                                                <label>Total Add On Charges.</label>-->
                                        <!--                                            </div>-->
                                        <!--                                            <div class="col-sm-7">-->
                                        <!--                                                <div class="input-group mb10">-->
                                        <!--                                                    <input class="form-control" type="text" name="addon-charges"-->
                                        <!--                                                           value="" placeholder=""/>-->
                                        <!--                                                    <div class="input-group-addon"><i class="fa fa-dollar-sign"></i>-->
                                        <!--                                                    </div>-->
                                        <!--                                                </div>-->
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                        <!--                                        <div class="row">-->
                                        <!--                                            <div class="col-sm-5 mb10">-->
                                        <!--                                                <label>Discount Amount.</label>-->
                                        <!--                                            </div>-->
                                        <!--                                            <div class="col-sm-7 mb10">-->
                                        <!--                                                <div class="input-group">-->
                                        <!--                                                    <input class="form-control total_discount_amount" type="text" name="discount-amount"-->
                                        <!--                                                           value="" placeholder="" disabled/>-->
                                        <!--                                                    <div class="input-group-addon"><i class="fa fa-dollar-sign"></i>-->
                                        <!--                                                    </div>-->
                                        <!--                                                </div>-->
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                        <div class="row">
                                            <div class="col-sm-5 mb10">
                                                <label>Down Payment.</label>
                                            </div>
                                            <div class="col-sm-7">
                                                <div class="input-group mb10">
                                                    <input class="form-control total_down_payment" type="text"
                                                           name="total_down_payment" value=""
                                                           placeholder=""/>
                                                    <div class="input-group-addon"><i class="fa fa-dollar-sign"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5 mb10">
                                                <label>Net Payable Amount.</label>
                                            </div>
                                            <div class="col-sm-7">
                                                <div class="input-group mb10">
                                                    <input class="form-control totalnetpayable" type="text"
                                                           name="totalnetpayable" value=""
                                                           placeholder="" readonly/>
                                                    <div class="input-group-addon"><i class="fa fa-dollar-sign"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb10">

                                        <div class="row">
                                            <div class="col-sm-5 mb10">
                                                <label>Discount Coupon Code.</label>
                                            </div>
                                            <div class="col-sm-4 mb10">
                                                <input class="form-control couponcode" type="text" name="couponcode"
                                                       value=""
                                                       placeholder=""/>
                                                <i>Discount Coupon Name:</i>
                                                <input class="form-control couponname" type="text" name="couponname"
                                                       value=""
                                                       placeholder="" disabled/>
                                                <i>Discount Amount/(%):</i>
                                                <input class="form-control discountamount" type="text"
                                                       name="discountamount" value=""
                                                       placeholder="" disabled/>
                                            </div>
                                            <div class="col-md-3 ">
                                                <button type="button" class="btn btn-info">Validate</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5 mb10">
                                                <label>Payment Mode.</label>
                                            </div>
                                            <div class="col-sm-7 mb10">
                                                <select class="form-control paymentmode" name="paymentmode">
                                                    <option value="cash" selected>Cash</option>
                                                    <option value="bank">Bank</option>
                                                    <option value="card">Card</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <input type="hidden"
                                                   value="<?php echo get_numbering("pm_booking_parent", $db) ?>"
                                                   name="bookingid_all">
                                            <input type="button" name="save" value="Save"
                                                   class="btn btn-success mt15 floatbtn save_all">
<!--                                            <a href="--><?php //echo $base . ADMIN_FOLDER ?><!--/modules/customcalendar/printbill.php?print=form&id=--><?php //echo get_numbering("pm_booking_parent", $db) ?><!--">-->
<!--                                                <input-->
<!--                                                        type="button" name="saveprint" value="Save and Print"-->
<!--                                                        class="btn btn-success mt15 floatbtn save_all_print"></a>-->
                                        </div>
                                    </div>
                                </form>
                                <?php
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
        <?php
    } else echo '<p>' . $texts['ACCESS_DENIED'] . '</p>'; ?>
</div>
<?php } ?>
</body>
</div>
</body>
</html>
<script type="text/javascript">
    $(document).ready(function() {
        $(".limitedNumbChosenSinglebooking").select2({
            width: '100%',
            dropdownAutoWidth: true
        });
        $(".limitedNumbSelectedSingleactivities").select2({width : '100px', dropdownAutoWidth : true});
        $(".limitedNumbSelectedSingleactivitiestax").select2({width : '100px', dropdownAutoWidth : true});
        $(".limitedNumbSelectedSingleactivitiesunit").select2({width : '100px', dropdownAutoWidth : true});

    });
    $(".delete-record").click(function () {
        var booking_id = $(".booking_id").val();
        var remove_id = $(this).attr("data-id");
        $(this).closest('tr').remove();
        //this request will remove the room id from temp booking table
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'booking_id': booking_id,
                'delete_room': 1
            },
            dataType: 'JSON',
            success: function (data) {

            }

        });
    });
    function deleteservice(id){
        var service_id = $(".delete_record_0"+id).val();
        $(this).closest('tr').remove();
        //this request will remove the room id from temp booking table
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'service_id': service_id,
                'delete_service': 1
            },
            dataType: 'JSON',
            success: function (data) {

            }

        });

    }
    function deleteactivity(id){
        var activity_id = $(".remove_activities_0"+id).val();
        $(this).closest('tr').remove();
        //this request will remove the room id from temp booking table
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'activity_id': activity_id,
                'delete_activity': 1
            },
            dataType: 'JSON',
            success: function (data) {

            }

        });

    }
    function findadult(id) {
        $(".error").html("");
        var maxadults = $(".maxadults" +id).val();
        if (maxadults != '') {
            if (isNaN(maxadults)) {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'remove_id': id,
                        'comparechild': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        if (parseFloat(maxadults) > parseFloat(data[0].maxadults)) {
                            $(".error").html("");
                            $(".error").append("<span><strong>Adults cannot be alphabets in " + data[0].roomtitle + "</strong></span>");
                        }
                    }
                });
            }
            else {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'remove_id': id,
                        'compareadults': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        if (parseFloat(maxadults) > parseFloat(data[0].maxadults)) {
                            $(".error").html("");
                            $(".error").append("<span><strong>Adults cannot be more than " + data[0].maxadults + " in " + data[0].roomtitle + "</strong></span>");
                        }
                    }
                });

            }
        }
    }

    function findchild(id) {
        $(".error").html("");
        var maxchild = $(".maxchild" + id).val();
        if (maxchild != '') {
            if (isNaN(maxchild)) {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'remove_id': id,
                        'comparechild': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        if (parseFloat(maxchild) > parseFloat(data[0].maxchild)) {
                            $(".error").html("");
                            $(".error").append("<span><strong>Childs cannot be alphabets in " + data[0].roomtitle + " </strong></span>");
                        }

                    }

                });
            } else {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'remove_id': id,
                        'comparechild': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        if (parseFloat(maxchild) > parseFloat(data[0].maxchild)) {
                            $(".error").html("");
                            $(".error").append("<span><strong>Childs cannot be more than " + data[0].maxchild + " in " + data[0].roomtitle + "</strong></span>");
                        }

                    }

                });
            }
        }

    }

    function updatenetprice(id) {
        $(".booking_save").prop('disabled', true);
        $(".netamount" + id).val('');
        var nightcharge = $(".nightcharge" + id).val();
        var staynight = $(".staynight" + id).val();
        var tax_id = $("#tax_id" + id).val();
        if (isNaN(staynight)) {
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'remove_id': id,
                    'comparechild': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".error").html("");
                    $(".error").append("<span>Please Use Numeric value Instead " + staynight + " in " + data[0].roomtitle + " </span>");
                    return false;
                }
            });
        } else {
            $(".error").html("");
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'tax_id': tax_id,
                    'gettaxrate': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".error").html("");
                    //totalnetamount without vat
                    $(".booking_save").prop('disabled', false);
                    var discount = 0;
                    var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                    var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                    var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                    var taxamount = parseFloat(data[0].taxrates);
                    var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                    $(".discount_amount"+id).val((getdiscountamount).toFixed(2));
                    $(".vat_amount"+id).val((getvatamount).toFixed(2));
                    var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                    $(".netamount" + id).val(totalnet.toFixed(2));
                }
            });
        }
    }

    function updatenetpricewithdiscount(id,event) {
        $(".booking_save").prop('disabled', true);
        $(".netamount" + id).val('');
        var nightcharge = $(".nightcharge" + id).val();
        var staynight = $(".staynight" + id).val();
        var discount = $(".discount" + id).val();
        var tax_id = $("#tax_id" + id).val();
        if (staynight != '') {
            $(".error").html("");
            if (discount == '') {
                discount = 0;
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'tax_id': tax_id,
                        'gettaxrate': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        $(".booking_save").prop('disabled', false);
                        var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                        var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                        var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                        var taxamount = parseFloat(data[0].taxrates);
                        var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                        $(".discount_amount"+id).val((getdiscountamount).toFixed(2));
                        $(".vat_amount"+id).val((getvatamount).toFixed(2));
                        var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                        $(".netamount" + id).val(totalnet.toFixed(2));
                    }
                });
            } else {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'tax_id': tax_id,
                        'gettaxrate': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        $(".booking_save").prop('disabled', false);
                        var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                        var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                        var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                        var taxamount = parseFloat(data[0].taxrates);
                        var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                        var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                        $(".discount_amount"+id).val((getdiscountamount).toFixed(2));
                        $(".vat_amount"+id).val((getvatamount).toFixed(2));
                        $(".netamount" + id).val(totalnet.toFixed(2));
                    }
                });
            }
        } else {
            $(".error").html("");
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'remove_id': id,
                    'comparechild': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".error").html("");
                    $(".error").append("<span>Number of nights cannot be empty in " + data[0].roomtitle + " </span>");
                    return false;
                }
            });
        }
    }

    function getuser() {
        var customer = $("#customername").val();
        var customername = [];
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'customer': customer,
                'get_user': 1
            },
            dataType: 'JSON',
            success: function (data) {
                var customername = data;
                $(".firstname").val(data[0].label);
                $(".lastname").val(data[0].lastname);
                $(".email").val(data[0].email);
                $(".country").val(data[0].country);
                $(".company").val(data[0].company);
                $(".city").val(data[0].city);
                $(".mobile").val(data[0].mobile);
                $(".phone").val(data[0].phone);
                $(".address").val(data[0].address);
                $("#customername").autocomplete({
                    minLength: 0,
                    source: customername,
                    focus: function (event, ui) {
                        $("#customername").val(ui.item.label);
                        return false;
                    },
                    select: function (event, ui) {
                        $("#customername").val(ui.item.label);
                        $("#customername-id").val(ui.item.value);
                        return false;
                    }
                })
                    .data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li>")
                        .append("<a>" + item.label + "</a>")
                        .appendTo(ul);
                };
            }

        });
    }

    function service_tab() {
        $(".data_success").html("");
        $(".delete_record_0").show();
        $(".errorservices").html("");
    }

    function updatenetpriceservice(id) {
        $(".errorservices").html("");
        $(".servicenetamount_00").val('');
        $(".service_save").prop('disabled', true);
        var servicecharges = $(".servicecharges_00").val();
        var servicequantity = $(".servicequantity_00").val();
        var servicetaxid = $(".taxdropdown_00").val();
        var servicediscount = $(".servicediscount_00").val();
        var selected_service_id = $('.serviceid_00').val();
        if (servicecharges <= 0 || servicecharges == '') {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use value Greater in charges than " + servicecharges + " For ID " + selected_service_id + "</strong></span>");
            return false;
        }
        if (isNaN(servicecharges)) {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use Numeric value in charges Instead " + servicecharges + " For ID " + selected_service_id + " </strong></span>");
            return false;
        }
        if (servicequantity <= 0 || servicequantity == '') {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use value Greater in quantity than " + servicequantity + " For ID " + selected_service_id + "</strong></span>");
            return false;
        }
        if (isNaN(servicequantity)) {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use Numeric value in quantity Instead " + servicequantity + " For ID " + selected_service_id + " </strong></span>");
            return false;
        }
        if (servicecharges != '' && servicequantity != '' && !isNaN(servicequantity) && !isNaN(servicecharges)) {
            $(".errorservices").html("");
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'tax_id': servicetaxid,
                    'gettaxrate': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".errorservices").html("");
                    //totalnetamount without vat
                    if (servicediscount != '') {
                        if (servicediscount <= 0) {
                            $(".errorservices").html("");
                            $(".errorservices").append("<strong><span>Please Use value Greater in discount than " + servicediscount + " For ID " + selected_service_id + " </strong></span>");
                            return false;
                        } else if (isNaN(servicediscount)) {
                            $(".errorservices").html("");
                            $(".errorservices").append("<strong><span>Please Use Numeric value in discount Instead " + servicediscount + " For ID " + selected_service_id + " </strong></span>");
                            return false;
                        } else {
                            $(".service_save").prop('disabled', false);
                            var totalnetamountvalservices = (parseFloat(servicecharges)) * (parseFloat(servicequantity));
                            var getdiscountamount = ((totalnetamountvalservices) * (servicediscount / 100 ));
                            var taxamount = parseFloat(data[0].taxrates);
                            var getvatamountservices = ((getdiscountamount) * (taxamount / 100 ));
                            $(".servicenetamount_00").val(getvatamountservices.toFixed(2));
                        }
                    } else {
                        $(".service_save").prop('disabled', false);
                        var totalnetamountvalservices = (parseFloat(servicecharges)) * (parseFloat(servicequantity));
                        var taxamount = parseFloat(data[0].taxrates);
                        var getvatamountservices = ((totalnetamountvalservices) * (taxamount / 100 ));
                        var totalnetservices = (totalnetamountvalservices + getvatamountservices);
                        $(".servicenetamount_00").val(totalnetservices.toFixed(2));
                    }
                }
            });
        }
    }

    var i = 0;
    function add_room(){
        $("#tbl_posts_body_edit").append('<tr id="rec_'+i+'"><td>' +
            '<input type="hidden" class="roomid'+i+'" name="room_id[]"><input type="hidden" class="taxbooking_id'+i+'" name="taxbooking_id[]"><input type="hidden" class="discount_amount'+i+'" name="discount_amount[]"><input type="hidden" class="vat_amount'+i+'" name="vat_amount[]">' +
            '<input type="hidden" name="room_book_id[]"><span class="roomcode'+i+'" name="roomcode[]"></span></td>' +
'                            <td><select name="room[]" id="roomdropdownid_'+i+'" required class="roomdropdown_'+i+' room_'+i+'"><?php echo getDropDownDataspecialpurpose('pm_room', 'roomtitle','Room',$db) ?></select></td>' +
'                            <td><span class="roomtitle'+i+'" name="roomtitle[]"></span></td>' +
'                            <td><select name="roomunit[]" id="roomunitdropdownid_'+i+'" required class="roomunitdropdown_'+i+' roomunit_'+i+'"><?php echo getDropDownDataUnit('Unit', '') ?></select></td>' +
'                            <td><input type="text" class="nightcharge'+i+'" disabled name="nightcharge[]" value=""size="4"/></td>' +
'                            <td><input type="text" name="adults[]" class="maxadults'+i+'"" value="" size="4"/></td>' +
'                            <td><input type="text" name="children[]" class="maxchild'+i+'" value="" size="4"/></td>' +
'                            <td><input type="text" name="night[]" class="staynight'+i+'" value="" size="4"/></td>' +
'                            <td><input type="text" class="discount'+i+'" name="discount[]" value="" size="4"/></td>'+
'                            <td><select name="taxdropdownroom[]" id="taxdropdownroom_' + i + '" required class="taxdropdownroom_'+i+'"><?php echo getSelectedDropDownDataspecialpurpose('pm_tax', 'taxname', 'Tax', $db, '') ?></select></td>'+
'                            <td><input type="text" class="roomnetamount_'+i+'" name="netamount[]" value="" size="6" disabled/></td>' +
'                            <td><a class="btn btn-xs delete_record_' + i + '" data-id="1"><i class="glyphicon glyphicon-trash trash-row_'+i+' remove_row_'+i+'" "></i></a></td></tr>');
        $(".taxdropdownroom_" + i).select2({
           width: '100%',
            dropdownAutoWidth : true
        });
        $(".roomunitdropdown_" + i).select2({
            width: '100%',
            dropdownAutoWidth : true
        });
        $(".roomdropdown_" + i).select2({
            width: '100%',
            dropdownAutoWidth : true
        }).change(function(evt){
            var roomid = $(evt.target).val();
            $("room_id"+i).val(roomid);
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'roomid': roomid,
                    'getroomdata': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".roomid"+i).val(data[0].room_id);
                    $(".roomcode" + i).text(data[0].roomcode);
                    $(".roomtitle" + i).text(data[0].roomtitle);
                    $(".nightcharge" + i).val(data[0].price);
                    $(".maxadults" + i).val(data[0].maxadults);
                    $(".maxchild" + i).val(data[0].maxchild);
                    $(".taxdropdownroom_"+i).select2().val(data[0].taxrates);
                    $(".taxdropdownroom_" + i).select2({
                        width: '100%',
                        dropdownAutoWidth : true
                    });
                }
            });
    });

        $(".delete_record_" + i).click(function () {
            $(this).closest('tr').remove();
        });
        $(".maxadults"+i).on('keyup',function(){
            $(".error").html("");
            var id = $(this).closest('tr').find(".roomid"+i).val();
            var maxadults = $(".maxadults" +i).val();
            if (maxadults != '') {
                if (isNaN(maxadults)) {
                    $.ajax({
                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                        type: 'POST',
                        data: {
                            'remove_id': id,
                            'comparechild': 1
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            if (parseFloat(maxadults) > parseFloat(data[0].maxadults)) {
                                $(".error").html("");
                                $(".error").append("<span><strong>Adults cannot be alphabets in " + data[0].roomtitle + "</strong></span>");
                            }
                        }
                    });
                }
                else {
                    $.ajax({
                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                        type: 'POST',
                        data: {
                            'remove_id': id,
                            'compareadults': 1
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            if (parseFloat(maxadults) > parseFloat(data[0].maxadults)) {
                                $(".error").html("");
                                $(".error").append("<span><strong>Adults cannot be more than " + data[0].maxadults + " in " + data[0].roomtitle + "</strong></span>");
                            }
                        }
                    });

                }
            }

        });
        $(".maxchild"+i).on('keyup',function(){
            $(".error").html("");
            var maxchild = $(".maxchild" + i).val();
            var id = $(this).closest('tr').find(".roomid"+i).val();
            if (maxchild != '') {
                if (isNaN(maxchild)) {
                    $.ajax({
                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                        type: 'POST',
                        data: {
                            'remove_id': id,
                            'comparechild': 1
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            if (parseFloat(maxchild) > parseFloat(data[0].maxchild)) {
                                $(".error").html("");
                                $(".error").append("<span><strong>Childs cannot be alphabets in " + data[0].roomtitle + " </strong></span>");
                            }

                        }

                    });
                } else {
                    $.ajax({
                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                        type: 'POST',
                        data: {
                            'remove_id': id,
                            'comparechild': 1
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            if (parseFloat(maxchild) > parseFloat(data[0].maxchild)) {
                                $(".error").html("");
                                $(".error").append("<span><strong>Childs cannot be more than " + data[0].maxchild + " in " + data[0].roomtitle + "</strong></span>");
                            }

                        }

                    });
                }
            }

        });
        $(".staynight"+i).on('keyup',function(){

            $(".booking_save").prop('disabled', true);
            $(".roomnetamount" + i).val('');
            var id = $(this).closest('tr').find(".roomid"+i).val();
            var nightcharge = $(".nightcharge" + i).val();
            var staynight = $(".staynight" + i).val();
            var tax_id = $(".taxdropdownroom_"+i).val();
            $(".taxbooking_id"+i).val(tax_id);
            if (isNaN(staynight)) {
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'remove_id': id,
                        'comparechild': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        $(".error").html("");
                        $(".error").append("<span>Please Use Numeric value Instead " + staynight + " in " + data[0].roomtitle + " </span>");
                        return false;
                    }
                });
            } else {
                $(".error").html("");
                $.ajax({
                    url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'tax_id': tax_id,
                        'gettaxrate': 1
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        $(".error").html("");
                        //totalnetamount without vat
                        $(".booking_save").prop('disabled', false);
                        var discount = 0;
                        var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                        var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                        var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                        var taxamount = parseFloat(data[0].taxrates);
                        var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                        var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                        $(".discount_amount"+i).val(getdiscountamount.toFixed(2));
                        $(".vat_amount"+i).val(getvatamount.toFixed(2));
                        $(".roomnetamount_" + i).val(totalnet.toFixed(2));
                    }
                });
            }
        });
        $(".discount"+i).on('keyup',function(){
                $(".booking_save").prop('disabled', true);
                $(".roomnetamount_" + i).val('');
                var id = $(this).closest('tr').find(".roomid"+i).val();
                var nightcharge = $(".nightcharge" + i).val();
                var staynight = $(".staynight" + i).val();
                var discount = $(".discount" + i).val();
                var tax_id = $(".taxdropdownroom_"+i).val();
               $(".taxbooking_id"+i).val(tax_id);
                if (staynight != '') {
                    $(".error").html("");
                    if (discount == '') {
                        discount = 0;
                        $.ajax({
                            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                            type: 'POST',
                            data: {
                                'tax_id': tax_id,
                                'gettaxrate': 1
                            },
                            dataType: 'JSON',
                            success: function (data) {
                                $(".booking_save").prop('disabled', false);
                                var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                                var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                                var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                                var taxamount = parseFloat(data[0].taxrates);
                                var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                                var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                                $(".discount_amount"+i).val(getdiscountamount.toFixed(2));
                                $(".vat_amount"+i).val(getvatamount.toFixed(2));
                                $(".roomnetamount_" + i).val(totalnet.toFixed(2));
                            }
                        });
                    } else {
                        $.ajax({
                            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                            type: 'POST',
                            data: {
                                'tax_id': tax_id,
                                'gettaxrate': 1
                            },
                            dataType: 'JSON',
                            success: function (data) {
                                $(".booking_save").prop('disabled', false);
                                var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                                var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                                var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                                var taxamount = parseFloat(data[0].taxrates);
                                var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                                var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                                $(".discount_amount"+i).val(getdiscountamount.toFixed(2));
                                $(".vat_amount"+i).val(getvatamount.toFixed(2));
                                $(".roomnetamount_" + i).val(totalnet.toFixed(2));
                            }
                        });
                    }
                } else {
                    $(".error").html("");
                    $.ajax({
                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                        type: 'POST',
                        data: {
                            'remove_id': id,
                            'comparechild': 1
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            $(".error").html("");
                            $(".error").append("<span>Number of nights cannot be empty in " + data[0].roomtitle + " </span>");
                            return false;
                        }
                    });
                }


        });
        $(".taxdropdownroom_" + i).change(function (event) {
            $(".error").html("");
            if (event.target == this) {
                var selectedarrayval = $(this).val();
                $(".taxdropdownroom_" +i).val(selectedarrayval);
                $(".taxbooking_id"+i).val(selectedarrayval);
                $(".roomnetamount_"+i).val('');
                var tax_id = selectedarrayval;
                var nightcharge = $(".nightcharge" +i).val();
                var staynight = $(".staynight" +i).val();
                var discount = $(".discount" +i).val();
                var id = $(this).closest('tr').find(".roomid"+i).val();
                if (staynight == '') {
                    $.ajax({
                        url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                        type: 'POST',
                        data: {
                            'remove_id': id,
                            'comparechild': 1
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            $(".error").html("");
                            $(".error").append("<span>No of Nights Stay cannot be empty in " + data[0].roomtitle + " </span>");
                            return false;
                        }
                    });
                } else {
                    if (isNaN(staynight)) {
                        $.ajax({
                            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                            type: 'POST',
                            data: {
                                'remove_id': id,
                                'comparechild': 1
                            },
                            dataType: 'JSON',
                            success: function (data) {
                                $(".error").html("");
                                $(".error").append("<span>Please Use Numeric value Instead " + staynight + " in " + data[0].roomtitle + " </span>");
                                return false;
                            }
                        });
                    }
                    else {
                        $(".error").html("");
                        $.ajax({
                            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                            type: 'POST',
                            data: {
                                'tax_id': tax_id,
                                'gettaxrate': 1
                            },
                            dataType: 'JSON',
                            success: function (data) {
                                $(".error").html("");
                                //totalnetamount without vat
                                if (discount == '') {
                                    discount = 0;
                                    var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                                    var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                                    var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                                    var taxamount = parseFloat(data[0].taxrates);
                                    var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                                    var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                                    $(".discount_amount"+i).val(getdiscountamount.toFixed(2));
                                    $(".vat_amount"+i).val(getvatamount.toFixed(2));
                                    $(".roomnetamount_" +i).val(totalnet.toFixed(2));
                                } else {
                                    var totalnetamountval = (parseFloat(nightcharge)) * (parseFloat(staynight));
                                    var getdiscountamount = ((totalnetamountval) * (discount / 100 ));
                                    var amountafterdiscoount = (totalnetamountval - getdiscountamount);
                                    var taxamount = parseFloat(data[0].taxrates);
                                    var getvatamount = ((amountafterdiscoount) * (taxamount / 100 ));
                                    var totalnet = ((totalnetamountval - getdiscountamount) + getvatamount);
                                    $(".discount_amount"+i).val(getdiscountamount.toFixed(2));
                                    $(".vat_amount"+i).val(getvatamount.toFixed(2));
                                    $(".roomnetamount_" +i).val(totalnet.toFixed(2));
                                }
                            }
                        });
                    }
                }
            }
        });
        $(".roomunitdropdown" +i).change(function (event) {
            $(".error").html();
            if (event.target == this) {
                var selectedarrayval = $(this).val();
                $(".roomunitdropdown" +i).val(selectedarrayval);
            }
        });



    }

    function add_row() {
        $(".delete_record_00").hide();
        $(".errorservices").html("");
        //  $(".delete_plus_"+i).hide();
        i++;
        $('.services_data').append('<tr id="rec-1">' +
            '                                                             <td><input type="hidden" name="service_book_id[]"><input type="text" class="serviceid_' + i + '" name="serviceid[]" value="" size="4" disabled/></td>' +
            '                                                            <input type="hidden" class="taxserviceid_' + i + '" name="taxservice[]"/>' +
            '                                                            <input type="hidden" class="serviceunitid_' + i + '" name="serviceunitid[]"/>' +
            '                                                            <input type="hidden" name="servicediscount_amount[]" class="servicediscount_amount_'+i+'"/>'+
            '                                                            <input type="hidden" name="service_book_id[]"/>'+
            '                                                            <input type="hidden" name="servicevat_amount[]"class="servicevat_amount_'+i+'"/>'+
            '                                                            <td><select name="servicedropdown[]" id="servicedropdownid_' + i + '" required class="limitedNumbSelectedSingle_' + i + ' getservicedropdown_' + i + '" "><?php echo getDropDownDataspecialpurpose('pm_service', 'servicetitle', 'Service', $db) ?>' + '</select>' +
            '                                                            </td>' +
            '                                                            <td><select name="serviceunit[]" id="serviceunitdropdownid_' + i + '" required class="limitedNumbSelectedSingle_' + i + ' serviceunitdropdown_' + i + '"><?php echo getDropDownDataUnit('Unit', '') ?>' + '</select>' +
            '                                                            </td>' +
            '                                                            <td><input type="text" class="servicequantity_' + i + '" name="servicequantity[]" value="" size="4" onkeyup="updatenetpriceserviceaddrow(' + i + ')"/>' +
            '                                                            </td>' +
            '                                                            <td><input type="text" class="servicecharges_' + i + '" name="servicecharges[]" value="" size="4" onkeyup="updatenetpriceserviceaddrow(' + i + ')"/>' +
            '                                                            </td>' +
            '                                                            <td><input type="text" class="servicediscount_' + i + '" name="servicediscount[]" value="" size="4" onkeyup="updatenetpriceserviceaddrow(' + i + ')"/></td>' +
            '                                                            <td><select name="taxdropdown[]" id="taxdropdownid_' + i + '" required class="limitedNumbSelectedSingle_' + i + ' taxdropdown_' + i + '"><?php echo getDropDownDataspecialpurposetax('pm_tax', 'taxname', 'Tax', $db) ?>' + '</select>' + '                                                            </td>' +
            '                                                            <td><input type="text" class="servicenetamount_' + i + '" name="servicenetamount[]" value="" size="8" disabled/>' +
            '                                                            </td>' +
            '                                                            <td><a class="btn btn-xs delete_record_' + i + '" data-id="1"><i class="glyphicon glyphicon-trash trash-row_' + i + ' remove_row_' + i + '" "></i></a>' +
            '                                                            </td>' +
            '                                                        </tr>');

        $(".limitedNumbSelectedSingle_" + i).select2({
            dropdownAutoWidth : true
    });
        $(".getservicedropdown_" + i).select2({
            dropdownAutoWidth : true
    }).bind('change', function (evt) {
            $(".errorservices").html("");
            var serviceid = $(evt.target).val();
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'serviceid': serviceid,
                    'getservicedata': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".serviceid_" + i).val(data[0].id);
                    $(".serviceunitdropdown_" + i).select2();
                    $(".serviceunitdropdown_" + i).val(data[0].servicechargesunit);
                    $(".servicecharges_" + i).val(data[0].servicecharges);
                    $(".taxdropdown_" + i).select2().val(data[0].taxes);
                    $(".taxserviceid_" + i).val(data[0].taxes);
                    $(".serviceunitid_" + i).val(data[0].servicechargesunit);
                    updatenetpriceserviceaddrow(i);
                }
            });
        });
        $(".taxdropdown_" + i).change(function (event) {
            $(".errorservices").html("");
            if (event.target == this) {
                var selectedarrayval = $(this).val();
                $(".taxserviceid_" + i).val(selectedarrayval);
            }
            updatenetpriceserviceaddrow(i);
        }).change();

        $(".serviceunitdropdown_" + i).change(function (event) {
            $(".errorservices").html("");
            if (event.target == this) {
                var selectedarrayval = $(this).val();
                $(".serviceunitid_" + i).val(selectedarrayval);
            }
            updatenetpriceserviceaddrow(i);
        }).change();
        $(".delete_record_" + i).click(function () {
            $(this).closest('tr').remove();
        });


    }

    function updatenetpriceserviceaddrow(id) {
        $(".service_save").prop('disabled', true);
        $(".errorservices").html("");
        $(".servicenetamount_" + id).val('');
        var servicecharges = $(".servicecharges_" + id).val();
        var servicequantity = $(".servicequantity_" + id).val();
        var servicetaxid = $(".taxdropdown_" + id).val();
        var servicediscount = $(".servicediscount_" + id).val();
        var selected_service_id = $(".serviceid_" + id).val();
        if (servicecharges <= 0 || servicecharges == '') {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use value Greater in charges than " + servicecharges + " For ID " + selected_service_id + "</strong></span>");
            return false;
        }
        if (isNaN(servicecharges)) {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use Numeric value in charges Instead " + servicecharges + " For ID " + selected_service_id + " </strong></span>");
            return false;
        }
        if (servicequantity <= 0 || servicequantity == '') {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use value Greater in quantity than " + servicequantity + " For ID " + selected_service_id + "</span>");
            return false;
        }
        if (isNaN(servicequantity)) {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use Numeric value in quantity Instead " + servicequantity + " For ID " + selected_service_id + " </strong></span>");
            return false;
        }
        if (servicecharges != '' && servicequantity != '' && !isNaN(servicequantity) && !isNaN(servicecharges)) {
            $(".errorservices").html("");
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'tax_id': servicetaxid,
                    'gettaxrate': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".errorservices").html("");
                    //totalnetamount without vat
                    if (servicediscount != '') {
                        if (servicediscount <= 0) {
                            $(".errorservices").html("");
                            $(".errorservices").append("<span>Please Use value Greater in discount than " + servicediscount + " For ID <strong>" + selected_service_id + " </strong></span>");
                            return false;
                        } else if (isNaN(servicediscount)) {
                            $(".errorservices").html("");
                            $(".errorservices").append("<span>Please Use Numeric value in discount Instead " + servicediscount + " For ID <strong>" + selected_service_id + " </strong></span>");
                            return false;
                        } else {
                            $(".service_save").prop('disabled', false);
                            var totalnetamountvalservices = (parseFloat(servicecharges)) * (parseFloat(servicequantity));
                            var getdiscountamount = ((totalnetamountvalservices) * (servicediscount / 100 ));
                            var amountafterdiscoount = (totalnetamountvalservices - getdiscountamount);
                            var taxamount = parseFloat(data[0].taxrates);
                            var getvatamountservices = ((amountafterdiscoount) * (taxamount / 100 ));
                            $(".servicediscount_amount_" + id).val((getdiscountamount).toFixed(2));
                            $(".servicevat_amount_" + id).val((getvatamountservices).toFixed(2));
                            var totalnetservices = (amountafterdiscoount  + getvatamountservices);
                            $(".servicenetamount_" + id).val(totalnetservices.toFixed(2));
                        }
                    } else {
                        $(".service_save").prop('disabled', false);
                        var totalnetamountvalservices = (parseFloat(servicecharges)) * (parseFloat(servicequantity));
                        var taxamount = parseFloat(data[0].taxrates);
                        var getvatamountservices = ((totalnetamountvalservices) * (taxamount / 100 ));
                        $(".servicediscount_amount_" + id).val(0);
                        $(".servicevat_amount_" + id).val((getvatamountservices ).toFixed(2));
                        var totalnetservices = (totalnetamountvalservices  + getvatamountservices);
                        $(".servicenetamount_" + id).val(totalnetservices.toFixed(2));
                    }
                }
            });
        }
    }
    function taxserviceeditchange(id) {
        $(".errorservices").html("");
        updatenetpriceserviceeditrow(id);
    }
    function updatenetpriceserviceeditrow(id) {
        $(".service_save").prop('disabled', true);
        $(".errorservices").html("");
        $(".servicenetamount_" + id).val('');
        var servicecharges = $(".servicecharges_0" + id).val();
        var servicequantity = $(".servicequantity_0" + id).val();
        var servicetaxid = $(".taxdropdownservice_0" + id).val();
        var servicediscount = $(".servicediscount_0" + id).val();
        var selected_service_id = $(".serviceid_0" + id).val();
        if (servicecharges <= 0 || servicecharges == '') {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use value Greater in charges than " + servicecharges + " For ID " + selected_service_id + "</strong></span>");
            return false;
        }
        if (isNaN(servicecharges)) {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use Numeric value in charges Instead " + servicecharges + " For ID " + selected_service_id + " </strong></span>");
            return false;
        }
        if (servicequantity <= 0 || servicequantity == '') {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use value Greater in quantity than " + servicequantity + " For ID " + selected_service_id + "</span>");
            return false;
        }
        if (isNaN(servicequantity)) {
            $(".errorservices").html("");
            $(".errorservices").append("<strong><span>Please Use Numeric value in quantity Instead " + servicequantity + " For ID " + selected_service_id + " </strong></span>");
            return false;
        }
        if (servicecharges != '' && servicequantity != '' && !isNaN(servicequantity) && !isNaN(servicecharges)) {
            $(".errorservices").html("");
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'tax_id': servicetaxid,
                    'gettaxrate': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".errorservices").html("");
                    //totalnetamount without vat
                    if (servicediscount != '') {
                        if (servicediscount <= 0) {
                            $(".errorservices").html("");
                            $(".errorservices").append("<span>Please Use value Greater in discount than " + servicediscount + " For ID <strong>" + selected_service_id + " </strong></span>");
                            return false;
                        } else if (isNaN(servicediscount)) {
                            $(".errorservices").html("");
                            $(".errorservices").append("<span>Please Use Numeric value in discount Instead " + servicediscount + " For ID <strong>" + selected_service_id + " </strong></span>");
                            return false;
                        } else {
                            $(".service_save").prop('disabled', false);
                            var totalnetamountvalservices = (parseFloat(servicecharges)) * (parseFloat(servicequantity));
                            var getdiscountamount = ((totalnetamountvalservices) * (servicediscount / 100 ));
                            var amountafterdiscoount = (totalnetamountvalservices - getdiscountamount);
                            var taxamount = parseFloat(data[0].taxrates);
                            var getvatamountservices = ((amountafterdiscoount) * (taxamount / 100 ));
                            $(".servicediscount_amount_0" + id).val((getdiscountamount).toFixed(2));
                            $(".servicevat_amount_0" + id).val((getvatamountservices).toFixed(2));
                            var totalnetservices = (amountafterdiscoount  + getvatamountservices);
                            $(".servicenetamount_0" + id).val(totalnetservices.toFixed(2));
                        }
                    } else {
                        $(".service_save").prop('disabled', false);
                        var totalnetamountvalservices = (parseFloat(servicecharges)) * (parseFloat(servicequantity));
                        var taxamount = parseFloat(data[0].taxrates);
                        var getvatamountservices = ((totalnetamountvalservices) * (taxamount / 100 ));
                        $(".servicediscount_amount_0" + id).val(0);
                        $(".servicevat_amount_0" + id).val((getvatamountservices ).toFixed(2));
                        var totalnetservices = (totalnetamountvalservices  + getvatamountservices);
                        $(".servicenetamount_0" + id).val(totalnetservices.toFixed(2));
                    }
                }
            });
        }
    }

    function activity_tab() {
        $(".data_success").html("");
        $(".delete_record_0").show();
        $(".erroractivities").html("");

    }

    var j = 0;

    function add_rowactivities() {
        $(".delete_recordactivities_00").hide();
        $(".erroractivities").html("");
        //  $(".delete_plus_"+i).hide();
        $(".limitedNumbSelectedSingleactivities_" + j).select2({width: '100%',
            dropdownAutoWidth : true
        });
        j++;
        $('.activities_data').append('<tr id="rec-1">' +
            '                                                            <td><input type="hidden" name="activity_book_id[]"><input type="text" class="activitiesid_' + j + '" name="activitiesid[]" value="" size="4" disabled/></td>' +
            '                                                            <input type="hidden" class="taxactivitiesid_' + j + '" name="taxactivitiesid[]" />' +
            '                                                            <input type="hidden" class="activitiesunitid_' + j + '" name="activitiesunitid[]" />' +
            '                                                            <input type="hidden" name="activitiesdiscount_amount[]"class="activitiesdiscount_amount_'+j+'"/>'+
            '                                                            <input type="hidden" name="activity_book_id[]"/>'+
            '                                                            <input type="hidden" name="activitiesvat_amount[]" class="activitiesvat_amount_'+j+'"/>'+
            '                                                            <td><select name="activities[]" id="activitiesdropdownid_' + j + '" required class="limitedNumbSelectedSingleactivities_' + j + ' getactivitiesdropdown_' + j + '" "><?php echo getDropDownDataspecialpurpose('pm_activity', 'activityname', 'Activity', $db) ?>' + '</select>' +
            '                                                            <td><input type="text" class="activitiesadult_' + j + '" name="activitiesadult[]" value="" size="4"/></td>' +
            '                                                            <td><input type="text" class="activitieschild_' + j + '" name="activitieschild[]" value="" size="4"/>' +
            '                                                            </td>' +
            '                                                            <td><select name="activitiesunit[]" id="activitiesunitdropdownid_' + j + '" required class="activitiesunitdropdown_' + j + ' test_'+j+'"><?php echo getDropDownDataUnit('Unit', '') ?>' + '</select>' +
            '                                                            </td>' +
            '                                                            <td><input type="text" class="activitiesquantity_' + j + '" name="activitiesquantity[]" value="" size="4" onkeyup="updatenetpriceactivitiesaddrow(' + j + ')"/>' +
            '                                                            </td>' +
            '                                                            <td><input type="text" class="activitiescharges_' + j + '" name="activitiescharges[]" value="" size="4" onkeyup="updatenetpriceactivitiesaddrow(' + j + ')"/>' +
            '                                                            </td>' +
            '                                                            <td><input type="text" class="activitiesdiscount_' + j + '" name="activitiesdiscount[]" value="" size="4" onkeyup="updatenetpriceactivitiesaddrow(' + j + ')"/></td>' +
            '                                                            <td><select name="taxdropdownactivities[]" id="taxdropdownidactivities_' + j + '" required class="limitedNumbSelectedSingleactivities_' + j + ' taxdropdownactivities_' + j + '"><?php echo getDropDownDataspecialpurposetax('pm_tax', 'taxname', 'Tax', $db) ?>' + '</select>' + '                                                            </td>' +
            '                                                            <td><input type="text" class="activitiesnetamount_' + j + '" name="activitiesnetamount[]" value="" size="8" disabled/>' +
            '                                                            </td>' +
            '                                                            <td><a class="btn btn-xs delete_recordactivities_' + j + '" data-id="1"><i class="glyphicon glyphicon-trash trash-rowactivities_' + j + ' remove_rowactivities_' + j + '" "></i></a>' +
            '                                                            </td>' +
            '                                                        </tr>');

        $(".limitedNumbSelectedSingleactivities_" + j).select2({width: '100%'});
        $('.test_'+j).select2({width: '100%', dropdownAutoWidth : true});
        $(".getactivitiesdropdown_" + j).select2().bind('change', function (evt) {
            $(".erroractivities").html("");
            var activityid = $(evt.target).val();
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'activityid': activityid,
                    'getactivitydata': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".activitiesid_" + j).val(data[0].id);
                    $(".activitiesadult_" + j).val(data[0].maxadults);
                    $(".activitieschild_" + j).val(data[0].maxchild);
                    $(".activitiesunitdropdown_" + j).select2({width: '85%'});
                    $(".activitiesunitdropdown_" + j).val(data[0].activityunit);
                    $(".activitiescharges_" + j).val(data[0].price);
                    $(".taxdropdownactivities_" + j).select2({width: '85%'}).val(data[0].taxrates);
                    $(".taxactivitiesid_" + j).val(data[0].taxrates);
                    $(".activitiesunitid_" + j).val(data[0].activityunit);
                    updatenetpriceactivitiesaddrow(j);

                }
            });
        }).select2({width: '100%', dropdownAutoWidth : true});
        $(".taxdropdownactivities_" + j).change(function (evt) {
            $(".erroractivities").html("");
            if (evt.target == this) {
                var selectedarrayval = $(this).val();
                $(".taxactivitiesid_" + j).val(selectedarrayval);
            }
            updatenetpriceactivitiesaddrow(j);

        }).change();
        $(".activitiesunitdropdown_" + j).change(function (evt) {
            if (evt.target == this) {
                var selectedarrayval = $(this).val();
                $(".activitiesunitid_" + j).val(selectedarrayval);
            }
            updatenetpriceservice();
        }).change();
        $(".delete_recordactivities_" + j).click(function () {
            $(this).closest('tr').remove();
        });


    }

    function updatenetpriceactivitiesaddrow(id) {
        $(".activity_save").prop('disabled', true);
        $(".erroractivities").html("");
        $(".activitiesnetamount_" + id).val('');
        var activitiescharges = $(".activitiescharges_" + id).val();
        var activitiesquantity = $(".activitiesquantity_" + id).val();
        var activitiestaxid = $(".taxdropdownactivities_" + id).val();
        var activitiesdiscount = $(".activitiesdiscount_" + id).val();
        var selected_activities_id = $(".activitiesid_" + id).val();
        if (activitiescharges <= 0 || activitiescharges == '') {
            $(".erroractivities").html("");
            $(".erroractivities").append("<strong><span>Please Use value Greater in charges than " + activitiescharges + " For ID " + selected_activities_id + "</strong></span>");
            return false;
        }
        if (isNaN(activitiescharges)) {
            $(".erroractivities").html("");
            $(".erroractivities").append("<strong><span>Please Use Numeric value in charges Instead " + activitiescharges + " For ID " + selected_activities_id + " </strong></span>");
            return false;
        }
        if (activitiesquantity <= 0 || activitiesquantity == '') {
            $(".erroractivities").html("");
            $(".erroractivities").append("<strong><span>Please Use value Greater in quantity than " + activitiesquantity + " For ID " + selected_activities_id + "</span>");
            return false;
        }
        if (isNaN(activitiesquantity)) {
            $(".erroractivities").html("");
            $(".erroractivities").append("<strong><span>Please Use Numeric value in quantity Instead " + activitiesquantity + " For ID " + selected_activities_id + " </strong></span>");
            return false;
        }
        if (activitiescharges != '' && activitiesquantity != '' && !isNaN(activitiesquantity) && !isNaN(activitiescharges)) {
            $(".erroractivities").html("");
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'tax_id': activitiestaxid,
                    'gettaxrate': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".erroractivities").html("");
                    //totalnetamount without vat
                    if (activitiesdiscount != '') {
                        if (activitiesdiscount <= 0) {
                            $(".erroractivities").html("");
                            $(".erroractivities").append("<span>Please Use value Greater in discount than " + activitiesdiscount + " For ID <strong>" + selected_activities_id + " </strong></span>");
                            return false;
                        } else if (isNaN(activitiesdiscount)) {
                            $(".erroractivities").html("");
                            $(".erroractivities").append("<span>Please Use Numeric value in discount Instead " + activitiesdiscount + " For ID <strong>" + selected_activities_id + " </strong></span>");
                            return false;
                        } else {
                            $(".activity_save").prop('disabled', false);
                            var totalnetamountvalactivities = (parseFloat(activitiescharges)) * (parseFloat(activitiesquantity));
                            var getdiscountamountactivities = ((totalnetamountvalactivities) * (activitiesdiscount / 100 ));
                            $(".activitiesdiscount_amount_" + id).val((getdiscountamountactivities).toFixed(2));
                            var amountafterdiscoountactivities = (totalnetamountvalactivities - getdiscountamountactivities);
                            var taxamountactivities = parseFloat(data[0].taxrates);
                            var getvatamountactivities = ((amountafterdiscoountactivities) * (taxamountactivities / 100 ));
                            $(".activitiesvat_amount_" + id).val(( getvatamountactivities).toFixed(2));
                            var totalnetactivities = ( amountafterdiscoountactivities  + getvatamountactivities);
                            $(".activitiesnetamount_" + id).val(totalnetactivities.toFixed(2));
                        }
                    } else {
                        var totalnetamountvalactivities = (parseFloat(activitiescharges)) * (parseFloat(activitiesquantity));
                        var taxamountactivities = parseFloat(data[0].taxrates);
                        var getvatamountactivities = ((totalnetamountvalactivities) * (taxamountactivities / 100 ));
                        var totalnetactivities = (totalnetamountvalactivities + getvatamountactivities);
                        $(".activitiesdiscount_amount_" + id).val(0);
                        $(".activitiesvat_amount_" + id).val((getvatamountactivities).toFixed(2));
                        $(".activity_save").prop("disabled", false);
                        $(".activitiesnetamount_" + id).val(totalnetactivities.toFixed(2));
                    }
                }
            });
        }
    }
    function taxactivitieseditchange(id) {
        $(".errorservices").html("");
        updatenetpriceactivitieseditrow(id);
    }
    function updatenetpriceactivitieseditrow(id) {
        $(".activity_save").prop('disabled', true);
        $(".erroractivities").html("");
        $(".activitiesnetamount_0" + id).val('');
        var activitiescharges = $(".activitiescharges_0" + id).val();
        var activitiesquantity = $(".activitiesquantity_0" + id).val();
        var activitiestaxid = $(".taxdropdownactivities_" + id).val();
        var activitiesdiscount = $(".activitiesdiscount_0" + id).val();
        var selected_activities_id = $(".activitiesid_0" + id).val();
        if (activitiescharges <= 0 || activitiescharges == '') {
            $(".erroractivities").html("");
            $(".erroractivities").append("<strong><span>Please Use value Greater in charges than " + activitiescharges + " For ID " + selected_activities_id + "</strong></span>");
            return false;
        }
        if (isNaN(activitiescharges)) {
            $(".erroractivities").html("");
            $(".erroractivities").append("<strong><span>Please Use Numeric value in charges Instead " + activitiescharges + " For ID " + selected_activities_id + " </strong></span>");
            return false;
        }
        if (activitiesquantity <= 0 || activitiesquantity == '') {
            $(".erroractivities").html("");
            $(".erroractivities").append("<strong><span>Please Use value Greater in quantity than " + activitiesquantity + " For ID " + selected_activities_id + "</span>");
            return false;
        }
        if (isNaN(activitiesquantity)) {
            $(".erroractivities").html("");
            $(".erroractivities").append("<strong><span>Please Use Numeric value in quantity Instead " + activitiesquantity + " For ID " + selected_activities_id + " </strong></span>");
            return false;
        }
        if (activitiescharges != '' && activitiesquantity != '' && !isNaN(activitiesquantity) && !isNaN(activitiescharges)) {
            $(".erroractivities").html("");
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'tax_id': activitiestaxid,
                    'gettaxrate': 1
                },
                dataType: 'JSON',
                success: function (data) {
                    $(".erroractivities").html("");
                    //totalnetamount without vat
                    if (activitiesdiscount != '') {
                        if (activitiesdiscount <= 0) {
                            $(".erroractivities").html("");
                            $(".erroractivities").append("<span>Please Use value Greater in discount than " + activitiesdiscount + " For ID <strong>" + selected_activities_id + " </strong></span>");
                            return false;
                        } else if (isNaN(activitiesdiscount)) {
                            $(".erroractivities").html("");
                            $(".erroractivities").append("<span>Please Use Numeric value in discount Instead " + activitiesdiscount + " For ID <strong>" + selected_activities_id + " </strong></span>");
                            return false;
                        } else {
                            $(".activity_save").prop('disabled', false);
                            var totalnetamountvalactivities = (parseFloat(activitiescharges)) * (parseFloat(activitiesquantity));
                            var getdiscountamountactivities = ((totalnetamountvalactivities) * (activitiesdiscount / 100 ));
                            $(".activitiesdiscount_amount_0" + id).val((getdiscountamountactivities).toFixed(2));
                            var amountafterdiscoountactivities = (totalnetamountvalactivities - getdiscountamountactivities);
                            var taxamountactivities = parseFloat(data[0].taxrates);
                            var getvatamountactivities = ((amountafterdiscoountactivities) * (taxamountactivities / 100 ));
                            $(".activitiesvat_amount_0" + id).val(( getvatamountactivities).toFixed(2));
                            var totalnetactivities = ( amountafterdiscoountactivities  + getvatamountactivities);
                            $(".activitiesnetamount_0" + id).val(totalnetactivities.toFixed(2));
                        }
                    } else {
                        var totalnetamountvalactivities = (parseFloat(activitiescharges)) * (parseFloat(activitiesquantity));
                        var taxamountactivities = parseFloat(data[0].taxrates);
                        var getvatamountactivities = ((totalnetamountvalactivities) * (taxamountactivities / 100 ));
                        var totalnetactivities = (totalnetamountvalactivities + getvatamountactivities);
                        $(".activitiesdiscount_amount_0" + id).val(0);
                        $(".activitiesvat_amount_0" + id).val((getvatamountactivities).toFixed(2));
                        $(".activity_save").prop("disabled", false);
                        $(".activitiesnetamount_0" + id).val(totalnetactivities.toFixed(2));
                    }
                }
            });
        }
    }








    function booking_save() {
        var booking_id = $('input[name="booking_id[]"]').map(function () {
            return this.value;
        }).get();
        var room_book_id = $('input[name="room_book_id[]"]').map(function () {
            return this.value;
        }).get();
        var booking_date = $('input[name="booking_date[]"]').map(function () {
            return this.value;
        }).get();
        var CheckIn_date = $('input[name="CheckIn_date[]"]').map(function () {
            return this.value;
        }).get();
        var CheckOut_date = $('input[name="CheckOut_date[]"]').map(function () {
            return this.value;
        }).get();
        var room_id = $('input[name="room_id[]"]').map(function () {
            return this.value;
        }).get();
        var unit_id = $('input[name="unit_id[]"]').map(function () {
            return this.value;
        }).get();
        var nightcharge = $('input[name="nightcharge[]"]').map(function () {
            return this.value;
        }).get();
        var adults = $('input[name="adults[]"]').map(function () {
            return this.value;
        }).get();
        var children = $('input[name="children[]"]').map(function () {
            return this.value;
        }).get();
        var night = $('input[name="night[]"]').map(function () {
            return this.value;
        }).get();
        var discount = $('input[name="discount[]"]').map(function () {
            return this.value;
        }).get();
        var taxbooking_id = $('input[name="taxbooking_id[]"]').map(function () {
            return this.value;
        }).get();
        var netamount = $('input[name="netamount[]"]').map(function () {
            return this.value;
        }).get();
        var discount_amount = $('input[name="discount_amount[]"]').map(function () {
            return this.value;
        }).get();
        var vat_amount = $('input[name="vat_amount[]"]').map(function () {
            return this.value;
        }).get();
        var sum = 0;
        netamount.toString().split(',').map(function (n) {
            if (!n) return;
            sum += parseFloat(n);
            return sum;
        });

        $(".total_room_charges").val(sum);
        if (CheckIn_date == '' || CheckOut_date == '') {
            $(".date_error").html("");
            $(".date_error").append("<span>Check In or Check Out date cannot be empty.</span>");
        }
        if (CheckIn_date != '' || CheckOut_date != '') {
            $.ajax({
                url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                type: 'POST',
                data: {
                    'booking_id[]': booking_id,
                    'room_book_id[]': room_book_id,
                    'booking_date[]': booking_date,
                    'CheckIn_date[]': CheckIn_date,
                    'CheckOut_date[]': CheckOut_date,
                    'room_id[]': room_id,
                    'nightcharge[]': nightcharge,
                    'adults[]': adults,
                    'children[]': children,
                    'night[]': night,
                    'discount[]': discount,
                    'taxbooking_id[]': taxbooking_id,
                    'netamount[]': netamount,
                    'unit_id[]': unit_id,
                    'discount_amount[]': discount_amount,
                    'vat_amount[]': vat_amount,
                    'getbookingform': 1
                },
                success: function (data) {
                    if (data == "success") {
                    }
                }
            });
        }
    }

    $(".customer_save").click(function () {
        $(".errorcustomer").val("");
        var booking_id = $(".booking_id").val();
        var customername = $("#customer-name_id").val();
        var firstname = $(".firstname").val();
        var email = $(".email").val();
        var phone = $(".phone").val();
        var mobile = $(".mobile").val();
        if (customername == '' || customername == 'undefined') {
            $(".errorcustomer").html("");
            $(".errorcustomer").append("<strong><span>Customer Name required</strong></span>");
        }
        if (firstname == '' || firstname == 'undefined') {
            $(".errorcustomer").html("");
            $(".errorcustomer").append("<strong><span>First Name required</strong></span>");
        }
        if (email == '' || email == 'undefined') {
            $(".errorcustomer").html("");
            $(".errorcustomer").append("<strong><span>Email is required</strong></span>");
        }
        if (phone == '' || phone == 'undefined') {
            $(".errorcustomer").html("");
            $(".errorcustomer").append("<strong><span>Phone No required</strong></span>");
        }
        if (mobile == '' || mobile == 'undefined') {
            $(".errorcustomer").html("");
            $(".errorcustomer").append("<strong><span>Mobile No required</strong></span>");
        }

    });

    function customer_save() {
        var booking_id = $('input[name="booking_id[]"]').map(function () {
            return this.value;
        }).get();        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'customerdata': $("#customer_form").serialize(),
                'booking_id': booking_id,
                'getcustomerform': 1
            },
            success: function (data) {
            }
        });
    }

    function service_save() {
        var booking_id = $('input[name="booking_id[]"]').map(function () {
            return this.value;
        }).get();
        var serviceid = $('input[name="serviceid[]"]').map(function () {
            return this.value;
        }).get();
        var service_book_id = $('input[name="service_book_id[]"]').map(function () {
            return this.value;
        }).get();
        var servicedropdown = $('input[name="servicedropdown[]"]').map(function () {
            return this.value;
        }).get();
        var serviceunit = $('input[name="serviceunit[]"]').map(function () {
            return this.value;
        }).get();
        var servicequantity = $('input[name="servicequantity[]"]').map(function () {
            return this.value;
        }).get();
        var servicecharges = $('input[name="servicecharges[]"]').map(function () {
            return this.value;
        }).get();
        var servicediscount = $('input[name="servicediscount[]"]').map(function () {
            return this.value;
        }).get();
        var taxservice = $('input[name="taxservice[]"]').map(function () {
            return this.value;
        }).get();
        var servicenetamount = $('input[name="servicenetamount[]"]').map(function () {
            return this.value;
        }).get();
        var serviceunitid = $('input[name="serviceunitid[]"]').map(function () {
            return this.value;
        }).get();
        var servicediscount_amount = $('input[name="servicediscount_amount[]"]').map(function () {
            return this.value;
        }).get();
        var servicevat_amount = $('input[name="servicevat_amount[]"]').map(function () {
            return this.value;
        }).get();

        var sum = 0;
        servicenetamount.toString().split(',').map(function (n) {
            if (!n) return;
            sum += parseFloat(n);
            return sum;
        });
        $(".total_service_charges").val(sum);
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'booking_id[]': booking_id,
                'serviceid[]': serviceid,
                'service_book_id[]': service_book_id,
                'servicedropdown[]': servicedropdown,
                'serviceunit[]': serviceunit,
                'servicequantity[]': servicequantity,
                'servicecharges[]': servicecharges,
                'servicediscount[]': servicediscount,
                'taxservice[]': taxservice,
                'servicenetamount[]': servicenetamount,
                'serviceunitid[]': serviceunitid,
                'servicediscount_amount[]': servicediscount_amount,
                'servicevat_amount[]': servicevat_amount,
                'getserviceform': 1
            },
            success: function (data) {
             }
        });

    }

    function activity_save() {
        var booking_id = $('input[name="booking_id[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesid = $('input[name="activitiesid[]"]').map(function () {
            return this.value;
        }).get();
        var activities = $('input[name="activities[]"]').map(function () {
            return this.value;
        }).get();
        var activity_book_id = $('input[name="activity_book_id[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesadult = $('input[name="activitiesadult[]"]').map(function () {
            return this.value;
        }).get();
        var activitieschild = $('input[name="activitieschild[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesunit = $('input[name="activitiesunit[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesquantity = $('input[name="activitiesquantity[]"]').map(function () {
            return this.value;
        }).get();
        var activitiescharges = $('input[name="activitiescharges[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesdiscount = $('input[name="activitiesdiscount[]"]').map(function () {
            return this.value;
        }).get();
        var taxdropdownactivities = $('input[name="taxdropdownactivities[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesnetamount = $('input[name="activitiesnetamount[]"]').map(function () {
            return this.value;
        }).get();
        var taxactivitiesid = $('input[name="taxactivitiesid[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesunitid = $('input[name="activitiesunitid[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesdiscount_amount = $('input[name="activitiesdiscount_amount[]"]').map(function () {
            return this.value;
        }).get();
        var activitiesvat_amount = $('input[name="activitiesvat_amount[]"]').map(function () {
            return this.value;
        }).get();

        var sum = 0;
        activitiesnetamount.toString().split(',').map(function (n) {
            if (!n) return;
            sum += parseFloat(n);
            return sum;
        });
        $(".total_activity_charges").val(sum);
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'booking_id[]': booking_id,
                'activitiesid[]': activitiesid,
                'activities[]': activities,
                'activity_book_id[]': activity_book_id,
                'activitiesadult[]': activitiesadult,
                'activitieschild[]': activitieschild,
                'activitiesunit[]': activitiesunit,
                'activitiesquantity[]': activitiesquantity,
                'activitiescharges[]': activitiescharges,
                'activitiesdiscount[]': activitiesdiscount,
                'taxactivitiesid[]': taxactivitiesid,
                'activitiesunitid[]': activitiesunitid,
                'activitiesnetamount[]': activitiesnetamount,
                'activitiesdiscount_amount[]': activitiesdiscount_amount,
                'activitiesvat_amount[]': activitiesvat_amount,
                'getactivityform': 1
            },
            success: function (data) {
            }
        });

    }

    $(".save_all").click(function () {
        booking_save();
        customer_save();
        service_save();
        activity_save();
        var booking_id = $('input[name="booking_id[]"]').map(function () {
            return this.value;
        }).get();        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'savepayment': $("#save_payment").serialize(),
                'booking_id': booking_id,
                'getsavepaymentform': 1
            },
            success: function (data) {
                $(".save_all").prop("disabled", true);
                $(".save_all").prop("disabled", true);
                $(".save_all_print").prop("disabled", true);
                $(".data_success").append("<strong><span>Data has been saved successfully</span><strong>");
                window.location = "<?php echo $base . ADMIN_FOLDER ?>/modules/booking/booking/index.php?view=list";
            }
        });
    });

    $(".save_all_print").click(function () {
        booking_save();
        customer_save();
        service_save();
        activity_save();
        var booking_id = $('input[name="booking_id[]"]').map(function () {
            return this.value;
        }).get();
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'savepayment': $("#save_payment").serialize(),
                'booking_id': booking_id,
                'getsavepaymentformprint': 1
            },
            success: function (data) {
                $(".save_all").prop("disabled", true);
                $(".save_all_print").prop("disabled", true);
               $.ajax({
                   url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/printbill.php',
                   type: 'POST',
                   data: {
                       'booking_id': booking_id,
                       'getbbokingid': 1
                   },
                   success: function (data) {
                       $(".data_success").append("<strong><span>Data has been saved successfully</span><strong>");
                   }
               });

            }
        });
    });

    function payment_tab() {
        var netamount = $('input[name="netamount[]"]').map(function () {
            return this.value;
        }).get();
        var sum1 = 0;
        netamount.toString().split(',').map(function (n) {
            if (!n) return;
            sum1 += parseFloat(n);
            return sum1;
        });
        var servicenetamount = $('input[name="servicenetamount[]"]').map(function () {
            return this.value;
        }).get();
        var sum2 = 0;
        servicenetamount.toString().split(',').map(function (n) {
            if (!n) return;
            sum2 += parseFloat(n);
            return sum2;
        });
        var activitiesnetamount = $('input[name="activitiesnetamount[]"]').map(function () {
            return this.value;
        }).get();
        var sum3 = 0;
        activitiesnetamount.toString().split(',').map(function (n) {
            if (!n) return;
            sum3 += parseFloat(n);
            return sum3;
        });
        var booking_id = $('input[name="booking_id[]"]').map(function () {
            return this.value;
        }).get();
        $(".total_service_charges").val(sum2);
        $(".total_room_charges").val(sum1);
        $(".total_activity_charges").val(sum3);
        $(".data_success").html("");
        if ($(".total_room_charges").val() == '') {
            $(".save_all").prop("disabled", true);
            $(".save_all_print").prop("disabled", true);
        } else {
            $(".save_all_print").prop("disabled", false);
            $(".save_all").prop("disabled", false);
            var total_room_charges = $(".total_room_charges").val();
            var total_activity_charges = $(".total_activity_charges").val();
            var total_service_charges = $(".total_service_charges").val();
            if (total_activity_charges == '') {
                total_activity_charges = 0;
            }
            if (total_room_charges == '') {
                total_room_charges = 0;
            }
            if (total_service_charges == 0) {
                total_service_charges = 0;
            }
            var url =  window.location.href;
            var data = (url.match(/(\d+)/g) || []);
            var aId = data[0];
            if(aId > 0){
                var total_down_payment_edit = $(".total_down_payment_edit").val();
                var plus = (parseFloat(total_room_charges) + parseFloat(total_activity_charges) + parseFloat(total_service_charges) );
                var getremain = (parseFloat(plus) - parseFloat(total_down_payment_edit));
                $(".totalnetpayable").val(getremain.toFixed(2));
            }else{
                var plus = (parseFloat(total_room_charges) + parseFloat(total_activity_charges) + parseFloat(total_service_charges) );
                $(".totalnetpayable").val(plus.toFixed(2));
            }
        }

        }
    

    function customer_click() {
        $(".data_success").html('');
    }

    $(".total_down_payment").keyup(function () {
        $(".errorpayment").html("");
        var downpayment = $(".total_down_payment").val();
        if (downpayment > 0 && downpayment != '') {
            $(".totalnetpayable").val('');
            var total_room_charges = $(".total_room_charges").val();
            var total_activity_charges = $(".total_activity_charges").val();
            var total_service_charges = $(".total_service_charges").val();
            if (total_activity_charges == '') {
                total_activity_charges = 0;
            }
            if (total_room_charges == '') {
                total_room_charges = 0;
            }
            if (total_service_charges == 0) {
                total_service_charges = 0;
            }
            var plus = (parseFloat(total_room_charges) + parseFloat(total_activity_charges) + parseFloat(total_service_charges) );
            var subtractdown = (parseFloat(plus) - parseFloat(downpayment));
            $(".totalnetpayable").val(subtractdown.toFixed(2));
            if (isNaN(downpayment)) {
                $(".errorpayment").html("");
                $(".errorpayment").append("<strong>Down Payment must be numeric.</strong>");
            } else {
                $(".totalnetpayable").val('');
                var total_room_charges = $(".total_room_charges").val();
                var total_activity_charges = $(".total_activity_charges").val();
                var total_service_charges = $(".total_service_charges").val();
                if (total_activity_charges == '') {
                    total_activity_charges = 0;
                }
                if (total_room_charges == '') {
                    total_room_charges = 0;
                }
                if (total_service_charges == 0) {
                    total_service_charges = 0;
                }
                var plus = (parseFloat(total_room_charges) + parseFloat(total_activity_charges) + parseFloat(total_service_charges) );
                var subtractdown = (parseFloat(plus) - parseFloat(downpayment));
                $(".totalnetpayable").val(subtractdown.toFixed(2));
            }

        } else {
            $(".errorpayment").html("");
            $(".errorpayment").append("<strong>Down Payment cannot be empty or must be greater than zero.</strong>");

            $(".totalnetpayable").val('');
            var total_room_charges = $(".total_room_charges").val();
            var total_activity_charges = $(".total_activity_charges").val();
            var total_service_charges = $(".total_service_charges").val();
            if (total_activity_charges == '') {
                total_activity_charges = 0;
            }
            if (total_room_charges == '') {
                total_room_charges = 0;
            }
            if (total_service_charges == 0) {
                total_service_charges = 0;
            }
            var plus = (parseFloat(total_room_charges) + parseFloat(total_activity_charges) + parseFloat(total_service_charges) );
            $(".totalnetpayable").val(plus.toFixed(2));
        }
    });


</script>