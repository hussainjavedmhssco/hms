<?php
/**
 * Template of the module listing
 */
debug_backtrace() || die ('Direct access not permitted');

// Action to perform
$action = (isset($_GET['action'])) ? htmlentities($_GET['action'], ENT_QUOTES, 'UTF-8') : '';

// Item ID
$id = (isset($_GET['id']) && is_numeric($_GET['id'])) ? $_GET['id'] : 0;

// current page
if(isset($_GET['offset']) && is_numeric($_GET['offset'])) $offset = $_GET['offset'];
elseif(isset($_SESSION['offset']) && isset($_SESSION['module_referer']) && $_SESSION['module_referer'] == MODULE) $offset = $_SESSION['offset'];
else $offset = 0;

// Items per page
if(isset($_GET['limit']) && is_numeric($_GET['limit'])){
    $limit = $_GET['limit'];
    $offset = 0;
}
elseif(isset($_SESSION['limit']) && isset($_SESSION['module_referer']) && $_SESSION['module_referer'] == MODULE) $limit = $_SESSION['limit'];
else $limit = 20;

$_SESSION['limit'] = $limit;

$_SESSION['offset'] = $offset;

// Inclusions
require_once(SYSBASE.ADMIN_FOLDER.'/includes/fn_list.php');

if($db !== false){

    // Initializations
    $cols = getCols();
    $filters = getFilters($db);
    if(is_null($cols)) $cols = array();
    if(is_null($filters)) $filters = array();
    $total = 0;
    $total_page = 0;
    $q_search = '';
    $result_lang = false;
    $total_lang = 1;
    $result = false;
    $referer = DIR.'index.php?view=list';

    if(isset($_GET['order'])) $order = htmlentities($_GET['order'], ENT_QUOTES, 'UTF-8');
    elseif(isset($_SESSION['order']) && $_SESSION['order'] != '' && isset($_SESSION['module_referer']) && $_SESSION['module_referer'] == MODULE) $order = $_SESSION['order'];
    else $order = getOrder();

    if(isset($_GET['sort'])) $sort = htmlentities($_GET['sort'], ENT_QUOTES, 'UTF-8');
    elseif(isset($_SESSION['sort']) && $_SESSION['sort'] != '' && isset($_SESSION['module_referer']) && $_SESSION['module_referer'] == MODULE) $sort = $_SESSION['sort'];
    else $sort = 'asc';

    if(strpos($order, ' ') !== false){
        $sort = strtolower(substr($order, strpos($order, ' ')+1));
        $order = substr($order, 0, strpos($order, ' '));
    }

    $_SESSION['order'] = $order;
    $_SESSION['sort'] = $sort;

    $rsort = ($sort == 'asc') ? 'desc' : 'asc';

    // Getting languages
    if(MULTILINGUAL){
        $result_lang = $db->query('SELECT id, title FROM pm_lang WHERE id != '.DEFAULT_LANG.' AND checked = 1');
        if($result_lang !== false)
            $total_lang = $db->last_row_count();
    }

    // Getting filters values
    if(isset($_SESSION['module_referer']) && $_SESSION['module_referer'] !== MODULE){
        unset($_SESSION['filters']);
        unset($_SESSION['q_search']);
    }
    if(isset($_POST['search'])){
        foreach($filters as $filter){
            $fieldName = $filter->getName();
            $value = (isset($_POST[$fieldName])) ? htmlentities($_POST[$fieldName], ENT_QUOTES, 'UTF-8') : '';
            $filter->setValue($value);
        }
        $q_search = htmlentities($_POST['q_search'], ENT_QUOTES, 'UTF-8');
        $_SESSION['filters'] = serialize($filters);
        $_SESSION['q_search'] = $q_search;
        $offset = 0;
        $_SESSION['offset'] = $offset;
    }else{
        if(isset($_SESSION['filters'])) $filters = unserialize($_SESSION['filters']);
        if(isset($_SESSION['q_search'])) $q_search = $_SESSION['q_search'];
    }

    // Getting items in the database
    $condition = '';

    if(MULTILINGUAL) $condition .= ' lang = '.DEFAULT_LANG;

    foreach($filters as $filter){
        $fieldName = $filter->getName();
        $fieldValue = $filter->getValue();
        if($fieldValue != ''){
            if($condition != '') $condition .= ' AND';
            $condition .= ' '.$fieldName.' = '.$db->quote($fieldValue);
        }
    }

    if(!in_array($_SESSION['user']['type'], array('administrator', 'manager', 'editor')) && db_column_exists($db, 'pm_'.MODULE, 'users')){
        if($condition != '') $condition .= ' AND';
        $condition .= ' users REGEXP \'(^|,)'.$_SESSION['user']['id'].'(,|$)\'';
    }

    $query_search = db_getRequestSelect_custom_booking($db,'pm_'.MODULE, getSearchFieldsList($cols), $q_search, $condition, $order.' '.$sort);

    $result_total = $db->query($query_search);
    if($result_total !== false)
        $total = $db->last_row_count();

    if($limit > 0) $query_search .= ' LIMIT '.$limit.' OFFSET '.$offset;

    $result = $db->query($query_search);
    if($result !== false)
        $total_page = $db->last_row_count();

    if(in_array('edit', $permissions) || in_array('all', $permissions)){

        // Setting main item
        if($action == 'define_main' && $id > 0 && check_token($referer, 'list', 'get'))
            define_main($db, 'pm_'.MODULE, $id, 1);

        if($action == 'remove_main' && $id > 0 && check_token($referer, 'list', 'get'))
            define_main($db, 'pm_'.MODULE, $id, 0);

        // Items displayed in homepage
        if($action == 'display_home' && $id > 0 && check_token($referer, 'list', 'get'))
            display_home($db, 'pm_'.MODULE, $id, 1);

        if($action == 'remove_home' && $id > 0 && check_token($referer, 'list', 'get'))
            display_home($db, 'pm_'.MODULE, $id, 0);

        if($action == 'display_home_multi' && isset($_POST['multiple_item']) && check_token($referer, 'list', 'get'))
            display_home_multi($db, 'pm_'.MODULE, 1, $_POST['multiple_item']);

        if($action == 'remove_home_multi' && isset($_POST['multiple_item']) && check_token($referer, 'list', 'get'))
            display_home_multi($db, 'pm_'.MODULE, 0, $_POST['multiple_item']);

        // Item activation/deactivation
        if($action == 'check' && $id > 0 && check_token($referer, 'list', 'get'))
            check($db, 'pm_'.MODULE, $id, 1);

        if($action == 'uncheck' && $id > 0 && check_token($referer, 'list', 'get'))
            check($db, 'pm_'.MODULE, $id, 2);

        if($action == 'archive' && $id > 0 && check_token($referer, 'list', 'get'))
            check($db, 'pm_'.MODULE, $id, 3);

        if($action == 'check_multi' && isset($_POST['multiple_item']) && check_token($referer, 'list', 'get'))
            check_multi($db, 'pm_'.MODULE, 1, $_POST['multiple_item']);

        if($action == 'uncheck_multi' && isset($_POST['multiple_item']) && check_token($referer, 'list', 'get'))
            check_multi($db, 'pm_'.MODULE, 2, $_POST['multiple_item']);

        if($action == 'archive_multi' && isset($_POST['multiple_item']) && check_token($referer, 'list', 'get'))
            check_multi($db, 'pm_'.MODULE, 3, $_POST['multiple_item']);
    }

    if(in_array('delete', $permissions) || in_array('all', $permissions)){

        // Item deletion
        if($action == 'delete' && $id > 0 && check_token($referer, 'list', 'get'))
            delete_item($db, $id);

        if($action == 'delete_multi' && isset($_POST['multiple_item']) && check_token($referer, 'list', 'get'))
            delete_multi($db, $_POST['multiple_item']);
    }

    if(in_array('all', $permissions)){

        // Languages completion
        if(MULTILINGUAL && isset($_POST['complete_lang']) && isset($_POST['languages']) && check_token($referer, 'list', 'post')){
            foreach($_POST['languages'] as $id_lang){
                complete_lang_module($db, 'pm_'.MODULE, $id_lang);
                if(NB_FILES > 0) complete_lang_module($db, 'pm_'.MODULE.'_file', $id_lang, true);
            }
        }
    }
}

$_SESSION['module_referer'] = MODULE;
$csrf_token = get_token('list'); ?>
<!DOCTYPE html>
<style>
    .card-default {
        color: #333;
        background: linear-gradient(#fff,#ebebeb) repeat scroll 0 0 transparent;
        font-weight: 600;
        border-radius: 6px;
    }
    .customer_history{
        border: 1px solid #cccccc;
        border-radius: 5px;
        padding: 10px;
    }
    .btn {
        display: inline-block;
        padding: 9px 12px;
        padding-top: 7px;
        margin-bottom: 0;
        font-size: 14px;
        line-height: 20px;
        color: #5e5e5e;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        background-color: #d1dade;
        -webkit-border-radius: 3px;
        -webkit-border-radius: 3px;
        -webkit-border-radius: 3px;
        background-image: none !important;
        border: none;
        text-shadow: none;
        box-shadow: none;
        transition: all 0.12s linear 0s !important;
        font: 14px/20px "Helvetica Neue",Helvetica,Arial,sans-serif;
    }
    .btn-cons {
        margin-right: 10px;
        min-width: 100px;
        margin-bottom: 8px;
        padding: 8px;
        margin-top: 10px;
    }
    .btn-default {
        color: #333;
        background-color: #fff;
        border-color: #ccc;
    }
    .btn-primary {
        color: #fff;
        background-color: #428bca;
        border-color: #357ebd;
    }
    .btn-success {
        color: #fff;
        background-color: #5cb85c;
        border-color: #4cae4c;
    }
    .btn-info {
        color: #fff;
        background-color: #5bc0de;
        border-color: #46b8da;
    }
    .btn-warning {
        color: #fff;
        background-color: #f0ad4e;
        border-color: #eea236;
    }
    .btn-danger {
        color: #fff;
        background-color: #d9534f;
        border-color: #d43f3a;
    }
    .btn-white {
        color: #5e5e5e;
        background-color: #fff;
        border: 1px solid #e5e9ec;
    }
    .btn-link, .btn-link:active, .btn-link[disabled], fieldset[disabled] .btn-link {
        background-color: transparent;
        -webkit-box-shadow: none;
        box-shadow: none;
    }
    .btn-link, .btn-link:hover, .btn-link:focus, .btn-link:active {
        border-color: transparent;
    }
    .btn-link {
        color: #5e5e5e;
        background-color: transparent;
        border: none;
    }
    .btn-link, .btn-link:hover, .btn-link:focus, .btn-link:active {
        border-color: transparent;
    }
    .btn-default, .btn-primary, .btn-success, .btn-info, .btn-warning, .btn-danger {
        text-shadow: 0 -1px 0 rgba(0,0,0,0.2);
        -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,0.15),0 1px 1px rgba(0,0,0,0.075);
        box-shadow: inset 0 1px 0 rgba(255,255,255,0.15),0 1px 1px rgba(0,0,0,0.075);
    }
</style>
<head>
    <?php include(SYSBASE . ADMIN_FOLDER . '/includes/inc_header_form.php'); ?>
    <?php include(SYSBASE.ADMIN_FOLDER.'/includes/inc_header_list.php'); ?>

</head>

<body>
<div class="container-fluid">
    <!-- The Modal -->
    <div class="modal fade" id="my_modal">
        <div class="modal-dialog modal-lg full_modal">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h3 class="modal-title">Customer Payment</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="error" style="color:red;"></div>
                    </div>
                </div>
                <div class="row">
                    <?php $current_date = date("d/m/Y"); ?>
                    <!-- Modal body -->
                    <div class="col-md-6">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Date:</label>
                                        <input class="form-control add_date" type="date" id="add_date" value="<?php echo $current_date;?>"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">BookingID:</label>
                                        <input class="form-control booking_id" type="text" disabled />
                                        <input class="pm_booking_payment_id" type="hidden"/>
                                        <input class="adjusted_discount" type="hidden"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Customer Account:</label>
                                        <input class="form-control customer_account" type="text" disabled />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">Customer CNIC/EI:</label>
                                    <input class="form-control customer_cnic" type="text" disabled />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Payment Method:</label>
                                        <select class="form-control payment_method" name="payment_method" id="payment_method">
                                            <option value="0" selected disabled>Select Payment Method</option>
                                            <option value="Cash">Cash</option>
                                            <option value="Credit">Credit Card</option>
                                            <option value="Cheque">Cheque</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Payment Amount:</label>
                                        <input class="form-control payment_amount" type="text" onkeyup="paymentamount();"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Check/ Ref #:</label>
                                        <input class="form-control check_ref" type="text" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Comments:</label>
                                        <textarea class="form-control comments" cols="2" rows="2"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <input type="checkbox" value="checked" name="checkout" class="checkout">
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="control-label">CheckOut</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="modal-body">
                            <div class="customer_history">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4>Transaction History</h4>
                                        <div class="form-group">
                                            <label class="control-label">Total Customer Balance:</label>
                                            <input class="form-control total_balance" type="text" disabled/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Down Payment:</label>
                                            <input class="form-control down_payment" type="text" disabled/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Discount & Credits:</label>
                                            <input class="form-control discount_credit" type="text" disabled/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Coupons Used:</label>
                                            <input class="form-control coupon" type="text" disabled/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Payment Received:</label>
                                            <input class="form-control payment_received" type="text" disabled/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Balance Amount:</label>
                                            <input class="form-control balance_amount" type="text" disabled/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <button type="button" class="btn btn-primary btn-cons save_payment" disabled>Save</button>
                                    <button type="button" class="btn btn-primary btn-cons" onclick="validate();">Validate</button>
<!--                                    <button type="button" class="btn btn-primary btn-cons" data-dismiss="modal">Close</button>-->
                                    <button type="button" class="btn btn-primary btn-cons clear_payment">Clear</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>



            </div>
        </div>
    </div>
</div>

<div id="wrapper">
    <?php include(SYSBASE.ADMIN_FOLDER.'/includes/inc_top.php'); ?>
    <div id="page-wrapper">
        <div class="page-header">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12 clearfix">
                        <h1 class="pull-left"><i class="fa fa-<?php echo ICON; ?>"></i> <?php echo TITLE_ELEMENT; ?></h1>
                        <div class="pull-left text-right">
                            &nbsp;&nbsp;
                            <?php
                            if(in_array('add', $permissions) || in_array('all', $permissions)){ ?>
                                <a href="index.php?view=form&id=0" class="btn btn-primary mt15">
                                    <i class="fa fa-plus-circle"></i> <?php echo $texts['NEW']; ?>
                                </a>
                                <?php
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="alert-container">
                <div class="alert alert-success alert-dismissable"></div>
                <div class="alert alert-warning alert-dismissable"></div>
                <div class="alert alert-danger alert-dismissable"></div>
            </div>
            <?php
            if($db !== false){
                if(!in_array('no_access', $permissions)){ ?>
                    <form id="form" action="index.php?view=list" method="post">
                        <input type="hidden" name="csrf_token" value="<?php echo $csrf_token; ?>"/>
                        <div class="panel panel-default">
                            <div class="panel-heading form-inline clearfix">
                                <div class="row">
                                    <div class="col-md-3 text-left">
                                        <div class="form-inline">
                                            <input type="text" id="searchtablefield" name="q_search" value="<?php echo $q_search; ?>" class="form-control input-sm" placeholder="<?php echo $texts['SEARCH']; ?>..."/>
                                        </div>
                                    </div>
                                    <div class="col-md-3 text-right">
                                        <div class="input-group">
                                            <button class="btn btn-default btn-sm" type="submit" id="search" name="search"><i class="fa fa-search"></i> <?php echo $texts['SEARCH']; ?></button>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <div class="input-group">
                                            <?php displayFilters($filters); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-th-list"></i> <?php echo $texts['DISPLAY']; ?></div>
                                            <select class="select-url form-control input-sm">
                                                <?php
                                                echo ($limit != 20) ? '<option value="index.php?view=list&limit=20">20</option>' : '<option selected="selected">20</option>';
                                                echo ($limit != 50) ? '<option value="index.php?view=list&limit=50">50</option>' : '<option selected="selected">50</option>';
                                                echo ($limit != 100) ? '<option value="index.php?view=list&limit=100">100</option>' : '<option selected="selected">100</option>'; ?>
                                            </select>
                                        </div>
                                        <?php
                                        if($limit > 0){
                                            $nb_pages = ceil($total/$limit);
                                            if($nb_pages > 1){ ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><?php echo $texts['PAGE']; ?></div>
                                                    <select class="select-url form-control input-sm">
                                                        <?php

                                                        for($i = 1; $i <= $nb_pages; $i++){
                                                            $offset2 = ($i-1)*$limit;

                                                            if($offset2 == $offset)
                                                                echo '<option value="" selected="selected">'.$i.'</option>';
                                                            else
                                                                echo '<option value="index.php?view=list&offset='.$offset2.'">'.$i.'</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                                <?php
                                            }
                                        } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5 class="success" style="color: lightgreen;font-size:small;float:left;"></h5>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped" id="listing_base">
                                        <thead>
                                        <tr class="nodrop nodrag">
                                            <th width="40">
                                                <strong>
                                                    <a href="index.php?view=list&order=id&sort=<?php echo ($order == 'id') ? $rsort : 'asc'; ?>">
                                                        Booking ID <i class="fa fa-sort<?php if($order == 'id') echo '-'.$sort; ?>"></i>
                                                    </a>
                                                </strong></th>
                                            <th width="40">
                                                <strong>
                                                    <a href="index.php?view=list&order=id&sort=<?php echo ($order == 'id') ? $rsort : 'asc'; ?>">
                                                        Customer <i class="fa fa-sort<?php if($order == 'id') echo '-'.$sort; ?>"></i>
                                                    </a>
                                                </strong>
                                            </th>
                                            <th width="40">
                                                <strong>
                                                    <a href="index.php?view=list&order=id&sort=<?php echo ($order == 'id') ? $rsort : 'asc'; ?>">
                                                        Check In <i class="fa fa-sort<?php if($order == 'id') echo '-'.$sort; ?>"></i>
                                                    </a>
                                                </strong>
                                            </th>
                                            <th width="40">
                                                <strong>
                                                    <a href="index.php?view=list&order=id&sort=<?php echo ($order == 'id') ? $rsort : 'asc'; ?>">
                                                        Check Out <i class="fa fa-sort<?php if($order == 'id') echo '-'.$sort; ?>"></i>
                                                    </a>
                                                </strong>
                                            </th>
                                            <th width="50">
                                                <strong>
                                                    <a href="index.php?view=list&order=id&sort=<?php echo ($order == 'id') ? $rsort : 'asc'; ?>">
                                                        Total Charges <i class="fa fa-sort<?php if($order == 'id') echo '-'.$sort; ?>"></i>
                                                    </a>
                                                </strong>
                                            </th>
                                            <th width="50">
                                                <strong>
                                                    <a href="index.php?view=list&order=id&sort=<?php echo ($order == 'id') ? $rsort : 'asc'; ?>">
                                                        Payment Status <i class="fa fa-sort<?php if($order == 'id') echo '-'.$sort; ?>"></i>
                                                    </a>
                                                </strong>
                                            </th>

                                            <th width="50">
                                                <strong>
                                                    <a href="index.php?view=list&order=id&sort=<?php echo ($order == 'id') ? $rsort : 'asc'; ?>">
                                                        Booking Status<i class="fa fa-sort<?php if($order == 'id') echo '-'.$sort; ?>"></i>
                                                    </a>
                                                </strong>
                                            </th>
                                            <th width="130"><Strong><?php echo $texts['ACTIONS']; ?></Strong></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if($result !== false){
                                            foreach($result as $i => $row){ ?>
                                                <tr>
                                                    <?php    $datecheckin = strtotime($row['checkin_date']);
                                                    $datecheckout = strtotime($row['checkout_date']);
                                                    ?>
                                                    <td class="text-center"><?php echo $row['booking_id']; ?></td>
                                                    <td class="text-center"><?php echo $row['customer_name']; ?></td>
                                                    <td class="text-center"><?php echo date("Y-m-d",$datecheckin); ?></td>
                                                    <td class="text-center"><?php echo date("Y-m-d",$datecheckout); ?></td>
                                                    <td class="text-center"><?php echo $row['booking_sum']; ?></td>
                                                    <td class="text-center"><?php
                                                        if($row['booking_status'] == 'reserve'){
                                                            echo 'Pending';
                                                        }
                                                        if($row['booking_status'] == 'cancel' || $row['booking_status'] == 'empty'){
                                                            echo 'Available/Free';
                                                        }
                                                        if($row['booking_status'] == 'checkin'){
                                                            echo 'Paid/Partial';
                                                        }
                                                        if($row['booking_status'] == 'checkout'){
                                                            echo 'Available/Free';
                                                        }
                                                        ?></td>
                                                    <td class="text-center booking_cancel_<?php echo $row['booking_id']; ?>"><?php echo ucfirst($row['booking_status']); ?></td>
                                                    <td class="text-center">
                                                        <?php if($row['booking_status'] == 'checkout'){ ?>
                                                            <i class="fa fa-edit"  style="color:black;"></i>
                                                        <?php  }else { ?>
                                                            <a href="<?php echo $base . ADMIN_FOLDER ?>/modules/booking/booking/index.php?view=form&id=<?php echo $row['booking_id']; ?>"><i class="fa fa-edit"  style="color:black;" title="Edit"></i></a>
                                                        <?php  }
                                                        if($row['booking_status'] == 'checkout'){ ?>
                                                            <i class="fa fa-money"  style="color:black;"></i>
                                                        <?php }else { ?>
                                                            <i class="fa fa-money"  style="color:black;" title="Receive Cash" onclick="receivedcash(<?php echo $row['booking_id']; ?>)"></i>
                                                        <?php }?>
                                                        <a href="<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/printbilllist.php?print=form&id=<?php echo $row['booking_id']; ?>"><i class="fa fa-print"  style="color:black;" title="Print"></i></a>
                                                        <?php if($row['booking_status'] == 'checkout'){ ?>
                                                            <i class="fa fa-minus-circle" style="color:red;" title="Cancel"></i>
                                                        <?php  }else{ ?>
                                                            <i class="fa fa-minus-circle" onclick="cancel(<?php echo $row['booking_id']; ?>);" style="color:red;" title="Cancel"></i>
                                                        <?php } ?>
                                                        <a href="<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/preview.php?preview=form&id=<?php echo $row['booking_id']; ?>"><i class="fa fa-eye" style="color:black;" title="Preview"></i></a>
                                                        <?php if($row['booking_status'] == 'checkout'){
                                                            ?>
                                                            <i class="fa fa-sign-in" style="color:green" title="Checkout"></i>
                                                            <?php
                                                        } if($row['booking_status'] == 'checkin') {?>
                                                            <i class="fa fa-sign-in" style="color:grey" title="CheckIn"></i></a>
                                                        <?php } if($row['booking_status'] == 'cancel' || $row['booking_status'] == 'empty'){
                                                            ?>
                                                            <i class="fa fa-sign-in" style="color:yellowgreen" title="Empty"></i>
                                                            <?php
                                                        } if($row['booking_status'] == 'reserve'){
                                                            ?>
                                                            <i class="fa fa-sign-in" style="color:lightgreen" title="Reserve"></i>
                                                            <?php
                                                        } ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php
                                if($total == 0){ ?>
                                    <div class="text-center mt20 mb20">- <?php echo $texts['NO_ELEMENT']; ?> -</div>
                                    <?php
                                } ?>
                            </div>
                            <div class="panel-footer form-inline clearfix">
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <?php
                                        if($total > 0){ ?>
                                            &nbsp;<input type="checkbox" class="selectall"/>
                                            <?php echo $texts['SELECT_ALL']; ?>&nbsp;
                                            <select name="multiple_actions" class="form-control input-sm">
                                                <option value="">- <?php echo $texts['ACTIONS']; ?> -</option>
                                                <?php
                                                if(in_array('publish', $permissions) || in_array('all', $permissions)){
                                                    if(VALIDATION){ ?>
                                                        <option value="check_multi"><?php echo $texts['PUBLISH']; ?></option>
                                                        <option value="uncheck_multi"><?php echo $texts['UNPUBLISH']; ?></option>
                                                        <?php
                                                    }
                                                    if(HOME){ ?>
                                                        <option value="display_home_multi"><?php echo $texts['SHOW_HOMEPAGE']; ?></option>
                                                        <option value="remove_home_multi"><?php echo $texts['REMOVE_HOMEPAGE']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                if(in_array('delete', $permissions) || in_array('all', $permissions)){ ?>
                                                    <option value="delete_multi"><?php echo $texts['DELETE']; ?></option>
                                                    <?php
                                                } ?>
                                            </select>
                                            <?php
                                        } ?>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-th-list"></i> <?php echo $texts['DISPLAY']; ?></div>
                                            <select class="select-url form-control input-sm">
                                                <?php
                                                echo ($limit != 20) ? '<option value="index.php?view=list&limit=20">20</option>' : '<option selected="selected">20</option>';
                                                echo ($limit != 50) ? '<option value="index.php?view=list&limit=50">50</option>' : '<option selected="selected">50</option>';
                                                echo ($limit != 100) ? '<option value="index.php?view=list&limit=100">100</option>' : '<option selected="selected">100</option>'; ?>
                                            </select>
                                        </div>

                                        <?php
                                        if($limit > 0){
                                            $nb_pages = ceil($total/$limit);
                                            if($nb_pages > 1){ ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><?php echo $texts['PAGE']; ?></div>
                                                    <select class="select-url form-control input-sm">
                                                        <?php

                                                        for($i = 1; $i <= $nb_pages; $i++){
                                                            $offset2 = ($i-1)*$limit;

                                                            if($offset2 == $offset)
                                                                echo '<option value="" selected="selected">'.$i.'</option>';
                                                            else
                                                                echo '<option value="index.php?view=list&offset='.$offset2.'">'.$i.'</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                                <?php
                                            }
                                        } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        if(in_array('all', $permissions)){ ?>
                            <div class="well">
                                <?php
                                if($db != false && MULTILINGUAL && $total_lang > 0){ ?>

                                    <div id="translation" style="display: none;">
                                        <p><?php echo $texts['COMPLETE_LANGUAGE']; ?></p>
                                        <?php
                                        foreach($result_lang as $row_lang){
                                            $id_lang = $row_lang['id'];
                                            $title_lang = $row_lang['title']; ?>

                                            <input type="checkbox" name="languages[]" value="<?php echo $id_lang; ?>">
                                            <?php
                                            $result_img_lang = $db->query('SELECT * FROM pm_lang_file WHERE id_item = '.$id_lang.' AND type = \'image\' AND file != \'\' ORDER BY rank LIMIT 1');
                                            if($result_img_lang !== false && $db->last_row_count() > 0){
                                                $row_img_lang = $result_img_lang->fetch();

                                                $id_img_lang = $row_img_lang['id'];
                                                $file_img_lang = $row_img_lang['file'];

                                                if(is_file(SYSBASE.'medias/lang/big/'.$id_img_lang.'/'.$file_img_lang))
                                                    echo '<img src="'.DOCBASE.'medias/lang/big/'.$id_img_lang.'/'.$file_img_lang.'" alt="" border="0" class="flag"> ';
                                            }
                                            echo $title_lang.'<br>';
                                        } ?>
                                        <button type="submit" name="complete_lang" class="btn btn-default mt10" data-toggle="tooltip" data-placement="right" title="<?php echo $texts['COMPLETE_LANG_NOTICE']; ?>"><i class="fa fa-magic"></i> <?php echo $texts['APPLY_LANGUAGE']; ?></button>
                                    </div>
                                    <?php
                                } ?>
                            </div>
                            <?php
                        } ?>
                    </form>
                    <?php
                }else echo '<p>'.$texts['ACCESS_DENIED'].'</p>';
            } ?>
        </div>
    </div>
</div>
</body>
</html>
<?php
$_SESSION['redirect'] = false;
$_SESSION['msg_error'] = array();
$_SESSION['msg_success'] = array();
$_SESSION['msg_notice'] = array(); ?>

<script type="text/javascript">
    function setInputDate(_id){
        var _dat = document.querySelector(_id);
        var hoy = new Date(),
            d = hoy.getDate(),
            m = hoy.getMonth()+1,
            y = hoy.getFullYear(),
            data;

        if(d < 10){
            d = "0"+d;
        };
        if(m < 10){
            m = "0"+m;
        };

        data = y+"-"+m+"-"+d;
        console.log(data);
        _dat.value = data;
    };

    function cancel(id) {
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'id': id,
                'cancel_book': 1
            },
            dataType: 'JSON',
            success: function (data) {
                $(".success").html("");
                $(".success").addClass("alert alert-success");
                $(".success").delay(4000).fadeOut(400);
                $(".success").append("<strong><span>Your Booking NO# " +id+ " has been Cancel Successfully.</strong></span>");
                $(".booking_cancel_"+id).text("");
                $(".booking_cancel_"+id).text("Cancel");
            }
        });
    }
    function receivedcash(id){
        $(".payment_amount").val("");
        $(".check_ref").val("");
        $(".comments").val("");
        setInputDate("#add_date");
        $(".modal").modal('show');
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'id': id,
                'receivecash': 1
            },
            dataType: 'JSON',
            success: function (data) {
                $(".booking_id").val("");
                $(".customer_account").val("");
                $(".customer_cnic").val("");
                $(".payment_method").val("");
                $(".payment_amount").val("");
                $(".check_ref").val("");
                $(".total_balance").val("");
                $(".down_payment").val("");
                $(".discount_credit").val("");
                $(".coupon").val("");
                $(".payment_received").val("");
                $(".balance_amount").val("");
                $(".booking_id").val(id);
                $(".customer_account").val(data[0].id);
                $(".customer_cnic").val(data[0].cnic);
                $(".payment_method").val(data[0].payment_mode);
                $(".down_payment").val(data[0].down_payment);
                $(".payment_received").val(data[0].payment_received);
                debugger;
                var total_discount = '';
                var get_disc_amount = '';
                var room_discount = (parseFloat(data[0].room_discount));
                var ser_discount = (parseFloat(data[0].service_discount));
                var act_discount = (parseFloat(data[0].activity_discount));

                get_disc_amount = (parseFloat(room_discount) + parseFloat(ser_discount) + parseFloat(act_discount));
                var netpayable = parseFloat(data[0].net_payable);
                var downpayment = $(".down_payment").val();
                $(".total_balance").val(parseFloat(netpayable) + parseFloat(get_disc_amount) + parseFloat(downpayment));
                $(".discount_credit").val(get_disc_amount.toFixed(2));
                var total_balance = $(".total_balance").val();
                var downpayment = $(".down_payment").val();
                var payment_received = $(".payment_received").val();
                $(".balance_amount").val(total_balance - get_disc_amount - downpayment - payment_received);

            }
        });
    }

    function validate() {
        var payment_amount = $(".payment_amount").val();
        var payment_received = $(".payment_received").val();
        var blnc_amount = $(".balance_amount").val();
        if (payment_amount == '') {
            $(".error").html("");
            $(".error").addClass("alert alert-danger");
            $(".error").delay(4000).fadeOut(400);
            $(".error").append("<span><strong>Payment Amount Cannot be Empty.</strong></span>");
        }if (payment_amount < 0) {
            $(".error").html("");
            $(".error").addClass("alert alert-danger");
            $(".error").delay(4000).fadeOut(400);
            $(".error").append("<span><strong>Amount cannot be less than or equal to zero.</strong></span>");
        }
        if (isNaN(payment_amount)) {
            $(".error").html("");
            $(".error").addClass("alert alert-danger");
            $(".error").delay(4000).fadeOut(400);
            $(".error").append("<span><strong>Only Numeric Values will be Accepted.</strong></span>");
        }
        if (!isNaN(payment_amount) && payment_amount > 0 && payment_amount != '') {
            if(payment_received == ''){
                $(".payment_received").val(payment_amount);
                $(".balance_amount").val("");
                $(".balance_amount").val(blnc_amount - payment_amount);
                $(".save_payment").prop("disabled",false);
            }else{
                $(".payment_received").val(parseFloat(payment_amount) + parseFloat(payment_received));
                var total_received = (parseFloat(payment_amount) + parseFloat(payment_received));
                $(".balance_amount").val("");
                $(".balance_amount").val(total_received);
                $(".balance_amount").val(blnc_amount - payment_amount);
                $(".save_payment").prop("disabled",false);
            }
        }
    }
    $(".clear_payment").click(function(){
        $(".payment_amount").val("");
        $(".check_ref").val("");
        $('#payment_method').val(0).prop('selected', true);

    });

    $(".save_payment").click(function(){
        var checked = $(':checkbox:checked').val();
        var checkbox = '';
        if(checked == 'checked'){
            checkbox = 1;
        }else{
            checkbox = 0;
        }
        $.ajax({
            url: '<?php echo $base . ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
            type: 'POST',
            data: {
                'add_date' : $(".add_date").val(),
                'booking_id'  :   $(".booking_id").val(),
                'customer_account' :   $(".customer_account").val(),
                'customer_cnic' : $(".customer_cnic").val(),
                'payment_method' : $(".payment_method").val(),
                'payment_amount': $(".payment_amount").val(),
                'check_ref' : $(".check_ref").val(),
                'comments' : $(".comments").val(),
                'total_balance': $(".total_balance").val(),
                'down_payment' : $(".down_payment").val(),
                'discount_credit': $(".discount_credit").val(),
                'coupon' : $(".coupon").val(),
                'payment_received' : $(".payment_received").val(),
                'balance_amount' : $(".balance_amount").val(),
                'pm_booking_parent_id' : $(".booking_id").val(),
                'adjusted_discount': $(".adjusted_discount").val(),
                'checkbox' : checkbox,
                'savesubpayment': 1
            },
            dataType: 'JSON',
            success: function (data) {
                if(data == 1){
                    $(".error").html("");
                    $(".error").addClass("alert alert-success");
                    $(".error").delay(4000).fadeOut(400);
                    $(".error").append("<span><strong>Payment has been received successfully.</strong></span>");
                    location.reload();
                }
            }
        });
    });

    $(".checkout").click(function(){
        var blnc_amount = $(".balance_amount").val();
        if(blnc_amount <= 0){
            if(confirm("Are you sure you want to checkout")){
                //balance amount
                $(".error").html("");
                $(".error").addClass("alert alert-success");
                $(".error").delay(4000).fadeOut(400);
                $(".error").append("<span><strong>You has been Sucessfully Checked Out, Please Save Details.</strong></span>");
                $(".save_payment").prop("disabled",false);

            }
        }
        if(blnc_amount > 0){
            if(confirm("Are you sure you want to checkout & allow Balance as Discount")){
                //balance amount
                $(".error").html("");
                $(".error").addClass("alert alert-success");
                $(".error").delay(4000).fadeOut(400);
                $(".error").append("<span><strong>Your remianing balance has been adjusted as discount,Please Save Details.</strong></span>");
                var disc = $(".discount_credit").val();
                var adjust_amount = (parseFloat(blnc_amount) + parseFloat(disc));
                $(".discount_credit").val("");
                $(".discount_credit").val(adjust_amount);
                $(".adjusted_discount").val(adjust_amount);
                $(".balance_amount").val(0);
                $(".payment_amount").val(0);
                $(".save_payment").prop("disabled",false);

            }
        }
    });

</script>
