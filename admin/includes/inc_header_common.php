<?php debug_backtrace() || die ("Direct access not permitted"); ?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>
    Safasha HMS-
    <?php
    echo TITLE_ELEMENT;
    if(defined("SITE_TITLE")) echo " | ".SITE_TITLE; ?>
</title>

<?php
if(defined("TEMPLATE")){ ?>
    <link rel="icon" type="image/png" href="<?php echo DOCBASE; ?>templates/<?php echo TEMPLATE; ?>/images/favicon.png">
    <?php
}

require_once(SYSBASE.ADMIN_FOLDER.'/includes/fn_actions.php');
?>

    
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="<?php echo DOCBASE; ?>common/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,700">
<link rel="stylesheet" href="<?php echo DOCBASE; ?>common/css/shortcodes.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo DOCBASE.ADMIN_FOLDER; ?>/css/layout.css">
<link rel="stylesheet" href="<?php echo DOCBASE.ADMIN_FOLDER; ?>/css/layouttwo.css">
<link rel="stylesheet" href="<?php echo DOCBASE; ?>common/js/plugins/magnific-popup/magnific-popup.css">
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script src="<?php echo DOCBASE.ADMIN_FOLDER; ?>/js/jquery-ui.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js"></script>-->
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.min.css">-->
<link rel="stylesheet" href="<?php echo DOCBASE.ADMIN_FOLDER; ?>/css/chosen.min.css">
<script src="<?php echo DOCBASE.ADMIN_FOLDER; ?>/css/chosen.jquery.min.js"></script>
<script src="<?php echo DOCBASE; ?>common/js/modernizr-2.6.1.min.js"></script>
<script src="<?php echo DOCBASE; ?>common/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo DOCBASE; ?>common/js/custom.js"></script>
<script src="<?php echo DOCBASE; ?>common/js/customtwo.js"></script>
<script src="<?php echo DOCBASE; ?>common/js/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
<script>
    $(window).on('load', function() {
        $(".removealert").delay(4000).fadeOut(400);

    });

    $(document).ready(function () {
        $(".refresh_search").prop("disabled",false);
        $(".search").bind('click',function () {
            var checkIn = $(".checkIn").val();
            var checkOut = $(".checkOut").val();
            var roomtype = $("#roomtype_id").val();
            var staynight = $("#staynight_id").val();
            var adult = $("#adult_id").val();
            var children = $("#children_id").val();
            var d = new Date();
            var month = d.getMonth()+1;
            var day = d.getDate();
            var current_date = d.getFullYear() + '-' +((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day;
            debugger;
            if(checkIn < current_date){
                $(".error").html('');
                $(".error").addClass("alert alert-danger");
                $(".error").append('<p style="color:red;">CheckIn Date Cannot be Less than todays date.</p>');
                return false;
            }
            if(checkOut < current_date){
                $(".error").html('');
                $(".error").addClass("alert alert-danger");
                $(".error").append('<p style="color:red;">CheckOut Date Cannot be Less than todays date.</p>');
                return false;
            }

            if (checkIn == '' || checkOut == ''){
                $(".error").html('');
                $(".error").addClass("alert alert-danger");
                $(".error").append('<p style="color:red;">CheckIn Date and CheckOut Date Cannot be Empty.</p>');
                return false;
            }
            if (checkIn > checkOut) {
                $(".error").html('');
                $(".error").addClass("alert alert-danger");
                $(".error").append('<p style="color:red;">CheckIn Date Cannot be greater than CheckOut Date.</p>');
                return false;
            }
            if (roomtype == '' || roomtype == null) {
                $(".error").html('');
                $(".error").addClass("alert alert-danger");
                $(".error").append('<p style="color:red;">Room Type Cannot be Empty.</p>');
                return false;
            }
            if (staynight == '') {
                $(".error").html('');
                $(".error").addClass("alert alert-danger");
                $(".error").append('<p style="color:red;">Stay Night Cannot be Empty.</p>');
                return false;
            }
            if (adult == '') {
                $(".error").html('');
                $(".error").addClass("alert alert-danger");
                $(".error").append('<p style="color:red;">Number of Adults Are Compulsory.</p>');
                return false;
            }
            if (staynight <= 0) {
                $(".error").html('');
                $(".error").addClass("alert alert-danger");
                $(".error").append('<p style="color:red;">Stay Night Cannot be zero or less than zero.</p>');
                return false;
            }
            if (adult <= 0) {
                $(".error").html('');
                $(".error").addClass("alert alert-danger");
                $(".error").append('<p style="color:red;">Number of Adults Cannot be zero or less than zero.</p>');
                return false;
            }
            if (checkIn != '' && checkOut != '' && roomtype != '' && staynight != '' && adult != '' && staynight >= 0 && adult >= 0) {
                $(".error").html("");
                $(".error").removeClass("alert alert-danger");
                $.ajax({
                    url: '<?php echo $base.ADMIN_FOLDER ?>/modules/customcalendar/calendarajax.php',
                    type: 'POST',
                    data: {
                        'checkIn': checkIn,
                        'checkOut': checkOut,
                        'roomtype': roomtype,
                        'staynight': staynight,
                        'adult': adult,
                        'children': children,
                        'getrooms': 1
                    },
                    dataType: 'html',
                    success: function (data) {
                        $(".floorhtml").html(data);
                        $(".refresh_search").prop("disabled",false);
                        $(".refresh_search").css("color","");
                        $(".refresh_search").css("background-color","#5cb85c");
                        $(".search").prop("disabled",true);
                        $(".book_room").click(function(event)
                        {
                            event.preventDefault();
                            var selected_room_id = $(this).closest('tr').find('.room_id').val();
                            $(".selected"+selected_room_id).css("border", "4px solid");
                            $(".selected"+selected_room_id).css("color", "lightgreen");
                            $(".crossbox"+selected_room_id).addClass("far fa-times-circle");
                            $(".crossbox"+selected_room_id).click(function(){
                                $(".crossremove"+selected_room_id).removeClass("far fa-times-circle");
                                $(".selected"+selected_room_id).css("border", "0px");
                                $(".selected"+selected_room_id).css("color", "");
                                $(".selected_room_id"+selected_room_id).val("");
                            });
                            $(".selected_room_id"+selected_room_id).val(selected_room_id);
                            $(".checkIn_date").val(checkIn);
                            $(".checkOut_date").val(checkOut);
                            $(".child_count").val(children);
                            $(".adult_count").val(adult);
                            $(".night_stay").val(staynight);
                            $(".room_type").val(roomtype);

                        });
                    }
                });
            } else {
                $(".error").html('');
                $(".error").append('<p style="color:red;">There must be some issue.</p>');
                return false;
            }
        });
        $(".refresh_search").click(function () {
            var checkIn = $(".checkIn").val("");
            var checkOut = $(".checkOut").val("");
            var staynight = $("#staynight_id").val("");
            var adult = $("#adult_id").val("");
            var children = $("#children_id").val("");
            $(".refresh_search").prop("disabled",true);
            $(".search").prop("disabled",false);
            $(".roomtype_dropdown").html("");
            $(".roomtype_dropdown").append('<?php echo getDropDownData('pm_roomtypes', 'roomtype', 'roomtype_id', 'roomtype', 'Room', 'required', $db) ?>');
            $("#roomtype_id").chosen({                 max_selected_options: 1,
            });
            $(".error").html("");
            $(".book_html").html("");
            $(".floor_html").html("");
        });

    });

    setTimeout(function ()
    {
        $(document).ready(function(){
            $(".limitedNumbChosen").chosen({
                max_selected_options: 15,
                placeholder_text_multiple: "Please Select",
            })
                .bind("chosen:maxselected", function (){
                    window.alert("You have reached at limit!");
                }).change(function(event){

                if(event.target == this){
                    var selectedarrayval = $(this).val();
                    $(".selectedarrayvalues").val(selectedarrayval);
                }

            });
        });
        //Selectedays

        $(document).ready(function(){

            var alreadyexistsarray = '';
            if($(".limitedNumbChosenCouponRoom").val() != ''){
                alreadyexistsarray = $(".limitedNumbChosenCouponRoom").val();
                $(".selectedCouponRoomvalues").val(alreadyexistsarray);
            }else{
                alreadyexistsarray = '';
                $(".selectedCouponRoomvalues").val(alreadyexistsarray);
            }


            $(".limitedNumbChosenCouponRoom").chosen({
                max_selected_options: 15,
                placeholder_text_multiple: "Please Select",
            })
                .bind("chosen:maxselected", function (){
                    window.alert("You have reached at limit!");
                }).change(function(event){

                if(event.target == this){
                    var selectedarrayval = $(this).val();
                    $(".selectedCouponRoomvalues").val(selectedarrayval);
                }

            });
        });

        //Digital Key Room Types
//        $(document).ready(function(){
//
//            var alreadyexistsarray = '';
//            if($(".limitedNumbChosendigitalkeyrooms").val() != ''){
//                alreadyexistsarray = $(".limitedNumbChosenCouponRoom").val();
//                $(".selectedCouponRoomvalues").val(alreadyexistsarray);
//            }else{
//                alreadyexistsarray = '';
//                $(".selectedCouponRoomvalues").val(alreadyexistsarray);
//            }
//
//
//            $(".limitedNumbChosenCouponRoom").chosen({
//                max_selected_options: 15,
//                placeholder_text_multiple: "Please Select",
//            })
//                .bind("chosen:maxselected", function (){
//                    window.alert("You have reached at limit!");
//                }).change(function(event){
//
//                if(event.target == this){
//                    var selectedarrayval = $(this).val();
//                    $(".selectedCouponRoomvalues").val(selectedarrayval);
//                }
//
//            });
//        });


        $(document).ready(function(){
            var alreadyexistsarray = '';
            if($(".limitedNumbChosenCoupondiscoutaplyon").val() != ''){
                alreadyexistsarray = $(".limitedNumbChosenCoupondiscoutaplyon").val();
                $(".selecteddiscoutaplyonvalues").val(alreadyexistsarray);
            }else{
                alreadyexistsarray = '';
                $(".selecteddiscoutaplyonvalues").val(alreadyexistsarray);

            }



            $(".limitedNumbChosenCoupondiscoutaplyon").chosen({
                max_selected_options: 15,
                placeholder_text_multiple: "Please Select",
            })
                .bind("chosen:maxselected", function (){
                    window.alert("You have reached at limit!");
                }).change(function(event){

                if(event.target == this){
                    var selectedarrayval = $(this).val();
                    $(".selecteddiscoutaplyonvalues").val(selectedarrayval);
                }

            });
        });
        $(document).ready(function(){
            var alreadyexistsarray = '';
            if($(".limitedNumbChosen").val() != ''){
                alreadyexistsarray = $(".limitedNumbChosen").val();
                $(".selectedarrayvalues").val(alreadyexistsarray);
            }else{
                alreadyexistsarray = '';
                $(".selectedarrayvalues").val(alreadyexistsarray);

            }
            $(".limitedNumbChosenRooms").chosen({
                max_selected_options: 15,
                placeholder_text_multiple: "Please Select",
            })
                .bind("chosen:maxselected", function (){
                    window.alert("You have reached at limit!");
                }).change(function(event){

                if(event.target == this){
                    var selectedarrayval = $(this).val();
                    $(".selectedarrayvalues").val(selectedarrayval);
                }

            });
        });
        $(document).ready(function(){
            var alreadyexistsarray = '';
            if($(".limitedNumbChosenservice").val() != ''){
                alreadyexistsarray = $(".limitedNumbChosenservice").val();
                $(".selectedarrayvaluesservice").val(alreadyexistsarray);
            }else{
                alreadyexistsarray = '';
                $(".selectedarrayvaluesservice").val(alreadyexistsarray);
            }

            $(".limitedNumbChosenservice").chosen({
                max_selected_options: 15,
                placeholder_text_multiple: "Please Select",
            })
                .bind("chosen:maxselected", function (){
                    window.alert("You have reached at limit!");
                }).change(function(event){

                if(event.target == this){
                    var selectedarrayvalservice = $(this).val();
                    $(".selectedarrayvaluesservice").val(selectedarrayvalservice);
                }

            });
        });



        $(document).ready(function(){
            $(".limitedNumbChosenSingle").chosen({
                max_selected_options: 1,
                placeholder_text_multiple: "Please Select",
                width: "100%"

            })
                .bind("chosen:maxselected", function (){
                    //window.alert("You have reached at limit!");
                })
        });
        $(document).ready(function(){
            $(".limitedNumbChosenSingletax").chosen({
                max_selected_options: 1,
                placeholder_text_multiple: "Please Select",
                width: "100%"

            })
                .bind("chosen:maxselected", function (){
                    //window.alert("You have reached at limit!");
                })
        });
        $(document).ready(function(){
            $(".limitedNumbChosenSingleroomtype").chosen({
                max_selected_options: 1,
                placeholder_text_multiple: "Please Select",
                width: "100%"

            })
                .bind("chosen:maxselected", function (){
                    //window.alert("You have reached at limit!");
                })
        });


        //limitednumbchosenRoomType
        $(document).ready(function(){
            $(".limitedNumbChosenroomtype").chosen({
                max_selected_options: 1,
                placeholder_text_multiple: "Please Select",
                width: "100%"

            })
                .bind("chosen:maxselected", function (){
                    //window.alert("You have reached at limit!");
                })
        });




        $("#building_city_2_0").select(function(){
            if( $('.cityclass ').has('option').length > 0 ) {
                $('.cityclass').empty()
            }
        });

    },3000);





    $(document).ready(function(){
        if($('#maxpeople').val()!=''){
            $( "#maxpeople" ).prop("disabled", false );
            $("#maxpeoplecb").prop('checked', true);

        }else {
            $( "#maxpeople" ).prop("disabled", true );
            $("#maxpeoplecb").prop('checked', false);
        }


        if($('#maxnight').val()!=''){
            $( "#maxnight" ).prop("disabled", false );
            $("#maxnightcb").prop('checked', true);
        }
        else {
            $( "#maxnight" ).prop("disabled", true );
            $("#maxnightcb").prop('checked', false);
        }



        $('#maxpeoplecb').change(function() {
            if(this.checked == true){
                $( "#maxpeople" ).prop( "disabled", false );
            }
            if(this.checked != true){
                $( "#maxpeople" ).prop( "disabled", true );
                $( "#maxpeople" ).val("");
            }
        });


        $('#maxnightcb').change(function() {
            if(this.checked == true){
                $( "#maxnight" ).prop( "disabled", false );
            }
            if(this.checked != true){
                $( "#maxnight" ).prop( "disabled", true );
                $( "#maxnight" ).val("");

            }
        });
    });


    $(document).ready(function(){

             $(".onlyalphabet").keypress(function (e) {
                var regex = new RegExp("^[a-zA-Z]+$");
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str)) {
                    return true;
                }
                else
                {
                    e.preventDefault();
                    $("#errormsg").html("Alphabets Only").show().fadeOut("4000");
                    return false;
                }
            });

            $(".onlynumeric").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    $("#errormsg").html("Digits Only").show().fadeOut("10000");
                    return false;
                }
            });

//            $("#dataadded").delay(500).fadeTo("slow", 0.6);


    });



    $(document).ready(function(){
        $(".choosefilecross").click(function (event) {
            $(".uploaded").hide();


            $('html, body').animate({scrollTop:$(document).height()}, 'slow');


        });
    });



    $(function(){
        <?php
        if(isset($_SESSION['msg_error']) && isset($_SESSION['msg_success']) && isset($_SESSION['msg_notice'])){
            
            if(!is_array($_SESSION['msg_error'])) $_SESSION['msg_error'] = array();
            if(!is_array($_SESSION['msg_success'])) $_SESSION['msg_success'] = array();
            if(!is_array($_SESSION['msg_notice'])) $_SESSION['msg_notice'] = array();
            
            $_SESSION['msg_error'] = array_unique($_SESSION['msg_error']);
            $_SESSION['msg_success'] = array_unique($_SESSION['msg_success']);
            $_SESSION['msg_notice'] = array_unique($_SESSION['msg_notice']); ?>
            
            var msg_error = '<?php echo str_replace(addslashes("\n"), "\n", addslashes(implode("<br>", $_SESSION['msg_error']))); ?>';
            var msg_success = '<?php echo str_replace(addslashes("\n"), "\n", addslashes(implode("<br>", $_SESSION['msg_success']))); ?>';
            var msg_notice = '<?php echo str_replace(addslashes("\n"), "\n", addslashes(implode("<br>", $_SESSION['msg_notice']))); ?>';
            
            var button_close = '<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>';
            if(msg_error != '') $('.alert-container .alert-danger').html(msg_error+button_close).show();
            if(msg_success != '') $('.alert-container .alert-success').html(msg_success+button_close).show();
            if(msg_notice != '') $('.alert-container .alert-warning').html(msg_notice+button_close).show();
            <?php
        } ?>
        
        $('[data-toggle="tooltip"]').tooltip();
        
        $('select[data-filter]').each(function(){
            var target = $(this);
            var currval = $(this).val();
            var curropt = $('option[value="'+currval+'"]', target);
            var input = $('select').filter('<?php if(defined("MODULE")) : ?>[name^="<?php echo MODULE; ?>_'+target.attr('data-filter')+'"],<?php endif; ?>[name="'+target.attr('data-filter')+'"]');
            input.on('change', function(){
                var val = $(this).val();
                $('option[value!=""]', target).hide().prop('selected', false);
                $('option[rel="'+val+'"]', target).show();
                if(curropt.attr('rel') == val) curropt.prop('selected', true);
            });
            input.trigger('change');
        });
        
        $(window).on('resize', function(){
            var h = $(this).height() - 50;
            $('.side-nav').css('max-height', h);
        });
        $(window).trigger('resize');
    })

    $(document).ready(function(){

        var resultselected="";
        $(".limitedNumbChosenbuildingcountry").chosen({
            max_selected_options: 1,
            placeholder_text_multiple: "Please Select",
            width: "100%"
        })
            .bind("chosen:maxselected", function (){
                //window.alert("You have reached at limit!");
            }).change(function(event){

            if(event.target == this)
            {
                resultselected = $(this).val();
                $(".resultselected").val(resultselected);
                if( resultselected != ''){
                    $.ajax({
                        url: '<?php echo $base.ADMIN_FOLDER ?>/modules/custombuilding/countrycityajax.php',
                        type: 'POST',
                        data: {
                            'countryid': resultselected,
                            'getcitylist': 1
                        },
                        dataType: 'json',
                        success: function (data) {
                            var array = [];
                            $(".cityclass").html("");
                            var options = new Array();
                            options.push('<option value="">Please Select City</option>');
                            $.each(data, function(index, option) {
                                options.push('<option value="' + option.id + '">'+ option.city +'</option>');
                            });
                            $('<select class="limit"/>').append(options.join('')).appendTo('.cityclass');
                            $('<input type="hidden" id="cityid" name="city" value="">').appendTo('.cityclass');

                            $(".limit").chosen({
                                width : '100%',
                            }).change(function (event) {
                                var cityid=$(this).val();
                                $('#cityid').val(cityid);
                            });


                        }//end of success
                    });

                }//end if temp

            }

        });
    });




</script>
