<?php debug_backtrace() || die ("Direct access not permitted"); ?>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo DOCBASE.ADMIN_FOLDER; ?>/"><?php echo SITE_TITLE; ?><img src="<?php echo DOCBASE.ADMIN_FOLDER; ?>/images/building.jpg" width="42" height="42"><span class="hidden-xs"> Safasha Hotel Management System</span></a>
        <div class="pull-right hidden-xs" id="info-header">
            <?php echo $texts['CONNECTED_AS']; ?> <i class="fa fa-user"></i> <?php echo "<b>".$_SESSION['user']['login']."</b> (".$_SESSION['user']['type'].")"; ?>&nbsp;
            <a href="<?php echo DOCBASE.ADMIN_FOLDER; ?>/login.php?action=logout"><i class="fa fa-power-off"></i> <?php echo $texts['LOG_OUT']; ?></a>
        </div>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav" style="max-height: 295px; font-size: 13px;">
            <li>
                <a href="<?php echo DOCBASE.ADMIN_FOLDER; ?>/modules/calendar/index.php?view=list">
                    <i class="fa-calendar"></i> <?php echo 'Schedular'; ?>
                </a>
            </li>

            <li>
                <a href="<?php echo DOCBASE.ADMIN_FOLDER; ?>/"<?php if(strpos($_SERVER['SCRIPT_NAME'], ADMIN_FOLDER."/index.php") !== false) echo " class=\"active\""; ?>>
                    <i class="fa fa-dashboard"></i> <?php echo $texts['DASHBOARD']; ?>
                </a>
            </li>

            <!--Transaction Menu-->


            <li class="dropdown">
                <a data-target="#transaction-menu" data-toggle="collapse" href="#"><i class="fa fa-th"></i> <?php echo 'Transaction'; ?> <i class="fa fa-angle-down"></i></a>
                <ul class="<?php if(array_key_exists($dirname, $indexes)) echo "in"; else echo "collapse"; ?>" role="menu" id="transaction-menu">

                    <?php
                    //$tempclassactive='';
                    foreach($modules as $module){
                        if($module->getTitle()=='Calendar'){
                            $CalendarList="CalendarList";
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";
                            $make_dir = "";
                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){
                                $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                            }else{
                                $link = $js;
                            }

                            if($icon == "") $icon = "calendar";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$CalendarList."</a></li>";


                        }
                        if($module->getTitle()=='Bookings'){
                            $temp="Add Bookings";
                            $temp2="Bookings List";
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";
                            $make_dir = "";
                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){
                                $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                            }else{
                                $link = $js;
                            }
                            if($icon == "") $icon = "puzzle-piece";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$temp."</a></li>";
                            echo "<li><a href=\"".$link."\"".""."><i class=\"far fa-list-alt\"></i> ".$temp2."</a></li>";


                        }
                        if($module->getTitle()=='SellPackage'){
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";
                            $make_dir = "";
                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){
                                $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                            }else{
                                $link = $js;
                            }

                            if($icon == "") $icon = "puzzle-piece";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                        }
                        if($module->getTitle()=='Room Shift'){
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";
                            $make_dir = "";
                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){
                                $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                            }else{
                                $link = $js;
                            }

                            if($icon == "") $icon = "puzzle-piece";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";

                               }

                    }
                    ?>


                </ul>



            </li>

            <!--Transaction Menu END-->

            <!--Setup Menu-->
            <li class="dropdown">
                <a data-target="#module-menu" data-toggle="collapse" href="#"><i class="fa fa-th"></i> <?php echo 'Setup'; ?> <i class="fa fa-angle-down"></i></a>
                <ul class="<?php if(array_key_exists($dirname, $indexes)) echo "in"; else echo "collapse"; ?>" role="menu" id="module-menu">
                    <?php
                    foreach($modules as $module){

                        if($module->getTitle()=='Services'){
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";
                            $make_dir = "";
                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){

                                if(strpos($url,'id=0')){
                                    $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                                }

                                $link = $js;

                            }
                            else{
                                $link = $js;
                            }
                            if($icon == "") $icon = "puzzle-piece";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                        }
                        if($module->getTitle()=='Building'){
                            $Bid="Bid";
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";

                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){

                                if(strpos($url,'id=0')){
                                    $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                                }

                                $link = $js;

                            }
                            else{
                                $link = $js;
                            }
                            if($icon == "") $icon = "puzzle-piece";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                $abc =  DOCBASE.ADMIN_FOLDER."/images/building.jpg";
                               // echo "<li><a href=\"".$link."\"".$classname." id=\"".$Bid."\"><img src=\"building.jpg\" width=\"22\" height=\"22\"> ".$title." </a></li>";
                                echo "<li><a href=\"".$link."\"".$classname." id=\"".$Bid."\"><img src=\"".$abc."\" width=\"22\" height=\"22\"> ".$title." </a></li>";
                        }
                        if($module->getTitle()=='Floor'){
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";
                            $make_dir = "";
                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){

                                if(strpos($url,'id=0')){
                                    $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                                }

                                $link = $js;

                            }
                            else{
                                $link = $js;
                            }
                            if($icon == "") $icon = "puzzle-piece";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa-".$icon."\"></i> ".$title."</a></li>";


                        }
                        if($module->getTitle()=='Rooms'){
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";
                            $make_dir = "";
                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){

                                if(strpos($url,'id=0')){
                                    $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                                }

                                $link = $js;

                            }
                            else{
                                $link = $js;
                            }

                            if($icon == "") $icon = "puzzle-piece";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                        }
                        if($module->getTitle()=='Room Types'){
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";
                            $make_dir = "";
                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){
                                if(strpos($url,'id=0')){
                                    $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                                }
                                $link = $js;
                            }
                            else{
                                $link = $js;
                            }
                            if($icon == "") $icon = "puzzle-piece";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                echo "<li><a href=\"".$link."\"".$classname."><i class=\"far".$icon."\"></i> ".$title."</a></li>";


                        }

                        if($module->getTitle()=='Facilities'){
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";
                            $make_dir = "";
                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){

                                if(strpos($url,'id=0')){
                                    $link = $js;
                                }

                                $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";

                            }else{
                                $link = $js;
                            }

                            if($icon == "") $icon = "puzzle-piece";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                        }
                        if($module->getTitle()=='Activities'){
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";
                            $make_dir = "";
                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){
                                $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                            }else{
                                $link = $js;
                            }

                            if($icon == "") $icon = "puzzle-piece";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                        }
                        if($module->getTitle()=='Activity sessions'){
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";
                            $make_dir = "";
                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){
                                $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                            }else{
                                $link = $js;
                            }

                            if($icon == "") $icon = "puzzle-piece";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                        }
                        if($module->getTitle()=='Taxes'){
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";
                            $make_dir = "";
                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){
                                $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                            }else{
                                $link = $js;
                            }

                            if($icon == "") $icon = "puzzle-piece";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                        }
                        if($module->getTitle()=='Digital Key'){
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";
                            $make_dir = "";
                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){
                                if(strpos($url,'id=0')){
                                    $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                                }
                                $link = $js;
                            }
                            else{
                                $link = $js;
                            }
                            if($icon == "") $icon = "fa-key";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa".$icon."\"></i> ".$title."</a></li>";


                        }

                        if($module->getTitle()=='Service Category'){
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";
                            $make_dir = "";
                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){
                                $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                            }else{
                                $link = $js;
                            }

                            if($icon == "") $icon = "puzzle-piece";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                        }
                    }
                    ?>
                </ul>
            </li>
            <!--Setup Menu END-->

            <!--Promotion Menu-->

            <li class="dropdown">
                <a data-target="#promotion-menu" data-toggle="collapse" href="#"><i class="fa fa-th"></i> <?php echo 'Promotions'; ?> <i class="fa fa-angle-down"></i></a>
                <ul class="<?php if(array_key_exists($dirname, $indexes)) echo "in"; else echo "collapse"; ?>" role="menu" id="promotion-menu">
                    <?php
                    foreach($modules as $module){
                        if($module->getTitle()=='Packages'){
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";
                            $make_dir = "";
                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){

                                if(strpos($url,'id=0')){
                                    $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                                }

                                $link = $js;

                            }
                            else{
                                $link = $js;
                            }
                            if($icon == "") $icon = "puzzle-piece";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                        }
                        if($module->getTitle()=='Coupons'){
                            $title = $module->getTitle();
                            $name = $module->getName();
                            $dir = $module->getDir();
                            $icon = $module->getIcon();
                            $js = $dir."/index.php?view=list";
                            $make_dir = "";
                            $link = "";
                            $url = $_SERVER['REQUEST_URI'];
                            if(strpos($url,'form')){

                                if(strpos($url,'id=0')){
                                    $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                                }

                                $link = $js;

                            }
                            else{
                                $link = $js;
                            }
                            if($icon == "") $icon = "puzzle-piece";

                            $classname = ($dirname == $name) ? " class=\"active\"" : "";

                            $rights = $module->getPermissions($_SESSION['user']['type']);

                            if(!in_array("no_access", $rights) && !empty($rights))
                                echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                        }

                    }
                    ?>
                </ul>
            </li>


            <!-- Promotion Menu END-->
            <li><a href="<?php echo DOCBASE; ?>"><i class="fa fa-eye"></i> <?php echo "WEB Preview"; ?></a></li>
            <!-- UserReports Menu -->
            <li class="dropdown">
                <a data-target="#userreports-menu" data-toggle="collapse" href="#"><i class="fa fa-th"></i> <?php echo 'User'; ?> <i class="fa fa-angle-down"></i></a>
                <ul " role="menu" id="userreports-menu">

                <?php
                foreach($modules as $module){
                    if($module->getTitle()=='Users'){
                        $addUSer="Add User";
                        $userProfile="User Profile";
                        $createuserProfile="Create User Profile";
                        $Change="Change Password";


                        $title = $module->getTitle();
                        $name = $module->getName();
                        $dir = $module->getDir();
                        $icon = $module->getIcon();
                        $link = $dir."/index.php?view=list";
                        $link2 = $dir."/index.php?view=form&id=0";


                        if($icon == "") $icon = "puzzle-piece";

                        $classname = ($dirname == $name) ? " class=\"active\"" : "";

                        $rights = $module->getPermissions($_SESSION['user']['type']);

                        if(!in_array("no_access", $rights) && !empty($rights))
                            echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";
                        echo "<li><a href=\"".$link2."\"".$classname."><i class=\"fas fa-users\"></i> ".$addUSer."</a></li>";
                        echo "<li><a href=\"".$link2."\"".$classname."><i class=\"fas fa-key\"></i> ".$Change."</a></li>";
                        echo "<li><a href=\"".$link."\"".$classname."><i class=\"fas fa-user-circle\"></i> ".$userProfile."</a></li>";
                        echo "<li><a href=\"".$link2."\"".$classname."><i class=\"fas fa-user-plus\"></i> ".$createuserProfile."</a></li>";

                    }

                }
                ?>




        </ul>

        </li>
        <!-- UserReports Menu END-->
        <!-- WebPanel Menu -->
        <li class="dropdown">
            <a data-target="#WebPanel-menu" data-toggle="collapse" href="#"><i class="fa fa-th"></i> <?php echo 'WebPanel'; ?> <i class="fa fa-angle-down"></i></a>
            <ul " role="menu" id="WebPanel-menu">

            <?php
            foreach($modules as $module){
                if($module->getTitle()=='Menus'){
                    $title = $module->getTitle();
                    $name = $module->getName();
                    $dir = $module->getDir();
                    $icon = $module->getIcon();
                    $js = $dir."/index.php?view=list";
                    $make_dir = "";
                    $link = "";
                    $url = $_SERVER['REQUEST_URI'];
                    if(strpos($url,'form')){
                        $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                    }else{
                        $link = $js;
                    }

                    if($icon == "") $icon = "puzzle-piece";

                    $classname = ($dirname == $name) ? " class=\"active\"" : "";

                    $rights = $module->getPermissions($_SESSION['user']['type']);

                    if(!in_array("no_access", $rights) && !empty($rights))
                        echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                }
                if($module->getTitle()=='Pages'){
                    $title = $module->getTitle();
                    $name = $module->getName();
                    $dir = $module->getDir();
                    $icon = $module->getIcon();
                    $js = $dir."/index.php?view=list";
                    $make_dir = "";
                    $link = "";
                    $url = $_SERVER['REQUEST_URI'];
                    if(strpos($url,'form')){
                        $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                    }else{
                        $link = $js;
                    }

                    if($icon == "") $icon = "puzzle-piece";

                    $classname = ($dirname == $name) ? " class=\"active\"" : "";

                    $rights = $module->getPermissions($_SESSION['user']['type']);

                    if(!in_array("no_access", $rights) && !empty($rights))
                        echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                }
                if($module->getTitle()=='Articles'){
                    $title = $module->getTitle();
                    $name = $module->getName();
                    $dir = $module->getDir();
                    $icon = $module->getIcon();
                    $js = $dir."/index.php?view=list";
                    $make_dir = "";
                    $link = "";
                    $url = $_SERVER['REQUEST_URI'];
                    if(strpos($url,'form')){
                        $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                    }else{
                        $link = $js;
                    }

                    if($icon == "") $icon = "puzzle-piece";

                    $classname = ($dirname == $name) ? " class=\"active\"" : "";

                    $rights = $module->getPermissions($_SESSION['user']['type']);

                    if(!in_array("no_access", $rights) && !empty($rights))
                        echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                }
                if($module->getTitle()=='Widgets'){
                    $title = $module->getTitle();
                    $name = $module->getName();
                    $dir = $module->getDir();
                    $icon = $module->getIcon();
                    $js = $dir."/index.php?view=list";
                    $make_dir = "";
                    $link = "";
                    $url = $_SERVER['REQUEST_URI'];
                    if(strpos($url,'form')){
                        $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                    }else{
                        $link = $js;
                    }

                    if($icon == "") $icon = "puzzle-piece";

                    $classname = ($dirname == $name) ? " class=\"active\"" : "";

                    $rights = $module->getPermissions($_SESSION['user']['type']);

                    if(!in_array("no_access", $rights) && !empty($rights))
                        echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                }
                if($module->getTitle()=='Slideshow'){
                    $title = $module->getTitle();
                    $name = $module->getName();
                    $dir = $module->getDir();
                    $icon = $module->getIcon();
                    $js = $dir."/index.php?view=list";
                    $make_dir = "";
                    $link = "";
                    $url = $_SERVER['REQUEST_URI'];
                    if(strpos($url,'form')){
                        $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                    }else{
                        $link = $js;
                    }

                    if($icon == "") $icon = "puzzle-piece";

                    $classname = ($dirname == $name) ? " class=\"active\"" : "";

                    $rights = $module->getPermissions($_SESSION['user']['type']);

                    if(!in_array("no_access", $rights) && !empty($rights))
                        echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                }
                if($module->getTitle()=='Medias'){
                    $title = $module->getTitle();
                    $name = $module->getName();
                    $dir = $module->getDir();
                    $icon = $module->getIcon();
                    $js = $dir."/index.php?view=list";
                    $make_dir = "";
                    $link = "";
                    $url = $_SERVER['REQUEST_URI'];
                    if(strpos($url,'form')){
                        $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                    }else{
                        $link = $js;
                    }

                    if($icon == "") $icon = "puzzle-piece";

                    $classname = ($dirname == $name) ? " class=\"active\"" : "";

                    $rights = $module->getPermissions($_SESSION['user']['type']);

                    if(!in_array("no_access", $rights) && !empty($rights))
                        echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                }
                if($module->getTitle()=='Comments'){
                    $title = $module->getTitle();
                    $name = $module->getName();
                    $dir = $module->getDir();
                    $icon = $module->getIcon();
                    $js = $dir."/index.php?view=list";
                    $make_dir = "";
                    $link = "";
                    $url = $_SERVER['REQUEST_URI'];
                    if(strpos($url,'form')){
                        $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                    }else{
                        $link = $js;
                    }

                    if($icon == "") $icon = "puzzle-piece";

                    $classname = ($dirname == $name) ? " class=\"active\"" : "";

                    $rights = $module->getPermissions($_SESSION['user']['type']);

                    if(!in_array("no_access", $rights) && !empty($rights))
                        echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                }
                if($module->getTitle()=='Messages'){
                    $title = $module->getTitle();
                    $name = $module->getName();
                    $dir = $module->getDir();
                    $icon = $module->getIcon();
                    $js = $dir."/index.php?view=list";
                    $make_dir = "";
                    $link = "";
                    $url = $_SERVER['REQUEST_URI'];
                    if(strpos($url,'form')){
                        $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                    }else{
                        $link = $js;
                    }

                    if($icon == "") $icon = "puzzle-piece";

                    $classname = ($dirname == $name) ? " class=\"active\"" : "";

                    $rights = $module->getPermissions($_SESSION['user']['type']);

                    if(!in_array("no_access", $rights) && !empty($rights))
                        echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                }
                if($module->getTitle()=='Emails Content'){
                    $title = $module->getTitle();
                    $name = $module->getName();
                    $dir = $module->getDir();
                    $icon = $module->getIcon();
                    $js = $dir."/index.php?view=list";
                    $make_dir = "";
                    $link = "";
                    $url = $_SERVER['REQUEST_URI'];
                    if(strpos($url,'form')){
                        $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                    }else{
                        $link = $js;
                    }

                    if($icon == "") $icon = "puzzle-piece";

                    $classname = ($dirname == $name) ? " class=\"active\"" : "";

                    $rights = $module->getPermissions($_SESSION['user']['type']);

                    if(!in_array("no_access", $rights) && !empty($rights))
                        echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                }
                if($module->getTitle()=='Texts'){
                    $title = $module->getTitle();
                    $name = $module->getName();
                    $dir = $module->getDir();
                    $icon = $module->getIcon();
                    $js = $dir."/index.php?view=list";
                    $make_dir = "";
                    $link = "";
                    $url = $_SERVER['REQUEST_URI'];
                    if(strpos($url,'form')){
                        $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                    }else{
                        $link = $js;
                    }

                    if($icon == "") $icon = "puzzle-piece";

                    $classname = ($dirname == $name) ? " class=\"active\"" : "";

                    $rights = $module->getPermissions($_SESSION['user']['type']);

                    if(!in_array("no_access", $rights) && !empty($rights))
                        echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                }
                if($module->getTitle()=='Tags'){
                    $title = $module->getTitle();
                    $name = $module->getName();
                    $dir = $module->getDir();
                    $icon = $module->getIcon();
                    $js = $dir."/index.php?view=list";
                    $make_dir = "";
                    $link = "";
                    $url = $_SERVER['REQUEST_URI'];
                    if(strpos($url,'form')){
                        $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                    }else{
                        $link = $js;
                    }

                    if($icon == "") $icon = "puzzle-piece";

                    $classname = ($dirname == $name) ? " class=\"active\"" : "";

                    $rights = $module->getPermissions($_SESSION['user']['type']);

                    if(!in_array("no_access", $rights) && !empty($rights))
                        echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                }
                if($module->getTitle()=='Social links'){
                    $title = $module->getTitle();
                    $name = $module->getName();
                    $dir = $module->getDir();
                    $icon = $module->getIcon();
                    $js = $dir."/index.php?view=list";
                    $make_dir = "";
                    $link = "";
                    $url = $_SERVER['REQUEST_URI'];
                    if(strpos($url,'form')){
                        $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                    }else{
                        $link = $js;
                    }

                    if($icon == "") $icon = "puzzle-piece";

                    $classname = ($dirname == $name) ? " class=\"active\"" : "";

                    $rights = $module->getPermissions($_SESSION['user']['type']);

                    if(!in_array("no_access", $rights) && !empty($rights))
                        echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                }
                if($module->getTitle()=='Languages'){
                    $title = $module->getTitle();
                    $name = $module->getName();
                    $dir = $module->getDir();
                    $icon = $module->getIcon();
                    $js = $dir."/index.php?view=list";
                    $make_dir = "";
                    $link = "";
                    $url = $_SERVER['REQUEST_URI'];
                    if(strpos($url,'form')){
                        $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                    }else{
                        $link = $js;
                    }

                    if($icon == "") $icon = "puzzle-piece";

                    $classname = ($dirname == $name) ? " class=\"active\"" : "";

                    $rights = $module->getPermissions($_SESSION['user']['type']);

                    if(!in_array("no_access", $rights) && !empty($rights))
                        echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                }
                if($module->getTitle()=='Loctions'){
                    $title = $module->getTitle();
                    $name = $module->getName();
                    $dir = $module->getDir();
                    $icon = $module->getIcon();
                    $js = $dir."/index.php?view=list";
                    $make_dir = "";
                    $link = "";
                    $url = $_SERVER['REQUEST_URI'];
                    if(strpos($url,'form')){
                        $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                    }else{
                        $link = $js;
                    }

                    if($icon == "") $icon = "puzzle-piece";

                    $classname = ($dirname == $name) ? " class=\"active\"" : "";

                    $rights = $module->getPermissions($_SESSION['user']['type']);

                    if(!in_array("no_access", $rights) && !empty($rights))
                        echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                }
                if($module->getTitle()=='Currencies'){
                    $title = $module->getTitle();
                    $name = $module->getName();
                    $dir = $module->getDir();
                    $icon = $module->getIcon();
                    $js = $dir."/index.php?view=list";
                    $make_dir = "";
                    $link = "";
                    $url = $_SERVER['REQUEST_URI'];
                    if(strpos($url,'form')){
                        $link = "javascript:if(confirm('All Data will be lost.')) window.location="."'$js'";
                    }else{
                        $link = $js;
                    }

                    if($icon == "") $icon = "puzzle-piece";

                    $classname = ($dirname == $name) ? " class=\"active\"" : "";

                    $rights = $module->getPermissions($_SESSION['user']['type']);

                    if(!in_array("no_access", $rights) && !empty($rights))
                        echo "<li><a href=\"".$link."\"".$classname."><i class=\"fa fa-".$icon."\"></i> ".$title."</a></li>";


                }
            }
            ?>
            </ul>
        </li>
        <!-- WebPanel Menu END-->
        <?php
        if($_SESSION['user']['type'] == "administrator"){ ?>
            <li>
                <a href="<?php echo DOCBASE.ADMIN_FOLDER; ?>/settings.php"<?php if(strpos($_SERVER['SCRIPT_NAME'], "settings.php") !== false) echo " class=\"active\""; ?>>
                    <i class="fa fa-cog"></i> <?php echo 'Organization Settings'; ?>
                </a>
            </li>
            <?php
        } ?>


        </ul>
    </div>
</nav>
